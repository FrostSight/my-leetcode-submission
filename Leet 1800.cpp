// Link: https://leetcode.com/problems/maximum-ascending-subarray-sum/description/

class Solution {
public:
    int maxAscendingSum(vector<int>& nums) 
    {
        int ans = nums[0], i = 0;

        while(i < nums.size()-1)
        {
            int sums = nums[i], j = i+1; 
            while(j < nums.size() && nums[j] > nums[j-1])
            {
                sums += nums[j];
                j++;
            }

            i = j; // directly moving as summation of elements within i to j will always be smaller so no need to check. Directly moved  jto
            ans = max(ans, sums);
        }

        return ans;
    }
};
