// Link: https://leetcode.com/problems/hand-of-straights/description/

class Solution {
public:
    bool isNStraightHand(vector<int>& hand, int groupSize) 
    {
        if(hand.size() % groupSize) return false;

        map<int, int> mp;  //ordered map

        for(int& h : hand)
            mp[h]++;

        while(!mp.empty())
        {
            int current = mp.begin()->first;

            for(int i = 0; i < groupSize; i++)
            {
                if(mp[current + i] == 0)
                    return false;
                
                mp[current + i]--;
                if(mp[current + i] < 1)
                    mp.erase(current + i);
            }
        }    

        return true;
    }
};

