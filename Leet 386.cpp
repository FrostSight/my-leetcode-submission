// Link: https://leetcode.com/problems/lexicographical-numbers/description/

class Solution {
public:

    void solve(int current, int n, vector<int>& result)
    {
        if(current > n) return; // terminating condition

        result.push_back(current); // if less than pushed back

        for(int append = 0; append <= 9; append++) // then creating number number
        {
            int newNumber = current * 10 + append;

            if(newNumber > n)   return; // terminating condition for newNumber

            solve(newNumber, n, result); // recursive call
        }
    }

    vector<int> lexicalOrder(int n) 
    {
        vector<int> result;

        for(int startNum = 1; startNum <= 9; startNum++)
            solve(startNum, n, result);
        
        return result;
    }
};


/*
start 1, n = 13
current = 1 so pushed to the result now creating new numeber by appending
current 10 and 10 < n so pushed
then current 11, then current 12, then current 13 (same goes)
then current 14 and 14 > 13 sp not pushed
........
then current 19
then current 100
then current 101
then current 102
.........
then current 2 and 2 < 13
then current 20 > 13
then current 21
then current 22
then current 29
then current 201
then current 202
-----

(recursion tree of appending)
*/