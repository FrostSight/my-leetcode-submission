// Link: https://leetcode.com/problems/island-perimeter/

#define visited -1

class Solution {
public:
    int solve(vector<vector<int>>& grid, int i, int j, int row, int col, int peri) 
    {
        if (i < 0 || i >= row || j < 0 || j >= col || grid[i][j] == 0) 
        {
            peri++;
            return peri;
        }

        if (grid[i][j] == visited)
            return peri;

        grid[i][j] = visited;

        peri = solve(grid, i + 1, j, row, col, peri);
        peri = solve(grid, i - 1, j, row, col, peri);
        peri = solve(grid, i, j + 1, row, col, peri);
        peri = solve(grid, i, j - 1, row, col, peri);

        return peri; 
    }

    int islandPerimeter(vector<vector<int>>& grid) 
    {
        int row = grid.size(), col = grid[0].size(), peri = 0;

        for (int i = 0; i < row; i++) 
        {
            for (int j = 0; j < col; j++) 
            {
                if (grid[i][j] == 1) 
                {
                    int perimeter = solve(grid, i, j, row, col, peri);
                    return perimeter;
                }
            }
        }

        return -1;
    }
};
