// Link: https://leetcode.com/problems/dota2-senate/description/

class Solution {
public:
    string predictPartyVictory(string senate) 
    {
        queue<int> rQ, dQ;
        int rIndex, dIndex, n = senate.size();
    
        for(int i = 0; i < senate.size(); i++)
        {
          if(senate[i] == 'R')
            rQ.push(i);
          else
            dQ.push(i);
        } 

        while(!rQ.empty() && !dQ.empty())
        {
            rIndex = rQ.front();
            dIndex = dQ.front();

            if(rIndex < dIndex)
            {
                rIndex = rQ.front();
                rQ.pop();
                rIndex = rIndex + n;  // initialized above but here updating
                rQ.push(rIndex);
                dQ.pop();
            }           
            else
            {
                dIndex = dQ.front();
                dQ.pop();
                dIndex = dIndex + n;
                dQ.push(dIndex);
                rQ.pop();
            }
        }

        if(dQ.size() == 0)
            return "Radiant";
        else
            return "Dire";
    }
};


/*
RRDDD
0 1 
2 3 4 
after 1st iteration:
1 5 (0 popped 0 becomes 5 and pushed)
3 4 (2 only popped)
5 6 (1 popped 1 becomes 6 and pushed)
4   (3 only popped)
6   (5 only popped)
7   (4 popped 4 becomes 7)
6
-   (7 only popped)
*/