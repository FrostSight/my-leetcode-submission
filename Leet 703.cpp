// Link: https://leetcode.com/problems/kth-largest-element-in-a-stream/description/

class KthLargest {
public:

    priority_queue<int, vector<int>, greater<int>> min_pq;
    int k2, ans;

    KthLargest(int k, vector<int>& nums) 
    {
        k2 = k;    
        for(int& n : nums)
        {
            min_pq.push(n);

            if(min_pq.size() > k2)
                min_pq.pop();
        }
    }
    
    int add(int val) 
    {
        min_pq.push(val);   

        if(min_pq.size() > k2)
                min_pq.pop();

        ans = min_pq.top(); 

        return ans;
    }
};

