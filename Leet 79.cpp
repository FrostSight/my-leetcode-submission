// Link: https://leetcode.com/problems/word-search/description/

class Solution {
public:

    int row, col;
    vector<vector<int>> directions{{0, 1}, {0, -1}, {1, 0}, {-1, 0}}; // 4 direction: right, left, up, down // in each iteration increasing or decresing coordinate

    bool find(vector<vector<char>>& board, int i, int j, string &word, int idx) 
    {        
        if(idx == word.size()) // reached to the end of the word
            return true;
        
        if(i < 0 || i >= row || j < 0 || j >= col || board[i][j] != word[idx] || board[i][j] != '$') // out of bound || not character || not visited
            return false;
        
        // the actual back tracking start here
        char temp = board[i][j]; // as we need to restore if not found
        board[i][j] = '$'; // marking visited
        
        for(auto& dir : directions) 
        {
            int new_i = i + dir[0];
            int new_j = j + dir[1];
            
            if(find(board, new_i, new_j, word, idx+1)) // found desired character now turn for the next 
                return true;
        }
        
        board[i][j] = temp; // not found so restoring
        return false;
    }
    
    bool exist(vector<vector<char>>& board, string word) 
    {
        row = board.size();
        col = board[0].size();
        
        for(int i = 0; i < row; i++)
        {
            for(int j  = 0; j < col; j++)
            {
                if(board[i][j] == word[0]) // first character found, time to check the rest
                {
                    bool found = find(board, i, j, word, 0);
                    if(found == true)
                        return true;
                }
            }
        } 
        
        return false;
    }
};