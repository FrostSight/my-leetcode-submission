// Link: https://leetcode.com/problems/gas-station/description/

class Solution {
public:
    int canCompleteCircuit(vector<int>& gas, vector<int>& cost) 
    {
        int totalGas, totalCost, disposableIncome = 0, index = 0;

        totalGas = accumulate(gas.begin(), gas.end(), 0); 
        totalCost = accumulate(cost.begin(), cost.end(), 0); 

        if(totalCost > totalGas)  // if expense is greater than income then never possible
            return -1;

        for(int i = 0; i < gas.size(); i++)  // if expense is not greater then its guranteed answer be found
        {
            disposableIncome = disposableIncome + gas[i] - cost[i]; // goal is to reach to the last element from the first index
            
            if(disposableIncome < 0)  // if equal is 0 then gets wrong answer
            {
                disposableIncome = 0;
                index = i + 1;  // if current disposableIncome is negative then current index not appropiate to start we can assume the next index is correct. 
            }
        }
        return index; 
    }
};

/*
Eventually we will get correct index. If normal the correct index would be the first. If there are multiple negative then the correct index is the next of the last negative.
At wrost n-2 would be negative and correct index would be n-1 (last) but it is guranteed to be found;
*/