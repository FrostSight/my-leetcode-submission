// Link: https://leetcode.com/problems/rank-transform-of-an-array/description/

class Solution {
public:
    vector<int> arrayRankTransform(vector<int>& arr) 
    {
        unordered_map<int, int> ump;
        set<int> st(arr.begin(), arr.end());

        int rank = 1;
        for(auto& num : st)
        {
            ump[num] = rank;
            rank++;
        }  

        for(int i = 0; i < arr.size(); i++)
            arr[i] = ump[arr[i]];

        return arr; 
    }
};

