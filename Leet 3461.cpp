// Link: https://leetcode.com/problems/check-if-digits-are-equal-in-string-after-operations-i/

class Solution {
public:
    bool hasSameDigits(string s) 
    {
        while(s.size() > 2)
        {
            string new_s = "";
            
            for(int i = 0; i < s.size()-1; i++)
            {
                int digit1 = s[i] - '0';
                int digit2 = s[i+1] - '0';
                int outcome = (digit1 + digit2) % 10;

                new_s += to_string(outcome);
            }

            s = new_s;
        }

        return s[0] == s[1] ? true : false;
    }
};
