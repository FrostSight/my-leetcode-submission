// Link: https://leetcode.com/problems/length-of-last-word/description/

class Solution {
public:
    int lengthOfLastWord(string s) 
    {
        int space_counter = 0, word_counter = 0;
        int k = s.size() - 1;

        while(k>= 0 && s[k] == ' ')
        {
            space_counter++;
            k--;
        }

        if(space_counter > 0)
        {
            for(int i = s.size() - 1 - space_counter; i >= 0; i--)
            {
                if(s[i] != ' ')
                    word_counter++;
                else if (s[i] == ' ')
                    break;
            }

        }
        else
        {
            for(int i = s.size() - 1; i >= 0; i--)
            {
                if(s[i] != ' ')
                    word_counter++;
                else if (s[i] == ' ')
                    break;
            }
        }
        return word_counter;
    }
};
