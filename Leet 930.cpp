// Link: https://leetcode.com/problems/binary-subarrays-with-sum/description/

class Solution {
public:
    int numSubarraysWithSum(vector<int>& nums, int goal) 
    {
        int prefixSum = 0, remain, result = 0;
        unordered_map<int, int> umap;
        umap[0] = 1;

        for(int& n : nums)
        {
            prefixSum += n;
            
            remain = prefixSum - goal;
            if(umap.find(remain) != umap.end())
                result += umap[remain];

            umap[prefixSum]++;
        }  

        return result; 
    }
};