// Link: https://leetcode.com/problems/delete-nodes-from-linked-list-present-in-array/description/

class Solution {
public:
    ListNode* modifiedList(vector<int>& nums, ListNode* head) 
    {
        unordered_set<int> ust(nums.begin(), nums.end());

        while(ust.find(head->val) != ust.end()) // when head need to remove 
        {
            ListNode* t = head;
            head = head->next;          
            delete(t);
        }
        
        ListNode* current = head;

        while(current->next != NULL) // when it is not head
        {
            if(ust.find(current->next->val) != ust.end())
            {
                ListNode* t = current->next;
                current->next = current->next->next;               
                delete(t);
            }
            else
                current = current->next;
        }

        return head;
    }
};
