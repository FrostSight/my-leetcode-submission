// Link: https://leetcode.com/problems/integer-to-roman/description/

class Solution {
public:
    string intToRoman(int num) 
    {
        string roman = "";

        vector<int> digit = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
        vector<string> symbols = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};

        for (int i = 0; i < digit.size(); i++) 
        {
            while (num >= digit[i]) 
            {
                num = num - digit[i];
                roman = roman + symbols[i];
            }
        }
        return roman;
    }
};
