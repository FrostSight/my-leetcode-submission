// Link: https://leetcode.com/problems/remove-outermost-parentheses/description/

class Solution {
public:
    string removeOuterParentheses(string s) 
    {
        int i = 0;
        stack<char> stk;
        string ans = "";

        while(i < s.size())
        {
            if(s[i] == '(')  // opening brackets
            {
                if(stk.empty())  // outermost enters at first on empty stack
                    stk.push(s[i]); // first bracket is obiously removable bracket so it was not added to ans. 
                else
                {
                    stk.push(s[i]);  // these are inner brackets thus added to ans
                    ans += s[i];
                }
            }
            else   // closing brackets
            {
                if(!stk.empty())
                {
                    stk.pop();  // // after popping if stack is not empty means the it is part of inner brackets thus added to ans
                    if(!stk.empty())
                        ans += s[i];
                }
                else if (stk.empty())  // after popping if stack becomes empty means the it is part of removable bracket thus not added ans
                    continue;
            }
            i++;
        }  
        return ans;
    }
};

/*
{()()}{()} outermost brackets are denoted by {}
{ added to stack
( added to stack and ans
) pops ( and after popping stack is not empty, added to ans
( added to stack and ans
} pops {and after popping stack becomes empty, not added
{ added to stack
( added to stack and ans
) pops ( and after popping stack is not empty, added to ans
} pops {and after popping stack becomes empty, not added
*/