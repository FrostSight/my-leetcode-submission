// Link: https://leetcode.com/problems/make-the-string-great/description/

class Solution {
public:
    string makeGood(string s) 
    {
        stack<char> stk;
        string ans = "";

        for(char ch : s)
        {
            if(stk.empty())
                stk.push(ch);
            else
            {
                if(abs(ch - stk.top()) == 32)  // equivalent character found
                    stk.pop();
                else
                    stk.push(ch);
            }
        }  

        if(!stk.empty())
        {
            while(!stk.empty())
            {
                ans += stk.top();
                stk.pop();
            }

            reverse(ans.begin(), ans.end());
        }

        return ans; 
    }
};

/*
absloute difference of 
a - A or b - B ..... Y - y Z - z = 32
*/