// Link: https://leetcode.com/problems/delete-the-middle-node-of-a-linked-list/description/

class Solution {
public:
    ListNode* deleteMiddle(ListNode* head) 
    {
        if(head == NULL || head->next == NULL)
            return NULL; // this is how to return empty in linklist

        ListNode* slow = head;
        ListNode* fast = head;
        ListNode* previous = NULL;

        while(fast != NULL && fast->next != NULL)
        {
            fast = fast->next->next;
            previous = slow;
            slow = slow->next;
        } 

        previous->next = slow->next;
        delete(slow);

        return head; 
    }
};