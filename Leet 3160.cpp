// Link: https://leetcode.com/problems/find-the-number-of-distinct-colors-among-the-balls/description/

class Solution {
public:
    vector<int> queryResults(int limit, vector<vector<int>>& queries) 
    {
        vector<int> result(queries.size());
        unordered_map<int, int> color_ump, ball_color; 

        for(int i = 0; i < queries.size(); i++) 
        {
            int ball  = queries[i][0];
            int color = queries[i][1];

            if(ball_color.count(ball)) // a ball is already paited, again re-painted
            {
                int prevColor = ball_color[ball];
                color_ump[prevColor]--;

                if(color_ump[prevColor] == 0) //saving space by removing a ball with no color
                    color_ump.erase(prevColor);
            }

            ball_color[ball] = color;
            color_ump[color]++;

            result[i] = color_ump.size();
        }

        return result;
    }
};
