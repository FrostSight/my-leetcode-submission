// Link: https://leetcode.com/problems/max-number-of-k-sum-pairs/description/

class Solution {
public:
    int maxOperations(vector<int>& nums, int k) 
    {
        int left = 0, right = nums.size()-1, counter = 0;

        sort(nums.begin(), nums.end());

        while(left < right)
        {
            if(nums[left] + nums[right] == k)
            {
                counter++;
                left++;
                right--;
            }
            else if(nums[left] + nums[right] < k)
                left++;
            else if(nums[left] + nums[right] > k)
                right--;
        }

        return counter;
    }
};