// Link: https://leetcode.com/problems/contains-duplicate-ii/description/

class Solution {
public:
    bool containsNearbyDuplicate(vector<int>& nums, int k) 
    {
        unordered_map <int, int> umap;
        for (int i = 0; i < nums.size(); i++)
        {
            auto it = umap.find(nums[i]);
            if(it != umap.end())                    // first checks if already present
            {
                if(abs(i - it->second) <= k)        // checks given condition
                    return true;
            }
            umap[nums[i]] = i;                      // if not present then insertion happen
        }
        return false;
    }
};
