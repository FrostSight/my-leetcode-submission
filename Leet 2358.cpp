// Link : https://leetcode.com/problems/maximum-number-of-groups-entering-a-competition/description/

// Approach 1:

class Solution {
public:
    int maximumGroups(vector<int>& grades) 
    {
        int x = grades.size(), i = 1, grp = 0, z = 0;

        while(z <= x)    
        {
            z = (i* (i+1))/2;  // natural number formula
            grp++;
            i++;
        }
        return --grp;  // pre decrement 
    }
};

/*
what are we doing?
sorting or not, first we make a group of 1 student, then a second group of 2 student and finally a group of 3 students (if there are 6 students). 1,2,3 is a natural number set. So I used natural number formula.
*/


// Approach 2: using binary search

class Solution {
public:

    bool sumcheck(long long mid, int x)
    {
        int sum;
        sum = (mid * (mid+1) / 2);

        return sum > x;
    }

    int maximumGroups(vector<int>& grades) 
    {
        int low = 1, high = grades.size(), x = grades.size(), ans;
        long long mid;

        if(x == 1)     // corner case
            return 1;

        while(low <= high)
        {
            mid = low + (high - low) / 2;

            if(sumcheck(mid, x))
            {
                ans = mid;
                high = mid - 1;
            }
            else
                low = mid + 1;
        }    
        return --ans;
    }
};
