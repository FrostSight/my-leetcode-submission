// Link: https://leetcode.com/problems/remove-duplicates-from-sorted-array/description/

class Solution {
public:
    int removeDuplicates(vector<int>& nums) 
    {
        int i = 0, j = 1;

        while(j < nums.size())
        {
            if(nums[i] == nums[j])
            {
                nums[j] = INT_MAX;
                j++;
            }
            else
            {
                i = j;
                j++;
            }
        }

        nums.erase(remove(nums.begin(), nums.end(), INT_MAX), nums.end());

        return nums.size();
    }
};