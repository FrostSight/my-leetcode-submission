// Link: https://leetcode.com/problems/two-sum-ii-input-array-is-sorted/description/

class Solution {
public:
    vector<int> twoSum(vector<int>& numbers, int target) 
    {
        int result = 0;
        unordered_map <int, int> umap;

        for(int i = 0; i < numbers.size(); i++)
        {
            result = target - numbers[i];

            if(umap.find(result) != umap.end())  // if found then return
                return {umap[result] + 1, i + 1};
            else
                umap[numbers[i]] = i;  // if not found then add in the map
        }
        return {};   
    }
};
