// Link: https://leetcode.com/problems/minimum-number-of-operations-to-make-array-empty/description/


class Solution {
public:
    int minOperations(vector<int>& nums) 
    {
        map<int, int> umap;
        int counter, frequency;

        for(int n : nums)
          umap[n]++;

        for(auto& pair : umap)
        {
            if(pair.second == 1) // accessing values of map
            {
                return -1;
                break;
            }
            else
            {
                frequency = pair.second;
                counter += frequency / 3;  // every element is divided by 3

                if (frequency % 3 == 1 || frequency % 3 == 2) // if only remainder found
                    counter++;
            }
        }
        return counter;
    }
};

/*
4 3 2 a frequency

*/