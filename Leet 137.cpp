// Link: https://leetcode.com/problems/single-number-ii/

class Solution {
public:
    int singleNumber(vector<int>& nums) 
    {
        int ans = 0;

        for(int k = 0; k < 32; k++)
        {
            int temp = (1 << k), count0 = 0, count1 = 0; // left shifting k bits

            for(int& num : nums)
            {
                if((num & temp) != 0)  // AND is used to detect bit
                    count1++;
                else
                    count0++;  // unnecessary
            }

            if(count1 % 3 == 1)  // as initialized with 0, using OR to replace certain bits to 1
                ans = (ans | temp);
        } 

        return ans;   
    }
};

/*
3 tasks;
1. left shift
2. AND
3. OR
*/