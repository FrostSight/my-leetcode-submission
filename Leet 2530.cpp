// Link: https://leetcode.com/problems/maximal-score-after-applying-k-operations/description/

class Solution {
public:
    long long maxKelements(vector<int>& nums, int k) 
    {
        priority_queue<int> max_pq(nums.begin(), nums.end());
        long long score = 0;

        while(k > 0)
        {
            int x = max_pq.top();
            max_pq.pop();
            score += x;
            x = ceil(x / 3.0);
            max_pq.push(x);
            k--;
        }

        return score;
    }
};

/*
as per question it should be divided by3 but we need ceiling value. Suppose ceil(10 / 3) will result in 3 but ceil(10 / 3.0) will result in 4 as desired.
*/
