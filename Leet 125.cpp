// Link : https://leetcode.com/problems/valid-palindrome/

class Solution {
public:

    string make_Lower(string s)
    {
        int i = 0, j = 0;

        while(i <= s.size() - 1)
        {
            if(s[i] >= 'A' && s[i] <= 'Z')  // converting capitals to smalls
            {
                s[j] = s[i] - 'A' + 'a';
                j++;
            }
            else if(s[i] >= 'a' && s[i] <= 'z') // removing punctuation
            {
                s[j] = s[i];
                j++;
            }
            else if(s[i] >= '0' && s[i] <= '9') // removing numbers
            {
                s[j] = s[i];
                j++;
            }
            i++;
        }
        return s.substr(0, j);  // shrinking string s to j (as within j everything pure small)
    }

    int checkPalindrome(string s)
    {
        int i = 0, j = s.size()-1, flag = 1;

        while(i <= j)
        {
            if(s[i] == s[j])
                flag = 1;
            else
            {
                flag = 0;  // if any single elements then breaks
                break;
            }
            i++;
            j--;
        }
        return flag;
    }

    bool isPalindrome(string s) 
    {
        int flag;

        s = make_Lower(s);
        flag = checkPalindrome(s);

        if(flag == 1)
            return true;
        else
            return false;    
    }
};
