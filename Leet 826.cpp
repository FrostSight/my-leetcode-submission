// Link: https://leetcode.com/problems/most-profit-assigning-work/description/

// approach 1 : brute force (gives TLE)
class Solution {
public:
    int maxProfitAssignment(vector<int>& difficulty, vector<int>& profit, vector<int>& worker) 
    {
        int mxProfit = 0, netProfit = 0;

        for(int i = 0; i < worker.size(); i++)
        {
            while(i < difficulty.size())
            {
                if(difficulty[i] <= worker[i])
                {
                    mxProfit = max(mxProfit, profit[i]);
                }
            }

            netProfit += mxProfit;
        } 

        return mxProfit;
    }
};


// approach 2: using max heap
class Solution {
public:
    int maxProfitAssignment(vector<int>& difficulty, vector<int>& profit, vector<int>& worker) 
    {
        priority_queue<pair<int, int>> mx_pq;
        int netProfit = 0;

        for(int i = 0; i < difficulty.size(); i++)
        {
            mx_pq.push( {profit[i], difficulty[i]} );
        }    

        sort(worker.rbegin(), worker.rend());  // descending order sorting

        for(int& w : worker)
        {
            while(!mx_pq.empty() && w < mx_pq.top().second)  // popping until w is >= difficulty is found
                mx_pq.pop();
            
            if (!mx_pq.empty())  // without this condition it gives wrong answer, there might be a possibility of getting an empty queue as I am popping on above line. So we get profit when queue is not empty
                netProfit += mx_pq.top().first;
        }

        return netProfit;
    }
};

