// Link: https://leetcode.com/problems/backspace-string-compare/description/

class Solution {
public:
    bool backspaceCompare(string s, string t) 
    {
        string rslt_s, rslt_t;

        for(int i = 0; i <= s.size() - 1; i++)
        {
            if (s[i] != '#')
                rslt_s = rslt_s + s[i];
            else if (! rslt_s.empty())
                rslt_s.pop_back();
        }

        for(int i = 0; i <= t.size() - 1; i++)
        {
            if (t[i] != '#')
                rslt_t = rslt_t + t[i];
            else if (! rslt_t.empty())
                rslt_t.pop_back();
        }

        if (rslt_s == rslt_t)
            return true;
        
        return false;
    }
};
