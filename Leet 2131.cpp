// Link: https://leetcode.com/problems/longest-palindrome-by-concatenating-two-letter-words/
class Solution {
public:
    int longestPalindrome(vector<string>& words) 
    {
        int len = 0;
        bool center = false;
        string reverse_word;
        unordered_map<string, int> umap;

        for(int i = 0; i < words.size(); i++)  // making a map
            umap[words[i]]++;    
        
        for(auto& w : words)
        {
            reverse_word = w;
            
            reverse(reverse_word.begin(), reverse_word.end()); // now it is reversed

            if(reverse_word == w) // like aa, bb
            {
                if(umap[w] >= 2) // if aa is present more than twice then possible like "aa*****aa"
                {
                    umap[w] = umap[w] - 2; // so subtracting 2
                    len = len + 4;
                }
                else if(umap[w] == 1 && center == false)  // present one time and center is empty
                {
                    center = true;
                    umap[w]--;  // so subtracting 1 time
                    len = len + 2;
                }
            }
            else  // ab and ba
            {
                if(umap[reverse_word] > 0 && umap[w] > 0) // if both are present
                {
                    umap[w]--;
                    umap[reverse_word]--; // decrementing both 1 time
                    len = len + 4;
                }
            }
        }
        return len;
    }
};