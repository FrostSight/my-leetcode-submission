https://leetcode.com/problems/successful-pairs-of-spells-and-potions/description/

int strengthCounter(vector<int>& potions, int m, int k, long long success)
{
    int low = 0, high = m - 1, mid, counter = 0;

    while(low <= high)
    {
        mid = low + (high - low) / 2;

        if(k >= ceil(1.0 * success/potions[mid])) // (k*potions[mid]) gives runtime error so on the other side the potions[mid] is divided. To convert into floating number 1.0 is multiplied and taking ceiling value or it takes lesser value.
        {
            counter =  m - mid;  // gives possible answer
            high = mid - 1;
        }
        else if (k < ceil(1.0 * success / potions[mid]))
            low = mid + 1;
    }
    return counter;
}

class Solution {
public:
    vector<int> successfulPairs(vector<int>& spells, vector<int>& potions, long long success) 
    {
        int n, m;

        n = spells.size();
        m = potions.size();
        vector <int> pairs;

        sort(potions.begin(), potions.end());

        for(int i = 0; i < n; i++)
        {
            pairs.push_back(strengthCounter(potions, m, spells[i], success));
        }

        return pairs;    
    }
};


/*
time complexity O(m*log n)
*/
