// Link: https://leetcode.com/problems/apple-redistribution-into-boxes/description/

class Solution {
public:
    int minimumBoxes(vector<int>& apple, vector<int>& capacity) 
    {
        int appleSum, counter = 0;
        
        appleSum = accumulate(apple.begin(), apple.end(), 0);

        sort(capacity.rbegin(), capacity.rend());
        for(int i = 0; i < capacity.size(); i++)
        {
            if(appleSum <= 0)
                break;

            appleSum -= capacity[i];
            counter++;
        }

        return counter;
    }
};