// Link: https://leetcode.com/problems/double-a-number-represented-as-a-linked-list/description/


// approach 1: twice reverse linked list
#define ac afterCurrent
#define c current


class Solution {
public:

    ListNode* reverseList(ListNode* head) 
    {
        if(head == NULL || head->next == NULL) 
            return head;
        
        ListNode* last = reverseList(head->next);
        head->next->next = head;
        head->next = NULL;
        
        return last;       
    }
    
    ListNode* doubleIt(ListNode* head) 
    {
        head = reverseList(head);
        
        ListNode* c = head;
        ListNode* ac = NULL;
        int carry = 0;
        
        while(c != NULL) 
        {           
            int newValue = (c->val * 2) + carry;
            c->val = newValue % 10;
            
            if(newValue >= 10) 
                carry = 1;
            else 
                carry = 0;
            
            ac = c;
            c = c->next;           
        }
        
        if(carry != 0) // 998*2 = 1996, msb 1 is the last carry so new node was added.
        {
            ListNode* newHead = new ListNode(carry);
            ac->next = newHead;
        }
        
        
        head =  reverseList(head);

        return head;
    }
};

