// Link: https://leetcode.com/problems/append-characters-to-string-to-make-subsequence/description/

class Solution {
public:
    int appendCharacters(string s, string t) 
    {
        int i = 0, j = 0;

        while(i < s.size())
        {
            if(s[i] == t[j])
            {
                i++;
                j++;
            }
            else
                i++;
        } 

        int remain = t.size()-j;
        return remain; 
    }
};
