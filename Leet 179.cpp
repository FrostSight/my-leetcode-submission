// Link: https://leetcode.com/problems/largest-number/description/

class Solution {
public:
    string largestNumber(vector<int>& nums) 
    {
        auto myComparator = [](int& a, int& b) // lambda
        {
            string s1 = to_string(a);
            string s2 = to_string(b);

            if(s1+s2 > s2+s1)  // let 9, 30 after concate 930 > 309 and returning maximum
                return true; 
            else
                return false;
        };

        sort(nums.begin(), nums.end(), myComparator); // custom sort

        string result = "";
        for(int num : nums)
        {
            result += to_string(num);
        }  

        if (result[0] == '0') return "0"; // if 0 return 0

        return result;  
    }
};
