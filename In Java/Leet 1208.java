// Link: https://leetcode.com/problems/get-equal-substrings-within-budget/description/

class Solution {
    public int equalSubstring(String s, String t, int maxCost) 
    {
        int currentCost = 0, mxLen = 0, i = 0, j = 0;

        while (j < s.length()) 
        {
            currentCost += Math.abs(s.charAt(j) - t.charAt(j));

            while (currentCost > maxCost) 
            {
                currentCost -= Math.abs(s.charAt(i) - t.charAt(i));
                i++;
            }

            mxLen = Math.max(mxLen, j + 1 - i);
            
            j++;
        }

        return mxLen;
    }
}
        