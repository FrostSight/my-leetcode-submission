// Link: https://leetcode.com/problems/max-consecutive-ones/description/

class Solution {
    public int findMaxConsecutiveOnes(int[] nums) 
    {
        int i = 0, counter = 0, max_counter = 0;

        while (i < nums.length) 
        {
            if (nums[i] == 1) 
            {
                counter++;
                if (counter > max_counter) 
                    max_counter = counter;           
            } 
            else 
                counter = 0;
            
            i++;
        }

        return max_counter;
    }
}

