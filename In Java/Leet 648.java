// Link: https://leetcode.com/problems/replace-words/description/

class Solution {

    public String findRoot(String word, Set<String> st)
    {
        int i = 0;
        while(i < word.length())
        {
            String root = word.substring(0, i);
            if(st.contains(root))
                return root;
            
            i++;
        }
        
        return word;
    }

    public String replaceWords(List<String> dictionary, String sentence) 
    {
        Set<String> st = new HashSet<>(dictionary);
        StringBuilder result = new StringBuilder();
        String[] words = sentence.split(" ");

        for (String w : words)
        {
            result.append(findRoot(w, st)).append(" ");
        }

        return result.toString().trim();  // removing last space
    }
}