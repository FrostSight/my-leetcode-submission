// Link: https://leetcode.com/problems/separate-black-and-white-balls/

class Solution {
    public long minimumSteps(String s) 
    {
        long counter = 0, total_swap = 0;

        for(char ch : s.toCharArray())
        {
            if(ch == '1')
                counter++;
            else if(ch == '0')
                total_swap += counter;
        }    

        return total_swap;
    }
}
