// Link: https://leetcode.com/problems/divide-players-into-teams-of-equal-skill/description/

class Solution {
    public long dividePlayers(int[] skill) 
    {
        Arrays.sort(skill);

        int i = 0, j = skill.length - 1;    
        int initial_sum = skill[i] + skill[j];
        long result = 0;

        while(i < j)
        {
            int currentSum = skill[i] + skill[j];

            if(currentSum != initial_sum)
                return -1;
            
            result += (long)skill[i] * (long)skill[j];

            i++;
            j--;
        }

        return result;
    }
}

