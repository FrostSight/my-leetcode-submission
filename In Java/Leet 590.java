// Link: https://leetcode.com/problems/n-ary-tree-postorder-traversal/description/

class Solution {

    public void solve(Node root, List<Integer> ans)
    {
        if(root == null)    return;

        for(int i = 0; i < root.children.size(); i++)
        {
            solve(root.children.get(i), ans);
        }

        ans.add(root.val);
    }

    public List<Integer> postorder(Node root) 
    {
        List<Integer> ans = new ArrayList<>();
        solve(root, ans);
        return ans;
    }
}
