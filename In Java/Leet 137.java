// Link: https://leetcode.com/problems/single-number-ii/

class Solution {
    public int singleNumber(int[] nums) 
    {
        int ans = 0;

        for(int k = 0; k < 32; k++)
        {
            int temp = (1 << k), count1 = 0;

            for(int num : nums)
            {
                if((num & temp) != 0)
                    count1++;
            }

            if(count1 % 3 == 1)
                ans = ans | temp;
        }  

        return ans; 
    }
}