// Link: https://leetcode.com/problems/binary-tree-postorder-traversal/description/

class Solution {

    public void solve(TreeNode root, List<Integer> ans)
    {
        if(root == null)    return;

        solve(root.left, ans);
        solve(root.right, ans);

        ans.add(root.val);
    }

    public List<Integer> postorderTraversal(TreeNode root) 
    {
        List<Integer> ans = new ArrayList<>();

        solve(root, ans);

        return ans;
    }
}
