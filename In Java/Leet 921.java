// Link: https://leetcode.com/problems/minimum-add-to-make-parentheses-valid/description/

class Solution {
    public int minAddToMakeValid(String s) 
    {
        int opening = 0, closing = 0;
        
        for(char ch : s.toCharArray())
        {
            if(ch == '(')
                opening++;
            else if(ch == ')')
            {
                if(opening > 0)
                    opening--;
                else
                    closing++;
            }
        }

        int ans = opening + closing;
        return ans;
    }
}
