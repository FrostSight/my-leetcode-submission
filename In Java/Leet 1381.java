// Link: https://leetcode.com/problems/design-a-stack-with-increment-operation/description/

class CustomStack {

    ArrayList<Integer> stk = new ArrayList<>();
    int n, top_idx = -1;

    public CustomStack(int maxSize) 
    {
        n = maxSize;
    }
    
    public void push(int x) 
    {
        if(stk.size() < n)
        {
            stk.add(x);
            top_idx++;
        }    
    }
    
    public int pop() 
    {
        if(stk.size() == 0)
            return -1;

        int top_val = stk.get(top_idx);
        stk.remove(top_idx);
        top_idx--;
        return top_val;    
    }
    
    public void increment(int k, int val) 
    {
        int x = Math.min(k, stk.size());
        for(int i = 0; i < x; i++)
            stk.set(i, stk.get(i) + val);    
    }
}

