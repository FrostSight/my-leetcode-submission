// Link: https://leetcode.com/problems/longest-palindrome/description/

class Solution {
    public int longestPalindrome(String s) 
    {
        if(s.length() == 1) return 1;
        
        Set<Character> st = new HashSet<>();
        int leng = 0;

        for(char ch : s.toCharArray())
        {
            if(st.contains(ch) == false)
                st.add(ch);
            else
            {
                leng += 2;
                st.remove(ch);
            }
        }

        if(st.isEmpty() == false)
            leng += 1;
        
        return leng;
    }
}

