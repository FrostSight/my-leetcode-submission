// Link: https://leetcode.com/problems/average-waiting-time/description/

class Solution {
    public double averageWaitingTime(int[][] customers) 
    {
        int waitTime, currentTime = 0, arrivalTime, cookTime;
        double totalWait = 0, avg= 0;

        for(int[] v : customers)
        {
            arrivalTime = v[0];
            cookTime = v[1];

            if(currentTime <= arrivalTime)
                currentTime = arrivalTime;

            waitTime = currentTime + cookTime - arrivalTime; 
            totalWait += waitTime;
            currentTime += cookTime;
        }

        avg = totalWait / customers.length;
        return avg;
    }
}
