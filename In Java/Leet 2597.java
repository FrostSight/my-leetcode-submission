// Link: https://leetcode.com/problems/the-number-of-beautiful-subsets/description/

class Solution {
    private int counter = 0;

    private void solve(int i, int[] nums, Map<Integer, Integer> ump, int k) 
    {
        if (i == nums.length) 
        {
            counter++;
            return;
        }

        solve(i+1, nums, ump, k);

        if (ump.getOrDefault(nums[i] - k, 0) == 0 && ump.getOrDefault(nums[i] + k, 0) == 0) 
        {
            ump.put(nums[i], ump.getOrDefault(nums[i], 0) + 1);
            solve(i+1, nums, ump, k);
            ump.put(nums[i], ump.get(nums[i]) - 1);
        }
    }

    public int beautifulSubsets(int[] nums, int k) 
    {
        Map<Integer, Integer> ump = new HashMap<>();

        solve(0, nums, ump, k);

        return counter - 1;
    }
}
