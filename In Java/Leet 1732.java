// Link: https://leetcode.com/problems/find-the-highest-altitude/description/

class Solution {
    public int largestAltitude(int[] gain) 
    {
        int alt = 0, max_alt = 0;

        for(int g : gain)
        {
            alt += g;
            max_alt = Math.max(max_alt, alt);
        }

        return max_alt;
    }
}
