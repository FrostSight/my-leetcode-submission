// Link: https://leetcode.com/problems/detect-capital/description/

class Solution {

    public boolean isAllUpper(String word)
    {
        for(char ch : word.toCharArray())
        {
            if(!Character.isUpperCase(ch))
                return false;
        }

        return true;
    }

    public boolean isAllSmall(String word)
    {
        for(char ch : word.toCharArray())
        {
            if(!Character.isLowerCase(ch))
                return false;
        }

        return true;
    }

    public boolean isAlright(String word)
    {
        for (int i = 1; i < word.length(); i++) 
        {
            if (Character.isUpperCase(word.charAt(0)) && Character.isUpperCase(word.charAt(i))) 
                return false;
            else if (Character.isLowerCase(word.charAt(0)) && !Character.isLowerCase(word.charAt(i))) 
                return false;         
        }
        
        return true;
    }

    public boolean detectCapitalUse(String word) 
    {
        if(isAllUpper(word) || isAllSmall(word) || isAlright(word))
            return true;
        else
            return false;    
    }
}