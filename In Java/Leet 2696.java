// Link: https://leetcode.com/problems/minimum-string-length-after-removing-substrings/description/

class Solution {
    public int minLength(String s) 
    {
        Stack<Character> stk = new Stack<>();

        for(char ch : s.toCharArray())
        {
            if(!stk.isEmpty() && ch == 'B' && stk.peek() == 'A')
                stk.pop();
            else if(!stk.isEmpty() && ch == 'D' && stk.peek() == 'C')
                stk.pop();
            else 
                stk.push(ch);
        }   

        int ans = stk.size();

        return ans; 
    }
}

