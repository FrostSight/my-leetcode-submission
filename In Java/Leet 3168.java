// Link: https://leetcode.com/problems/minimum-number-of-chairs-in-a-waiting-room/description/

class Solution {
    public int minimumChairs(String s) 
    {
        List<Integer> psChair = new ArrayList<>(s.length());
        psChair.add(0, 1);

        for (int i = 1; i < s.length(); i++) 
        {
            if (s.charAt(i) == 'E')
                psChair.add(psChair.get(i - 1) + 1);
            else
                psChair.add(psChair.get(i - 1) - 1);
        }

        int ans = psChair.stream().max(Integer::compareTo).orElse(0);
        return ans;
    }
}
