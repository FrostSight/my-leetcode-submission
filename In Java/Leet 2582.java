// Link: https://leetcode.com/problems/pass-the-pillow/

class Solution {
    public int passThePillow(int n, int time) 
    {
        int fullRound, time_left, pillow;

        fullRound = time / (n-1);
        time_left = time % (n-1);

        if((fullRound & 1) == 1) // odd
            pillow = n - time_left;
        else // even
            pillow = 1 + time_left;

        return pillow;    
    }
}

        
