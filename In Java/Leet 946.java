// Link: https://leetcode.com/problems/validate-stack-sequences/description/

class Solution {
    public boolean validateStackSequences(int[] pushed, int[] popped) 
    {
        Stack<Integer> stk = new Stack<>();    
        int i = 0, j = 0;

        while(i < pushed.length && j < popped.length)
        {
            stk.push(pushed[i]);
            
            while(!stk.isEmpty() && popped[j] == stk.peek())
            {
                stk.pop();
                j++;
            }
            
            i++;
        }

        return stk.isEmpty() ? true : false;
    }
}

