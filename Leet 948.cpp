// Link: https://leetcode.com/problems/bag-of-tokens/description/

class Solution {
public:
    int bagOfTokensScore(vector<int>& tokens, int power) 
    {
        int i = 0, j = tokens.size() - 1, score = 0, max_score = 0;
        
        sort(tokens.begin(), tokens.end());    

        while(i <= j)
        {
            if(power >= tokens[i])
            {
                power = power - tokens[i];
                score++;
                i++;

                max_score = max(score, max_score);  // if(i<j) then case 3 gives wrong answer. so if(i<=j) implemented and to TLE max_score was added. 
            }
            else if(score >= 1 && i != j)
            {
                power = power + tokens[j];
                j--;
                score--;
            }
            else
                break;
        }
        return max_score;
    }
};