// Link: https://leetcode.com/problems/count-subarrays-where-max-element-appears-at-least-k-times/description/

#define mxe mx_element
#define ll long long

class Solution {
public:
    long long countSubarrays(vector<int>& nums, int k)
    {
        int i = 0, j = 0, countMax = 0, n = nums.size();
        int mx_element = *max_element(nums.begin(), nums.end());
        ll rslt = 0;


        while(j < n)
        {
            if(nums[j] == mxe)
                countMax++;

            while(countMax >= k)
            {
                rslt += n-j; // not n-j+1, it got me error

                if(nums[i] == mxe)
                    countMax--;

                i++;
            }

            j++;
        }

        return rslt;
    }
};
