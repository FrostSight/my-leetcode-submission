// Link : https://leetcode.com/problems/koko-eating-bananas/description/

long hourCalculation(vector<int>& piles, int mid)
{
    long totalHour = 0;

    for(int i = 0; i < piles.size(); i++)
        totalHour = totalHour + ceil(double(piles[i]) / double(mid));

    return totalHour; 
}

class Solution {
public:
    int minEatingSpeed(vector<int>& piles, int h) 
    {
        int low, high, mid, result;
        long hour;

        low = 1;
        high = *max_element(piles.begin(), piles.end());

        while(low <= high)
        {
            mid = low + (high - low) / 2;

            hour = hourCalculation(piles, mid);

            if(hour <= h)
            {
                result = mid;
                high = mid - 1;
            }
            else
                low = mid + 1;
        }
        return result;    
    }
};

// Time complexity O(n log n)
