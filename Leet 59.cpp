// Link: https://leetcode.com/problems/spiral-matrix-ii/description/

class Solution {
public:
    vector<vector<int>> generateMatrix(int n) 
    {
        vector<vector<int>> matrix(n, vector<int>(n));
        int top = 0, down = n-1, left = 0, right = n-1, digit = 1, step = 1;
        //step
        //1   -> left  to right
        //2   -> top   to down
        //3   -> right to left
        //4   -> down  to top

        while(top <= down && left <= right)
        {
            if(step == 1)
            {
                for(int col = left; col <= right; col++)
                    matrix[top][col] = digit++;
            
                top++;
            }
            else if(step == 2)
            {
                for(int row = top; row <= down; row++)
                    matrix[row][right] = digit++;

                right--;
            }
            else if(step == 3)
            {
                for(int col = right; col >= left; col--)
                    matrix[down][col] = digit++;
                
                down--;
            }
            else if(step == 4)
            {
                for(int row = down; row >= top; row --)
                    matrix[row][left] = digit++;
                
                left++;
            }

            step++;
            if(step == 5)
                step = 1;
        }

        return matrix;
    }
};
