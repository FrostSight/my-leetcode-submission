// Link: https://leetcode.com/problems/binary-search-tree-to-greater-sum-tree/description/

class Solution {
public:

    void solve(TreeNode* root, int& sum) 
    {
        if(root == NULL) return;   // base condition

        solve(root->right, sum);  // getting right till leaf node
        sum += root->val;
        root->val = sum;
        solve(root->left, sum);  // getting left
    }

    TreeNode* bstToGst(TreeNode* root) 
    {
        int sum = 0;
        
        solve(root, sum);

        return root;    
    }
};

/*
just opposite of in-order traversal, visiting right then left storing sum.
As we are visiting right then left we already fulfill given condition
*/