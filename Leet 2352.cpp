// Link: https://leetcode.com/problems/equal-row-and-column-pairs/description/

class Solution {
public:
    int equalPairs(vector<vector<int>>& grid) 
    {
        int n = grid.size(), counter = 0;

        map<vector<int>,  int> mp;

        for(int row = 0; row < n; row++)  // making frequency of occurance of rows
            mp[grid[row]]++;
        
        for(int col = 0; col < n; col++)
        {
            vector<int> temp;

            for(int row = 0; row < n; row++)
            {
                temp.push_back(grid[row][col]); // making a vector of column wise elements
            }

            counter += mp[temp]; // if only temp vector presents then frequency sums up
        }

        return counter;
    }
};