// Link: https://leetcode.com/problems/delete-leaves-with-a-given-value/description/

class Solution {
public:
    TreeNode* removeLeafNodes(TreeNode* root, int target) 
    {
        if(root == NULL)    return NULL;

        root->left = removeLeafNodes(root->left, target);
        root->right = removeLeafNodes(root->right, target);

        if(root->left == NULL && root->right == NULL && root->val == target) // leaf and target
            return NULL;  // return NULL so the parent points NULL and becomes leaf 
        
        return root;
    }
};
