// Link: https://leetcode.com/problems/move-pieces-to-obtain-a-string/description/

class Solution {
public:
    bool canChange(string start, string target) 
    {
        int i = 0, j = 0;

        // basically start and target both size are same
        while(i < start.size() || j < target.size()) // if anyone reaches to the end 
        {
            while(start[i] == '_')
                i++;
            
            while(target[j] == '_')
                j++;
            
            if(i == start.size() || j == target.size()) 
                return i == start.size() && j == target.size() ? true : false;
            
            if(start[i] != target[j]) // if anyone does not match 
                return false;
            
            
            if(start[i] == 'L' && i < j) // i < j means  no space for L
                return false;
            
            if(target[j] == 'R' && i > j) // i > j means no space for R
                return false;
            
            i++;
            j++;
        }

        return true;
    }
};

