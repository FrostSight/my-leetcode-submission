// Link: https://leetcode.com/problems/relative-sort-array/description/

class Solution {
public:
    vector<int> relativeSortArray(vector<int>& arr1, vector<int>& arr2) 
    {
        map<int, int> ump;
        vector<int> arr3;

        for(int& a : arr1)
            ump[a]++;

        for(int& a : arr2)
        {
            auto it = ump.find(a);
            if(it != ump.end())
            {
                int temp = it->second;
                while(temp != 0)
                {
                    arr3.push_back(a);
                    temp--;
                }

                ump.erase(it);
            }
        }

        for(auto it = ump.begin(); it != ump.end(); it++)
        {
            int temp = it->second;
            while (temp != 0)
            {
                arr3.push_back(it->first);
                temp--;
            }
        }    

        return arr3;
    }
};

