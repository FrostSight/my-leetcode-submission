// Link: https://leetcode.com/problems/bitwise-xor-of-all-pairings/description/

class Solution {
public:
    int getXor(vector<int>& nums)
    {
        int ans = 0;

        for(int& num : nums)
            ans ^= num;
        
        return ans;
    }

    int xorAllNums(vector<int>& nums1, vector<int>& nums2) 
    {
        int ans = 0;

        if(nums1.size() % 2 != 0)
            ans ^= getXor(nums2);    
        
        if(nums2.size() % 2 != 0)
            ans ^= getXor(nums1);    

        // as we are accumulating ans so if both are odd ans will calculate xof of both eventually
        return ans;
    }
};

/*
Explanation:
nums1 = [a,b,c]
nums2 = [d,e]
ans = [(a^d), (a^e), (b^d), (b^e), (c^e), (c^e)]
frequrncy
a - 2
b - 2
c - 2
d - 1
e - 1
elements of nums1 appeared elemenst of nums2 times which is even
elements of nums2 appeared elemenst of nums1 times which is odd
so here can 4 cases
1. len(nums1) == even
    find xor nums2
1. len(nums2) == even
    find xor nums1
3. both even
    xor is 0
4. both odd
    find xor nums1 and nums2
*/