// Link: https://leetcode.com/problems/find-the-difference-of-two-arrays/description/

class Solution {
public:
    vector<vector<int>> findDifference(vector<int>& nums1, vector<int>& nums2) 
    {
        unordered_set<int> ust1(nums1.begin(), nums1.end()), ust2(nums2.begin(), nums2.end());
        vector<int> ans1, ans2;
        vector<vector<int>> result;

        for(auto& it : ust1)
        {
            if(ust2.find(it) == ust2.end())
                ans1.push_back(it);
        }   
        
        for(auto& it : ust2)
        {
            if(ust1.find(it) == ust1.end())
                ans2.push_back(it);
        }   

        result.push_back(ans1);
        result.push_back(ans2);

        return result;
    }
};
