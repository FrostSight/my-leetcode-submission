// Link: https://leetcode.com/problems/valid-anagram/description/

class Solution {
public:
    bool isAnagram(string s, string t) 
    {
        map <char, int> umap1, umap2;

        for(char c : s)
            umap1[c]++;

        for(char c : t)
            umap2[c]++;
    
        if (umap1 == umap2)
            return true;

        return false;    
    }
};
