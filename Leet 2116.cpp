// Link: https://leetcode.com/problems/check-if-a-parentheses-string-can-be-valid/description/

class Solution {
public:
    bool canBeValid(string s, string locked) 
    {
        if(s.size() % 2 != 0)  return false;

        stack<int> open, openClose;

        for(int i = 0; i < s.size(); i++)
        {
            if(locked[i] == '0')
                openClose.push(i);
            else if(locked[i] == '1')
            {
                if(s[i] == '(')
                    open.push(i);
                else if(s[i] == ')')
                {
                    if(!open.empty())
                        open.pop();
                    else if(!openClose.empty())
                        openClose.pop();
                    else
                        return false;
                }
            }
        }

        while(!open.empty() && !openClose.empty() && open.top() < openClose.top())
        {
            open.pop();
            openClose.pop();
        }

        return open.empty() == true ? true : false;
    }
};
