// Link: https://leetcode.com/problems/sort-the-jumbled-numbers/description/

#define mv mappedValue
#define vp valuePair
#define oi originalIndex
#define md mappedDigit

class Solution {
public:

    int getMappedValue(vector<int>& mapping, int n)  // explanation below
    {
        if (n < 10)   return mapping[n];

        int val = 0, placeValue = 1;

        while(n != 0)
        {
            int lastDigit = n % 10;
            int md = mapping[lastDigit];
            val += md * placeValue;
            n = n / 10;
            placeValue = placeValue * 10;
        }

        return val;
    }

    vector<int> sortJumbled(vector<int>& mapping, vector<int>& nums) 
    {
        vector<int> result;
        vector<pair<int, int>> vp;

        for(int i = 0; i < nums.size(); i++)
        {
            int mv = getMappedValue(mapping, nums[i]);
            vp.push_back( {mv, i} );  // as we sort later, index will be shuffleed
        }    

        sort(vp.begin(), vp.end());

        for(auto& pr : vp)
        {
            int oi = pr.second;
            result.push_back(nums[oi]);
        }

        return result;
    }
};
    

/*
suppose 991, How can we make it?
place value of 1, 9, 9 are respectively 1, 10, 100
so we it is amde this way,
(9x100) + (9x10) + (1x1)
Now we replace with the mapping value
(6x100) + (6x10) + (9x1) = 669
*/   