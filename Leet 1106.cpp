// Link: https://leetcode.com/problems/parsing-a-boolean-expression/

class Solution {
public:

    char calculate(vector<char>& temp, char op)
    {
        if(op == '!')
        {
            char ch = temp.back();
            temp.pop_back();
            return ch == 't' ? 'f' : 't';  // as NOT so flipping
        }
        else if (op == '&')  
        {
            for(int i = 0; i < temp.size(); i++)
            {
                if(temp[i] == 'f') // as AND, so one false ,may result is false
                    return 'f';
            }
            return 't';
        }
        else if (op == '|')
        {
            for(int i = 0; i < temp.size(); i++)
            {
                if(temp[i] == 't')  // as OR, so one true ,may result is true
                    return 't';
            }
            return 'f';
        }

        return 't';
    }

    bool parseBoolExpr(string expression) 
    {
        stack<char> stk;

        for(char ch : expression)
        {
            if(ch == ')')
            {
                vector<char> temp;

                while(stk.top() != '(')  // insert characters to temps until '(' is found
                {
                    char x = stk.top();
                    stk.pop();
                    temp.push_back(x);
                }

                stk.pop();  // discarding '('

                char op = stk.top(); // getting operator to perform which operation AND/OR/NOT
                stk.pop();

                char newVal = calculate(temp, op);
                stk.push(newVal);  // pushing new character to stack to reevaluate 
            }
            else
                stk.push(ch);  // push everything else ')'
        }

        char ans = stk.top();
        return ans == 't' ? true : false;
    }
};

