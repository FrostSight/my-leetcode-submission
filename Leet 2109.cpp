// Link: https://leetcode.com/problems/adding-spaces-to-a-string/description/

class Solution {
public:
    string addSpaces(string s, vector<int>& spaces) 
    {
        unordered_set<int> ust (spaces.begin(), spaces.end());
        string ans = "";

        for(int i = 0; i < s.size(); i++)
        {
            if(ust.find(i) != ust.end())
                ans.push_back(' ');
            
            ans.push_back(s[i]); // always add korbo but age ekta uporer condition check dibo
        }

        return ans;
    }
};
