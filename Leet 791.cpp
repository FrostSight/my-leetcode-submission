// Link: https://leetcode.com/problems/custom-sort-string/description/

class Solution {
public:
    string customSortString(string order, string s) 
    {
        vector<int> freq(26, 0);
        string ans = "";

        for(char& ch : s)
            freq[ch - 'a']++; 

        for(char& ch : order)
        {
          while(freq[ch - 'a'] > 0)
          {
            ans += ch;
            freq[ch - 'a']--;
          }
        }

        for(char& ch : s)
        {
          if(freq[ch - 'a'] > 0)
            ans += ch;
        } 

        return ans;
    }
};
