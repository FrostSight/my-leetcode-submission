// Link: https://leetcode.com/problems/final-value-of-variable-after-performing-operations/description/

class Solution {
public:
    int finalValueAfterOperations(vector<string>& operations) 
    {
        unordered_map <string, int> umap;
        int sum = 0, x = 0;

        for(auto op : operations)
        {
            if (op == "++X" || op == "X++")
                umap["++X"]++;
            else
                umap["--X"]--;
        }

        for(auto it = umap.begin(); it != umap.end(); it++)
            sum = sum + it->second;
        
        return sum;
    }
};
