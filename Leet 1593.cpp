// Link: https://leetcode.com/problems/split-a-string-into-the-max-number-of-unique-substrings/description/

class Solution {
public:
    void solve(int idx, string& s, unordered_set<string>& ust, int currentCount, int& maxCount) 
    {
        if(currentCount + (s.length() - idx) <= maxCount) 
            return;

        if(idx == s.size()) 
        {
            maxCount = max(maxCount, currentCount);
        }

        for(int j = idx; j < s.size(); j++) 
        {
            string temp = s.substr(idx, j-idx+1);
            if(ust.find(temp) == ust.end()) 
            {
                ust.insert(temp);
                solve(j + 1, s, ust, currentCount + 1, maxCount);
                ust.erase(temp);
            }
        }
    }

    int maxUniqueSplit(string s)
    {
        unordered_set<string> ust;
        int maxCount  = 0, currentCount = 0;

        solve(0, s, ust, currentCount, maxCount);

        return maxCount;
    }
};
