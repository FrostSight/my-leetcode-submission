// Link: https://leetcode.com/problems/maximum-number-of-vowels-in-a-substring-of-given-length/description/

class Solution {
public:

    bool isVowel(char& ch)
    {
        return ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u' ? true : false;
    }

    int maxVowels(string s, int k) 
    {
        int i = 0, j = 0, counter = 0, maxCounter = 0;

        while(j < s.size())
        {
            if(isVowel(s[j]))
                counter++;
            
            if(j+1 - i == k)
            {
                maxCounter = max(maxCounter, counter);
                
                if(isVowel(s[i]))
                    counter--;
                
                i++; 
            }
            j++;
        }    
        
        return maxCounter;
    }
};
