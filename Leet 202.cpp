// Link: https://leetcode.com/problems/happy-number/description/

class Solution {
public:
    bool isHappy(int n) 
    {
        set <int> s;

        while (1)
        {
            int rem, sum = 0;
            while(n)
            {
                rem = n % 10;
                sum = sum + pow(rem, 2);
                n = n / 10;
            }
           // corner case
            if (sum == 1)
                return true;
            else if (s.find(sum) != s.end())
                return false;
        
            s.insert(sum);
            n = sum;            
        }
        return false;
    }
};
