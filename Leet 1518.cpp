// Link: https://leetcode.com/problems/water-bottles/description/

class Solution {
public:
    int numWaterBottles(int numBottles, int numExchange) 
    {
        int consumed, emptyBottle;
        consumed = numBottles;
        emptyBottle = numBottles;

        while(emptyBottle >= numExchange) 
        {
            int newBottle = emptyBottle / numExchange;

            int remain = emptyBottle % numExchange;

            consumed += newBottle;

            emptyBottle = remain + newBottle;
        }

        return consumed;
  
    }
};

