// Link: https://leetcode.com/problems/extra-characters-in-a-string/description/

class Solution {
public:

    int t[51];

    int solve(int idx, string& s, unordered_set<string>& ust, int n)
    {
        if(idx >= n) return 0;  // terminating condition

         if(t[idx] != -1) return t[idx];

        int counter = 1 + solve(idx + 1, s, ust, n); // skipping current character

        for(int j = idx; j < n; j++)  // taking current character
        {
            string current = s.substr(idx, j-idx+1);
            if(ust.find(current) != ust.end()) 
            {
                counter = min(counter, solve(j+1, s, ust, n));
            }
        }

        return t[idx] = counter;
    }

    int minExtraChar(string s, vector<string>& dictionary) 
    {
        memset(t, -1, sizeof(t));
        unordered_set<string> ust(dictionary.begin(), dictionary.end());
        int n = s.size();

        int result = solve(0, s, ust, n);

        return result;
    }
};

/*
Explanation:
s = "leetscode", dictionary = ["leet","code","leetcode"]
let leetcode is already found
option 1: taking s along with all the characters after s check if present is dictionary (ust)
option 2: skipping s and taking all the characters after s check if present is dictionary (ust)
*/