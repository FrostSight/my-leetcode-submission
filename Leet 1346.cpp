// Link: https://leetcode.com/problems/check-if-n-and-its-double-exist/

class Solution {
public:
    bool checkIfExist(vector<int>& arr)
    {
        unordered_map<int, int> ump;

        for(int i = 0; i < arr.size(); i++)
        {
            ump[arr[i]] = i;
        }    

        for(int j = 0; j < arr.size(); j++)
        {
            int desired = arr[j] * 2;
            if(ump.find(desired) != ump.end() && ump[desired] != j)
            {
                return true;
            }
        }

        return false;
    }
};
