// Link: https://leetcode.com/problems/assign-cookies/description/

class Solution {
public:
    int findContentChildren(vector<int>& g, vector<int>& s) 
    {
        int current = 0, total = 0, biggest_cookie; //current is current_cookies

        sort(g.rbegin(), g.rend()); // reverse sort
        sort(s.rbegin(), s.rend());

        biggest_cookie = s.size();

        for(int i = 0; i < g.size(); i++)
        {
            if(current < biggest_cookie && g[i] <= s[current])
            {
                total++;
                current++; 
            }
        }

        return total;
    }
};
