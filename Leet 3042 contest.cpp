// Link: https://leetcode.com/contest/weekly-contest-385/problems/count-prefix-and-suffix-pairs-i/

class Solution {
public:
    
    bool isPrefixAndSuffix(string s1, string s2)
    {
        int n1 = s1.size(), n2 = s2.size();
        
        if(n1 > n2)
            return false;
        
        for(int i = 0; i < n1; i++)
        {
            if(s1[n1-1 - i] != s2[n2-1 - i])
                return false;
        }
        
        auto it = mismatch(s1.begin(), s1.end(), s2.begin());
        return it.first == s1.end() ? true : false;
    }
    
    int countPrefixSuffixPairs(vector<string> &words)
    {
        int counter = 0;
        
        for (int i = 0; i < words.size(); i++)
        {
            for (int j = i + 1; j < words.size(); j++)
            {
                if (isPrefixAndSuffix(words[i], words[j]))
                    counter++;
            }
        }
        return counter;
    }
};