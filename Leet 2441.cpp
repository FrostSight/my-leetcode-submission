// Link:https://leetcode.com/problems/largest-positive-integer-that-exists-with-its-negative/description/

class Solution {
public:
    int findMaxK(vector<int>& nums) 
    {
        if(nums.size() == 1)
            return -1;
            
        int i = 0, j = nums.size()-1, sum;

        sort(nums.begin(), nums.end());
        
        while(i < j)
        {
            sum = nums[i] + nums[j];
            if(sum < 0)
                i++;
            else if(sum > 0)
                j--;
            else
                return nums[j];
        }

        return -1;
    }
};
