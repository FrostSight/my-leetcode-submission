// Link: https://leetcode.com/problems/squares-of-a-sorted-array/description/


// Approach 1
class Solution {
public:
    vector<int> sortedSquares(vector<int>& nums) 
    {
        for(int i = 0; i < nums.size(); i++)
        {
            nums[i] = nums[i] * nums[i];
        }    

        sort(nums.begin(), nums.end());

        return nums;
    }
};


// Approach 2
class Solution {
public:
    vector<int> sortedSquares(vector<int>& nums) {

        int n = nums.size();
        vector<int> result(n);

        int i = 0, j = n - 1, k = n - 1;

        while (i <= j) 
        {
            if(pow(nums[i], 2) > pow(nums[j], 2)          )
            {
                result[k] = pow(nums[i], 2);
                i++;
            } 
            else 
            {
                result[k] = pow(nums[j], 2);
                j--;
            }
            k--;
        }

        return result;
    }
};