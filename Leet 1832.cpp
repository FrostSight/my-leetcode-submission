// Link: https://leetcode.com/problems/check-if-the-sentence-is-pangram/description/

class Solution {
public:
    bool checkIfPangram(string sentence) 
    {
        char ch = 'a';
        set<char> charSet; 

        for(char ch : sentence)
            charSet.insert(ch);
        
        if(charSet.size() == 26)  // if all present then after removing duplicacy there will be always 26
            return true;
        else
            return false;
    }
};