// Link: https://leetcode.com/problems/delete-nodes-and-return-forest/

class Solution {
public:

    TreeNode* makeDeletion(TreeNode* root, unordered_set<int>& st, vector<TreeNode*>& ans)
    {
        if(root == NULL)    return NULL;

        root->left = makeDeletion(root->left, st, ans);
        root->right = makeDeletion(root->right, st, ans);

        if(st.find(root->val) != st.end())
        {
            if(root->left != NULL)
                ans.push_back(root->left);
            
            if(root->right != NULL)
                ans.push_back(root->right);
            
            return NULL;
        }
        else
            return root;
    }

    vector<TreeNode*> delNodes(TreeNode* root, vector<int>& to_delete) 
    {
        vector<TreeNode*> ans;
        unordered_set<int> st;

        for(int& i : to_delete)
            st.insert(i);

        makeDeletion(root, st, ans);  

        if(st.find(root->val) == st.end())
            ans.push_back(root);
        
        return ans;
    }
};
