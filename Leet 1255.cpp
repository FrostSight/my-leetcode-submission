// Link: https://leetcode.com/problems/maximum-score-words-formed-by-letters/description/

class Solution {
public:

    int mxScore;

    void solve(int i, vector<string>& words, vector<int>& freq, vector<int>& score, int currentScore)
    {
        mxScore = max(mxScore, currentScore);

        if(i == words.size())   return;  // base condition

        vector<int> tempFreq = freq;
        int j = 0, tempScore = 0;

        while(j < words[i].size()) // permissible to take
        {
            char ch  = words[i][j];

            tempFreq[ch - 'a']--;
            tempScore += score[ch - 'a'];

            if(tempFreq[ch - 'a'] < 0) // frequency is already negative the word should not have been taken so instantly breaking
                break;
            
            j++;
        }

        if(j == words[i].size()) // take, word exists and taken
            solve(i+1, words, tempFreq, score, currentScore + tempScore);
        
        solve(i+1, words, freq, score, currentScore);  // not take
    }

    int maxScoreWords(vector<string>& words, vector<char>& letters, vector<int>& score) 
    {
        vector<int> freq(26, 0);
        mxScore = INT_MIN;

        for(char& ch : letters)
            freq[ch - 'a']++;

        solve(0, words, freq, score, 0);

        return mxScore;
    }
};

