// Link: https://leetcode.com/problems/remove-zero-sum-consecutive-nodes-from-linked-list/description/

class Solution {
public:
    ListNode* removeZeroSumSublists(ListNode* head) 
    {
        int prefixSum = 0;
        unordered_map<int, ListNode*> umap;        
        ListNode* dummyNode = new ListNode(0);
        
        dummyNode->next = head;
        umap[0] = dummyNode;       
        
        while(head != NULL) 
        {
            prefixSum += head->val;            
            if(umap.find(prefixSum) != umap.end()) 
            {                
                ListNode* start = umap[prefixSum];
                ListNode* temp = start;
                int pSum = prefixSum;
                
                while(temp != head) 
                {                   
                    temp = temp->next;
                    pSum += temp->val;
                    
                    if(temp != head)
                        umap.erase(pSum);
                }
                
                start->next = head->next;                
            } 
            else 
                umap[prefixSum] = head;
            
            head = head->next;
        }
        
        return dummyNode->next;
    }
};
