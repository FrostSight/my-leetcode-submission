// Link: https://leetcode.com/problems/minimum-number-of-changes-to-make-binary-string-beautiful/

class Solution {
public:
    int minChanges(string s) 
    {
        int change = 0;

        for(int i = 0; i < s.size(); i += 2)
        {
            if(s[i] != s[i+1])
                change++;
        }

        return change;
    }
};

/*
approach:
checking 2 block or 2 characters at a time
let s = "100010000111"
10 - 00 - 10 - 00 - 01 - 11
1  - 0  -  1 -  0 -  1 -  0 number of changes
whenever we see different digit we increment changes veriable
*/