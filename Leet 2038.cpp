// Link: https://leetcode.com/problems/remove-colored-pieces-if-both-neighbors-are-the-same-color/description/

class Solution {
public:
    bool winnerOfGame(string colors) 
    {
        int counterA = 0, counterB = 0;

        for(int i = 1; i < colors.size()-1; i++)
        {
            if(colors[i] == 'A' && colors[i-1] == 'A' && colors[i+1] == 'A')
                counterA++;
            else if(colors[i] == 'B' && colors[i-1] == 'B' && colors[i+1] == 'B')
                counterB++;
        }

        if(counterA > counterB)    
            return true;
        else
            return false;
    }
};