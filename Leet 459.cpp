// Link: https://leetcode.com/problems/repeated-substring-pattern/

class Solution {
public:
    bool repeatedSubstringPattern(string s) 
    {
        int n = s.size();

        for(int l = 1; l <= n/2; l++) 
        {
            
            if(n % l == 0) 
            {
                int times = n/l;
                
                string chunk   = s.substr(0, l);
                string newStr = "";
                while(times--) 
                {
                    newStr += chunk;
                }
                
                if(newStr == s)
                    return true;
            }           
        }
        
        return false; 
    }
};


/*
things to remember
let s = abcdabcd ans n = 8
at minimum i can pick one character and concate it for 8 time. Concating more or less than 8 times is not allowed.
If i pick "abcda" and repeat it twice, it exceeds n. So at max a substing can be repeated twice times can be at max 2. So without 
n%l == 0 no chance of being true; 
*/