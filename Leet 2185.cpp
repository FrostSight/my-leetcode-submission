// Link: https://leetcode.com/problems/counting-words-with-a-given-prefix/description/

class Solution {
public:
    int prefixCount(vector<string>& words, string pref) 
    {      
        int counter = 0;

        for (string& word : words) 
        {
            if (word.find(pref) == 0) 
                counter++;
        }

        return counter;
    }
};
