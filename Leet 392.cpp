// LinK: https://leetcode.com/problems/is-subsequence/description/

// approach 1:
class Solution {
public:
    size_t findCharacter(string& t, char ch, int i)
    {
        while (i < t.size())
        {
            if (ch == t[i])
                return i;

            i++;
        }
        return string::npos;
    }

    bool isSubsequence(string s, string t)
    {
        size_t i = string::npos;

        for (char& ch : s)
        {
            i = findCharacter(t, ch, i + 1);

            if (i == string::npos) 
                return false;
        }
        return true;
    }
};


// approach 2: 2 pointer approach (easier)
class Solution {
public:
    bool isSubsequence(string s, string t) 
    {
        if(t.size() < s.size())
            return false;

        int i = 0, j = 0;

        while(j < t.size())
        {
            if(s[i] == t[j])
            {
                i++;
                j++;
            }
            else if(s[i] != t[j])
            {
                j++;
            }
        }  

        return i == s.size() ? true : false;
    }
};
