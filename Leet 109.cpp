// Link: https://leetcode.com/problems/convert-sorted-list-to-binary-search-tree/

#define sp slow_previous

class Solution {
public:
    TreeNode* sortedListToBST(ListNode* head) 
    {
        // base conditions
        if(head == NULL)  // if no head
            return NULL;
        if(head->next == NULL) // if only one element exists 
            return new TreeNode(head->val);  
        
        ListNode* slow =  head;
        ListNode* fast = head;
        ListNode* slow_previous = NULL;    // to cut off the connection to the mid node 

        while(fast != NULL && fast->next != NULL) // point 1
        {
            sp = slow;
            slow = slow->next;
            fast = fast->next->next;
        }

        sp->next = NULL;  // cutting off connection   
        TreeNode* root = new TreeNode(slow->val); // point 2
        root->left = sortedListToBST(head);  // point 3
        root->right = sortedListToBST(slow->next);  
        

        return root;
    }
};

/*
3 main things to do
1. find the mid of linked list
2. make the mid the root
3. left side of the root bacomes left branch and right side of the root becomes right branch through recursive calls
*/