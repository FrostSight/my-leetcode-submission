// Link: https://leetcode.com/problems/minimum-increment-to-make-array-unique/description/

class Solution {
public:
    int minIncrementForUnique(vector<int>& nums) 
    {
        map<int, int> mp;
        int moves = 0, extra = 0, mx = 0;

        for(int& i : nums)
        {
            mx = max(mx, i);
            mp[i]++;
	    }
        
        for (int i = 1; i < mx; i++)
        {
            if (mp.find(i) == mp.end())
                mp[i] = 0;
        }

        for(auto it = mp.begin(); it != mp.end(); it++)
        {
            if(it->second <= 1)
                continue;
   
            extra = it->second - 1;
            moves += extra;
            mp[it->first + 1] += extra;
        } 


        return moves;
    }
};

