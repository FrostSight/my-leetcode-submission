// Link: https://leetcode.com/problems/palindrome-partitioning/description/

class Solution {
public:
    bool isPalindrome(string& s, int left, int right)
    {
        while(left <= right)
        {
            if(s[left] != s[right])
                return false;
            
            left++;
            right--;
        }

        return true;
    }

    void solve(string& s, int idx, vector<string>& current, vector<vector<string>>& result)
    {
        if(idx == s.size())
        {
            result.push_back(current);
            return;
        }

        for(int i = idx; i < s.size(); i++)
        {
            if(isPalindrome(s, idx, i) == true)
            {
                current.push_back(s.substr(idx, i-idx +1));
                solve(s, i+1, current, result);
                current.pop_back(); // reached to end of a sub tree
            }
        }
    }

    vector<vector<string>> partition(string s) 
    {
        vector<vector<string>> result;
        vector<string> current;

        solve(s, 0, current, result);

        return result;
    }
};
