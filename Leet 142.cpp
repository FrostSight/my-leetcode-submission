// Link: https://leetcode.com/problems/linked-list-cycle-ii/

// approach 1: brute force

class Solution {
public:
    ListNode *detectCycle(ListNode *head) 
    {
        set<ListNode*> st;

        if(head == NULL || head->next == NULL || head->next->next == NULL)
            return NULL;

        while(head->next != NULL)
        {
            if(st.find(head) != st.end()) // node is already seen before now found again so cycle exists
                return head;
            else
               st.insert(head); // if node is not seen before then insert into set
            
            head = head->next;
        }

        return NULL;

    }
};



// approach 2: slow - fast

class Solution {
public:
    ListNode *detectCycle(ListNode *head) 
    {
        if(head == NULL || head->next == NULL || head->next->next == NULL)
            return NULL;

        ListNode* slow= head;
        ListNode* fast = head;

		// both forwads
        slow = slow->next;
        fast = fast->next->next;

        while(slow != fast)  // this is cycle detection loop, if cycle exists then it will be slow == fast
        {
            slow = slow->next;
            fast = fast->next->next;

            if(fast == NULL || fast->next == NULL)  // fast has reached to the end means no cycle found
                return NULL;
        }    

        slow = head;  // cycle found so slow comes back to starting point

        while(slow != fast) // now both moves by 1 node, their meeting point is cycle starting point
        {
            slow = slow->next;
            fast = fast->next;
        } 

        return slow; // or return fast;
    }
};
