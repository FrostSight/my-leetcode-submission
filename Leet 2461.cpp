// Link: https://leetcode.com/problems/maximum-sum-of-distinct-subarrays-with-length-k/description/

class Solution {
public:
    long long maximumSubarraySum(vector<int>& nums, int k) 
    {
        unordered_set<int> ust;
        int i = 0, j = 0;
        long long sums = 0, maxSum = 0;
        while(j < nums.size())
        {
            while(ust.count(nums[j])) // shrinking
            {
                ust.erase(nums[i]);
                sums -= nums[i];
                i++;
            }

            ust.insert(nums[j]);
            sums += nums[j];

            if(j-i + 1 == k) // shrinking
            {
                maxSum = max(maxSum, sums);
                ust.erase(nums[i]);
                sums -= nums[i];
                i++;
            }

            j++;
        }

        return maxSum;
    }
};

/*
I need to shrink twice
let nums = [1,5,4,2,9,9,9,8,7], k = 3
[2,9,9], [9,9,9], [9,9,8] are not acceptable but [9,8,7] is acceptable
first shrink handle duplicacy
second shrink occurs when k size is found 
*/