// Link: https://leetcode.com/problems/longest-palindrome/description/

class Solution {
public:
    int longestPalindrome(string s) 
    {
        if(s.size() == 1)   return 1;

        set<char> st;
        int leng = 0;
        
        for(char& ch : s)
        {
            auto it = st.find(ch);
            if(it == st.end()) // if not found
                st.insert(ch);
            else // if found
            {
                leng += 2;  // pair added
                st.erase(it); // and removed
            }
        }

        if(!st.empty())  // odd numbers of characters are still there
            leng += 1; 

        return leng;
    }
};

/*
if palindrome length is even
all the characters have even frequency
if palindrome length is odd
all the characters have even frequency except one which has 1 frequency
*/