// Link: https://leetcode.com/problems/contiguous-array/description/

class Solution {
public:
    int findMaxLength(vector<int>& nums) 
    {
        int len = 0, prefixSum = 0;
        unordered_map<int, int> umap;

        umap[0] = -1; 

        for(int& n : nums)
            if(n == 0)
                n = -1; 

        for(int i = 0; i < nums.size(); i++)
        {
            prefixSum += nums[i];

            if(umap.find(prefixSum) != umap.end())
            {
                len = max(len, i - umap[prefixSum]);
            }
            else
            {
                umap[prefixSum] = i;    
            }
        }

        return len;    
    }
};