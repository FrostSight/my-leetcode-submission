// Link: https://leetcode.com/problems/unique-length-3-palindromic-subsequences/description/

class Solution {
public:
    int countPalindromicSubsequence(string s) 
    {
        unordered_set<char> ust1;
        int result = 0;

        for(auto& ch : s)
            ust1.insert(ch);

        for(auto& ch : ust1)
        {
            int leftmost_idx = -1, rightmost_idx = -1;

            for(int i = 0; i < s.size(); i++)
            {
                if(s[i] == ch)
                {
                    if(leftmost_idx == -1)
                        leftmost_idx = i;

                    rightmost_idx = i;
                }
            }

            unordered_set<char> ust2;
            for(int middle = leftmost_idx + 1; middle <= rightmost_idx - 1; middle++)
                ust2.insert(s[middle]);
            
            result += ust2.size();
        }  
        
        return result;
    }
};
