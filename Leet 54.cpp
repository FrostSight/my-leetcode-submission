// Link: https://leetcode.com/problems/spiral-matrix/description/

class Solution {
public:
    vector<int> spiralOrder(vector<vector<int>>& matrix) 
    {
        vector<int> result;
        int top = 0, down = matrix.size()-1, left = 0, right = matrix[0].size()-1, step = 1;

        //step
        //1   -> left  to right
        //2   -> top   to down
        //3   -> right to left
        //4   -> down  to top

        while(top <= down && left <= right)
        {
            if(step == 1)
            {
                for(int col = left; col <= right; col++)
                    result.push_back(matrix[top][col]);
            
                top++;
            }

            if(step == 2)
            {
                for(int row = top; row <= down; row++)
                    result.push_back(matrix[row][right]);

                right--;
            }

            if(step == 3)
            {
                for(int col = right; col >= left; col--)
                    result.push_back(matrix[down][col]);
                
                down--;
            }

            if(step == 4)
            {
                for(int row = down; row >= top; row --)
                    result.push_back(matrix[row][left]);
                
                left++;
            }

            step++;
            if(step == 5)
                step = 0;
        }    

        return result;
    }
};
