// Link: https://leetcode.com/problems/isomorphic-strings/

class Solution {
public:
    bool isIsomorphic(string s, string t) 
    {
        unordered_map<char, char> ump1, ump2;

        for(int i = 0; i < s.size(); i++)
        {
            char cS = s[i];
            char cT = t[i];

            if(ump1.find(cS) != ump1.end() && ump1[cS] != cT ||  // if cS is already present in map and cS is already mapped to cT
                ump2.find(cT) != ump2.end() && ump2[cT] != cS)   // if cT is already present in map and cT is already mapped to cS
                return false;
            
            ump1[cS] = cT;  // mapping cS to cT
            ump2[cT] = cS;  // mapping cT to cS
        }

        return true;
    }
};



/*
explanation
mapped means present in map

case 1:
s = a b c
t = x y z
mapping takes place from both sides a to x and x  to a, b to y and y to b, c to z and z to c

case 2;
s = a b c
t = x y x
given condition, No two characters may map to the same character. 
Here x is mapped by a and c

case 3:
s = a b a
t = x y z
here, a is mapped by x and z
*/
