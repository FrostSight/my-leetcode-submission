// Link: https://leetcode.com/problems/contains-duplicate/description/

// approach 1: 2 loops. First loop makes map secondnd loop checks condition. Time complexity: 92 ms and Space complexity: 71.98 mb 

class Solution {
public:
    bool containsDuplicate(vector<int>& nums) 
    {
        unordered_map <int, int> umap;

        for(int i = 0; i < nums.size(); i++)
            umap[nums[i]]++;

        for (auto it = umap.begin(); it != umap.end(); it++)
        {
            if(it->second > 1)
                {
                    return true;
                    break;
                }
        }
        return false;
    }
};


// approach 2: Single loop first check condition if matches then code ens else creates map. Time complexity: 100 ms and Space complexity: 69.91 mb 

class Solution {
public:
    bool containsDuplicate(vector<int>& nums) 
    {
        unordered_map <int, int> umap;

        for(int i = 0; i < nums.size(); i++)
        {
            if(umap[nums[i]]++ > 1)
                return true;
            
            umap[nums[i]]++;
        }
        return false;
    }
};
