// Link: https://leetcode.com/problems/average-waiting-time/description/

class Solution {
public:
    double averageWaitingTime(vector<vector<int>>& customers) 
    {
        int waitTime, currentTime, cookTime, arrivalTime;
        double totalWait = 0, avg;

        for(auto v : customers)
        {
            arrivalTime = v[0];
            cookTime = v[1];

            if(currentTime <= arrivalTime)
                currentTime = arrivalTime;
            
            waitTime = currentTime + cookTime - arrivalTime;
            totalWait += waitTime;
            currentTime += cookTime;
        }    

        avg = totalWait / customers.size();
        return avg;
    }
};

