// Link: https://leetcode.com/problems/count-common-words-with-one-occurrence/description/

// way 1:
class Solution {
public:
    int countWords(vector<string>& words1, vector<string>& words2) 
    {        
        int counter = 0;
        unordered_map<string, int> ump1, ump2;
        
        for(string& str : words1)
            ump1[str]++;
        
        for(string& str : words2)
            ump2[str]++;
        
        for(string& s : words1)
        {
            if(ump1[s] == 1 && ump2[s] == 1)
                counter++;
        }
        
        return counter;        
    }
};


// way 2: better time complexity
class Solution {
public:
    int countWords(vector<string>& words1, vector<string>& words2) 
    {
        unordered_map<string, int> ump1, ump2;
        int counter = 0;

        for(string& str : words1)
            ump1[str]++;
        for(string& str : words2)
            ump2[str]++;
        
        for(auto it = ump1.begin(); it != ump1.end(); it++)
        {
            if(it->second == 1)
            {
                if(ump2[it->first] == 1)
                    counter++;
            }
        }

        return counter;   
    }
};

