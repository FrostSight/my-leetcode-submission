// Link: https://leetcode.com/problems/letter-combinations-of-a-phone-number/description/

class Solution {
public:
    void solve(int idx, string& digits, unordered_map<char, string>& ump, string& temp, vector<string>& result)
    {
        if(idx == digits.size())
        {
            result.push_back(temp);
            return;
        }

        char ch = digits[idx];
        string s = ump[ch];
        for(int i = 0; i < s.size(); i++)
        {
            temp.push_back(s[i]);
            solve(idx+1, digits, ump, temp, result);
            temp.pop_back();
        }
    }

    vector<string> letterCombinations(string digits) 
    {
        if(digits.size() == 0)  return {};

        string temp;
        vector<string> result;
        unordered_map<char, string> ump;

        ump['2'] = "abc";
        ump['3'] = "def";
        ump['4'] = "ghi";
        ump['5'] = "jkl";
        ump['6'] = "mno";
        ump['7'] = "pqrs";
        ump['8'] = "tuv";
        ump['9'] = "wxyz";

        solve(0, digits, ump, temp, result);

        return result;
    }
};
