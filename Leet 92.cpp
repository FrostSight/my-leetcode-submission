// Link: https://leetcode.com/problems/reverse-linked-list-ii/description/

#define p previous_current
#define c current

class Solution {
public:
    ListNode* reverseBetween(ListNode* head, int left, int right) 
    {
        if(head == NULL || head->next == NULL)
            return head;

        ListNode* dummy = new ListNode(-1);
        dummy->next = head;
        ListNode* previous_current = dummy;

        for(int i = 1; i < left; i++)
            p = p->next;
        
        ListNode* current = p->next;

        for(int i = 1; i <= right-left; i++)
        {
            ListNode* temp = p->next;           
            p->next = c->next;
            c->next = c->next->next;
            p->next->next = temp;
        }

        return dummy->next;
    }
};
