// link: https://leetcode.com/problems/minimum-replacements-to-sort-the-array/description/

class Solution {
public:
    long long minimumReplacement(vector<int>& nums) 
    {
        long long parts = 0, operation = 0;

        for(int i = nums.size()-2; i >= 0; i--)
        {
            if(nums[i] <= nums[i+1])
                continue;

            parts = nums[i] / nums[i+1];

            if(nums[i] % nums[i+1] != 0)
                parts++;

            operation = operation + parts - 1;
            nums[i] = nums[i] / parts;
        }    
        return operation;
    }
};