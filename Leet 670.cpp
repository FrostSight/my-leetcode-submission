// Link: https://leetcode.com/problems/maximum-swap/description/

class Solution {
public:
    int maximumSwap(int num) 
    {
        string s = to_string(num);
        int n = s.size();
        vector<int> maxRight(n);

        maxRight[n - 1] = n - 1;
        for (int i = n - 2; i >= 0; i--) 
        {
            if(s[i] > s[maxRight[i + 1]]) // if the current is greater then the next we assign its index
                maxRight[i] = i;
            else
                maxRight[i] = maxRight[i + 1];   // if the current is greater then the next we assign same index as next
        }

        for (int i = 0; i < n; i++) 
        {
            if (s[i] < s[maxRight[i]]) // lesser value found so swap takes place
            {
                swap(s[i], s[maxRight[i]]);
                
                return stoi(s);
            }
        }

        return num;
    }
};

/*
we need to ensure that maxRight always holds the index of the maximum character to the right for each position
*/