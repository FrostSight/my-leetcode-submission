// Link: https://leetcode.com/problems/partition-list/description/

#define sh smallHead
#define lh largeHead

class Solution {
public:
    ListNode* partition(ListNode* head, int x) 
    {
        ListNode* small = new ListNode(0); // these 2 starting are node
        ListNode* large = new ListNode(0);
        ListNode* smallHead = small;  // these 2 are pointers
        ListNode* largeHead = large;

        while(head != NULL)
        {
            if(head->val < x)
            {
                sh->next = head;
                sh = sh->next;
            }
            else
            {
                lh->next = head;
                lh = lh->next;
            }

            head = head->next;
        }

        sh->next = large->next;
        lh->next = NULL;

        return small->next;
    }
};
