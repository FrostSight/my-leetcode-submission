// Link: https://leetcode.com/problems/maximum-subarray/description/

class Solution {
public:
    int maxSubArray(vector<int>& nums) 
    {
        int currentSum = 0, maxSum = 0, a;

        a = *std::max_element(nums.begin(), nums.end());

        // corner case
        if(a < 0)
            return a;

        for(int i = 0; i < nums.size(); i++)
        {
            currentSum = currentSum + nums[i];

            if(currentSum > maxSum)
                maxSum = currentSum;
            
            if(currentSum < 0)
                currentSum = 0;
        }

        return maxSum;    
    }
};
