// Link: https://leetcode.com/problems/path-with-maximum-gold/description/

class Solution {
public:
    vector<vector<int>> direction = { {1, 0}, {-1, 0}, {0, 1}, {0, -1} };

    int solveDFS(int i, int j, vector<vector<int>>& grid)
    {
        if(i < 0 || i >= grid.size() || j < 0 || j >= grid[0].size() || grid[i][j] == 0) //out of bound checking
            return 0;
        
        int maxGoldFromCurrentCell = 0, originalValue = grid[i][j];
        grid[i][j] = 0;
        for(auto& dir : direction)
        {
            int new_i = i + dir[0];
            int new_j = j + dir[1];

            int goldCollected = solveDFS(new_i, new_j, grid); 
            if (goldCollected > maxGoldFromCurrentCell) 
                maxGoldFromCurrentCell = goldCollected;
        }

        grid[i][j] = originalValue;
        
        return originalValue + maxGoldFromCurrentCell;
    }

    int getMaximumGold(vector<vector<int>>& grid) 
    {
        int mxGold = 0;

        for(int i = 0; i < grid.size(); i++)
        {
            for(int j = 0; j < grid[0].size(); j++)
            {
                if(grid[i][j] != 0)
                {
                    int collectedGold = solveDFS(i, j, grid);
                    mxGold = max(mxGold, collectedGold);   
                }
            }
        }    

        return mxGold;
    }
};

// DFS + backtracking