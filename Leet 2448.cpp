// Link: https://leetcode.com/problems/minimum-cost-to-make-array-equal/

class Solution {
public:

    typedef long long ll;

    ll findCost(vector<int>& nums, vector<int>& cost, ll x)
    {   
        ll sum = 0;

        for(int i = 0; i < nums.size(); i++)
        {
            sum = sum + abs(x - nums[i]) * cost[i];
        }
        return sum;
    }

    long long minCost(vector<int>& nums, vector<int>& cost) 
    {
        ll low, high, mid, cost1, cost2, minimumCost;

        low = *min_element(nums.begin(), nums.end());
        high = *max_element(nums.begin(), nums.end());

        while(low <= high)
        {
            mid = low + (high - low) / 2;

            cost1 = findCost(nums, cost, mid);
            cost2 = findCost(nums, cost, mid+1);
            minimumCost = min(cost1, cost2); // finding one answer

            if(cost2 > cost1)   // whatever the the answer is the time to decide left or right discard
                high = mid - 1;
            else
                low = mid + 1;
        }
        return minimumCost;
    }
};

/*
Time Complexity: O(n log(max(nums)))
The min_element and max_element functions take O(N) time to find the minimum and maximum elements in the nums vector, respectively. The while loop runs log(max(nums)) times, where max(nums) is the maximum element in the nums vector. The findCost function takes O(N) time to compute the cost for each iteration of the while loop.
*/
