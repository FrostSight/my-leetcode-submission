// Link: https://leetcode.com/problems/get-equal-substrings-within-budget/description/

class Solution {
public:
    int equalSubstring(string s, string t, int maxCost) 
    {
        int currentCost = 0, mxLen = 0, i = 0, j = 0;

        while(j < s.size())
        {
            currentCost += abs(s[j] - t[j]);

            while(currentCost > maxCost)
            {
                currentCost -= abs(s[i] - t[i]);
                i++;
            }

            mxLen = max(mxLen, j+1 -i);

            j++;
        }    

        return mxLen;
    }
};    