// Link: https://leetcode.com/problems/boats-to-save-people/description/

class Solution {
public:
    int numRescueBoats(vector<int>& people, int limit) 
    {
        int boat_counter = 0, i = 0, j, contain;

        sort(people.begin(), people.end());    

        j = people.size() - 1;

        if(people[j] > limit)  // boundary condition
            return boat_counter;

        while(i <= j)
        {
            if(limit >= people[i] + people[j])  // suppose 1 4 6 9 and limit = 10 if 9+1 <= limit boat_counter increases else boat will carry only the last person and j decremented 
            {
                boat_counter++;
                i++;
                j--;
            }
            else
            {
               boat_counter++;
               j--;
            }
        }
        return boat_counter;
    }
};
