// Link: https://leetcode.com/problems/number-of-arithmetic-triplets/

class Solution {
public:
    int arithmeticTriplets(vector<int>& nums, int diff) 
    {
        vector<int> v1, v2;
        map<int, int> umap1, umap2;

        for(int n : nums)
          umap1[n] = diff + n;

        for(auto it = umap1.begin(); it != umap1.end(); it++)
        {
          if(find(nums.begin(), nums.end(), it->second) != nums.end())
            v1.push_back(it->second);
        }


        for(int v : v1)
          umap2[v] = diff + v;

        for(auto it = umap2.begin(); it != umap2.end(); it++)
        {
          if(find(nums.begin(), nums.end(), it->second) != nums.end())
            v2.push_back(it->second);
        }

        return v2.size();    
    }
};