// Link: https://leetcode.com/problems/count-number-of-bad-pairs/description/

class Solution {
public:
    long long countBadPairs(vector<int>& nums) 
    {
        vector<int> diffs(nums.size(), -1);
        long long badPair = 0;

        for(int i = 0; i < nums.size(); i++)
        {
            diffs[i] = nums[i] - i; // eq: nums[j]-j != nums[i]-i
        }

        unordered_map<int, int> ump;
        ump[nums[0]] = 1;
        for(int j = 1; j < diffs.size(); j++)
        {
            int totalPair, goodPair;
            totalPair = j;
            goodPair = ump[diffs[j]]; // if seen than its frequency else 0
            badPair += totalPair - goodPair; // formula
            ump[diffs[j]]++;
        }

        return badPair;
    }
};
