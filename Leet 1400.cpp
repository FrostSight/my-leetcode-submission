// Link: https://leetcode.com/problems/construct-k-palindrome-strings/description/

class Solution {
public:
    bool canConstruct(string s, int k) 
    {
        if(k > s.size())    return false;

        vector<int> freq(26, 0);
        int totalOdds = 0;

        for(char& ch : s)
            freq[ch - 'a']++;

        for(int i = 0; i < 26; i++)
        {
            if(freq[i] % 2 != 0)
                totalOdds++;
        }    

        return totalOdds > k ? false : true; // This is the main logic
    }
};

/*
Explanation:
every single character is a palindrome.
s = true, at max k can be 4
by this way there can be at max k numbers of odd characters to make k palindromes
*/