// Link: https://leetcode.com/problems/maximum-running-time-of-n-computers/description/

class Solution {
public:
    typedef long long ll;
    
    bool check(vector<int>& batteries, ll mid, int n) 
    {      
        ll total_required_time = n * mid;  // total time combining like 2 computer takes 3 min each so 2 computer totally takes 6 min combindly
        
        for(int i = 0; i < batteries.size(); i++)   // target is to decrease this combined time
        {    
            total_required_time = total_required_time - min((ll)batteries[i], mid);
            
            if(total_required_time <= 0)
                return true;
            
        }
        return false;
    }
    
    long long maxRunTime(int n, vector<int>& batteries) 
    {
        
        ll low, high, mid, ans = 0;

        low = *min_element(batteries.begin(), batteries.end());  // minimum all will run
        high = accumulate(batteries.begin(), batteries.end(), 0LL) / n;  // an average assumption
        
        while(low <= high) 
        {    
            mid = low + (high - low) / 2;
            
            if(check(batteries, mid, n)) 
            {
                ans = mid;
                low = mid + 1;
            } else 
                high = mid - 1;
        }
        return ans;
    }
};
