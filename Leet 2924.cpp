// Link: https://leetcode.com/problems/find-champion-ii/description/

class Solution {
public:
    int findChampion(int n, vector<vector<int>>& edges) 
    {
        vector<int> indegree(n, 0);
        int champ = -1, counter = 0;

        for(auto& edge : edges)
        {
            int u = edge[0];
            int v = edge[1]; // u ----> v

            indegree[v] += 1; // so the value if vth index increases by 1
        }    

        for(int i = 0; i < n; i++)
        {
            if(indegree[i] == 0)
            {
                champ = i;
                counter++;
            }

            if(counter > 1) // as there can only be 1 champion
            {
                return -1;
                break;
            }
        }

        return champ;
    }
};

