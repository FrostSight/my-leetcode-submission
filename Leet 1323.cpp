// Link: https://leetcode.com/problems/maximum-69-number/description/

class Solution {
public:
    int maximum69Number (int num) 
    {
        int i = 0, remainder, temp = num, x = 0;
        vector<int> nums;

        while(temp != 0)
        {
            remainder = temp % 10;
            nums.push_back(remainder);  // The code first extracts the digits of the given number and stores them in a vector from behind
            temp = temp / 10;
        }

        reverse(nums.begin(), nums.end());  // reverse brings actual form

        for(int i = 0; i < nums.size(); i++)
        {
            if(nums[i] == 6)
            {
                nums[i] = 9; // changes the first occurrence of 6 to 9.
                break;
            }
        }

        for(int i = 0; i < nums.size(); i++)
            x = x * 10 + nums[i];    //converts the vector back to an integer

        return x;    
    }
};


/*
Time complexity:
O(n)

Space complexity:
O(n)

n is the number of digits in the input integer num
*/