// Link: https://leetcode.com/problems/minimum-number-of-swaps-to-make-the-string-balanced/description/

class Solution {
public:
    int minSwaps(string s) 
    {
        stack<char> stk;
        
        for(char &ch : s) 
        {
            if(ch == '[')
                stk.push(ch);
            else if(!stk.empty())
                stk.pop();
        }
        
        int ans = (stk.size() + 1) / 2;

        return ans;
    }
};

