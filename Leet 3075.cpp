// Link: https://leetcode.com/problems/maximize-happiness-of-selected-children/description/

using lli = long long int;

class Solution {
public:
    long long maximumHappinessSum(vector<int>& happiness, int k) 
    {
        lli cSum = 0, counter = 0;
        int i = happiness.size() - 1;
        
        sort(happiness.begin(), happiness.end());    
        
        while(k > 0)
        {
            lli diff = happiness[i] - cSum;
            if(diff < 0)
                return counter;
            
            counter += lli(happiness[i]) - cSum;
            k--;
            i--;
            cSum++;
        }
        return counter;
    }
};