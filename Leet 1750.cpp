// Link: https://leetcode.com/problems/minimum-length-of-string-after-deleting-similar-ends/description/

// approach 1: basic approach
class Solution {
public:
    int minimumLength(string s) 
    {
        int i = 0, j = s.size()-1, counter = 0, ans;
        set<char> st;

        if(s.size() == 1) // base case
            return 1;

        for(char& ch : s)
            st.insert(ch);

        if(st.size() == 1)  // handling duplicacy
            return 0;

        while(i < j)
        {
            if(s[i] == s[j])
            {
                counter++;
                while(s[i] == s[i+1]) // if consecutive found
                {
                    i++;
                    counter++;
                }

                counter++;
                while(s[j-1] == s[j]) // if consecutive found
                {
                    j--;
                    counter++;
                }

                i++;
                j--;
            }
            else
                break;
        }

        ans = s.size() - counter;

        return ans < 0 ? 0 : ans;
    }
};

// approach 2:
class Solution {
public:
    int minimumLength(string s) 
    {
        int i = 0, j = s.size()-1;

        while(i < j && s[i] == s[j])
        {
            char ch = s[i];

            while(i < j && ch == s[i])
                i++;
            
            while(j >= i && ch == s[j])
                j--;
        }

        return j-i+1;
    }
};