// Link: https://leetcode.com/problems/number-complement/description/

class Solution {
public:
    int findComplement(int num) 
    {
        int bit_counter, ans;
        unsigned int mask; // as there are very big numbers

        bit_counter = (int)log2(num) + 1;  // it is the technique to find how many bits are there

        mask = (1U << bit_counter) - 1;  // below explanation ans U is for unsigned

        ans = num ^ mask;

        return ans;
    }
};

/*
This approach is an improvement of another approach.
Another approach is complementing (using xor) bit by bt. But here we figured a mask of the same size of binary representation of a decimal number. Now same as before just perform xor between mask and num.

explanation:
left shifting 1 bit_counter times and we need all 1s.
if bit_counter is 3 and we left shift 1 three times we get 1000 = 8 = 2^3. Now we subtract 1 to get 0111 = 7. This all 1s(111) are disired mask.
*/