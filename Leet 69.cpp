// Link: https://leetcode.com/problems/sqrtx/description/

int rootFinding(int x)
{
    int low = 1, high = x, mid, ans;
    while(low <= high)
    {
        mid = low + (high - low) / 2;

        if(mid <= x / mid)
        {
            ans = mid;
            low = mid + 1;
        }
        else
            high = mid - 1;
    }
    return ans;
}


class Solution {
public:
    int mySqrt(int x) 
    {
        // corner case
        if(x == 0 || x == 1)
            return x;

        int root = rootFinding(x);    

        return root;
    }
};
