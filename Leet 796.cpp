// Link: https://leetcode.com/problems/rotate-string/

class Solution {
public:
    bool rotateString(string s, string goal) 
    {
        if(s.size() != goal.size())
            return false;

        s = s + s; // concating the opriginal string

        if(s.find(goal) != string::npos) // if goal is a substring of concated string or goal is present in concated string
            return true;

        return false;    
    }
};

/*
This is the technique to check rotation of string
*/