// Link: https://leetcode.com/problems/detect-capital/description/

class Solution {
public:

    bool isAllUpper(string& word)
    {
        for(char& ch : word)
        {
            if(!isupper(ch))
                return false;
        }

        return true;
    }

    bool isAllSmall(string& word)
    {
        for(char& ch : word)
        {
            if(!islower(ch));
                return false;
        }

        return true;
    }

    bool isalright(string& word)
    {
        for(int i = 1; i < word.size(); i++)
        {
            if(isupper(word[0]) && isupper(word[i]))
                return false;
            else if(islower(word[0]) && !islower(word[i]))
                return false;
        }

        return true;
    }

    bool detectCapitalUse(string word) 
    {
        if(isAllUpper(word))
            return true;
        else if(isAllSmall(word))
            return true;
        else if(isalright(word))
            return true;
        else
            return false;

    }
};