// Link: https://leetcode.com/problems/max-sum-of-a-pair-with-equal-sum-of-digits/description/

class Solution {
public:
    int getDigitSum(int n)
    {
        int sums = 0;
        while(n != 0)
        {
            sums += n % 10;
            n /= 10;
        }

        return sums;
    }

    int maximumSum(vector<int>& nums) 
    {
        int result = -1, temp = -1;
        unordered_map<int, int> ump;

        for(int n : nums)
        {
            int digitSum = getDigitSum(n);

            if(ump.find(digitSum) != ump.end()) // if seen
            {
                temp = ump[digitSum] + n; // getting the sum
                
                if(ump[digitSum] < n) // if the ncurrentext value is greater
                    ump[digitSum] = n;
                
                result = max(result, temp);
            }
            else
                ump[digitSum] = n; // if not seen
        }

        return result;
    }
};
