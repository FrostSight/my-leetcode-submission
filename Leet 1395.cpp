// Link: https://leetcode.com/problems/count-number-of-teams/description/

#define csl countSmallerLeft
#define csr countSmallerRight
#define cll countLargerLeft
#define clr countLargerRight

class Solution {
public:
    int numTeams(vector<int>& rating) 
    {
        int teams = 0;
        
        for(int j = 1; j < rating.size()-1; j++)
        {
            int csl = 0, cll = 0, csr = 0, clr = 0;

            for(int i = 0; i < j; i++)
            {   
                if(rating[i] < rating[j])
                    csl++;
                else if(rating[i] > rating[j])
                    cll++;
            }

            for(int k = j+1; k < rating.size(); k++)
            {
                if(rating[j] < rating[k])
                    clr++;
                else if(rating[j] > rating[k])
                    csr++;
            }

            teams += (cll * csr) + (csl * clr);
        }  

        return teams;
    }
};
