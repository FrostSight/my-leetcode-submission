// Link: https://leetcode.com/problems/two-sum/description/

class Solution {
public:
    vector<int> twoSum(vector<int>& nums, int target) 
    {
        int result;
        map <int, int> finder;
        for(int i = 0; i < nums.size(); i++)
        {
            result = target - nums[i];

            if(finder.find(result) != finder.end()) // checking if result exists on the map
                return {finder[result], i};
            else
                finder[nums[i]] = i; // if not exist then adding to the map
        }
        return {};
    }
};
