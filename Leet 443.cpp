// Link: https://leetcode.com/problems/string-compression/description/

class Solution {
public:
    int compress(vector<char>& chars) 
    {
        int i = 0, j = 0, counter = 0, ans = 0, n = chars.size();
        string str;

        while(i < n)
        {
            while(j < n && chars[j] == chars[i]) // j < n for boundary check
                j++;  // at first 1 i and j same then i stays and j move forwards to counting

            counter = j - i;
            chars[ans] = chars[i];
            ans++;
            
            if(counter > 1)
            {
                str = to_string(counter);
                for(char ch : str)  // when contains more than 1 character
                {
                    chars[ans] = ch;
                    ans++;
                }
            }                    
            i = j; // update
        }
        return ans;    
    }
};
