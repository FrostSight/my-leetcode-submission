// Link: https://leetcode.com/problems/non-decreasing-subsequences/description/

class Solution {
public:
  void solve(vector<int>& nums, int idx, vector<int>& current, vector<vector<int>>& result) 
    {
        if(current.size() >= 2)
            result.push_back(current);
        
        unordered_set<int> ust;
        
        for(int i = idx; i < nums.size(); i++) 
        {
            if( (current.empty() || nums[i]  >= current.back()) && ust.find(nums[i]) == ust.end() ) 
            {                
                current.push_back(nums[i]);
                solve(nums, i+1, current, result);
                current.pop_back();               
                ust.insert(nums[i]);
            }
        }
    }
    
    vector<vector<int>> findSubsequences(vector<int>& nums) 
    {
        vector<vector<int>> result;     
        vector<int> current;
        
        solve(nums, 0, current, result);
        
        return result;
    }
};
