// Link: https://leetcode.com/problems/minimum-string-length-after-removing-substrings/description/

class Solution {
public:
    int minLength(string s) 
    {
        stack<char> stk;

        for(int i = 0; i < s.size(); i++)
        {
            if(!stk.empty() && s[i] == 'B' && stk.top() == 'A')
                stk.pop();
            else if(!stk.empty() && s[i] == 'D' && stk.top() == 'C')
                stk.pop();
            else
                stk.push(s[i]);
        }

        int ans = stk.size();

        return ans;
    }
};

// just as parenthesis code. If ending bracket(B, D) found pop out beginning bracket(A, C)

