// Link: https://leetcode.com/problems/special-array-i/description/

// Approach 1:
class Solution {
public:
    bool isArraySpecial(vector<int>& nums) 
    {
        for(int i = 0; i < nums.size()-1; i++)
        {
            if((nums[i] % 2) == (nums[i+1] % 2)) // if both are even or odd
                return false;
        }    

        return true;
    }
};

// Approach 2:
class Solution {
public:
    bool isArraySpecial(vector<int>& nums) 
    {
        for(int i = 0; i < nums.size()-1; i++)
        {
            if((nums[i] & 1) == (nums[i+1] & 1))
                return false;
        }    

        return true;
    }
};

/*
Explanation:
Task is are adjacent elements even-odd or vice versa

Approach 2:
In binary representation, we need to check LSB of two adjacent element. We can check by doing And operation with 1
*/
