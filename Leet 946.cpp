// Link: https://leetcode.com/problems/validate-stack-sequences/description/

class Solution {
public:
    bool validateStackSequences(vector<int>& pushed, vector<int>& popped) 
    {
        stack<int> stk;
        int i = 0, j = 0;

       while(i < pushed.size() && j < popped.size())  // pushed, popped same size
       {
            stk.push(pushed[i]); // first we push then pop

            while(!stk.empty() && stk.top() == popped[j])
            {
                stk.pop();
                j++;
            }

            i++;
       }

        return stk.empty() ? true : false;   
    }
};


// got me TLE
class Solution {
public:
    bool validateStackSequences(vector<int>& pushed, vector<int>& popped) 
    {
        stack<int> stk;
        int i = 0, j = 0;

       while(j < popped.size())
       {
            while((stk.empty() || popped[j] != stk.top()) && i < pushed.size())
            {
                stk.push(pushed[i]);
                i++;
            }

            while(!stk.empty() && popped[j] == stk.top())
            {
                stk.pop();
                j++;
            }
       }

        return stk.empty() ? true : false;   
    }
};

