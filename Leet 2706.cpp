// Link: https://leetcode.com/problems/buy-two-chocolates/

class Solution {
public:
    int buyChoco(vector<int>& prices, int money) 
    {
        int minPrice = INT_MAX, minPrice2 = INT_MAX;

        for(int i = 0; i < prices.size(); i++)
        {
            if(prices[i] < minPrice)
            {
                minPrice2 = minPrice;
                minPrice = prices[i];
            }
            else
                minPrice2 = min(minPrice2, prices[i]);
        }    

        if(minPrice + minPrice2 > money)
            return money;
        
        int remain = money - (minPrice + minPrice2);
        
        return remain;
    }
};

