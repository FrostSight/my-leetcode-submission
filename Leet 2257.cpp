// Link: https://leetcode.com/problems/count-unguarded-cells-in-the-grid/description/

class Solution {
public:
    void solve(vector<vector<int>>& grid, int i, int j, int di, int dj) 
    {
        int m = grid.size(), n = grid[0].size();
        
        i += di; // moving to the coordinate
        j += dj;
        
        if (i < 0 || i >= m || j < 0 || j >= n || grid[i][j] == 2 || grid[i][j] == 3)   return; // out of boundary || guard || wall
        
        if (grid[i][j] == 0) 
            grid[i][j] = 1; // mark as guarded
        
        solve(grid, i, j, di, dj); //  di, dj gives direction
    }

    int countUnguarded(int m, int n, vector<vector<int>>& guards, vector<vector<int>>& walls) 
    {
        vector<vector<int>> grid(m, vector<int>(n, 0));
        int counter = 0;

        for (auto& guard : guards) 
        {
            int i = guard[0];
            int j = guard[1];
            grid[i][j] = 2; // guard
        }

        for (auto& wall : walls) 
        {
            int i = wall[0];
            int j = wall[1];
            grid[i][j] = 3; // wall
        }

        for (int i = 0; i < m; i++) 
        {
            for (int j = 0; j < n; j++) 
            {
                if(grid[i][j] == 2) // guard found
                {
                    solve(grid, i, j, 0, 1); // up
                    solve(grid, i, j, 0, -1); // down
                    solve(grid, i, j, 1, 0); // right
                    solve(grid, i, j, -1, 0); // left
                }
            }
        }

        for (int i = 0; i < m; i++) 
        {
            for (int j = 0; j < n; j++) 
            {
                if (grid[i][j] == 0) // unguarded                 
                    counter++;                
            }
        }

        return counter;
    }
};

