// Link: https://leetcode.com/problems/peak-index-in-a-mountain-array/description/

int searching(vector <int> &arr)
{
    int low = 0, high = arr.size() - 1, mid, ans;
    while(low <= high)
    {
        mid = low + (high - low) / 2;

        if(arr[mid] > arr[mid + 1])
            high = mid - 1;
        else
            low = mid + 1;
    }
    return low;
}

class Solution {
public:
    int peakIndexInMountainArray(vector<int>& arr) 
    {
        int peak = searching(arr);
        return peak;
    }
};
