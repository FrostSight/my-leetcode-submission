// Link: https://leetcode.com/problems/merge-strings-alternately/description/

class Solution {
public:
    string mergeAlternately(string word1, string word2) 
    {
        int i = 0, j = word1.size();
        string word4;

        string word3 = word1 + word2;

        for(int i = 0; i < word1.size(); i++)
        {
            word4.push_back(word3[i]);
            if(j < word3.size()) // checking if j is within limits
            {
                word4.push_back(word3[j]);
                j++;
            }
        }

        while(j < word3.size()) // when word2.size() is greater
        {
            word4.push_back(word3[j]);
            j++;
        }

        return word4;
    }
};
