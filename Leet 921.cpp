// Link: https://leetcode.com/problems/minimum-add-to-make-parentheses-valid/description/

class Solution {
public:
    int minAddToMakeValid(string s) 
    {
        int opening = 0, closing = 0;

        for(char& ch : s)
        {
            if(ch == '(')
                opening++;
            else if(ch == ')')
            {
                if(opening > 0) // if this means no opening bracket was found 
                    opening--; // when closing brackets are found corresponding to its opening brackets
                else
                    closing++;
            }
        }  

        int ans = opening + closing;
        return ans;
    }
};

