// LinK: https://leetcode.com/problems/minimum-window-substring/

#define si start_i
#define mws minWindowSize
#define cws currentWindowSize
#define rl requird_length // required window size

class Solution {
public:
    string minWindow(string s, string t) 
    {
        if(t.size() > s.size())  // corner case
            return "";

        unordered_map<char, int> ump; // frequency table
        int i = 0, j = 0, s_len = s.size(), t_len = t.size(), start_i = 0, minWindowSize = INT_MAX, currentWindowSize, requird_length = t.size();

        for(char& ch : t)
            ump[ch]++;  // those who have frequency greater then 0 only these characters affect rl and points of concerns
        
        while(j < s_len)
        {
            char ch = s[j];

            if(ump[ch] > 0) // present and greater than 0 so it affects rl
                rl--;

            ump[ch]--;

            // j stops when rl = 0
            while(rl == 0)  // requied window size found and it starts shrinking as long as rl is 0
            {
                cws = j+1 -i; // length
                if(mws > cws)
                {
                    mws = cws;
                    si = i;
                }
                
                ump[s[i]]++;
                if(ump[s[i]] > 0)
                    rl++;
                
                i++;
            }

            j++; 
        }

        return mws == INT_MAX ? "" : s.substr(si, mws);
    }
};

/*
Notes:
=> the value of j index, does it has a frequency greater than 0?
    -> frequency of the value of index j will always decrease (if not registered then register it then decrement it i.e start from -1)
    -> the value of i index increments always
*/