// Link: https://leetcode.com/problems/maximum-nesting-depth-of-the-parentheses/description/

class Solution {
public:
    int maxDepth(string s) 
    {
        int depth = 0, maxDepth = 0;

        for(char& ch: s)
        {
            if(ch == '(')
            {
                depth++;
                maxDepth = max(depth, maxDepth);
            }
            else if(ch == ')')
                depth--;
        }   
         
        return maxDepth;
    }
};


// approach 2 : using stack

class Solution {
public:
    int maxDepth(string s) 
    {
        stack<char> stk;
        int depth = 0;

        for(char& ch : s)
        {
            if(ch == '(')
                stk.push(ch);
            else if(ch == ')')
                stk.pop();
            
            depth = max(depth, (int)stk.size());
        }    

        return depth;
    }
};