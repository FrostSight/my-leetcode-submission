// Link: https://leetcode.com/problems/spiral-matrix-iv/description/

class Solution {
public:
    vector<vector<int>> spiralMatrix(int m, int n, ListNode* head) 
    {
        vector<vector<int>> matrix(m, vector<int>(n, -1));
        int top = 0, down = m-1, left = 0, right = n-1, step = 1;
        //step
        //1   -> left  to right
        //2   -> top   to down
        //3   -> right to left
        //4   -> down  to top


        while(top <= down && left <= right) 
        {
            if(step == 1) 
            { 
                for(int col = left; head != NULL && col <= right; col++) // untill head null or linked list is finished
                {
                    matrix[top][col] = head->val;
                    head = head->next;
                }

                top++;
            }

            else if(step == 2) 
            { 
                for(int row = top; head != NULL && row <= down; row++)  // untill head null or linked list is finished
                {
                    matrix[row][right] = head->val;
                    head = head->next;
                }

                right--;
            }

            else if(step == 3) 
            { 
                for(int col = right; head != NULL && col >= left; col--)  // untill head null or linked list is finished
                {
                    matrix[down][col] = head->val;
                    head = head->next;
                }

                down--;
            }

            else if(step == 4) 
            { 
                for(int row = down; head != NULL && row >= top; row--)  // untill head null or linked list is finished
                {
                    matrix[row][left] = head->val;
                    head = head->next;
                }
                
                left++;
            }

            step++;
            if(step == 5)
                step = 1;
        }

        return matrix;
    }
};
