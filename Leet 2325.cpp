// Link: https://leetcode.com/problems/decode-the-message/

class Solution {
public:
    string decodeMessage(string key, string message) 
    {
        unordered_map<char, char> umap;
        int i = 0;
        string msg = "";

        for(char k : key)
        {
            if(k == ' ') // if space found skipped
                continue;
            
            if(umap.count(k) == 0) // count = 0 means no such character is found in map
            {
                umap[k] = 'a' + i;  // a+0=a, a+1=b, a+9=j
                i++;
            }
        }

        for(char m : message)
        {
            if(m == ' ')
                msg = msg + ' ';
            else
                msg = msg + umap[m]; // if not space then concate the value of umap[m]     
        }   
           
        return msg;    
    }
};