// Link: https://leetcode.com/problems/reveal-cards-in-increasing-order/description/

class Solution {
public:
    vector<int> deckRevealedIncreasing(vector<int>& deck)
     {
        bool skip = false;
        int n = deck.size(), i = 0, j = 0;
        vector<int> result(n, 0);
        
        sort(deck.begin(), deck.end());
        
        while(i < n) 
        {
            
            if(result[j] == 0) 
            {  
                if(skip == false) 
                {
                    result[j] = deck[i];
                    i++;
                }
                
                skip = !skip;               
            }
            
            j = (j+1)%n;         
        }
        
        return result;
    }
};