// Link: https://leetcode.com/problems/longest-happy-string/description/

#define P pair<int, char>

class Solution {
public:
    string longestDiverseString(int a, int b, int c) 
    {
        priority_queue<P> max_pq;

        if(a > 0)
            max_pq.push({a, 'a'});    
        if(b > 0)
            max_pq.push({b, 'b'});    
        if(c > 0)
            max_pq.push({c, 'c'});    
        
        string result = "";

        while(!max_pq.empty())
        {
            auto x = max_pq.top();
            max_pq.pop();

            int current_count = x.first;
            char current_char = x.second;

            if(result.size() >= 2 && result[result.size()-1] == current_char && result[result.size()-2] == current_char)
            {
                if(max_pq.empty())
                    break;
                
                auto y = max_pq.top();
                max_pq.pop();

                int next_count = y.first;
                char next_char = y.second;

                result.push_back(next_char);
                next_count--;
                if(next_count > 0) 
                    max_pq.push({next_count, next_char});
            }
            else 
            {
                current_count--;
                result.push_back(current_char);
            }

            if(current_count > 0) 
            {
                max_pq.push({current_count, current_char});
            }
        }

        return result;
    }
};

