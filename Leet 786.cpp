// Link: https://leetcode.com/problems/k-th-smallest-prime-fraction/description/

#define dbl double

class Solution {
public:
    vector<int> kthSmallestPrimeFraction(vector<int>& arr, int k) 
    {
        priority_queue<vector<double>> mxq;

        for(int i = 0; i < arr.size()-1; i++)
        {
            for(int j = i+1; j < arr.size(); j++)
            {
                dbl x, y, z;
                x = dbl(arr[i]);
                y = dbl(arr[j]);
                z = x / y;

                mxq.push({z, x, y});

                if(mxq.size() > k)  // k size exceed
                    mxq.pop();  // so we remove those greater elemets
            }
        }

        auto temp = mxq.top();  // fraction, arr[i], arr[j]
        vector<int> ans(2, 0);
        ans[0] = temp[1];
        ans[1] = temp[2];

        return ans;
    }
};

/*
We use max heap (max elements on top)
ith small element will be on top ith position from bottom
Above ith elements, greater elements than ith will be there so k size
*/