// Link: https://leetcode.com/problems/maximum-width-ramp/description/

class Solution {
public:
    int maxWidthRamp(vector<int>& nums) 
    {
        vector<int> maxRight(nums.size());
        maxRight[nums.size() - 1] = nums[nums.size() - 1];

        for(int i = nums.size()-2; i >= 0; i--) 
        {
            maxRight[i] = max(maxRight[i + 1], nums[i]);
        }

        int ramp = 0, i = 0, j = 0;

        while(j < nums.size()) 
        {
            while(i < j && nums[i] > maxRight[j]) 
            {
                i++;
            }

            ramp = max(ramp, j - i);
            j++;
        }

        return ramp;
    }
};

