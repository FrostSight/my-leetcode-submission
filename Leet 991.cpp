// Link: https://leetcode.com/problems/broken-calculator/description/

class Solution {
public:
    int brokenCalc(int startValue, int target) 
    {
        int counter = 0;

        while(target > startValue)
        {
            counter++;

            if(target % 2 != 0)  // if odd 1 incremented
                target++;
            else
                target = target / 2; // if even get divided
        }

        return counter + startValue - target;  // in any case where startvalue>target there counter will 0 and vice versa
    }
};
