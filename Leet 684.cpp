// Link: https://leetcode.com/problems/redundant-connection/description/

class Solution {
public:
    bool dfsTraverse(vector<vector<int>>& graph, int startingNode, int endingNode, vector<bool>& visited)
    {
        if(startingNode ==  endingNode)
            return true;
        
        visited[startingNode] = true;

        for(int ngbr : graph[startingNode])
        {
            if(visited[ngbr] == true)
                continue;
            
            if(dfsTraverse(graph, ngbr, endingNode, visited) == true)
                return true;
        }

        return false;
    }

    vector<int> findRedundantConnection(vector<vector<int>>& edges) 
    {
        int n = 0;
        for(auto& edge : edges) 
        {
            n = max(n, max(edge[0], edge[1]));
        }

        vector<vector<int>> graph(n + 1); 

        for (vector<int>& edge : edges)
        {
            int u = edge[0];
            int v = edge[1];

            vector<bool> visited(n + 1, false);
            if (dfsTraverse(graph, u, v, visited))
            {
                return edge; 
            }

            graph[u].push_back(v);
            graph[v].push_back(u);
        } 

        return {};  
    }
};
