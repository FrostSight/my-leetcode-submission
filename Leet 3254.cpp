// Link: https://leetcode.com/problems/find-the-power-of-k-size-subarrays-i/

class Solution {
public:
    vector<int> resultsArray(vector<int>& nums, int k) 
    {
        int n = nums.size(), consecutive = 1;
        vector<int> result(n-k+1, -1);

        for(int i = 1; i < k; i++)  //preprocess the first window
        {
            if(nums[i] == nums[i-1]+1) 
                consecutive++;
            else
                consecutive = 1;
        }

        if(consecutive == k) 
            result[0] = nums[k-1];

        int i = 1, j = k;

        while(j < n) 
        {
            if(nums[j] == nums[j-1]+1) 
                consecutive++;
            else 
                consecutive = 1;         

            if(consecutive >= k) 
                result[i] = nums[j];
    
            i++;
            j++;
        }

        return result;

    }
};

