// Link: https://leetcode.com/problems/find-the-student-that-will-replace-the-chalk/

class Solution {
public:
    int chalkReplacer(vector<int>& chalk, int k) 
    {
        int ans;
        long long chalk_sum   = 0;  // using accumulate gives error
        for(int &chalkReq : chalk) 
            chalk_sum   += chalkReq;

        int chalk_remain = k % chalk_sum; // skpping every full turn and and finding the remain

        for(int i = 0; i < chalk.size(); i++) 
        { 
            if(chalk_remain  < chalk[i]) 
            {
                ans = i;
                break;
            }

            chalk_remain  -= chalk[i]; // deduction of chalk
        }

        return ans;
    }
};

