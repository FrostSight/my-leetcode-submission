// Link: https://leetcode.com/problems/check-distances-between-same-letters/description/

#define ci character_integer
#define cc current_character
#define dc desired_character

class Solution {
public:
    bool checkDistances(string s, vector<int>& distance) 
    {
        set<char> st;

        for(int i = 0; i < s.size(); i++)
        {
            char cc = s[i];
            auto it = st.find(cc);

            if(it != st.end())  // duplicate found and ignored
                continue;
            else
            {            
                int ci = s[i] - 'a';
                int pos = i + 1 + distance[ci];
                int dc = s[pos];
                
                if (pos >= s.size() || dc != cc)  // first part is corner case
                    return false;

                st.insert(cc);
            }
        }    

        return true;
    }
};

