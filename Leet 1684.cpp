// Link: https://leetcode.com/problems/count-the-number-of-consistent-strings/description/

class Solution {
public:
    int countConsistentStrings(string allowed, vector<string>& words) 
    {
        unordered_set<char> allowSet;
        bool status;
        int counter = 0;

        for(char a : allowed)
            allowSet.insert(a);
        
        for(auto word : words)
        {
            status = true;  // we consider word contains all and only elements of allowSet
            for(int i = 0; i < word.size(); i++)
            {
                if(allowSet.find(word[i]) == allowSet.end()) // if any elements of word mismatches with status becomes false
                {
                    status = false;
                    break;
                }
            }
            if(status)
                counter++;
        }
        return counter;
    }
};