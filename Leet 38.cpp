// Link: https://leetcode.com/problems/count-and-say/

class Solution {
public:
    string countAndSay(int n) 
    {
        string say, ans, cnt;
        int counter1 = 1, temp; 

        if(n == 1)
            return "1";
        
        say = countAndSay(n - 1);


        for(int i = 0; i < say.size(); i++)
        {
          char ch = say[i];

          if(say[i] == say[i+1])  // if consecutive elements are same
          {
            counter1++;
          }
          else  // if consecutive elements are not same
          {
            temp = counter1;  // process to integer to string conversion
            counter1 = 1;
            cnt = to_string(temp);
            ans += cnt + ch;
          }
        }
        return ans; 
    }
};
