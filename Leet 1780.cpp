// Link: https://leetcode.com/problems/check-if-number-is-a-sum-of-powers-of-three/description/

class Solution {
public:
    bool checkPowersOfThree(int n) 
    {
        int p = 0;
        
        while(pow(3, p) < n)
            p++;  // max p can be got for 3^p < n
        
        while(n > 0)
        {
            if(n >= pow(3, p))
                n -= pow(3, p);
            
            if(n >= pow(3, p)) // it is the main logic. See te below explanation
                return false;

            p--;
        }

        return true;
    }
};

/*
Explanation:
n = 21
max p is 2 as 3^2=9 which is less than 21
21-9=12 after subtracting 9 we have 12
12-3=9 after subtracting 9 we have again 9
new "n" can not be greater than previous 9
*/
