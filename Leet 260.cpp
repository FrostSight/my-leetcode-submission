// Link: https://leetcode.com/problems/single-number-iii/description/

#define ll long long

class Solution {
public:
    vector<int> singleNumber(vector<int>& nums) 
    {
        ll xor_r = 0;
        int mask, groupA = 0, groupB= 0;

        for(int& i : nums)
            xor_r ^= i;
        
        mask = (xor_r) & (-xor_r);

        for(int& i : nums)
        {
            if((i & mask) != 0)
                groupA ^= i;
            else
                groupB ^= i;
        }

        return {groupA, groupB};
    }
};
