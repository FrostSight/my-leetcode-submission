// LinK: https://leetcode.com/problems/merge-k-sorted-lists/description/

class Solution {
public:
    ListNode* mergeTwoSortedList(ListNode* l1, ListNode* l2)
    {
        if(l2 == NULL) return l1;
        if(l1 == NULL) return l2;
        
        if(l1->val <= l2->val)
        {
            l1->next = mergeTwoSortedList(l1->next, l2);
            return l1;
        }
        else if (l2->val < l1->val)
        {
            l2->next = mergeTwoSortedList(l2->next, l1);
            return l2;
        }
        return NULL;
    } 

    ListNode* divideList(int start, int end, vector<ListNode*>& lists)
    {
        if(start > end)
            return NULL;
        if(start == end)
            return lists[start];

        int mid = start + (end - start) / 2;
        ListNode* L1 = divideList(start, mid, lists);
        ListNode* L2 = divideList(mid+1, end, lists);

        ListNode* L = mergeTwoSortedList(L1, L2);

        return L;
    }

    ListNode* mergeKLists(vector<ListNode*>& lists) 
    {
        if(lists.size() == 0)
            return NULL;
        
        ListNode* result = divideList(0, lists.size()-1, lists);

        return result;
    }
};
