// Link: https://leetcode.com/problems/capacity-to-ship-packages-within-d-days/description/

class Solution {
public:

    bool canShip(vector<int>& weights, int capacity, int days) 
	{
        int load = 0, days_count = 1; // day never 0

        for (int wt : weights) 
		{
            load = load + wt;
            if (load > capacity) 
			{
                load = wt;  // new day new load
                ++days_count;  // new day
                
				if (days_count > days) 
                    return false;
            }
        }
        return true;
    }

    int shipWithinDays(std::vector<int>& weights, int days) 
	{
        int low, high, mid;
        low = *max_element(weights.begin(), weights.end());
        high = accumulate(weights.begin(), weights.end(), 0);
		
        while (low < high) 
		{
            mid = low + (high - low) / 2;
            
			if (canShip(weights, mid, days)) 
                high = mid; 
			else 
                low = mid + 1;
        }
        return low;
    }
};

/*
Time complexity O (n log m)
*/


/* poorly optimized

```
class Solution {
public:
    
    int calculateDays(vector<int>& weights, int capacity)
    {
        int load = 0, days_count = 1;

        for(int i = 0; i < weights.size(); i++)
        {
            if(load + weights[i] > capacity)
            {
                days_count++;
                load = weights[i];
            }
            else
                load = load + weights[i]; 
        }
        return days_count;
    }

    int shipWithinDays(vector<int>& weights, int days) 
    {
        int low, high, mid, days_required, ans;

        low = *max_element(weights.begin(), weights.end()); 
        high = accumulate(weights.begin(), weights.end(), 0); 

        while(low <= high)
        {
            mid = low + (high - low) / 2;

            days_required = calculateDays(weights, mid);

            if(days_required <= days)
            {
                ans = mid;
                high = mid - 1;
            }
            else
                low = mid + 1;
        }
        return ans;
    }
};
```

*/
