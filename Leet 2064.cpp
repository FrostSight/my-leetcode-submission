// Link: https://leetcode.com/problems/minimized-maximum-of-products-distributed-to-any-store/

class Solution {
public:

    bool isPossible(vector<int>& quantities, int mid, int n)
    {
        int store = 0;

        for(int i = 0; i < quantities.size(); i++)
        {
            store += (quantities[i] + mid - 1) / mid;  // formula to find the number of stores for distributing all products
        }

        return store <= n ? true : false;
    }

    int minimizedMaximum(int n, vector<int>& quantities) 
    {
        int low = 1, high, mid, ans;

        high = *max_element(quantities.begin(), quantities.end());

        while(low <= high)
        {
            mid = low + (high - low) / 2;

            if(isPossible(quantities, mid, n))
            {
                ans = mid;
                high = mid - 1;
            }
            else
                low = mid + 1;
        }

        return ans;
    }
};

/*
Can we distribute mid products in stores?
it takes "store" number of stores to distrihbute at most mid products untill all the products are distributed
if "store" is less than n than true and we move forward to find next minimum
*/

