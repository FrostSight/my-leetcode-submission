// Link: https://leetcode.com/problems/removing-stars-from-a-string/description

// approach 1: using stack

class Solution {
public:
    string removeStars(string s) 
    {
        stack<char> stk;
        string ans = "";

        for(char& ch : s)
        {
            if(ch != '*')
                stk.push(ch);
            else if(ch == '*')
                stk.pop();
        }  

        while(!stk.empty())
        {
            ans += stk.top();
            stk.pop();
        }

        reverse(ans.begin(), ans.end());

        return ans;
    }
};


// approach 2: without stack

class Solution {
public:
    string removeStars(string s) 
    {
        stack<char> stk;
        string ans = "";

        for(char& ch : s)
        {
            if(ch != '*')
                ans += ch;
            else if(ch == '*')
                ans.pop_back();
        }  

        return ans;
    }
};