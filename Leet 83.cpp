// Link: https://leetcode.com/problems/remove-duplicates-from-sorted-list/description/

class Solution {
public:
    ListNode* deleteDuplicates(ListNode* head) 
    {
        if (head == NULL) return nullptr;

        ListNode* p1 = head;
        ListNode* p2 = head->next;

        while (p2 != NULL)
        {
            if (p1->val == p2->val)
            {
                p1->next = p2->next;
                delete p2;
                p2 = p1->next;
            }
            else
            {
                p1 = p2;
                p2 = p2->next;
            }
        }

        return head;
    }
};
