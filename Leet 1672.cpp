// Link: https://leetcode.com/problems/richest-customer-wealth/description/

class Solution {
public:
    int maximumWealth(vector<vector<int>>& accounts) 
    {
        int ans = -1, temp;

        for(int i = 0; i < accounts.size(); i++)
        {
            temp = 0;  // every time before entering to a new column
            for(int j = 0; j < accounts[i].size(); j++)
            {
                temp = temp + accounts[i][j];
            }
            
            ans = max(ans, temp); // whenever a column ends max value is stored
        }  
        return ans;  
    }
};