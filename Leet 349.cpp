// Link: https://leetcode.com/problems/intersection-of-two-arrays/description/

// Approach 1

class Solution {
public:
    vector<int> intersection(vector<int>& nums1, vector<int>& nums2) 
    {
        set<int> s1, s2;
        vector<int> ans;

        for(int n1 : nums1)
            s1.insert(n1);  // set used to remove duplicacy

        for(int n2 : nums2)
            s2.insert(n2);    
        
        for(int n1 : s1)
        {
            for(int n2 : s2)
            {
                if(n1 == n2)  // equal values are intersection point
                    ans.push_back(n1);
            }
        }
        return ans;
    }
};

// Approach 1a

class Solution {
public:
    vector<int> intersection(vector<int>& nums1, vector<int>& nums2) 
    {
        set<int> s1, s2;
        vector<int> ans;

        for(int n1 : nums1)
            s1.insert(n1);

        for(int n2 : nums2)
            s2.insert(n2);    
        
        for(int n1 : s1)
        {
            auto it = s2.find(n1);
            if(it != s2.end())
                ans.push_back(n1);
        }
        return ans;
    }
};


// Approach 2: Binary search

class Solution {
public:

    int searching(int target, vector<int>& nums2)
    {
        int low = 0, high = nums2.size()-1, mid;

        while(low <= high)
        {
            mid = low + (high - low) / 2;

            if(nums2[mid] == target)
                return nums2[mid];
            else if(nums2[mid] < target)
                low = mid + 1;
            else
                high = mid - 1;
        }
        return -1;
    }

    vector<int> intersection(vector<int>& nums1, vector<int>& nums2) 
    {
        vector<int> ans;
        set<int> s1;
        int a;

        sort(nums1.begin(), nums1.end());
        sort(nums2.begin(), nums2.end());

        for(int i = 0; i < nums1.size(); i++)
        {
            a = searching(nums1[i], nums2);

            if(a != -1)
                ans.push_back(a);
        }

         ans.erase(unique(ans.begin(), ans.end()), ans.end());

        return ans;
    }
};