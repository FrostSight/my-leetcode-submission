// Link: https://leetcode.com/problems/find-the-longest-substring-containing-vowels-in-even-counts/

class Solution {
public:
    int findTheLongestSubstring(string s) 
    {
        unordered_map<string, int> ump;
        vector<int> state(5, 0);
        int maxLength = 0;
        string currentState = "00000"; 
        ump[currentState] = -1;
        
        
        
        for (int i = 0; i < s.length(); ++i) 
        {
            if (s[i] == 'a') state[0] ^= 1; 
            else if (s[i] == 'e') state[1] ^= 1; 
            else if (s[i] == 'i') state[2] ^= 1; 
            else if (s[i] == 'o') state[3] ^= 1; 
            else if (s[i] == 'u') state[4] ^= 1; 

            currentState = "";
            for (int j = 0; j < 5; ++j) 
            {
                currentState += to_string(state[j]);
            }


            if (ump.find(currentState) != ump.end()) 
                maxLength = max(maxLength, i - ump[currentState]);
            else 
                ump[currentState] = i;
        }

        return maxLength;
    }
};
