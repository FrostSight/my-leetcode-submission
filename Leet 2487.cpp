// Link: https://leetcode.com/problems/remove-nodes-from-linked-list/description/

class Solution {
public:
    ListNode* removeNodes(ListNode* head) 
    {
        stack<ListNode*> stk;
        ListNode* current = head;
        int maxVal;

        while(current != NULL)
        {
            stk.push(current);
            current = current->next;
        }

        current = stk.top();
        stk.pop();
        maxVal = current->val;
        ListNode* result = new ListNode(current->val);

        while(!stk.empty())
        {
            current = stk.top();
            stk.pop();
            
            if(current->val < maxVal)
                continue;
            else
            {
                ListNode* newNode = new ListNode(current->val);
                newNode->next = result;
                result = newNode;
                maxVal = current->val;
            }
        }
        return result;
    }
};
