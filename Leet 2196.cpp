// Link: https://leetcode.com/problems/create-binary-tree-from-descriptions/

class Solution {
public:
    TreeNode* createBinaryTree(vector<vector<int>>& descriptions) 
    {
        unordered_map<int, TreeNode*> ump;
        unordered_set<int> childSet;
        TreeNode* ans;

        for(auto& v : descriptions)
        {
            int parent = v[0];
            int child = v[1];
            int isLeft = v[2];

            if(ump.find(parent) == ump.end())
                ump[parent] = new TreeNode(parent);
            
            if(ump.find(child) == ump.end())
                ump[child] = new TreeNode(child);
            
            if(isLeft == 1)
                ump[parent]->left = ump[child];
            else if(isLeft == 0)
                ump[parent]->right = ump[child];
            
            childSet.insert(child);
        }

        for(auto& v : descriptions)
        {
            int parent = v[0];
            if(childSet.find(parent) == childSet.end())
            {
                ans = ump[parent];
                break;
            }
        }

        return ans;
    }
};
