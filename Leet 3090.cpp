// Link: https://leetcode.com/problems/maximum-length-substring-with-two-occurrences/description/

class Solution
{
public:
    int maximumLengthSubstring(string s)
    {
        int ans = 0;
        for (int i = 0; i < s.size(); i++)
        {
            unordered_map<char, int> umap;

            for (int j = i; j < s.size(); j++)
            {
                umap[s[j]]++;

                if (umap[s[j]] > 2)
                    break;

                ans = max(ans, j+1 - i);
            }
        }
        return ans;
    }
};