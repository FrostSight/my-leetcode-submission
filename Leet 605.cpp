// Link: https://leetcode.com/problems/can-place-flowers/description/

#define leftEmpty le
#define rightEmpty re

class Solution {
public:
    bool canPlaceFlowers(vector<int>& flowerbed, int n) 
    {   
        if(n == 0)
            return true;
        
        
        for(int i = 0; i < flowerbed.size(); i++) 
        {
            bool leftEmpty = false, rightEmpty = false;
            
            if(flowerbed[i] == 0) 
            {

                if (i == 0) 
                    le = true;
                else 
                    le = (flowerbed[i - 1] == 0);

                
                if (i == flowerbed.size() - 1) 
                    re = true;
                else 
                    re = (flowerbed[i + 1] == 0);

                
                if(le && re) 
                {
                    flowerbed[i] = 1;
                    n--;

                    if(n == 0)
                        return true;
                }
            }           
        }       
        return false;        
    }
};