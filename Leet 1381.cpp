// Link: https://leetcode.com/problems/design-a-stack-with-increment-operation/description/

class CustomStack {
public:

    vector<int> stk;
    int n, top_idx = -1; 

    CustomStack(int maxSize) 
    {   
        n = maxSize;
    }
    
    void push(int x) 
    {
        if(stk.size() < n)
        {
            stk.push_back(x);
            top_idx++;
        }    
    }
    
    int pop() 
    {
        if(stk.size() == 0)
            return -1;
        
        int top_val = stk[top_idx];
        stk.pop_back();
        top_idx--;
        return top_val;
    }
    
    void increment(int k, int val) 
    {
        int x = min(k, (int)stk.size());
        for(int i = 0; i < x; i++)
        {
            stk[i] += val;
        }    
    }
};

