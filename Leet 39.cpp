// Link: https://leetcode.com/problems/combination-sum/description/

class Solution {
public:

    void solve(int index, vector<int>& candidates, vector<int>& current, int target, vector<vector<int>>& result) 
    {
        if (target == 0) 
        {
            result.push_back(current);
            return;
        }

        if (index >= candidates.size() || target < 0)  // actual terminating condition
            return;

        current.push_back(candidates[index]);  // pick
        solve(index, candidates, current, target - candidates[index], result);
        current.pop_back(); // unpick
        solve(index + 1, candidates, current, target, result);
    }

    vector<vector<int>> combinationSum(vector<int>& candidates, int target) 
    {
        vector<vector<int>> result;
        vector<int> current;

        solve(0, candidates, current, target, result);

        return result;
    }
};
