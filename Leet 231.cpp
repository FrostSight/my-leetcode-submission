// Link: https://leetcode.com/problems/power-of-two/description/

class Solution {
public:
    bool isPowerOfTwo(int n) 
    {
        int ans = 1;

        // corner case
        if (n == 1)
            return true;

        for (int i = 1; i <= 30; i++)
        {
            ans = ans * 2;
            if (n == ans)
            {
                return true;
                break;
            }
        }
        return false;   
    }
};
