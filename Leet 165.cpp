// Link: https://leetcode.com/problems/compare-version-numbers/description/

class Solution {
public:

    vector<string> getToken(string& v)
    {
        stringstream ss(v);
        string tk = "";
        vector<string> token;

        while(getline(ss, tk, '.'))
            token.push_back(tk);
        
        return token;
    }

    int compareVersion(string version1, string version2) 
    {
        vector<string> v1 = getToken(version1);
        vector<string> v2 = getToken(version2);
        int i = 0;

        while(i < v1.size() || i < v2.size())
        {
            int a = i < v1.size() ? stoi(v1[i]) : 0;
            int b = i < v2.size() ? stoi(v2[i]) : 0;

            if(a < b)
                return -1;
            else if(a > b)
                return 1;
            else 
                i++;
        }

        return 0;
    }
};

