// Appraoach 1: Bunary search
class Solution {
public:

    int isPresent(vector<int>& nums2, int k)
    {
        int low = 0, high = nums2.size()-1, mid;

        while(low <= high)
        {
            mid = low + (high - low) / 2;

            if(nums2[mid] == k)
                return nums2[mid];
            else if(nums2[mid] < k)
                low = mid + 1;
            else if(nums2[mid] > k)
                high = mid - 1;
        }
        return -1;
    }

    int getCommon(vector<int>& nums1, vector<int>& nums2) 
    {
        int val;

        for(int i = 0; i < nums1.size(); i++)
        {
            val = isPresent(nums2, nums1[i]);

            if(val != -1)
                return val;
            else
                continue;
        }    
        return -1;
    }
};

// appraoach 2: two pointers
class Solution {
public:
    int getCommon(vector<int>& nums1, vector<int>& nums2) 
    {
        int i = 0, j = 0;

        while(i < nums1.size() && j < nums2.size())
        {
            if(nums1[i] == nums2[j])
                return nums1[i];
            else if(nums1[i] < nums2[j])
                i++;
            else if(nums1[i] > nums2[j])
                j++;
        }    
        return -1;
    }
};