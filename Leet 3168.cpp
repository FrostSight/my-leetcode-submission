// Link: https://leetcode.com/problems/minimum-number-of-chairs-in-a-waiting-room/description/

#define ps_chair prefixSum_chair

class Solution {
public:
    int minimumChairs(string s) 
    {
        vector<int> ps_chair(s.size(), 0);
        ps_chair.insert(ps_chair.begin(), 1); // at least 1 chair

        for(int i = 1; i < s.size(); i++)
        {
            if(s[i] == 'E')
                ps_chair[i] = ps_chair[i-1] + 1; // if E, increments
            else
                ps_chair[i] = ps_chair[i-1] - 1; // if L, decrements
        }    

        auto ans = *max_element(ps_chair.begin(), ps_chair.end());

        return ans;
    }
};
