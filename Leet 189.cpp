// Link: https://leetcode.com/problems/rotate-array/description/

class Solution {
public:
    void rotate(vector<int>& nums, int k) 
    {
        int n = nums.size();
        vector <int> temp(n);

        for(int i = 0; i < n; i++)
            temp[(i+k) % n] = nums[i];  // putting the value in new moded (desired) position
        
        nums = temp; // copy temps array to nums
    }
};

/*
something mod n with result in range 0 to n-1
nums = [1,2,3,4,5,6,7], k = 3 => index of 7 is 6
otate 3 steps to the right: [5,6,7,1,2,3,4] => index of 7 is 2
*/
