// Link: https://leetcode.com/contest/weekly-contest-385/problems/find-the-length-of-the-longest-common-prefix/

// Link: https://leetcode.com/problems/find-the-length-of-the-longest-common-prefix/description/

class Solution {
public:
    set<string> findPrefix(vector<int>& arr) 
    {
        string str;
        set<string> pref;

        for (auto& a : arr) 
        {
            str = to_string(a);
            string pf = "";
            for (auto& ch : str) 
            {
                pf += ch;
                pref.insert(pf);
            }
        }
        return pref;
    }

    int longestCommonPrefix(vector<int>& arr1, vector<int>& arr2) 
    {
        set<string> s1, s2;
        int len = 0;

        s1 = findPrefix(arr1);
        s2 = findPrefix(arr2);

        for(auto& a : s1)
        {
            if(s2.count(a))
                len = max(len, (int)(a.size()));
        }

        return len;
    }
};
