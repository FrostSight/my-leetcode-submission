// Link: https://leetcode.com/problems/sort-colors/description/

class Solution {
public:
    void sortColors(vector<int>& nums) 
    {
        vector<int> freq(3, 0);
        int j = 0;

        for(int i = 0; i < nums.size(); i++)
            freq[nums[i]]++;

         for(int i = 0; i < freq.size(); i++)
        {
            int temp = freq[i];
            while(temp > 0)
            {
                nums[j] = i;
                temp--;
                j++;
            }
        }    
    }
};

