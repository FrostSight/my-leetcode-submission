// Link: https://leetcode.com/problems/intersection-of-two-arrays-ii/

class Solution {
public:
    vector<int> intersect(vector<int>& nums1, vector<int>& nums2) 
    {
        unordered_map<int, int> umap;
        vector<int> ans;

        for(int& n : nums1)
            umap[n]++;
        
        for(int& n : nums2)
        {
            auto it = umap.find(n);
            if(it != umap.end() && it->second > 0)  // if same element found frequency is decremented by 1
            {
                ans.push_back(n);
                it->second--;
            }
        }
        return ans;
    }
};