// Link: https://leetcode.com/problems/palindrome-number/description/

class Solution {
public:
    bool isPalindrome(int x) 
    {
        int lastdigit;
        lastdigit = x % 10;

        //corner case
        if (x == 0)
            return true;

        //another corner case
        if (x < 0 || lastdigit == 0)
            return false;
        
        int reverse = 0, remainder;

        while(x > reverse)
        {
            remainder = x % 10;
            reverse = reverse * 10 + remainder;
            x = x / 10;
        }

        // even case
        if(reverse == x || x == reverse / 10)
            return true;
        else
            return false;
    }
};
