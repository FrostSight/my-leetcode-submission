// Link: https://leetcode.com/problems/permutations-ii/description/

class Solution {
public:
    void solve(int n, unordered_map<int, int>& ump, vector<int>& temp, vector<vector<int>>& ans)
    {
        if(temp.size() == n)
        {
            ans.push_back(temp);
            return;
        }

        for(auto it = ump.begin(); it != ump.end(); it++)
        {
            if(it->second > 0)
            {
                temp.push_back(it->first);
                it->second--;
                solve(n, ump, temp, ans);
                temp.pop_back();
                it->second++;
            }
        }
    }

    vector<vector<int>> permuteUnique(vector<int>& nums) 
    {
        vector<int> temp;
        vector<vector<int>> ans;
        unordered_map<int, int> ump;

        for(int num : nums) // frequency for keeping track of repetitive elements
            ump[num]++;
        
        solve(nums.size(), ump, temp, ans);

        return ans;
    }
};
