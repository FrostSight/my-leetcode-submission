// Link: https://leetcode.com/problems/first-completely-painted-row-or-column/description/

class Solution {
public:
    int firstCompleteIndex(vector<int>& arr, vector<vector<int>>& mat) 
    {
        int row = mat.size(), col = mat[0].size(), ans;
        vector<int> countRow(row, 0), countCol(col, 0);
        unordered_map<int, vector<int>> ump;

        for(int i = 0; i < row; i++)
        {
            for(int j = 0; j < col; j++)
            {
                ump[mat[i][j]] = {i, j};
            }
        }

        for(int i = 0; i < arr.size(); i++)
        {
            vector<int> temp = ump[arr[i]];
            int roW = temp[0], coL = temp[1];
            
            countRow[roW]++;
            countCol[coL]++;

            if(countRow[roW] == col || countCol[coL] == row) // check rows column-wise and column row-wise
            {
                ans = i;
                break;
            }
        }

        return ans;
    }
};
