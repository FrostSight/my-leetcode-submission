// Link: https://leetcode.com/problems/make-sum-divisible-by-p/description/

class Solution {
public:
    int minSubarray(vector<int>& nums, int p) 
    {
        int n = nums.size(), cs_sum = 0;

        for(int &num : nums) 
        {
            cs_sum = (cs_sum + num) % p;
        }

        int target = cs_sum%p;

        if(target == 0) return 0;

        unordered_map<int, int> ump; 

        int current = 0;
        ump[0] = -1;

        int result = n;
        for(int j = 0; j < n; j++) 
        {
            current = (current + nums[j]) % p;

            int remain = (current - target + p) % p;
            if(ump.find(remain) != ump.end()) 
            {
                result = min(result, j - ump[remain]);
            }

            ump[current] = j;
        }

        return result == n ? -1 : result;

    }
};

