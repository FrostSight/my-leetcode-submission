// Link: https://leetcode.com/problems/find-pivot-index/description/

class Solution {
public:
    int pivotIndex(vector<int>& nums) 
    {
        int totalSum, leftSum = 0, rightSum = 0, flag = 0, a;

        totalSum =  accumulate(nums.begin(), nums.end(), 0);

        rightSum = totalSum - nums[0];
        
        // corner case or case 3
        if(rightSum == leftSum)
            return 0;

        for(int i = 1; i < nums.size(); i++)
        {
            leftSum = leftSum + nums[i - 1];
            rightSum = totalSum - (leftSum + nums[i]);           
            
            if(rightSum == leftSum)
            {        
                return i;
            }
        }
        return -1;
    }
};

