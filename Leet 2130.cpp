class Solution {
public:
    int pairSum(ListNode* head) 
    {
        stack<int> stk;
        ListNode* current = head;
        int n = 0, sum, maxSum = -1;

        while(current != NULL)  // not current->next != NULL => this damn condition gave error
        {
            stk.push(current->val);
            n++;  // getting size of link list
            current = current->next;
        }

        current = head;  // current again becomes the first node
        n = (n/2) + 1;  // half and incremented by 1

        while(n > 0)
        {
            sum = current->val + stk.top();
            maxSum = max(sum, maxSum);
            
            current = current->next;
            stk.pop();
            n--;
        }

        return maxSum;
    }
};
