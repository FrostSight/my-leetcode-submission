// Link: https://leetcode.com/problems/find-the-minimum-cost-array-permutation/description/

class Solution {
public:
    void solve(vector<int>& nums, int sums, vector<bool>& visited, vector<int>& temp, vector<int>& result, int& minSum)
    {
        if(minSum <= sums)   return;

        if(temp.size() == nums.size())
        {
            sums += abs(temp.back() - nums[temp[0]]);
            
            if(sums < minSum)
            {
                minSum = sums;
                result = temp;
            }
        }

        for(int i = 0; i < nums.size(); i++)
        {
            if(!visited[i])
            {
                visited[i] = true;
                temp.push_back(i);
                solve(nums, sums + abs(temp[temp.size() - 2] - nums[temp[temp.size() - 1]]), visited, temp, result, minSum);
                temp.pop_back();
                visited[i] = false;
            }
        }
    }

    vector<int> findPermutation(vector<int>& nums) 
    {
        vector<int> result, temp = {0};
        vector<bool> visited(nums.size(), false);
        visited[0] = true;
        int minSum = INT_MAX;

        solve(nums, 0, visited, temp, result, minSum);

        return result;
    }
};

