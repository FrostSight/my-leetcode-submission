// Link: https://leetcode.com/problems/flipping-an-image/

class Solution {
public:
    vector<vector<int>> flipAndInvertImage(vector<vector<int>>& image) 
    {
        int i, j, k = 0;

        for(i = 0; i < image.size(); i++)  // reverse
        {
            j = 0, k = image[i].size()-1;
            while(j < k)
            {
                swap(image[i][j], image[i][k]);
                j++;
                k--;
            }
        }     

        for(i = 0; i < image.size(); i++) // replace
        {
            for(j = 0; j < image[i].size(); j++)
            {
                if(image[i][j] == 1)
                    image[i][j] = 0;
                else
                    image[i][j] = 1;
            }
        }
        return image;
    }
};