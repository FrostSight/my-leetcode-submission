// Link: https://leetcode.com/problems/count-sub-islands/description/

class Solution {
public:

    bool solve(vector<vector<int>>& grid1, vector<vector<int>>& grid2, int i, int j)
    {
        if(i < 0 || i >= grid1.size() || j < 0 || j >= grid1[0].size()) 
            return true;
        
        if(grid2[i][j] != 1) 
            return true;

        grid2[i][j] = -1;  // marking visited

        bool found = (grid1[i][j] == 1) ? true : false; // if same cell contains 1 then true

        // doing AND found and solve is boolean and for false case output becomes false
        found = found & solve(grid1, grid2, i+1, j); 
        found = found & solve(grid1, grid2, i-1, j);
        found = found & solve(grid1, grid2, i, j+1);  
        found = found & solve(grid1, grid2, i, j-1); 

        return found;
    }

    int countSubIslands(vector<vector<int>>& grid1, vector<vector<int>>& grid2) 
    {
        int subIsland = 0;

        for(int i = 0; i < grid2.size(); i++)
        {
            for(int j = 0; j < grid2[0].size(); j++)
            {
                if(grid2[i][j] == 1 && solve(grid1, grid2, i, j))
                    subIsland++;
            }
        }

        return subIsland;
    }
};

