// Link: https://leetcode.com/problems/sum-of-digits-of-string-after-convert/description/

class Solution {
public:
    int getLucky(string s, int k) 
    {
        string num = "";

        for(char& ch : s)
        {
            int alpha_int = ch - 'a' + 1;  // we get for a = 1, b = 2, i = 9, z = 26
            num = num + to_string(alpha_int); // as we need to sum each character so cancate now later we separate each character and sum them up
        }    

        while(k--)
        {
            int sum = 0;
            
            for(char& ch : num)
                sum += ch - '0'; // converts single digit character to integer
            
            num = to_string(sum); // as num is declared as string and sum is a integer
        }

        int ans = stoi(num);
        return ans;
    }
};
