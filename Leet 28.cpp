// Link: https://leetcode.com/problems/find-the-index-of-the-first-occurrence-in-a-string/description/

class Solution {
public:
    int strStr(string haystack, string needle) 
    {
        if(haystack.size() < needle.size()) return -1; // corner case

        int ans = -1;

        for(int i = 0; i <= (haystack.size() - needle.size()); i++) // lesser and equal
        {
            int j = 0;
            while(j < needle.size() && haystack[i + j] == needle[j])
            {
                j++;

                if(j == needle.size()) // iteration of j comes to an end and first needle found
                {
                    return i;
                }
            }
        }

        return -1;
    }
};

/*
Failed approach
class Solution {
public:
    int strStr(string haystack, string needle) 
    {
        if(haystack.size() < needle.size()) return -1;

        int i = 0, j = 0, ans = -1;

        while(i < haystack.size())
        {
            if(haystack[i] == needle[j])
            {
                if(ans == -1) 
                    ans = i;

                j++;

                if(j ==  needle.size())
                    break;
            }
            else if(haystack[i] != needle[j])
            {
                j = 0;
                ans = -1;
            }
            
            i++;
        }

        return ans;
    }
};
*/