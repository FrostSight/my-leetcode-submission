// Link: https://leetcode.com/problems/maximum-number-of-achievable-transfer-requests/description/

class Solution {
public:
    int result = INT_MIN;

    void solve(int idx, int counter, vector<int>& total_request, int n, vector<vector<int>>& requests)
    {
        if(idx == requests.size()) // iteration reached to the end of the requests
        {
            // as per question the summation of all incoming and outgoing must be 0 to be valid
            bool allZero = true;
            for(int x : total_request)
            {
                if(x != 0) 
                {
                    allZero = false;
                    break;
                }
            }

            if(allZero == true)
                result = max(result, counter);
            
            return;
        }

        int from = requests[idx][0];
        int to = requests[idx][1];

        // accepting transfer request
        total_request[from]--;
        total_request[to]++;
        solve(idx+1, counter+1, total_request, n, requests); 
        // rejecting transfer request
        total_request[from]++;
        total_request[to]--;
        solve(idx+1, counter, total_request, n, requests);

    }

    int maximumRequests(int n, vector<vector<int>>& requests) 
    {
        vector<int> total_request(n, 0);

        solve(0, 0, total_request, n, requests);

        return result;    
    }
};
