// Link: https://leetcode.com/problems/delete-characters-to-make-fancy-string/description/

class Solution {
public:
    string makeFancyString(string s) 
    {
        string ans = "";
        int counter = 0;

        for(int i = 0; i < s.size(); i++)
        {
            if (i > 0 && s[i-1] == s[i]) 
                counter++;
            else 
                counter = 0;

            if (counter < 2) 
                ans += s[i];
        }

        return ans;
    }
};

/*
failed approach 1:
class Solution {
public:
    string makeFancyString(string s) 
    {
        string ans = "";
        int counter = 0;

        for(int i = s.size()-1; i >= 0; i--)
        {
            if(i-1 > -1 && s[i-1] != s[i])
            {
                counter = 0;
                ans = s[i] + ans;
            }
            else if(i-1 > -1 && s[i-1] == s[i])
            {
                counter++;
                ans = s[i] + ans;
                if(counter ==  2)
                {
                    counter = 0;
                    i--;
                }               
            }
            else if(i == 0)
                ans = s[i] + ans;
        }

        return ans;
    }
};
vecdict : failed in some cases


failed apparoch 2:
class Solution {
public:
    string makeFancyString(string s) 
    {
        string ans = "";
        int counter = 0;

        for(int i = s.size()-1; i >= 0; i--)
        {
            if (i > 0 && s[i-1] == s[i]) 
                counter++;
            else 
                counter = 0;

            if (counter < 2) 
                ans = s[i] + ans;  // it caused MLE as 

            
            // if we are traversing right to left
            // then write this way
            // if (counter < 2) {
            //     ans.push_back(s[i]);
            // }
            // then out of loop do reverse
            // reverse(ans.begin(), ans.end());
            
        }

        return ans;
    }
};

veddict : memory limit exceed
*/