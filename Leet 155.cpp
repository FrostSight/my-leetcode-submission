// Link: https://leetcode.com/problems/min-stack/description/

#define ll long long

class MinStack {
public:
    stack<ll> stk;
    ll minVal;

    MinStack() {}

    void push(int val) 
    {
        ll val2 = (ll)val;
        if (stk.empty()) 
        {
            minVal = val2;
            stk.push(val2);
        } 
        else 
        {
            if (val2 < minVal) 
            {
                ll newVal = (2 * val2 * 1ll) - minVal;
                stk.push(newVal);
                minVal = val2;
            } 
            else 
                stk.push(val2);
        }
    }

    void pop() 
    {
        if (!stk.empty()) 
        {
            ll x = stk.top();
            stk.pop();
            if (x < minVal) 
                minVal = 2 * minVal - x;
        }
    }

    int top() 
    {
        if(stk.empty()) return -1;

        ll x = stk.top();
        return minVal < x ? x : minVal;
    }

    int getMin() 
    {
        return minVal;
    }
};

