// Link: https://leetcode.com/problems/di-string-match/description/

// way 1: 
class Solution {
public:
    vector<int> diStringMatch(string s) 
    {
        int high = s.size(), low = 0;
        vector<int> ans;

        for(int i = 0; i < s.size(); i++)
        {
            if(s[i] == 'I')
            {
                ans.push_back(low);
                low++;
            }
            else
            {
                ans.push_back(high);
                high--;
            }
        }

        if(s[s.size()-1] == 'I')
            ans.push_back(low);
        else
            ans.push_back(high);

        return ans;
    }
};

// way 2:
class Solution {
public:
    vector<int> diStringMatch(string s) 
    {
        int high = s.size(), low = 0;
        vector<int> ans;

        for(int i = 0; i < s.size(); i++)
        {
            if(s[i] == 'I')
            {
                ans.push_back(low);
                low++;
            }
            else
            {
                ans.push_back(high);
                high--;
            }
        }

        ans.push_back(low);

        return ans;
    }
};

