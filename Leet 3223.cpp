// Link: https://leetcode.com/problems/minimum-length-of-string-after-operations/description/

class Solution {
public:
    int minimumLength(string s) 
    {
        vector<char> freq(26, 0);

        for(char& ch : s)
        {
            int x = ch - 'a'; 
            freq[x]++;

            if(freq[x] == 3)
                freq[x] = 1;        
        }

        int counter = accumulate(freq.begin(), freq.end(), 0);
        
        return counter;
    }
};

/*
Find where the frequency of a character is 3. If a character has a frequency at least 3 means 2 can remove 2 character.
*/
