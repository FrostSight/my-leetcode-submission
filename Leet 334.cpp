// Link : https://leetcode.com/problems/increasing-triplet-subsequence/description/

class Solution {
public:
    bool increasingTriplet(vector<int>& nums) 
    {
        int left = INT_MAX, mid = INT_MAX;

        for(int i = 0; i < nums.size(); i++)
        {
            if(nums[i] > mid)
                return true;
            if(left > nums[i])
                left = nums[i];
            else if(left < nums[i] && nums[i] < mid)
                mid = nums[i];
        }   
        return false; 
    }
};
