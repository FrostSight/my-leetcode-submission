// Link: https://leetcode.com/problems/count-number-of-nice-subarrays/description/

class Solution {
public:
    int numberOfSubarrays(vector<int>& nums, int k) 
    {
        int oddCounter = 0, previousCounter = 0, nice = 0, i = 0, j= 0;

        while(j < nums.size())
        {
            if(nums[j] % 2 != 0)  // odd found
            {
                oddCounter++;
                previousCounter = 0;
            }

            while(oddCounter == k)
            {
                previousCounter++;

                if(nums[i] % 2 != 0) // odd found and shrinking
                    oddCounter--;
                
                i++;
            }

            nice += previousCounter;
            j++;
        }

        return nice;
    }
};

/*
j keeps moving until k = 0 or oddCounter is less than k
i keeps moving as long as k = 0 or oddCounter = k
*/