// Link: https://leetcode.com/problems/sum-root-to-leaf-numbers/description/

class Solution {
public:

    int getSum(TreeNode* root, int current)
    {
        if(root == NULL)
            return 0;

        current = (current * 10) + root->val; // current*10 + previous number

        if(root->left == NULL && root->right == NULL)  // leaf node
            return current;

        int lSum = getSum(root->left, current);
        int rSum = getSum(root->right, current);

        return lSum + rSum;
    }

    int sumNumbers(TreeNode* root) 
    {
        int sum = getSum(root, 0);
        return sum;    
    }
};