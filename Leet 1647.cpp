// Link: https://leetcode.com/problems/minimum-deletions-to-make-character-frequencies-unique/description/

class Solution {
public:
    int minDeletions(string s) 
    {
        vector<int> frequency(26);
        int op = 0;
        set<int> st;

        for (char ch : s) 
            frequency[ch - 'a']++;

        for(int i = 0; i < frequency.size(); i++)
        {
          if(st.find(frequency[i]) != st.end())
          {
            if(frequency[i] == 0)
              continue;
            else
            {
                frequency[i] = frequency[i] - 1;
                op++;
                i--;
            }
          }
          else
            st.insert(frequency[i]);
        }

        return op;    
    }
};