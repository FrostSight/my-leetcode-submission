// Link: https://leetcode.com/problems/check-if-a-word-occurs-as-a-prefix-of-any-word-in-a-sentence/description/

class Solution {
public:
    int isPrefixOfWord(string sentence, string searchWord) 
    {
        stringstream ss(sentence);
        string word = "";
        int idx = 1;

        while(getline(ss, word, ' '))
        {
            if(word.find(searchWord) == 0)
                return idx;
            
            idx++;
        }

        return -1;
    }
};
