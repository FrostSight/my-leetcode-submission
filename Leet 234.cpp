// Link: https://leetcode.com/problems/palindrome-linked-list/

class Solution {
public:

    ListNode* reverseLinkList(ListNode* head) 
    {
        if(!head || !head->next)
            return head;
        
        ListNode* tail = reverseLinkList(head->next);
        head->next->next = head;
        head->next = NULL;
        
        return tail;
    }

    ListNode* halfCut(ListNode* head)
    {
        ListNode* slow = head;
        ListNode* fast = head;
        ListNode* previous = NULL;

        while(fast && fast->next) 
        {
            fast = fast->next->next;
            previous = slow;
            slow = slow->next;
        }

        previous->next = NULL;  // used for 2NULL

        return slow;
    }

    bool isPalindrome(ListNode* head) 
    {
        if(!head || !head->next)  // base condition
            return true;
     
        ListNode* mid = halfCut(head); // cut it into half and find the mid
        
        ListNode* reverse_head = reverseLinkList(mid);  // reverse the rest
        
        
        while(reverse_head != NULL && head != NULL) 
        {
            if(reverse_head->val != head->val)  // if any unmatched found then not a palindrome
                return false;

            head = head->next;
            reverse_head = reverse_head->next;
        }

        return true;
    }
};


/*
Explanation
suppose *head -> 1_ -> 2_ -> 3_ ->4NULL
after halfCut *head -> 1_ 2NULL and *reverse_head -> 4_ -> 3NULL
*/
