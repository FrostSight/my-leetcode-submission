// Link: https://leetcode.com/problems/linked-list-cycle/description/

class Solution {
public:
    bool hasCycle(ListNode *head) 
    {
        ListNode* slow = head;    
        ListNode* fast = head;
        
        while (slow != NULL && fast != NULL && fast->next != NULL)
        {
            fast = fast->next;
            fast = fast->next;
            slow = slow->next;

            if (fast == slow)
                return true;
        }
        return false;
    }
};