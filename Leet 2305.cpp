// Link: https://leetcode.com/problems/fair-distribution-of-cookies/description/

class Solution {
public:
    int result = INT_MAX;

    void solve(vector<int>& cookies, vector<int>& children, int idx)
    {
        if(idx >= cookies.size())
        {
            int unfair = *max_element(children.begin(), children.end());
            result = min(result, unfair);
            
            return;
        }

        for(int i = 0; i < children.size(); i++)
        {
            children[i] += cookies[idx]; // explore
            solve(cookies, children, idx+1);
            children[i] -= cookies[idx];  // undo explore
        }
    }

    int distributeCookies(vector<int>& cookies, int k) 
    {
        vector<int> children(k, 0);    
        solve(cookies, children, 0);

        return result;
    }
};

