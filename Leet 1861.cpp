// Link: https://leetcode.com/problems/rotating-the-box/description/

class Solution {
public:
    vector<vector<char>> rotateTheBox(vector<vector<char>>& box) 
    {
        int row = box.size(), col = box[0].size();
        vector<vector<char>> rotatedBox(col, vector<char>(row));

        // first transpose than reverse eacg column, make a 90 degree rotation
        for(int i = 0; i < col; i++) 
            for(int j = 0; j < row; j++) 
                rotatedBox[i][j] = box[j][i];
            

        for(auto& currentRow : rotatedBox)
            reverse(currentRow.begin(), currentRow.end());
        

        for(int j = 0; j < row; j++) 
        {
            int emptyCell  = col-1;
            for(int i = col-1; i >= 0; i--) 
            {
                if(rotatedBox[i][j] == '*') // obstacle found
                    emptyCell  = i-1;

                else if(rotatedBox[i][j] == '#') 
                {
                    rotatedBox[i][j] = '.'; // current cell(stone cell) becomes empty
                    rotatedBox[emptyCell][j] = '#'; // empty becomes stone cell
                    emptyCell --; // the upper cell get empty now
                }
            }
        }

        return rotatedBox;
    }
};
