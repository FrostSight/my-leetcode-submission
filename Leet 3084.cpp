// Link: https://leetcode.com/problems/count-substrings-starting-and-ending-with-given-character/description/

using ll = long long;

class Solution {
public:
    long long countSubstrings(string s, char c) 
    {
        ll counter = 0, ans = 0;
        
        for(auto& ch : s)
        {
            if(ch == c)
            {
                counter++;
                ans += counter;         
            }
        }
        
        return ans;
    }
};