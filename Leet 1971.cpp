// Link: https://leetcode.com/problems/find-if-path-exists-in-graph/

class Solution {
public:
    
    bool check(unordered_map<int, vector<int>>& ump, int node, int destiny, vector<bool>& visiting) {
        if(node == destiny)
            return true;
        
        if(visiting[node] == true)
            return false;
        
        visiting[node] = true;
        for(auto &it : ump[node]) 
        {
            if(check(ump, it, destiny, visiting))
                return true;
        }

        return false;
    }
    
    bool validPath(int n, vector<vector<int>>& edges, int source, int destination) 
    {        
        if(source == destination)
            return true;
        
        unordered_map<int, vector<int>> ump;
        
        for(vector<int> &vec : edges) 
        {
            int u = vec[0];
            int v = vec[1];
            
            ump[u].push_back(v);
            ump[v].push_back(u);
        }
        
        vector<bool> visiting(n, false);
        bool isVisited = check(ump, source, destination, visiting);

        return isVisited == true ? true : false;
    }
};
