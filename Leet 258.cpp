// Link: https://leetcode.com/problems/add-digits/description/

class Solution {
public:
    int sum = 0;
    
    int getCountDigits(int num) 
    {
        sum = 0;
        
        int counter = 0;
        while(num != 0) 
        {
            int x = num % 10;
            sum += x;
            num /= 10;
            counter++;
        }
        
        return counter;
        
    }
    
    int addDigits(int num) 
    {
        
        while(getCountDigits(num) > 1) 
        {
            num = sum;
        }
        
        return sum;
        
    }
};