// Link: https://leetcode.com/problems/subarrays-with-k-different-integers/description/

class Solution {
public:
    
    int slidingWindow(vector<int>& nums, int k) 
    {
        unordered_map<int, int> umap;
        int i = 0, j = 0, counter = 0;
        
        while(j < nums.size()) 
        {            
            umap[nums[j]]++;
            
            while(umap.size() > k) 
            {
                umap[nums[i]]--;
                if(umap[nums[i]] == 0)
                    umap.erase(nums[i]);

                i++;
            }
            
            counter += j-i+1;
            j++;
        }
        
        return counter;
    }
    
    int subarraysWithKDistinct(vector<int>& nums, int k) 
    {
        int total = slidingWindow(nums, k);
        int toCut = slidingWindow(nums, k-1);
        int remain = total - toCut;

        return remain;
    }
};