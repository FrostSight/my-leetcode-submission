// Link: https://leetcode.com/problems/subarray-product-less-than-k/

class Solution {
public:
    int numSubarrayProductLessThanK(vector<int>& nums, int k) 
    {
        int i = 0, j = 0, counter = 0, product = 1;

        if(k <= 1)  // corner case
            return 0;

        while(j < nums.size())
        {
            product *= nums[j];

            while(product >= k)
            {
                product /= nums[i];
                i++;
            }

            counter += (j+1 - i);
            j++;
        }    

        return counter;
    }
};
