// Link: https://leetcode.com/problems/remove-sub-folders-from-the-filesystem/description/

class Solution {
public:
    vector<string> removeSubfolders(vector<string>& folder) 
    {
        unordered_set<string> st(folder.begin(), folder.end());
        vector<string> result;

        for(string currentFolder : folder)
        {
            bool isSubFolder = false;
            string tempFolder = currentFolder; // currentFolder will be altered so keeping the backup
            // suppose currentFolder = ""/a/b/c/d""

            while(!currentFolder.empty())  // checking through "/a/b/c/d", "/a/b/c", "/a/b", "/a", "" -> how checking is done 
            {
                int position = currentFolder.find_last_of('/');
                currentFolder = currentFolder.substr(0, position); // altering 

                if(st.find(currentFolder) != st.end()) // similar folder found
                {
                    isSubFolder = true;
                    break;
                }
            }
            
            if(isSubFolder == false) // similar folder not found
                result.push_back(tempFolder);
        }

        return result;
    }
};

/*
test case example
"let folder ="/a", "/a/b", "/a/b/c"
"/a/b/c" is subfolder of "/a/b"
"/a/b" is subfolder "/a"
*/