// Link: https://leetcode.com/problems/find-the-duplicate-number/description/

class Solution {
public:
    int findDuplicate(vector<int>& nums) 
    {
        int slow, fast;

        slow = nums[0];  // at first both initializes from same position
        fast = nums[0];

		// now we move them towards else the while loop will terminate before it begins
        slow = nums[slow];  // 1 step forward
        fast = nums[nums[fast]];  // 2 step forward

		// to detect cycle
        while(slow != fast) // their meeting point will be the proof presence of cycle 
        {
            slow = nums[slow];
            fast = nums[nums[fast]];
        }     

        slow = nums[0];  // cycle found, slow moves to the first and fast remains at its last position

        while(slow != fast) // now their meeting point will be the starting of cycle and and it is duplicate value here
        {
            slow = nums[slow];  // both moves 1 step
            fast = nums[fast];
        }

        return slow; // or return fast as their meeting point is the same
    }
};
