// Link: https://leetcode.com/problems/uncommon-words-from-two-sentences/description/

class Solution {
public:
    vector<string> uncommonFromSentences(string s1, string s2) 
    {
        vector<string> result;
        unordered_map<string, int> ump;
        string word = "";

        stringstream ss1(s1);
        while(ss1 >> word)
            ump[word]++;
        
        stringstream ss2(s2);
        while(ss2 >> word)
            ump[word]++;
        
        for(auto it = ump.begin(); it != ump.end(); it++)
        {
            if(it->second == 1)
                result.push_back(it->first);
        }

        return result;
    }
};
