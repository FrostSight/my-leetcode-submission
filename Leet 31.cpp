// Link: https://leetcode.com/problems/next-permutation/description/

class Solution {
public:
    void nextPermutation(vector<int>& nums) 
    {
        int underLine_index = -1, swap_index = -1;

        for(int i = nums.size()-1; i > 0; i--)
        {
            if(nums[i-1] < nums[i])
            {
                underLine_index = i-1;
                break;
            }
        }    

        if(underLine_index != -1)
        {
            for(int i = nums.size()-1; i >= underLine_index+1; i--)
            {
                if(nums[i] > nums[underLine_index])
                {
                    swap_index = i;
                    break;
                }
            }
        
            swap(nums[underLine_index], nums[swap_index]);
        }

        reverse(nums.begin() + underLine_index + 1, nums.end());
    }
};