// Link: https://leetcode.com/problems/minimize-the-maximum-difference-of-pairs/

class Solution {
public:

    bool check(vector<int>& nums, int mid, int p)
    {
        int  i = 0, pair_count = 0;

        while(i < nums.size()-1)
        {
            if(nums[i+1] - nums[i] <= mid)  // as already sorted the next value is greater than the previous one  
            {
                pair_count++;
                i = i + 2;  // as pair found so the immediate value is not considered
            }
            else
                i++;
        }
        
        if(pair_count >= p)  
            return true;

        return false;
    }

    int minimizeMax(vector<int>& nums, int p) 
    {
        int low, high, mid, ans;

        sort(nums.begin(), nums.end());

        low = 0;  // minimum difference is 0
        high = nums[nums.size()-1] - nums[0];  // maximum difference is subtraction of first and last value

        while(low <= high)
        {
            mid = low + (high - low) / 2;

            if(check(nums, mid, p))
            {
                ans = mid;
                high = mid - 1;   // possible answer is found now look for minimum
            }
            else
                low = mid + 1;
        }
        return ans;
    }
};