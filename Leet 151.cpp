// Link: https://leetcode.com/problems/reverse-words-in-a-string/description/

class Solution {
public:
    string reverseWords(string s) 
    {
        int i = 0, left = 0, right = 0;

        reverse(s.begin(), s.end());

        while(i < s.size())
        {
          while(i < s.size() && s[i] != ' ')
          {
            s[right] = s[i];
            i++;
            right++;
          }

          if(left < right)
          {
            reverse(s.begin() + left, s.begin() + right);
            s[right] = ' ';
            right++;
            left = right;
          }

          i++;
        }

        s = s.substr(0, right-1);

        return s;    
    }
};