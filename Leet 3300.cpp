// Link: https://leetcode.com/problems/minimum-element-after-replacement-with-digit-sum/description/

class Solution {
public:

    int getSumDigit(int n)
    {
        int sum = 0;

        while(n != 0)
        {
            int remainder = n % 10;
            sum += remainder;
            n /= 10;
        }

        return sum;
    }

    int minElement(vector<int>& nums) 
    {
        int min_element = INT_MAX;

        for(int i : nums)
        {
            min_element = min(min_element, getSumDigit(i));
        }

        return min_element;
    }
};
