// Link: https://leetcode.com/problems/add-two-numbers-ii/description/

class Solution {
public:
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) 
    {
        stack<int> stk1, stk2;
        int sum = 0, carry = 0;
        ListNode* ans = new ListNode();

        while(l1!= NULL)
        {
            stk1.push(l1->val);
            l1 = l1->next;
        }    

        while(l2 != NULL)
        {
            stk2.push(l2->val);
            l2 = l2->next;
        }    

        while(!stk1.empty() || !stk2.empty())  // if both stacks are running. Anyone becomes empty loop breaks. As: l1 = [7,2,4,3], l2 = [5,6,4], here stk2 will become empty a step ahead 
        {
            if(!stk1.empty())
            {
                sum += stk1.top();
                stk1.pop();
            }

            if(!stk2.empty())
            {
                sum += stk2.top();
                stk2.pop();
            }

            ans->val = sum % 10;
            carry = sum / 10;

            ListNode* prevNode = new ListNode(carry); // prevNode carries carry 
            prevNode->next = ans; // and it sits before ans;

            ans = prevNode; // update: ans becomes the node that was its previous
            sum = carry;  // and a new prevNode that sits before ans and it carries sum as carry
        }

        return carry == 0 ? ans->next: ans;
    }
};
