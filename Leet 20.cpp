// Link: https://leetcode.com/problems/valid-parentheses/

class Solution {
public:
    bool isValid(string s) 
    {
        stack<char> st;

        for(auto& ch : s)
        {
            if(ch == '(' || ch == '{' || ch == '[')
                st.push(ch);           
            else if(!st.empty())  // suppose s = "}]"  here nothing will be push to stack and stack is empty. So 
            {
                if((ch == ')' && st.top() == '(') || (ch == '}' && st.top() == '{') || (ch == ']' && st.top() == '['))
                    st.pop();
                else
                    return false;  // order does not match
            }
            else
                return false; // suppose s = "}]"  here nothing will be push to stack and stack is empty. So stack is empty
        }

        return st.empty() ? true : false;
    }
};