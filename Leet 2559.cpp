// Link: https://leetcode.com/problems/count-vowel-strings-in-ranges/description/

class Solution {
public:
    vector<int> vowelStrings(vector<string>& words, vector<vector<int>>& queries) 
    {
        vector<int> result(queries.size()), ps_words(words.size());
        int sum = 0;
        unordered_set<char> ust;

        ust.insert('a');
        ust.insert('e');
        ust.insert('i');
        ust.insert('o');
        ust.insert('u');

        for(int i = 0; i < words.size(); i++)
        { 
            string word = words[i];
            int left = 0, right = word.size()-1;

            if(ust.find(word[left]) != ust.end() && ust.find(word[right]) != ust.end())
                sum++;

            ps_words[i] = sum;
        }

        for(int i = 0; i < queries.size(); i++) 
        { 
            int left = queries[i][0];
            int right = queries[i][1];

            result[i] = ps_words[right] - ( (left > 0) ? ps_words[left - 1] : 0 );
        }

        return result;
    }
};

/*
TLE approaches

class Solution {
public:
    vector<int> vowelStrings(vector<string>& words, vector<vector<int>>& queries) 
    {
        vector<int> ans;
        unordered_set<char> ust;
        ust.insert('a');
        ust.insert('e');
        ust.insert('i');
        ust.insert('o');
        ust.insert('u');

        for(auto& query : queries)
        {
            int start = query[0];
            int end = query[1];
            int counter = 0;

            for(int i = start; i <= end; i++)
            {
                string word = words[i];
                int left = 0, right = word.size()-1;

                if(ust.find(word[left]) != ust.end() && ust.find(word[right]) != ust.end())
                    counter++;
            }

            ans.push_back(counter);
        }   

        return ans;
    }
};

class Solution {
public:
    vector<int> vowelStrings(vector<string>& words, vector<vector<int>>& queries) 
    {
        vector<int> ans;
        unordered_set<char> ust;
        unordered_set<string> vowelConteiner;


        ust.insert('a');
        ust.insert('e');
        ust.insert('i');
        ust.insert('o');
        ust.insert('u');

        for(auto& word : words)
        {   
            int left = 0, right = word.size()-1;
            if(ust.find(word[left]) != ust.end() && ust.find(word[right]) != ust.end())
                vowelConteiner.insert(word);
        }

        for(auto& query : queries)
        {
            int start = query[0];
            int end = query[1];
            int counter = 0;

            for(int i = start; i <= end; i++)
            {
                string word = words[i];

                if(vowelConteiner.find(word) != vowelConteiner.end())
                    counter++;
            }

            ans.push_back(counter);
        }   

        return ans;
    }
};


*/