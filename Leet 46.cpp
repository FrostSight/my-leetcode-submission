// Link: https://leetcode.com/problems/permutations/description/

class Solution {
public: 
    unordered_set<int> ust;
    
    void solve(vector<int>& nums, vector<int>& temp, vector<vector<int>>& result) 
    {
        if(temp.size() == nums.size()) 
        {
            result.push_back(temp);
            return;
        }
        
        for(int i = 0; i < nums.size(); i++) // for each recursion call, starts from the beginning
        {
            if(ust.find(nums[i]) == ust.end()) // it not exist
            {
                temp.push_back(nums[i]);
                ust.insert(nums[i]);
                solve(nums, temp, result);
                ust.erase(nums[i]);
                temp.pop_back();
            }
        }
        
    }
    
    vector<vector<int>> permute(vector<int>& nums) 
    {
        vector<vector<int>> result;
        vector<int> temp;
        
        solve(nums, temp, result);
        
        return result;
    }
};
