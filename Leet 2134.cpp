// Link: https://leetcode.com/problems/minimum-swaps-to-group-all-1s-together-ii/description/

class Solution {
public:
    int minSwaps(vector<int>& nums) 
    {
        int n = nums.size(), i = 0, j = 0, maxCount = 0, count1 = 0, totalOnes;
        vector<int> nums2(2*n, 0);

        totalOnes = accumulate(nums.begin(), nums.end(), 0);

        for(int i = 0; i < nums2.size(); i++) // as circular so copying it 
            nums2[i] = nums[i % n];    
        
        while(j < nums2.size())
        {
            if(nums2[j] == 1)
                count1++;
            
            while(j+1 - i > totalOnes)
            {
                if(nums2[i] == 1)
                    count1--;

                i++;
            }

            maxCount = max(maxCount, count1);

            j++;
        }

        int minSwap = totalOnes - maxCount;
        return minSwap;
    }
};
