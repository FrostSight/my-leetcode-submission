// Link https://leetcode.com/problems/find-the-pivot-integer/description/

class Solution {
public:

    int naturalSum(int i)
    {
      return (i * (i+1)) / 2;
    }

    int pivotInteger(int n) 
    {
        int sum = 0, a, b = -1;
    
        for(int i = n; i >= 1; i--)
        {
          sum += i;

          a = naturalSum(i);

          if(sum == a)
          {
            b = i;
            break;
          }
        }
        
        return b;
    }
};