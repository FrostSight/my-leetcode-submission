// Link: https://leetcode.com/problems/flip-columns-for-maximum-number-of-equal-rows/description/

class Solution {
public:
    int maxEqualRowsAfterFlips(vector<vector<int>>& matrix) 
    {
        int maxRows = 0;

        for(auto& currentRow : matrix) // taking each row
        {
            vector<int> invertRow(currentRow.size());

            for(int col = 0; col < currentRow.size(); col++) // making an inverted array of the taken row
            {
                invertRow[col] = currentRow[col] == 0 ? 1 : 0;
            }

            // here comes te actual logic
            int counter = 0;
            for(auto& row : matrix) // for each row, we are checking every row(including selected row) of the matrix
            {
                if(row == currentRow || row == invertRow) // if we find same row || inverted row
                    counter++;
            }

            maxRows = max(maxRows, counter);
        }    

        return maxRows;
    }
};

/*
Problem description: I have a matrix. I can flip (o to 1 and vice versa) the values of any number of columns (0 to n). After this operation how many rows are there where the value of each column within that row is same?
let A = 
1 0 1
0 0 1
1 0 1

let B = 
1 0 1
0 0 0
0 1 0

for A matrix output is 0 and for B matrix is 1 (in the middle row, values of each column are identical)

key point:
we can perform this operation only in 2 cases where multiple row are same or inverted. So we need to count how many rows are same or inverted. That is the answer.

Explanation:
In A, 1st and 3rd row are same, so the middle column can be flipped. AAns = 
1 1 1
0 1 1
1 1 1
output : 2

In B, 1st and 3rd row are inverted, so the middle column can be flipped. BAns = 
1 1 1
0 1 1
0 0 0
output: 2
*/