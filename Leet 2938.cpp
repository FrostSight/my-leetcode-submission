// Link: https://leetcode.com/problems/separate-black-and-white-balls/

#define ll long long

class Solution {
public:
    long long minimumSteps(string s) 
    {
        ll counter = 0, total_swap = 0;
        int i = 0;
        
        while(i < s.size())
        {
            if(s[i] == '1')
                counter++;
            else if(s[i] == '0')
                total_swap += counter;

            i++;
        }
        
        return total_swap;
    }
};

/*
The idea is to count 1s before every 0s
*/