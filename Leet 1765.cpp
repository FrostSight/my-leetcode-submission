// Link: https://leetcode.com/problems/map-of-highest-peak/description/

class Solution {
public:
    vector<vector<int>> directions { {0, 1}, {0, -1}, {1, 0}, {-1, 0} };

    vector<vector<int>> highestPeak(vector<vector<int>>& isWater) 
    {
        vector<vector<int>> mat(isWater.size(), vector<int> (isWater[0].size(), -1)); // this is the main working matrix
        queue<pair<int, int>> qu;
        int row = isWater.size(), col = isWater[0].size();

        for(int i = 0; i < row; i++)
        {
            for(int j = 0; j < col; j++)
            {
                if(isWater[i][j] == 1)
                {
                    mat[i][j] = 0; // fulfilling main matrix
                    qu.push( {i, j} ); // BFS will start here and it is a multi source BFS
                }
            }
        }

        while(!qu.empty())
        {
            int n = qu.size();
            while(n--)
            {
                pair<int, int> pr = qu.front();
                qu.pop();

                int i = pr.first; 
                int j = pr.second;

                for(vector<int>& direction : directions)
                {
                    int new_i = i + direction[0];
                    int new_j = j + direction[1];

                    if(new_i >= 0 && new_i < row && new_j >= 0 && new_j < col && mat[new_i][new_j] == -1) // checking boudary and not seen
                    {
                        mat[new_i][new_j] = mat[i][j] + 1;
                        qu.push( {new_i, new_j} );
                    }
                }
            }
        }

        return mat;
    }
};
