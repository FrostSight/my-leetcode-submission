// Link: https://leetcode.com/problems/simplify-path/description/

class Solution {
public:
    string simplifyPath(string path) 
    {
        string token = "", ans = "";       
        stringstream ss(path);
        stack<string> stk;
        
        while(getline(ss, token, '/')) // tokenization, after tokenization '/' will be replaced by "" and left with string, "."
        {
            if(token == "" || token == ".") // empty string (base condition) or 
                continue;           
            else if (token != "..") // string found and pushed to stack
                stk.push(token);
            else if (!stk.empty()) // doube dot found so pop and not empty is mendatory condition popping
                stk.pop();

            /*
            // gives wrong answer
            if(token == "." || token == "") 
                continue;            
            else if (token == ".." && !stk.empty()) 
                stk.pop();
            else 
                stk.push(token);
            */
        }

        if(stk.empty())  // corner case, after tokenization nothing is found to be pushed to stack thus no string will be remade
            return "/";
        
        while(!stk.empty())
        {
            ans = "/" + stk.top() + ans;
            stk.pop();
        }
        
        return ans;
    }
};