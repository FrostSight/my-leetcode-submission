// Link: https://leetcode.com/problems/longest-subarray-with-maximum-bitwise-and/https://leetcode.com/problems/longest-subarray-with-maximum-bitwise-and/

class Solution {
public:
    int longestSubarray(vector<int>& nums) 
    {
        int maxVal = 0, result = 0, streak = 0;

        for(int& num : nums)
        {
            if(num > maxVal)
            {
                maxVal = num;
                result = 0;
                streak = 0;
            }

            if(maxVal == num)
                streak++;
            else
                streak = 0;
            
            result = max(result, streak);
        }

        return result;
    }
};


/*
AND has a property. If AND is performed between X and Y (let x<y), the resulted value wll never exceed both X and Y. Here the resulted value will be either less than both X and Y or at max X.

nums = [1,2,3,4]
How do we find the max?
Ans: for 1 AND 2, max result will not be more than 1
   for 1 AND 2 AND 3 max result will not be more than 1
so we need to maximize the minimum value
  for 2 AND 3, max result will not be more than 2
So we can say only 4 can give maximum result. Performing AND with any value less than 4 will reduce the maximum result

=> we need to find the maximum value and a consecutive serise of maximum values
that is the solution.
*/