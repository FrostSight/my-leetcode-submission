// Link: https://leetcode.com/problems/divide-players-into-teams-of-equal-skill/description/

#define ll long long

class Solution {
public:
    long long dividePlayers(vector<int>& skill) 
    {
        sort(skill.begin(), skill.end());  // as pairs are made with the smallest and greatest elements
        ll result = 0;

        int i = 0, j = skill.size()-1;
        int initial_sum = skill[i] + skill[j]; // all sum must be equal to initial sum ref. by (3)

        while(i < j)
        {
            int currentSum = skill[i] + skill[j];

            if(currentSum != initial_sum) // if any sum failed to match initial sum return -1
                return -1;
            
            result += (ll)skill[i] * (ll)skill[j];  // else ref.(4) happens

            i++;
            j--;
        }    

        return result;
    }
};

/*
Question explanation
1. make teams of 2 elements
2. total n/2 elements
3. sum of every elements must be equal i.e (1, 5) => 6, (2, 4) => 6, (3, 3) => 6
4. return the sum of products of every elements i.e. (1 * 5) => 5 + (4 * 2) => 8 + (3 * 3) => 9 = 22
*/