// Link: https://leetcode.com/problems/maximum-number-of-integers-to-choose-from-a-range-i/description/

class Solution {
public:
    int maxCount(vector<int>& banned, int n, int maxSum) 
    {
        unordered_set<int> ust(banned.begin(), banned.end());
        int counter = 0, sums = 0;

        for(int i = 1; i <= n; i++)
        {
            if(ust.find(i) != ust.end())
                continue;
            else if(sums + i <= maxSum)
            {
                sums += i;
                counter++;
            }
            else
                break;
        }

        return counter;
    }
};

