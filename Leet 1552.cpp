// Link: https://leetcode.com/problems/magnetic-force-between-two-balls/description/

class Solution {
public:
    typedef long long ll;

    bool force(vector<int>& position, ll mid, int m)
    {
        int current_ball = position[0], ball_placed = 1;  // current ball is set on position [0]

        for(int i = 1; i < position.size(); i++)  // comparing starts from 1 
        {
            if(abs(current_ball - position[i]) >= mid)
            {    ball_placed++;
            
                if(ball_placed == m) // if all balls are already placed
                    return true;

                current_ball = position[i]; // (next ball updated) suppose 3 balls now current ball is ball number 2 and comparison takes place between ball 2 and 3.
            }
        }
        return false;
    }

    int maxDistance(vector<int>& position, int m) 
    {
        ll low, high, mid, ans;

        sort(position.begin(), position.end());

        low =  1; 
        high = position[position.size()-1]; // last value is the maximum

        while(low <= high)
        {
            mid = low + (high - low) / 2;

            if(force(position, mid , m))
            {
                ans = mid;  // possible answer
                low = mid + 1;  // getiing larger values
            }
            else
                high = mid - 1;
        }
        return ans;
    }
};
