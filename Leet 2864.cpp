// Link: https://leetcode.com/problems/maximum-odd-binary-number/description/

class Solution {
public:
    string maximumOddBinaryNumber(string s) 
    {
        string ans = "";
        int count0 = 0;

        for(char& ch : s)
        {
          if(ch == '1')
            ans += ch;
          else
            count0++;
        }

        if(count0 == 0)  // corner case
            return s;

        ans[ans.size() - 1] = '0'; // all the 1's has been assigned but a 1 is needed for LSB. So the last 1 is replaced with 0
        count0 -= 1;  // so 0 is decremented by 1

        while(count0 != 0)
        {
          ans += '0';
          count0--;
        }

        ans += '1'; // assigining 1 at LSB

        return ans;
    }
};