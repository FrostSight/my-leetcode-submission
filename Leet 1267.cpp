// Link: https://leetcode.com/problems/count-servers-that-communicate/description/

class Solution {
public:
    int countServers(vector<vector<int>>& grid) 
    {
        vector<int> indexRowCounter(grid.size(), 0), indexColCounter(grid[0].size(), 0);
        int servers = 0;

        for(int row = 0; row < grid.size(); row++)
        {
            for(int col = 0; col < grid[0].size(); col++)
            {
                if(grid[row][col] == 1)
                {
                    indexRowCounter[row]++;
                    indexColCounter[col]++;
                }
            }
        }
        
        
        for(int row = 0; row < grid.size(); row++)
        {
            for(int col = 0; col < grid[0].size(); col++)
            {
                if(grid[row][col] == 1 && (indexRowCounter[row] > 1 || indexColCounter[col] > 1))
                {
                    servers++;
                }
            }
        }

        return servers;
    }
};
