// Link: https://leetcode.com/problems/count-subarrays-with-fixed-bounds/description/

#define ll long long
#define mini minIndex
#define mxi maxIndex 
#define ci culpritIndex

class Solution {
public:
    long long countSubarrays(vector<int>& nums, int minK, int maxK) 
    {
        int minIndex = -1, maxIndex = -1, culpritIndex = -1, i = 0;
        ll ans = 0, len, smaller;

        while(i < nums.size())
        {
            if(nums[i] < minK || nums[i] > maxK)
                ci = i;
            
            if(nums[i] == minK)
                mini = i;
            
            if(nums[i] == maxK)
                mxi = i;
            
            smaller = min(mini, mxi);
            len = smaller - ci;
            len <= 0 ? ans += 0 : ans+= len;
            
            i++;
        }

        return ans;
    }
};