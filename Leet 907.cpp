// Link: https://leetcode.com/problems/sum-of-subarray-minimums/description/

#define m modulus
#define ll long long
#define rs right_small
#define ls left_small
#define sc subarray_count

class Solution {
public:

    vector<int> getNSL(vector<int>& arr, int n)
    {
        vector<int> result(n);
        stack<int> stk;

        for(int i = 0; i < n; i++)
        {
            if(stk.empty())
                result[i] = -1;
            else
            {
                while(!stk.empty() && arr[stk.top()] >= arr[i])
                    stk.pop();
                
                result[i] = stk.empty() ? -1 : stk.top();
            }
            stk.push(i);
        }

        return result;
    }

    vector<int> getNSR(vector<int>& arr, int n)
    {
        vector<int> result(n);
        stack<int> stk;

        for(int i = n-1; i >= 0; i--)
        {
            if(stk.empty())
                result[i] = n;
            else
            {
                while(!stk.empty() && arr[stk.top()] > arr[i])
                    stk.pop();
                
                result[i] = stk.empty() ? n : stk.top();
            }
            stk.push(i);
        }

        return result;
    }

    int sumSubarrayMins(vector<int>& arr) 
    {
        int n = arr.size(), modulus = 1e9+7;
        ll ans = 0, totalSum, rs, ls, subarray_count;
        vector<int> NSL = getNSL(arr, n);
        vector<int> NSR = getNSR(arr, n);

        for(int i = 0; i < n; i++)
        {
            ls = i - NSL[i];
            rs = NSR[i] - i;
            sc = ls*rs;
            totalSum = arr[i]*sc;
            ans = (ans + totalSum) % m;
        } 

        return ans;
    }
};