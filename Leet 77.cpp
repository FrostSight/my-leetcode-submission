// Link: https://leetcode.com/problems/combinations/

class Solution {
public:

    void solve(int start, int n, int k, vector<int>& temp, vector<vector<int>>& ans)
    {
        if(k == 0)
        {
            ans.push_back(temp);
            return;
        }

        if(start > n) // actual terminating condition
            return;
        
        temp.push_back(start);  // pick
        solve(start+1, n, k-1, temp, ans);
        temp.pop_back(); // unpick
        solve(start+1, n, k, temp, ans);

        // alternate of line 14 to 20
        /*
        for(int i = 1; i <= n; i++)
        {
            temp.push_back(i);  // pick
            solve(i+1, n, k-1, temp, ans);
            temp.pop_back(); // unpick
        }
        */
    }

    vector<vector<int>> combine(int n, int k) 
    {
        vector<vector<int>> ans;
        vector<int> temp;
        solve(1, n, k, temp, ans);    

        return ans;
    }
};
