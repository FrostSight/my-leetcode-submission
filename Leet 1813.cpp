// Link: https://leetcode.com/problems/sentence-similarity-iii/description/

class Solution {
public:
    bool areSentencesSimilar(string s1, string s2) 
    {
        if(s1.size() < s2.size()) 
            swap(s1, s2);

        vector<string> vec1, vec2;
        stringstream ss1(s1);
        string token;

        while(ss1 >> token) 
        {
            vec1.push_back(token);
        }

        stringstream ss2(s2);
        while(ss2 >> token) 
        {
            vec2.push_back(token);
        }

        int i = 0, j = vec1.size()-1;
        int start = 0, end = vec2.size()-1; 

        while(start < vec2.size() && i < vec1.size() && vec2[start] == vec1[i]) 
        {
            start++;
            i++;
        }

        while(end >= start && vec2[end] == vec1[j]) 
        {
            j--;
            end--;
        }
        
        return end < start ? true : false;

    }
};

