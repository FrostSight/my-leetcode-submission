// Link: https://leetcode.com/problems/largest-substring-between-two-equal-characters/description/

class Solution {
public:
    int maxLengthBetweenEqualCharacters(string s) 
    {
        vector <int> previous(26, -1);
        int ans = -1;

        for(int i = 0; i < s.size(); i++)
        {
            if(previous[s[i] - 'a'] == -1)
                previous[s[i] - 'a'] = i;
            else
                ans = max(ans, i - previous[s[i] - 'a'] - 1);
        }

        return ans;    
    }
};
