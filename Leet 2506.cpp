// Link: https://leetcode.com/problems/count-pairs-of-similar-strings/description/

class Solution {
public:
    int similarPairs(vector<string>& words) 
    {
        unordered_map<string, int> ump;
        int counter = 0;

        for(string& s1 : words)
        {
            set<char> ust(s1.begin(), s1.end());
            string s2(ust.begin(), ust.end());
            ump[s2]++;
        }

        for(auto& it : ump)
        {
            int x = it.second;
            counter += (x * (x-1)) / 2;
        }

        return counter;
    }
};

