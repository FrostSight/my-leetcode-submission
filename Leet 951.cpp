// Link: https://leetcode.com/problems/flip-equivalent-binary-trees/description/

class Solution {
public:
    bool flipEquiv(TreeNode* root1, TreeNode* root2) 
    {
        if(root1 == NULL && root2 == NULL)    return true; // both of the tree came to an end
        else if(root1 == NULL || root2 == NULL)    return false; // a single of the tree came to an end
        
        if(root1->val == root2->val)
        {
            bool withoutFlip = flipEquiv(root1->left, root2->left) && flipEquiv(root1->right, root2->right);
            bool withFlip = flipEquiv(root1->left, root2->right) && flipEquiv(root1->right, root2->left);

            return withoutFlip || withFlip;
        }

        return false;
    }
};

