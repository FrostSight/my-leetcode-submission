// Link: https://leetcode.com/problems/merge-sorted-array/description/

class Solution {
public:
    void merge(vector<int>& nums1, int m, vector<int>& nums2, int n) 
    {
        vector<int> nums3(nums1.size()); 
        int j = 0;

        if(n == 0 )
            return;

        copy(nums1.begin(), nums1.end(), nums3.begin());

        for(int i = m; i < nums3.size(); i++)
            {
                nums3[i] = nums2[j];
                j++;
            } 

        sort(nums3.begin(), nums3.end());

        copy(nums3.begin(), nums3.end(), nums1.begin());    
    }
};