// Link: https://leetcode.com/problems/find-minimum-in-rotated-sorted-array/description/

class Solution {
public:
    int findMin(vector<int>& nums) 
    {
        int low = 0, high = nums.size() -1, mid;
        
        while(low <= high)
        {
            mid = low + (high - low) / 2;

            if( mid > 0 && nums[mid - 1] > nums [mid]) // mid > 0 is corner case
                return nums[mid];
            else if (nums[mid] > nums[high])
                low = mid + 1;
            else
                high = mid -1;
        }
        return nums[low]; // corner case: if the array is sorted without rotation then first element is minimum 
    }
};

 /* 
 in acending order left < right
 if value of mid is greater then the rightmost or high value then the minimum value is on right side 
 otherwise minimum value is on left side
 */
