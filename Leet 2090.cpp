// Link: https://leetcode.com/problems/k-radius-subarray-averages/

#define ps_num prefixSum_num
#define cs cumulativeSum
#define te totalElements
#define ll long long
#define ls left_side
#define rs rifgt_side
#define fsum final_sum

class Solution {
public:
    vector<int> getAverages(vector<int>& nums, int k) 
    {
        if(k == 0)  // corner case
            return nums;

        vector<int> ans(nums.size(), -1);
        vector<ll> ps_num(nums.size(), 0);
        ll cs = 0, te = 2*k+1;

        if(nums.size() < te) // corner case
            return ans;

        for(int i = 0; i < nums.size(); i++)
        {
            cs += nums[i];
            ps_num[i] = cs;
        }

        for(int i = k; i < nums.size()-k; i++)
        {
            int ls  = i-k;  // i.e subtracting sum
            int rs = i+k;   // i.e temp sum
            ll fsum = ps_num[rs];  // actually fsum = temp sum - subtracting sum or fsum = rs - ls but ls has a corner case
            if(ls > 0)// ls corner case
                fsum -= ps_num[ls - 1]; // i was inclusively add twice in ls and rs so 1 is subtracting0
                       
            int gor = fsum / te;            
            ans[i] = gor;
        }

        return ans;
    }
};