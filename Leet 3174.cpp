// Link: https://leetcode.com/problems/clear-digits/description/

class Solution {
public:
    string clearDigits(string s) 
    {
        stack<char> stk;

        for(char ch : s)
        {
            if(!stk.empty() && isdigit(ch))
                stk.pop();
            else
                stk.push(ch);
        }

        string ans = "";
        while(!stk.empty())
        {
            ans = stk.top() + ans;
            stk.pop();
        }

        return ans;
    }
};
