// Link: https://leetcode.com/problems/walking-robot-simulation/description/

class Solution {
public:
    int robotSim(vector<int>& commands, vector<vector<int>>& obstacles) 
    {
        unordered_set<string> ust;

        for(vector<int>& obs : obstacles)
        {
            string key = to_string(obs[0]) + "_" + to_string(obs[1]); // let keey = 110, to make difference between (11, 0) and (1, 10) we put "_" so it becomes 11_0
            ust.insert(key);
        }    

        int x = 0, y = 0, maxDistance = 0;  // initialization point
        pair<int, int> direction = {0, 1};  // initialization face to North

        for(int& cmd : commands)
        {
            if(cmd == -2)  // if left
                direction = {-direction.second, direction.first};  // updating coordinate using further derivation
            else if (cmd == -1) // if right
                direction = {direction.second, -direction.first};  // updating coordinate using further derivation
            else // if moving straight
            {
                for(int step = 0; step < cmd; step++)
                {
                    int newX = x + direction.first;   // moving to next coordinate
                    int newY = y + direction.second;

                    string nextKey = to_string(newX) + "_" + to_string(newY);  // check for collision, if collide then stops
                    if(ust.find(nextKey) != ust.end())
                        break;
                    
                    x = newX, y = newY;  // updating current coordinate by new ones
                }
            }

            maxDistance = max(maxDistance, (x*x + y*y));
        }

        return maxDistance;
    }
};

/*
Things need to be cleared first
1) according to question +y is North, -y is South, +x is East, -x is West
2) Robot initialized at (0, 0) coordinate
3) Robot is facing to north at initialization

Derivation:
------------
moving North => (x->constant, y->increases by 1) => (x+0, y+1) => increases by(0, 1)
moving South => (x->constant, y->decreases by 1) => (x+0, y-1) => increases by(0, -1)
moving East => (x->increases by 1, y->constant) => (x+1, y+0) => increases by(1, 0)
moving West => (x->decreases by 1, y->constant) => (x-1, y+0) => increases by(-1, 0)

We further derive
let initialized at (x, y),
after turning 90 degree Left (x`, y`) = (-y, x)
after turning 90 degree Right (x`, y`) = (y, -x)
*/