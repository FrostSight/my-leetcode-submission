// Link: https://leetcode.com/problems/count-substrings-that-satisfy-k-constraint-i/description/

class Solution {
public:
    int countKConstraintSubstrings(string s, int k) 
    {
        int counter1 = 0, counter0 = 0, i = 0, j = 0, substr_counter = 0;

        while(j < s.size())
        {
            if(s[j] == '0')
                counter0++;
            else if(s[j] == '1')
                counter1++;
            

            while(counter1 > k && counter0 > k)
            {
                if(s[i] == '0')
                    counter0--;
                else if(s[i] == '1')
                    counter1--;
                
                i++;
            }
            
            substr_counter += (j+1 - i);  // usually j-i to find length but here as 0 indexing so 1 is added
            j++;
        } 

        return substr_counter;   
    }
};

