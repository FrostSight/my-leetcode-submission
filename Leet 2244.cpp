// Link: https://leetcode.com/problems/minimum-rounds-to-complete-all-tasks/

class Solution {
public:
    int minimumRounds(vector<int>& tasks) 
    {
        unordered_map<int, int> umap;
        int rounds = 0;

        for(int t : tasks)
            umap[t]++;

        for(auto it = umap.begin(); it != umap.end(); it++)
        {
            if(it->second == 1) // if any element found of with only once occurance
                return -1;
            
            if(it->second % 3 == 0)  // if element count is divisible by 3
                rounds = rounds + it->second / 3;
            else  // if element count that is not divisible by 3
                rounds = rounds + (it->second / 3 + 1);
        }
        return rounds;
    }
};