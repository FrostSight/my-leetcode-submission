// Link: https://leetcode.com/problems/asteroid-collision/description/

// approach 1: using stack

#define top_positive stk.top()>0
#define current_negative current<0
#define sum_negative sum<0
#define sum_positive sum>0
#define both_destroyed sum==0

class Solution {
public:
    vector<int> asteroidCollision(vector<int>& asteroids) 
    {
        stack<int> stk;
        
        for(int &current : asteroids) 
        {            
            while(!stk.empty() && current_negative && top_positive) 
            {
                int sum = current + stk.top();
                
                if(sum_negative) 
                    stk.pop();
                else if(sum_positive) 
                    current = 0;
                else if(both_destroyed)
                {
                    stk.pop();
                    current = 0;
                }
            }
            
            if(current != 0)
                stk.push(current);
            
        }

        
        vector<int> ans(stk.size());
        int i = ans.size()-1;
        
        while(!stk.empty()) 
        {
            int x = stk.top();
            stk.pop();
            ans[i] = x;
            i--;
        }
        
        return ans;
    }
};

//approach 2 : no stack

#define top_positive stk.back()>0
#define current_negative current<0
#define sum_negative sum<0
#define sum_positive sum>0
#define both_destroyed sum==0

class Solution {
public:
    vector<int> asteroidCollision(vector<int>& asteroids) 
    {
        vector<int> stk;
        int sum;

        for(int& current : asteroids)
        {
            while(!stk.empty() && current < 0 && top_positive)
            {
                sum = current + stk.back();

                if(sum_negative)
                    stk.pop_back();
                else if(sum_positive)
                    current = 0;
                else if(both_destroyed)
                {
                    current = 0;
                    stk.pop_back();
                }
            }

            if(current != 0)
                stk.push_back(current);
        }    

        return stk;
    }
};