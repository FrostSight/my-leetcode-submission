// Link: https://leetcode.com/problems/ipo/description/

#define taka w
#define cp capital_profit

class Solution {
public:
    int findMaximizedCapital(int k, int w, vector<int>& profits, vector<int>& capital) 
    {    
        vector<pair<int, int>> cp;
        priority_queue<int> max_pq;
        
        for(int i = 0; i < profits.size(); i++)
        {
            cp.push_back( {capital[i], profits[i]} );
        }

        sort(cp.begin(), cp.end());

        int i = 0;        
        while (k--) 
        {
            while (i < cp.size() && cp[i].first <= taka) 
            {
                max_pq.push(cp[i].second);
                i++;
            }

            if (!max_pq.empty())
            {
                taka += max_pq.top();
                max_pq.pop();
            }            
        }
        
        return taka;
    }
};

