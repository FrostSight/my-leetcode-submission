// Link: https://leetcode.com/problems/construct-the-lexicographically-largest-valid-sequence/description/

class Solution {
public:
    bool solve(int idx, int n, vector<bool>& seen, vector<int>& result)
    {
        if(idx == result.size())    return true; // base case

        if(result[idx] != -1)
            return solve(idx+1, n, seen, result); // without this all will be -1
        
        for(int num = n; num >= 1; num--)
        {
            if(seen[num] == true) // if seen then pass
                continue;
            
            else // if not seen
            {   // DO
                seen[num] = true;
                result[idx] = num; // placing the 1st one
                if(num == 1) // if 1 then it occurs only one time and everything else occurs twice
                {
                    if(solve(idx+1, n, seen, result) == true) // exploring the next one
                        return true;
                }
                else // hassle to set up the 2nd one
                {
                    int j = num + idx; // getting the index
                    if(j < result.size() && result[j] == -1) // if index is in bound and it is occupied (i.e. -1)
                    {
                        result[j] = num; // we place it 
                        if(solve(idx+1, n, seen, result) == true) // exploring the next one
                            return true;
                        
                        result[j] = -1; // if the recursion above fails we unoccupy the 2nd one
                    }
                }

                // UNDO
                seen[num] = false;
                result[idx] = -1;}
        }

        return false;
    }

    vector<int> constructDistancedSequence(int n) 
    {
        vector<int> result(2*n-1, -1);
        vector<bool> seen(n+1, false);

        solve(0, n, seen, result);

        return result;
    }
};