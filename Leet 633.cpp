// Link: https://leetcode.com/problems/sum-of-square-numbers/description/

class Solution {
public:
    bool judgeSquareSum(int k) 
    {
        for(long a = 0; a*a <= k; a++)
        {
            int x = k - (int)(a*a);
            int b_low = 0, b_high = x;

            while(b_low <= b_high)
            {
                long mid = b_low + (b_high - b_low) / 2;

                long expected = mid*mid;
                if(expected == x)
                    return true;
                else if(expected < x)
                    b_low = mid + 1;
                else if(expected > x)
                    b_high = mid - 1;
            }
        }

        return false;
    }
};

