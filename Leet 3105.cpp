// Link: https://leetcode.com/problems/longest-strictly-increasing-or-strictly-decreasing-subarray/description/

class Solution {
public:
    int longestMonotonicSubarray(vector<int>& nums) 
    {
        int result = 1;

        for(int i = 0; i < nums.size()-1; i++)
        {
            int increasing = 1, j = i+1;
            while(j < nums.size() && nums[j] > nums[j-1])
            {
                increasing++;
                j++;
            }

            int decreasing = 1; 
            j = i+1;
            while(j < nums.size() && nums[j] < nums[j-1])
            {
                decreasing++;
                j++;
            }

            result = max({result, increasing, decreasing});
        }    

        return result;
    }
};
