// Link: https://leetcode.com/problems/merge-nodes-in-between-zeros/description/

class Solution {
public:
    ListNode* mergeNodes(ListNode* head) 
    {
        ListNode* p1 = head->next;
        ListNode* p2 = head->next;

        while(p2 != NULL)
        {
            int sum = 0;
            while(p2->val != 0)
            {
                sum += p2->val;
                p2 = p2->next;
            }

            p1->val = sum; // assigning sum
            p2 = p2->next; // forwarding p2
            p1->next = p2; // making connection
            p1 = p1->next; // forwarding p1
        }

        return head->next;
    }
};
