// Link: https://leetcode.com/problems/number-of-flowers-in-full-bloom/

// Approach 1: Binary search using STL 
class Solution {
public:
    vector<int> fullBloomFlowers(vector<vector<int>>& flowers, vector<int>& people) 
    {
        int bloomed_flower, dead_flower;
        vector<int> answer, starting_time, ending_time;

        for(auto& elements : flowers) // making two separate vectors of 2D vector's row-column
        {
            starting_time.push_back(elements[0]); // 2D vector row
            ending_time.push_back(elements[1]); // // 2D vector coumn
        }

        sort(starting_time.begin(), starting_time.end());
        sort(ending_time.begin(), ending_time.end());

        for(int i = 0; i < people.size(); i++) 
        {
            bloomed_flower = upper_bound(begin(starting_time), end(starting_time), people[i]) - begin(starting_time);  // these bound search using BS technique and they return pointers so subtracting the beginning returns actual value
            dead_flower    = lower_bound(begin(ending_time), end(ending_time), people[i]) - begin(ending_time);  // upper_bound returns (less than people[i]) lower_bounds returns (greater than or equal to people[i]) 
            
            answer.push_back(bloomed_flower - dead_flower);
        }

        return answer;
    }
};



// Approach 2: it gave me TLE

class Solution {
public:

    int check(int x, vector<vector<int>>& flowers)
    {
        int counter = 0;
        for(int i = 0; i < flowers.size(); i++)
        {
            if(x >= flowers[i][0] && x <= flowers[i][1])  // if visits between greater than or equal to bloom_day and less than equal to dead_day
            {
                counter++;
            }
        }
        return counter;
    }

    vector<int> fullBloomFlowers(vector<vector<int>>& flowers, vector<int>& people) 
    {
        vector <int> answer;

        for(int i = 0 ; i <  people.size(); i++)
        {
            answer.push_back(check(people[i], flowers));
        }    

        return answer;
    }
};