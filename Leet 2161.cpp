// Link: https://leetcode.com/problems/partition-array-according-to-given-pivot/description/

// approach 1: brute force
class Solution {
public:
    vector<int> pivotArray(vector<int>& nums, int pivot) 
    {
        vector<int> result;

        for(int& n : nums)
        {
            if(n < pivot)
                result.push_back(n);
        }
        
        for(int& n : nums)
        {
            if(n == pivot)
                result.push_back(n);
        }
        
        for(int& n : nums)
        {
            if(n > pivot)
                result.push_back(n);
        }

        return result;
    }
};


// approach 2: 
class Solution {
public:
    vector<int> pivotArray(vector<int>& nums, int pivot) 
    {
        vector<int> result(nums.size(), 0);
        int smaller = 0, larger = 0, equals = 0;

        for(int& n : nums)
        {
            if(n < pivot)
                smaller++;
            else if(n == pivot)
                equals++;
            else
                larger++;
        }    

        int i = 0, j = smaller, k = smaller + equals;

        for(int& n : nums)
        {
            if(n < pivot)
            {
                result[i] = n;
                i++;
            }
            else if(n == pivot)
            {
                result[j] = n;
                j++;
            }
            else
            {
                result[k] = n;
                k++;
            }
        }

        return result;
    }
};
