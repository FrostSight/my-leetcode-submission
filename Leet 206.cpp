// Link: https://leetcode.com/problems/reverse-linked-list/


class Solution {
public:
    ListNode* reverseList(ListNode* head) 
    {
        if(head == NULL || head->next == NULL)  // no node || only one node
            return head;


        ListNode* tail = reverseList(head->next);  // travelling to the last node
        head->next->next = head;  // reversal begins when reached to the last node
        head->next = NULL;

        return tail;
    }
};

/*
explanation:

let, *head -> 1_ -> 2 NULL here head pointer points to 1, 1(head->next)->next points to 2 and 2 has NULL. So instead to NULL 2->next redirected to head and it becomes circular.
on the next we cut off the link to 2 by doing head->next = NULL

for *head -> 1_ -> 2_ -> 3_ -> 4_ -> 5 NULL
recursively 1 to its next node 2, 2 to its next node 3, 3 to its next node 4, 4 to its next node 5.

above explanation happens (reversal happens) 5 point at 4 and 4 has NULL. For 1,2 and 3, 4 is last node as it has NULL.
Again reversal happens now 5,4, and 3 is reversed and for 1 and 2, 3 is the last as it has NULL.
this goes on.

*/