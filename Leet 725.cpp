// Link: https://leetcode.com/problems/split-linked-list-in-parts/

#define bn buckets_Nodes
#define rn remaining_Nodes
#define pc previous_current

class Solution {
public:

    int getListLen(ListNode* head)
    {
        ListNode* h = head;
        int counter = 0;

        while(h != NULL)
        {
            counter++;
            h = h->next;
        }

        return counter;
    }

    vector<ListNode*> splitListToParts(ListNode* head, int k) 
    {        
        vector<ListNode*> result(k, NULL);
        int listLen, buckets_Nodes, remaining_Nodes;
        ListNode* current = head;
        ListNode* previous_current = NULL;
               
        listLen = getListLen(head);        
        bn = listLen / k; 
        rn  = listLen % k;         
        
        for(int i = 0; i < k ; i++) 
        {            
            result[i] = current;

            for(int cnt = 1; cnt <= bn + (rn > 0 ? 1 : 0); cnt++) 
            {
                pc = current;
                current = current->next;
            }
            
            rn--;
            if(pc != NULL)
                pc->next = NULL;            
        }
        
        return result;
    }
};
