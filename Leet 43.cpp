// Link: https://leetcode.com/problems/multiply-strings/description/

class Solution {
public:
    string multiply(string num1, string num2) 
    {
        if(num1 == "0" || num2 == "0") return "0";  // corner case

        vector<int> result(num1.size() + num2.size(), 0);  // num1.size() + num2.size() == max no. of digits

        // reverse iteration as we do in hands while multiplication
        for(int i = num1.size()-1; i >= 0; i--)
        {
            for(int j = num2.size()-1; j >= 0; j--)
            {
                int digit1 = num1[i] - '0';  // character to integer conversion
                int digit2 = num2[j] - '0';
                result[i + j + 1] += digit1 * digit2;  // a 1 is added as it is a 0 indexing
                result[i + j] += result[i + j + 1] / 10;
                result[i + j + 1] = result[i + j + 1] % 10;
            }
        }   
        
        int i = 0;
        while(i < result.size() && result[i] == 0) // skip leading 0's
            i++;
        
        string ans;
        while(i < result.size())
        {
            ans.push_back(result[i] + '0'); // integer to character conversion
            i++;
        }
        return ans;
    }
};

