// Link: https://leetcode.com/problems/group-the-people-given-the-group-size-they-belong-to/description/

class Solution {
public:
    vector<vector<int>> groupThePeople(vector<int>& groupSizes) 
    {
        vector<vector <int>> ans;
        unordered_map<int, vector<int>> umap;
        int value;

        for(int i = 0; i < groupSizes.size(); i++)
        {
            value = groupSizes[i];
            umap[value].push_back(i); // storing index for that particular value

            if(umap[value].size() == value)  // if vector size == value
            {
                ans.push_back(umap[value]);
                umap[value].clear();
            }
        }    
        return ans;
    }
};