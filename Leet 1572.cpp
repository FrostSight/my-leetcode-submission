// Link: https://leetcode.com/problems/matrix-diagonal-sum/description/

class Solution {
public:
    int diagonalSum(vector<vector<int>>& mat) 
    {
        int n = mat.size(), sum = 0;

        for(int i = 0; i < mat.size(); i++)
        {
            for(int j = 0; j < mat[i].size(); j++)
            {
                if(i == j)  // primary diagonal
                    sum = sum + mat[i][j];
            }
            sum = sum + mat[i][n-1 - i];  // secondary diagonal
        }
        
        if(n & 1)  // if odd size, centered cell is counted twice so subtracting one time
            sum = sum - mat[n/2][n/2];
        
        return sum;
    }
};