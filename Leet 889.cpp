// Link: https://leetcode.com/problems/construct-binary-tree-from-preorder-and-postorder-traversal/description/

#define prs preorder_start
#define pre preorder_end
#define pos postorder_start

class Solution {
public:
    TreeNode* solve(int prs, int pre, int pos, vector<int>& preorder, vector<int>& postorder)
    {
        if(prs > pre)   return NULL; // terminating condition

        // figuring out
        TreeNode* root = new TreeNode(preorder[prs]); // current root
        if(prs == pre)
            return root; // leaf node or single root 
        
        int nextNode = preorder[prs+1]; // nextNode is the root of sub-tree
        int j = pos;
        while(postorder[j] != nextNode) // how many nodes are in that sub-tree
            j++;
        
        int ranges = j-pos +1; // getting the nodes

        // division by recursive call
        root->left = solve(prs+1, prs+ranges, pos, preorder, postorder);
        root->right = solve(prs+ranges+1, pre, j+1, preorder, postorder);

        return root;
    }

    TreeNode* constructFromPrePost(vector<int>& preorder, vector<int>& postorder) 
    {
        int pre = preorder.size();

        TreeNode* result = solve(0, pre-1, 0, preorder, postorder);

        return result;
    }
};

/*
Explanation:
preorder = root->left->right | postorder = left->right->root
preorder = [1,2,4,5,3,6,7], postorder = [4,5,2,6,7,3,1]
2 major steps:
1. figure out which nodes will be on left and right side.
2. Then make the tree by calling recursion

1 is the root so 2 is root of left sub-tree. Find 2 in postorder and 4,5 are on the left of 2 so 2,4,5 are nodes of left sub tree (already known 2 is root now among 4 and 5 which one is leaf or root of another subtree can be found through recursive call).
As we have found 4,5,2 of postorder are inside left side so we find range 3, and add this range to `prs` in preorder. Finally recursive call.

*/