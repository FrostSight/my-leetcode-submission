// Link: https://leetcode.com/problems/concatenation-of-array/

class Solution {
public:
    vector<int> getConcatenation(vector<int>& nums) 
    {
        vector <int> ans(nums.size() * 2);
        int j = nums.size();

        for(int i = 0; i < nums.size(); i++)
        {
            ans[i] = nums[i];
            ans[j] = nums[i];
            j++;
        }

        return ans;    
    }
};
