// Link: https://leetcode.com/problems/string-compression-iii/description/


// this code handle every situation regardlessly product or non product of 9
class Solution {
public:
    string compressedString(string word) 
    {
        int i = 0, j = 0;
        string comp = "";

        while (i < word.size())
        {
            while (j < word.size() && word[i] == word[j])
                j++;
            
            int counter = j - i;
            while (counter > 9)
            {
                comp.push_back('9');
                comp.push_back(word[i]);
                counter -= 9;
            }
            if (counter > 0)
            {
                comp.push_back(counter + '0');
                comp.push_back(word[i]);
            }

            i = j;
        }

        return comp;
    }
};


/*
failed approach

class Solution {
public:
    string compressedString(string word) 
    {
        int i = 0, j = 0, counter = 0;
        string comp = "";

        while(i < word.size())
        {
            while(j < word.size() && word[i] == word[j])
                j++;
            
            counter = j - i;
            if(counter > 9)
            {
                comp.push_back('9');
                comp.push_back(word[i]);
                int x = counter - 9;
                char ch = x + '0';
                comp.push_back(ch);
                comp.push_back(word[i]);
            }
            else if (counter <= 9)
            {
                comp.push_back(counter + '0');
                comp.push_back(word[i]);
            }

            i = j;
        }

        return comp;
    }
};

reason to fail:
it failed because of occuring products of 9.
suppose a character occurs 9 ot 18 or 27 or 36 or 81 times. Eventually counter will become 0 after every 9 subtraction. 
so counter + '0' will print a garbage value.

the code is only valid when counter is not any product of 9 like 14. 14 - 9 = 5 and 5 can be converted into character 5.

*/