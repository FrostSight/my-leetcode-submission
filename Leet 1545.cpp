// Link: https://leetcode.com/problems/find-kth-bit-in-nth-binary-string/description/

class Solution {
public:
    char findKthBit(int n, int k) 
    {
        if (n == 1) return '0';

        int bit_length = pow(2, n) - 1;

        if (k < ceil(bit_length / 2.0))
            return findKthBit(n - 1, k);
        else if (k == ceil(bit_length / 2.0))
            return '1';
        else if (k > ceil(bit_length / 2.0))
        {
            char ch = findKthBit(n - 1, bit_length - (k - 1));
            return (ch == '1') ? '0' : '1';  // handling the flipping bit
        }

        return '0'; 
    }
};

