// Link: https://leetcode.com/problems/count-triplets-that-can-form-two-arrays-of-equal-xor/description/

class Solution {
public:
    int countTriplets(vector<int>& arr) 
    {
        int ;
        vector<int> prefixXor(begin(arr), end(arr));
        prefixXor.insert(prefixXor.begin(), 0); 
        int n = prefixXor.size(), counter = 0;

        for(int i = 1; i < n; i++) 
        {
            prefixXor[i] ^= prefixXor[i-1];
        }


        for(int i = 0; i < n; i++) 
        {
            for(int k = i+1; k < n; k++) 
            {
                if(prefixXor[k] == prefixXor[i])
                    counter += k-i-1;

            }
        }

        return counter;
    }
};
