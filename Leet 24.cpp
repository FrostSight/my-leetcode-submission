// Link: https://leetcode.com/problems/swap-nodes-in-pairs/description/

class Solution {
public:
    ListNode* swapPairs(ListNode* head) 
    {
        if(head == NULL || head->next == NULL)
            return head;

        ListNode* newHead = head->next;  // 2nd will be the next head. Keeping it safe

        head->next = swapPairs(head->next->next);

        newHead->next = head; // making 2nd node head

        return newHead;
    }
};