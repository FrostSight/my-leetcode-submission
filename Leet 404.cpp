// Link: https://leetcode.com/problems/sum-of-left-leaves/description/

class Solution {
public:

    int sum = 0;

    bool isLeaf(TreeNode* root)
    {
        if(root == NULL)
            return 0;

        return root->left == NULL && root->right == NULL ? true : false;
    }

    int sumOfLeftLeaves(TreeNode* root) 
    {
        if(root == NULL)
            return 0;

        bool leafFound = isLeaf(root->left);
        if(leafFound == true)
            sum += root->left->val;
        
        sumOfLeftLeaves(root->left);
        sumOfLeftLeaves(root->right);

        return sum;
    }
};