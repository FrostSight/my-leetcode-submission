// Link: https://leetcode.com/problems/baseball-game/description/

class Solution {
public:
    int calPoints(vector<string>& operations) 
    {
        vector<int> record;
        int i = 0;
        int rslt;
        
        for (auto op : operations) 
        {
            if (op == "+") 
            {
                record.push_back(record[i - 1] + record[i - 2]);
                i++;
            } 
            else if (op == "D") 
            {
                record.push_back(2 * record[i - 1]);
                i++;
            } 
            else if (op == "C") 
            {
                i--;
                record.pop_back();
            } 
            else
            {
                i++;
                record.push_back(stoi(op));
            }
        }
        
        rslt = accumulate(record.begin(), record.end(), 0);

        return rslt;
    }
};
