// Link: https://leetcode.com/problems/evaluate-reverse-polish-notation/description/

class Solution {
public:

    int operation(int a, int b, string& op)
    {
        if(op == "+")
            return a+b;
        else if(op == "-")
            return a-b;
        else if(op == "*")
            return a*b;
        else
            return a/b;
        
        return NULL;
    }

    int evalRPN(vector<string>& tokens) 
    {
        stack<int> stk;   
        int a, b, ans; 

        for(string& ch : tokens)
        {
            if(ch == "+" || ch == "-" || ch == "*" || ch == "/")
            {
                b = stk.top();
                stk.pop();

                a = stk.top();
                stk.pop();

                ans = operation(a, b, ch);
                stk.push(ans);
            }
            else
            {
                stk.push(stoi(ch));
            }
        }

        return stk.top();
    }
};
