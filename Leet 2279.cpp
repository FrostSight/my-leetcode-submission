// Link: https://leetcode.com/problems/maximum-bags-with-full-capacity-of-rocks/description/

class Solution {
public:
    int maximumBags(vector<int>& capacity, vector<int>& rocks, int additionalRocks) 
    {
        vector<int> remaining(capacity.size());
        int bag_counter = 0;

        for(int i = 0; i < capacity.size(); i++)
            remaining[i] = capacity[i] - rocks[i];

        sort(remaining.begin(), remaining.end());

        for(int i = 0; i < remaining.size(); i++)
        {
            if(remaining[i] == 0 || additionalRocks >= remaining[i])
            {
                bag_counter++;
                additionalRocks = additionalRocks - remaining[i];
            }
        }
        return bag_counter;
    }
};

/*
Approach
For each bag, calculate the remaining capacity by subtracting.

Sorting for the least remaining capacity comes first.

if the remaining capacity is 0 (bag is already full) or if the remaining capacity is less than or equal to the number of additional rocks, fill the bag with rocks (subtract the number of rocks used from the total number of additional rocks) and increment the bag counter.

Finally return .
*/