// Link: https://leetcode.com/problems/middle-of-the-linked-list/description/

// Approach 1
class Solution {
public:

    int getLength(ListNode* head)
    {
        int counter = 0;
        while(head != NULL)
        {
            head = head->next;
            counter++;
        }
        return counter;
    }

    ListNode* middleNode(ListNode* head) 
    {
        int travel, listlen;

        listlen = getLength(head);   
        travel = (listlen/2);  // i need print from mid (including) to last all elements. So i travelled to mid node
        while(travel != 0)
        {
            head = head->next;
            travel--;
        }

        return head;
    }
};


// Approach 2: slow fast pointer

class Solution {
public:
    ListNode* middleNode(ListNode* head) 
    {
        ListNode* slow  = head;
        ListNode* fast = head;
        
        while(slow != NULL && fast != NULL && fast->next != NULL) 
        {
            slow = slow->next;
            fast = fast->next->next; // as fast pointer moves 2 steps, slow pointer will be at mid when fast pointer reached at the end
        }
        
        return slow;    
    }
};
