// link: https://leetcode.com/problems/reverse-integer/

class Solution {
public:
    int reverse(int x) 
    {
        int z = 0, y;

        while(x != 0)
        {
            y = x % 10;

            if((z > INT_MAX / 10) || (z < INT_MIN / 10))
                return 0;
            
            z = z * 10 + y;
            x = x / 10;
        }
        return z;
    }
};
