// Link: https://leetcode.com/problems/valid-parenthesis-string/description/

class Solution {
public:

    int t[101][101];
    bool solve(int idx, int open, string s, int n)
    {
        if(idx == n)
        {
            return open == 0 ? true : false;
        }

        if(t[idx][open] != -1) 
            return t[idx][open] == 1 ? true : false;
        

        bool isValid = false;

        if(s[idx] == '*')
        {
            isValid |= solve(idx+1, open+1, s, n); //(s[idx] == '(')
            isValid |= solve(idx+1, open, s, n);   //(s[idx] == '')
            if(open > 0) // else if(s[idx] == ')') but subtraction is only possible when open  > 0
                isValid |= solve(idx+1, open-1, s, n);
        }
        else if(s[idx] == '(')
            isValid |= solve(idx+1, open+1, s, n);
        else if(open > 0)  // else if(s[idx] == ')') but subtraction is only possible when open  > 0
            isValid |= solve(idx+1, open-1, s, n);
        

        return t[idx][open] = isValid;
    }

    bool checkValidString(string s) 
    {
        bool sol;
        
        int n = s.size();
        memset(t, -1, sizeof(t));
        sol = solve(0, 0, s, n);

        return sol ? true : false;
    }
};