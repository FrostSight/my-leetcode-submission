// Link: https://leetcode.com/problems/defanging-an-ip-address/description/

class Solution {
public:
    string defangIPaddr(string address) 
    {
        string ip = "";

        for(char ch : address)
        {
            if(ch == '.')
                ip = ip + "[.]";        
            else
                ip = ip + ch;
        }  
        return ip;   
    }
};
