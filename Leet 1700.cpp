// Link: https://leetcode.com/problems/number-of-students-unable-to-eat-lunch/description/

class Solution {
public:
    int countStudents(vector<int>& students, vector<int>& sandwiches) 
    {
        stack<int> stk;
        queue<int> qu;
        int served = 0;

        int j = sandwiches.size() - 1;

        for (int i = 0; i < students.size(); i++) 
        {
            qu.push(students[i]);
            stk.push(sandwiches[j]);
            j--; 
        }

        while (!qu.empty() && served < qu.size()) 
        {
            if (stk.top() == qu.front()) 
            {
                stk.pop();
                qu.pop();
                served = 0;
            } 
            else 
            {
                int x = qu.front();
                qu.pop();
                qu.push(x);
                served++;
            }
        }

        return qu.size();
    }
};