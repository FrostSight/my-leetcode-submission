// Link: https://leetcode.com/problems/count-pairs-whose-sum-is-less-than-target/description/

class Solution {
public:
    int countPairs(vector<int>& nums, int target) 
    {
        int left = 0, right = nums.size() - 1, pairs = 0;

        sort(nums.begin(), nums.end());

        while(left < right)
        {
            if(nums[left] + nums[right] < target)
            {
                pairs += right - left;  // counting number of pairs
                left++;
            }
            else
                right--;
        }    
        return pairs;
    }
};

/*
1 2 3 4 5 6 7 8 9 10 target 7
if 5+1 is less than target then then all values in range 1 to 5 can sum u with 1 resulting less than 6
(1,2), (1,3) (1,4), (1,5) all less than 6
(2,3), (2,4) all less than 6
*/