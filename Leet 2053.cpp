// Link: https://leetcode.com/problems/kth-distinct-string-in-an-array/description/

class Solution {
public:
    string kthDistinct(vector<string>& arr, int k) 
    {
        unordered_map<string, int> ump;
        string ans;

        for(string s : arr)
            ump[s]++;

        for(string s : arr)
        {
            if(ump[s] > 1)
                continue;
            
            k--;
            if(k == 0)
                ans = s;
        }   

        return ans;
    }
};