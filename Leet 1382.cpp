// Link: https://leetcode.com/problems/balance-a-binary-search-tree/description/

class Solution {
public:

    void inOrder(TreeNode* root, vector<int>& nodes)
    {
        if(root == NULL)    return;

        inOrder(root->left, nodes);
        nodes.push_back(root->val);
        inOrder(root->right, nodes);
    }

    TreeNode* construct(int left, int right, vector<int>& nodes)
    {
        if(left > right)    return NULL;

        int mid = left + (right - left) / 2;
        TreeNode* root = new TreeNode(nodes[mid]);

        root->left = construct(left, mid-1, nodes);
        root->right = construct(mid+1, right, nodes);

        return root;
    }

    TreeNode* balanceBST(TreeNode* root) 
    {
        vector<int> nodes;
        inOrder(root, nodes);

        int left = 0, right = nodes.size()-1;
        TreeNode* newRoot = construct(left, right, nodes);

        return newRoot;
    }
};
