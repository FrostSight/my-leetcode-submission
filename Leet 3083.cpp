// Link: https://leetcode.com/problems/existence-of-a-substring-in-a-string-and-its-reverse/description/

class Solution {
public:
    bool isSubstringPresent(string s) 
    {
        unordered_set<string> st;
        
        for (int i = 0; i < s.size() - 1; i++) 
            st.insert(s.substr(i, 2));
        
        string rev(s.rbegin(), s.rend());
        
        for (auto& it : st) 
        {
            if (rev.find(it) != string::npos) 
            {
                return true;
            }
        }
        
        return false;    
    }
};