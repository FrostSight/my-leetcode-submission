// Link : https://leetcode.com/problems/generate-parentheses/description/

class Solution {
public:

    void making_Parenthesis(string current, int openP, int closeP, int n, vector<string>&result)
    {
        if(n == openP && n == closeP)
        {
            result.push_back(current);
            return;
        }

        if(openP < n)
            making_Parenthesis(current + '(', openP + 1, closeP, n, result);

        if(closeP < openP)
            making_Parenthesis(current + ')', openP, closeP + 1, n, result);
    }

    vector<string> generateParenthesis(int n) 
    {
        vector <string> result;

        making_Parenthesis("", 0, 0, n, result);  

        return result;
    }
};
