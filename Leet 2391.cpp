// Link: https://leetcode.com/problems/minimum-amount-of-time-to-collect-garbage/description/

class Solution {
public:
    int garbageCollection(vector<string>& garbage, vector<int>& travel) 
    {
        vector <int> travel_sum(travel.size());
        string temp;
        int m_index = 0, p_index = 0, g_index = 0, picking_time = 0;
        
        travel_sum[0] = travel[0];
        for(int i = 1; i < travel.size(); i++)
        {
            travel_sum[i] = travel_sum[i - 1] + travel[i];  // prefix sum
        }

        for(int i = 0; i < garbage.size(); i++)
        {
            temp = garbage[i];
            for(char ch : temp)   // getting the last index
            {
                if(ch == 'M')
                    m_index = i;
                if(ch == 'P')
                    p_index = i;
                if(ch == 'G')
                    g_index = i;
            }

            picking_time = picking_time + temp.size();
        }

        if(m_index >= 1)
            picking_time = picking_time + travel_sum[m_index - 1];
        if(p_index >= 1)
            picking_time = picking_time + travel_sum[p_index - 1];
        if(g_index >= 1)
            picking_time = picking_time + travel_sum[g_index - 1];

        return picking_time;
    }
};

/*
The time complexity of the code is O(nm), where n is the length of the garbage vector and m(max 10) is the maximum length of a string in the garbage vector.
*/
