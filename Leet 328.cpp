// Link: https://leetcode.com/problems/odd-even-linked-list/description/

class Solution {
public:
    ListNode* oddEvenList(ListNode* head) 
    {
        if(head == NULL || head->next == NULL)
            return head;

        ListNode* odd  = head;
        ListNode* even = head->next;
        
        ListNode* even_start = head->next;
        
        while(even != NULL && even->next != NULL) 
        {
            odd->next  = even->next;
            even->next = even->next->next; 
            
            odd  = odd->next;
            even = even->next;
        }
        
        odd->next = even_start;
                
        return head;
    }
};