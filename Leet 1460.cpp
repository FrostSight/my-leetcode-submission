// Link: https://leetcode.com/problems/make-two-arrays-equal-by-reversing-subarrays/description/

class Solution {
public:
    bool canBeEqual(vector<int>& target, vector<int>& arr) 
    {
        unordered_map<int, int> ump;

        for(int& t : target)
            ump[t]++;

        for(int& a : arr)
        {
            ump[a]--;
            if(ump[a] == 0)
                ump.erase(a);
        }    

        return ump.empty() == true ? true : false;
    }
};

