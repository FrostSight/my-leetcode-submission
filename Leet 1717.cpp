// Link: https://leetcode.com/problems/maximum-score-from-removing-substrings/description/

class Solution {
public:
    string removeSubstring(string& s, string& str) 
    {
        stack<char> stk;

        for (char &ch : s) 
        {
            if (!stk.empty() && stk.top() == str[0] && ch == str[1]) 
                stk.pop();
            else
                stk.push(ch);
        }

        string temp;
        while (!stk.empty()) 
        {
            temp.push_back(stk.top());
            stk.pop();
        }
        reverse(temp.begin(), temp.end());
        return temp;
    }

    int maximumGain(string s, int x, int y) 
    {
        string first_remove, second_remove, temp_s1, temp_s2;
        int score = 0, removed_pairs;

        first_remove = (x > y) ? "ab" : "ba";
        second_remove = (x < y) ? "ab" : "ba";

        temp_s1 = removeSubstring(s, first_remove);
        removed_pairs = (s.size() - temp_s1.length()) / 2;
        score += removed_pairs * max(x, y);

        temp_s2 = removeSubstring(temp_s1, second_remove);
        removed_pairs  = (temp_s1.size() - temp_s2.length()) / 2;
        score += removed_pairs * min(x, y);

        return score;
    }


};

