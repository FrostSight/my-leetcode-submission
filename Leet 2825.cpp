// Link: https://leetcode.com/problems/make-string-a-subsequence-using-cyclic-increments/

class Solution {
public:
    bool canMakeSubsequence(string str1, string str2) 
    {
        int i = 0, j = 0;

        while(i < str1.size())
        {
            if(str1[i] == str2[j]) // same character
                j++;
            else if(str1[i] != str2[j] && str1[i] + 1 == str2[j]) // not same but increment makes same
                j++;
            else if(str1[i] != str2[j] && str1[i] - 25 == str2[j]) // not same but z to a
                j++;

            i++; // i will always increase but i need to check when j will increase
        }

        return j == str2.size() ? true : false; // if j reaches to the end of the str2 means subsequence found
    }
};
