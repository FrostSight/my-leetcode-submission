// Link: https://leetcode.com/problems/row-with-maximum-ones/description/

class Solution {
public:

    int oneCounter(vector<int>& v)
    {
        int low = 0, high = v.size()-1, mid, ans;

        while(low <= high)
        {
            mid = low + (high - low) / 2;

            if(v[mid] == 1)
            {
                ans = mid;
                high = mid - 1;
            }
            else
                low = mid + 1;
        }

        int numberOfOnes = v.size() - ans;  // see below

        return numberOfOnes;
    }

    vector<int> rowAndMaximumOnes(vector<vector<int>>& mat) 
    {
        int index = 0, countMaxOnes = 0, countOnes;  // if index and countMaxOne is not initialized with -1, it will handle corner(no One in the row) case

        for(int row = 0; row < mat.size(); row++)
        {
            sort(mat[row].begin(), mat[row].end());  // sorting each row as in the given order is unsorted 

            int lastCol = mat[row].size()-1;
            if(mat[row][lastCol] == 1)  
                countOnes = oneCounter(mat[row]);
            else  // after the sorting if the last digit is 0 means there is no One so we skip searching
                continue;
            
            if(countOnes > countMaxOnes)
            {
                countMaxOnes = countOnes;
                index = row;
            }
        }    

        return {index, countMaxOnes};
    }
};


/*
v = 0 1 1 1 1 1 
v.size() - ans
= 6 - 1
= 5
in this way number of ones is easily found
*/

