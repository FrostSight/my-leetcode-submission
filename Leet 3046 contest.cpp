// Link: https://leetcode.com/contest/weekly-contest-386/problems/split-the-array/

class Solution {
public:
    bool isPossibleToSplit(vector<int>& nums) 
    {
        unordered_map<int, int> umap;
        
        for(int nm : nums)
            umap[nm]++;
        
        for(auto it = umap.begin(); it != umap.end(); it++)
        {
            if(it->second > 2)
            {
                return false;
                break;
            }
        }
        return true;
    }
};
