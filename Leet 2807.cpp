// Link: https://leetcode.com/problems/insert-greatest-common-divisors-in-linked-list/description/

class Solution {
public:
    ListNode* insertGreatestCommonDivisors(ListNode* head) 
    {
        ListNode* currentNode = head;   
        ListNode* nextNode = head->next;

        while(currentNode != NULL && currentNode->next != NULL)
        {
            ListNode* gcdNode = new ListNode(__gcd(currentNode->val, nextNode->val));
            
            gcdNode->next = nextNode;  // 2 nodes: currentNode nad gcdNode pointing to nextNode
            currentNode->next = gcdNode;  // breaking and making connection
            
            // node update
            currentNode = nextNode; // next becomes current
            nextNode = currentNode->next;  // new next is assigned
        }   

        return head;
    }
};

/*
Explanation:
let 18 -> 6
currentNode = 18, nextNode = 6, gcdNode = 6
first 2 nodes: urrentNode nad gcdNode points to nectNode so that the connection from currentNode to nextNode don't get broken and gcdNode can be the middle node
then we break connection from currentNode to nextNode and make connection from currentNode to gcdNode (and gcdNode is already pointing to nextNode)
*/