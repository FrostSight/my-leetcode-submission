// Link: https://leetcode.com/problems/the-number-of-beautiful-subsets/description/

class Solution {
public:

    int counter = 0;

    void solve(int i, vector<int> &nums, unordered_map<int, int> &ump, int k) 
    {
        if(i == nums.size()) 
        {
            counter++;
            return;
        }

        solve(i+1, nums, ump, k);  // not take
        
        if(ump[nums[i] - k] == 0 && ump[nums[i] + k] == 0) // x+k and x-k can not be taken and 0 frequency means x+k and x-k is not present on map
        {
            ump[nums[i]]++; // do
            solve(i+1, nums, ump, k);  // take
            ump[nums[i]]--; // undo as frequency is not that importent
        }
    }

    int beautifulSubsets(vector<int>& nums, int k) 
    {
        unordered_map<int, int> ump; // a frequency counter but it is not that necessary main concern is if that element exsist on map or not

        solve(0, nums, ump, k);
        
        return counter-1; // removing empty subset
    }
};