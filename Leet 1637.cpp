// Link: https://leetcode.com/problems/widest-vertical-area-between-two-points-containing-no-points/

class Solution {
public:
    int maxWidthOfVerticalArea(vector<vector<int>>& points) 
    {
        sort(points.begin(), points.end());

        int width, maxWidth = -1;

        for(int i = 1; i < points.size(); i++)
        {
            width = points[i][0] - points[i-1][0];
            maxWidth = max(width, maxWidth);
        }    

        return maxWidth;
    }
};
