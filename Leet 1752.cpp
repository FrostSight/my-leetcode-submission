// Link: https://leetcode.com/problems/check-if-array-is-sorted-and-rotated/description/

class Solution {
public:
    bool check(vector<int>& nums) 
    {
        int n = nums.size(), counter = 0;

        if(nums[n-1] > nums[0] || nums[n-1] == nums[0])  // sorted (last > first) or rotated-sorted (first and last is equal)
            counter++;

        for(int i = 1; i < n; i++)
        {
            if(nums[i-1] > nums[i])  // any middle element is smaller than its previous
                counter++;
        }

        if(counter == 1)
            return true;
        else if(counter == 2 && nums[n-1] == nums[0])
            return true;

        return false;    
    }
};

/*
usually in sorted [i-1] < [i] < [i+1]
sorted   1 2 3 4 5 here last element is greater then first element
rotated  4 5 1 2 3 here last element will not be greater then first element instead any middle element will be smaller then its previous element menas [i-1] > [i]
rotated or rotated-sorted such occurance can only be found once
but 1 1 1 or  1 1 2 1 1 in these conditions where array is rotated-sorted and first and last element is equal here such occurance will be found twice.
*/
