// Link: https://leetcode.com/problems/count-number-of-pairs-with-absolute-difference-k/

class Solution {
public:
    int countKDifference(vector<int>& nums, int k) 
    {
        unordered_map<int, int> umap;
        int counter = 0, j;

       for(int n : nums)  // finding frequency
        umap[n]++;

       for(int i = 0; i < nums.size(); i++)  // iterating each element 
       {
           for(auto it = umap.begin(); it != umap.end(); it++)
           {
               if(nums[i] - k == it->first) // if any element-k is found on map
                counter = counter + it->second;
           }
       } 
           
        return counter;    
    }
};