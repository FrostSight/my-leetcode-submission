// Link: https://leetcode.com/problems/longest-common-prefix/description/

class Solution {
public:
    string longestCommonPrefix(vector<string>& strs) 
    {
        string strs1, strs2, ans = "";
        int counter, minLength;

        sort(strs.begin(), strs.end());   

        strs1 = strs[0];
        strs2 = strs[strs.size() - 1];

        minLength = min(strs1.size(), strs2.size()); // more than minLength are sure mismatching

        for(int i = 0; i < minLength; i++)
        {   
            if(strs1[i] != strs2[i])
                break;
            else
                ans += strs1[i];
        }

        return ans;
    }
};

/*
[cluster, clue, clutch, club, clumsy]
after sorting
club
clue
clumsy
cluster
clutch

now i just need to compare the first and the last till any unmatched found. 
*/