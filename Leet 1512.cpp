// Link: https://leetcode.com/problems/number-of-good-pairs/description/

class Solution {
public:
    int numIdenticalPairs(vector<int>& nums) 
    {
        int sum  = 0, n;
        unordered_map <int, int> counter;  // unordered takes less time 

        for(int i = 0; i < nums.size(); i++)
            counter[nums[i]]++;

        for(auto it = counter.begin(); it != counter.end(); it++)
        {
            if(it->second > 1)  // to make a pair frequency has to be greater than 1
            {
                n = it->second;  // just to make code more readable
                sum = sum + (n * (n - 1) / 2);  // formula given on hint.
            }
        }
        return sum;    
    }
};
