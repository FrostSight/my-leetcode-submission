// Link: https://leetcode.com/problems/add-one-row-to-tree/description/

//  approach 1: using DFS
class Solution {
public:

    TreeNode*makeNewTree(TreeNode* root, int val, int depth, int current)
    {
        if(root == NULL)  //  terminating condition
            return NULL;
        
        if(current == depth-1) // 2nd condition
        {
            TreeNode* oldLeft = root->left;  // preserving old node
            TreeNode* oldRight = root->right;

            TreeNode* newLeft = new TreeNode(val);  // making new node
            TreeNode* newRight = new TreeNode(val);

            root->left = newLeft;  // adding new nodes (as question said) (replacing the old ones)
            root->right = newRight;

            root->left->left = oldLeft;  // old nodes are placd. root->left(current root)->(child node) = oldLeft(old node). 
            root->right->right = oldRight;
        }

        root->left = makeNewTree(root->left, val, depth, current+1);  // recursion call
        root->right = makeNewTree(root->right, val, depth, current+1);

        return root;
    }

    TreeNode* addOneRow(TreeNode* root, int val, int depth) 
    {
        if(depth == 1) // 2 conditions on question. 1st condition
        {
            TreeNode* newRoot = new TreeNode(val);  // making a new node
            newRoot->left = root; // root get connected to new node and newRoot becomes root

            return newRoot;
        }    

        TreeNode* newTree = makeNewTree(root, val, depth, 1); // 1 here is current depth

        return newTree;
    }
};