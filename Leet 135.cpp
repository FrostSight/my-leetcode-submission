// Link: https://leetcode.com/problems/candy/description/

class Solution {
public:
    int candy(vector<int>& ratings) 
    {
        int n = ratings.size(), sum = 0;    
        vector<int> rtoL(n, 1), ltoR(n, 1);

        for(int i = 1, j = n-2; i < n, j >= 0; i++, j--)
        {
            if(ratings[i-1] < ratings[i])
                rtoL[i] = rtoL[i] + rtoL[i-1];
            
            if(ratings[j] > ratings[j+1])
                ltoR[j] = ltoR[j] + ltoR[j+1];
        }

        for(int i = 0; i < n; i++)
            sum = sum + max(rtoL[i], ltoR[i]);

        return sum;        
    }
};