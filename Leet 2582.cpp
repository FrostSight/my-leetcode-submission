// Link: https://leetcode.com/problems/pass-the-pillow/

class Solution {
public:
    int passThePillow(int n, int time) 
    {
        int fullRound, time_left, pillow;;

        fullRound = time / (n-1);
        time_left = time % (n-1);

        if((fullRound & 1) == 1) // odd
            pillow = n - time_left;
        else // even
            pillow = 1 + time_left;
        
        return pillow;
    }
};

/*
1 -> 2 -> 3 -> 4 -> 5 // makes 1 fullRound
1 <- 2 <- 3 <- 4 <- 5 // makes 1 fullRound
when fullRound is odd pillow goes from n to 1
when fullRound is even pillow goes from 1 to n

Algo:
1. Find out how many fullrounds are there
2. Find time_left
3. Calculate according to time_left

*/


        
