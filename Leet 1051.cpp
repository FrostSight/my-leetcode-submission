// Link: https://leetcode.com/problems/height-checker/description/

class Solution {
public:
    int heightChecker(vector<int>& heights) 
    {
        vector<int> temp = heights;
        int counter = 0;

        sort(heights.begin(), heights.end());    

        for(int i = 0; i < heights.size(); i++)
        {
            if(heights[i] != temp[i])
                counter++;
        }

        return counter;
    }
};
