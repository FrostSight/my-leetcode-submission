// Link: https://leetcode.com/problems/longest-ideal-subsequence/

class Solution {
public:
    int longestIdealString(string s, int k) 
    {
        int n = s.length(), result = 0;       
        vector<int> temp(26, 0);

        for(int i = 0; i < n; i++) 
        {            
            int current = s[i] - 'a';
            int left = max(0, current-k);
            int right = min(25, current+k);

            int longest = 0;
            for(int j = left; j <= right; j++) 
            {
                longest = max(longest, temp[j]);
            }
            
            temp[current] = max(temp[current], longest+1);
            result = max(result, temp[current]);
        }
        
        return result;        
    }
};
