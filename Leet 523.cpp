// Link: https://leetcode.com/problems/continuous-subarray-sum/description/

#define ps_num prefixSum_nums

class Solution {
public:
    bool checkSubarraySum(vector<int>& nums, int k) 
    {
        unordered_map<int, int> ump;  
        ump[0] = -1;  
        vector<int> ps_num(nums.size(), 0);
        int sum = 0;

        for(int i = 0; i < nums.size(); i++)
        {
            sum += nums[i];
            ps_num[i] = sum;
        }

        for(int i = 0; i < ps_num.size(); i++)
        {
            int remainder = ps_num[i] % k;
            if(ump.find(remainder) != ump.end())
            {
                if(i - ump[remainder] >= 2)
                    return true;
            }
            else
                ump[remainder] = i;
        }

        return false;
    }
};

