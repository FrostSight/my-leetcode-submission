// Link: https://leetcode.com/problems/delete-node-in-a-linked-list/description/

class Solution {
public:
    void deleteNode(ListNode* node) 
    {
        ListNode* previous = NULL;

        while(node != NULL && node->next != NULL) 
        {
            node->val = node->next->val;  // replacing current value with the next one
            previous = node;  // before moving forward assigning it to "previous"
            node = node->next; // moving forward
        }

        previous->next = NULL; // making the 2nd last last
        delete(node);  // removing the last one
    }
};