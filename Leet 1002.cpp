// Link: https://leetcode.com/problems/find-common-characters/

class Solution {
public:

    void fillCountArray(string &word, vector<int>& freq) 
    {
        for(char &ch : word) 
            freq[ch-'a']++;
    }

    vector<string> commonChars(vector<string>& words) 
    {
        vector<string> result;     
        vector<int> freq(26, 0);
        
        fillCountArray(words[0], freq);

        for(int i = 1; i < words.size(); i++) 
        {
            vector<int> temp(26, 0);
            
            fillCountArray(words[i], temp);

            for(int i = 0; i < 26; i++) 
            {
                if(freq[i] != temp[i])
                    freq[i] = min(freq[i], temp[i]);
            }  
        }
        
        for(int i = 0; i < 26; i++) 
        {
            if(freq[i] != 0) 
            {
                int c = freq[i];
                while(c--) 
                {
                    result.push_back(string(1, i+'a'));
                }
            }
        }
        
        return result;
    }
};

