// Link: https://leetcode.com/problems/the-k-weakest-rows-in-a-matrix/description/


// Approach 1: Binary search

class Solution {
public:

    int oneSearch(vector<int>& row)
    {
        int low = 0, high = row.size() - 1, mid, ans = -1;

        while(low <= high)
        {
            mid = low + (high - low) / 2;

            if(row[mid] == 1)
            {
                ans = mid;
                low = mid + 1;
            }
            else
                high = mid - 1;
        }
        return ans + 1;

    }

    vector<int> kWeakestRows(vector<vector<int>>& mat, int k) 
    {
        int counter = 0, y = 0;
        vector<pair<int, int>> v;
        vector<int> ans(k);

        for(int i = 0; i < mat.size(); i++)
        {
            counter = oneSearch(mat[i]);
            v.push_back(make_pair(counter, i));
        }    

        sort(v.begin(), v.end());

        for(int i = 0; i < k; i++)
        {
            ans[i] = v[i].second;
        }

        return ans;

    }
};

// Approach 2: Using binary search and priority queue (max heap)

class Solution {
public:

    int oneSearch(vector<int>& row)
    {
        int low = 0, high = row.size() - 1, mid, ans = -1;

        while(low <= high)
        {
            mid = low + (high - low) / 2;

            if(row[mid] == 1)
            {
                ans = mid;
                low = mid + 1;
            }
            else
                high = mid - 1;
        }
        return ans + 1;
    }

    vector<int> kWeakestRows(vector<vector<int>>& mat, int k) 
    {
        int counter = 0, y;
        priority_queue<pair<int, int>> pq;
        pair<int, int> temp;
        vector<int> ans(k);

        for(int i = 0; i < mat.size(); i++)
        {
            counter = oneSearch(mat[i]);
            
            pq.push(make_pair(counter, i));
            if(pq.size() > k)
                pq.pop();

        }    

        y = k - 1;
        while(!pq.empty())
        {
            temp = pq.top();
            pq.pop();

            ans[y] = temp.second;
            y--;
        }

        return ans;
    }
};