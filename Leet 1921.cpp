// Link: https://leetcode.com/problems/eliminate-maximum-number-of-monsters/description/

class Solution {
public:
    int eliminateMaximum(vector<int>& dist, vector<int>& speed) 
    {
        int counter = 1, time_passed = 1, t;
        vector<int> monster_time; 

        for(int i = 0; i < dist.size(); i++)
        {
            t = ceil((float)dist[i] / (float)speed[i]);
            monster_time.push_back(t);
        }    

        sort(monster_time.begin(), monster_time.end());

        for(int i = 1; i < monster_time.size(); i++)
        {
            if(monster_time[i] - time_passed <= 0)
                return counter;

            counter++;
            time_passed++;
        }
        return counter;
    }
};