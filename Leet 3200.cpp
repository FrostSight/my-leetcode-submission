// Link: https://leetcode.com/problems/maximum-height-of-a-triangle/description/

class Solution {
public:

    int getHeight(int ball1, int ball2)
    {
        int height = 0, row = 1, row_length = 1;

        while(ball1 > 0 || ball2 > 0)
        {
            if(row == 1)  // 1 means odd means row = 1,3,5,7,...
            {
                if(ball1 >= row_length) 
                    ball1 -= row_length;
                else
                    break;
            }
            else	// 0 means even means row = 2,4,6,8,...
            {
                if(ball2 >= row_length) 
                    ball2 -= row_length;
                else
                    break;
            }

            height++;  // vertical height
            row_length++; // width
            row = row ^ 1; // xor operation
        }

        return height;
    }

    int maxHeightOfTriangle(int red, int blue) 
    {
        int h1 = getHeight(red, blue);
        int h2 = getHeight(blue, red);

        int mxHeight = max(h1, h2);

        return mxHeight;
    }
};

