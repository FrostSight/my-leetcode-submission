// Link: https://leetcode.com/problems/powx-n/description/

// approach 1 : recursive approach || it gives runtime error for extreamly low and high values else it works fine
#include <bits/stdc++.h>
using namespace std;

double powerCall(double x, int n)
{
    if (n == 0) 
        return 1;

    double ans;

    if (n < 0)
    {
        x = 1 / x;
        n = -n;
    }

    ans = powerCall(x, n - 1);

    return x * ans;
}

int main()
{
    double x = 0.00001, ans;
    int n = 2147483647;

    ans = powerCall(x, n);
    printf("Power is %f\n", ans);
}




// approach 2: binary exponantiation || it just works fine for all (recomanded)
double powerCall(double x, int y)
{
    if (y == 0) 
        return 1;

    double result = 1;
    int n = abs(y);
    
    while (n > 0)
    {
        if (n & 1)
            result = result * x;
        n = n / 2;
        x = x * x;
    }

    if(y < 0)
        return 1 / result;
    else
        return result;
}

class Solution {
public:
    double myPow(double x, int n) 
    {
        double ans = powerCall(x, n);
        return ans;    
    }
};
