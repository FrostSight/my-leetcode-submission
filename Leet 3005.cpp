// Link: https://leetcode.com/problems/count-elements-with-maximum-frequency/description/

class Solution {
public:
    int maxFrequencyElements(vector<int>& nums) 
    {
        vector<int> frequency(101, 0);
        int maxFreq = 0, counter = 0;

        for(int& n : nums)
        {
            frequency[n]++;
            maxFreq = max(maxFreq, frequency[n]);
        }
        
        for(int i = 0; i < frequency.size(); i++)
        {
            if(frequency[i] == maxFreq)
                counter += maxFreq;
        }
        return counter;
    }
};