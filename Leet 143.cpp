// Link: https://leetcode.com/problems/reorder-list/description/

class Solution {
public:

    ListNode* halfCut(ListNode* head)
    {
        ListNode* slow = head;
        ListNode* fast = head;

        while(fast && fast->next)
        {
            fast = fast->next->next;
            slow = slow->next;
        }

        return slow;
    }

    ListNode* reverseLinkList(ListNode* head)
    {
        if(!head || !head->next)
            return head;
        
        ListNode* tail = reverseLinkList(head->next);
        head->next->next = head;
        head->next = NULL;

        return tail;
    }

    void reorderList(ListNode* head) 
    {
        ListNode* mid = halfCut(head);   
        ListNode* reverse_head = reverseLinkList(mid);

        ListNode* current_head = head;

        while(reverse_head->next != NULL)
        {
            ListNode* temp_current_head = current_head->next;
            ListNode* temp_reverse = reverse_head->next;
   
            current_head->next = reverse_head;
            reverse_head->next = temp_current_head;

            current_head = temp_current_head;
            reverse_head = temp_reverse;
        }
    }
};
