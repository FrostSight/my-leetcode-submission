// Link: https://leetcode.com/problems/complement-of-base-10-integer/description/

class Solution {
public:
    int bitwiseComplement(int n) 
    {
        int x = n, y = 0, count = 0;
        if(n == 0)
            return 1;
        
        while(x != 0)
        {
            x = x >> 1;
            count++;
        }

        for (int i = 0; i < count; i++)
        {
            y = (y << 1) | 1;
        }

        return (~n) & y;        
    }
};

// A more simpler approach is below
// https://gitlab.com/FrostSight/my-leetcode-submission/-/blob/main/Leet%20476.cpp
