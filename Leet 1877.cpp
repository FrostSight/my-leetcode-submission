// Link: https://leetcode.com/problems/minimize-maximum-pair-sum-in-array/description/

class Solution {
public:
    int minPairSum(vector<int>& nums) 
    {
        vector <int> pairSum;

        sort(nums.begin(), nums.end());

        for (int i = 0, j = nums.size() - 1; i < nums.size(), j>= 0; i++, j--)
        {
            if (i >= j)
                break;
            
            pairSum.push_back(nums[i] + nums[j]);
        }

        auto max_value = max_element(pairSum.begin(), pairSum.end());

        return *max_value;    
    }
};
