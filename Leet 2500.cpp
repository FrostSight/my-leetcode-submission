// Link: https://leetcode.com/problems/delete-greatest-value-in-each-row/description/

class Solution {
public:
    int deleteGreatestValue(vector<vector<int>>& grid) 
    {
        int sum = 0, value;

        for(int i = 0; i < grid.size(); i++)
            sort(grid[i].begin(), grid[i].end());


        for(int j = 0; j < grid[0].size(); j++)
        {
            value = grid[0][j];

            for(int i = 1; i < grid.size(); i++)
                value = max(value, grid[i][j]);

            sum = sum + value;
        }
        return sum;
    }
};