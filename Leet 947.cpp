// Link: https://leetcode.com/problems/most-stones-removed-with-same-row-or-column/

class Solution {
public:

    void solve(vector<int>& visited, vector<vector<int>>& stones, int index)
    {
        visited[index] = true; // marking visited

        for(int i = 0; i < stones.size(); i++)
        {
            if(!visited[i] && (stones[i][0] == stones[index][0] || stones[i][1] == stones[index][1])) // not visited and (same row or same column)
                solve(visited, stones, i);
        }
    }

    int removeStones(vector<vector<int>>& stones) 
    {
        vector<int> visited(stones.size(), false);
        int numberOfStones = stones.size(), numberOfGroups = 0;

        for(int i = 0; i < stones.size(); i++)
        {
            if(visited[i] == true) // already visited
                continue;
            
            solve(visited, stones, i); 
            numberOfGroups++;
        }  

        int removedStones = numberOfStones - numberOfGroups;
        return removedStones;
    }
};

/*
There will be only one remaining from every group

let all members - 1 (alive one) and 3 groups G1, G2, G3 and summing up them
G1 + G2 + G3
= (x1-1) + (x2-1) + (x3-1)
= x1 + x2 + x3 - (1 + 1 + 1)
= numberOfStones - numberOfGroups

main Taks is to find out numberOfGroups

*/