// Link: https://leetcode.com/problems/apply-operations-to-an-array/submissions/

class Solution {
public:
    vector<int> applyOperations(vector<int>& nums) 
    {
        vector<int> ans(nums.size(), 0);
        ans[0] = nums[0];

        // performing operations
        for(int i = 0; i < nums.size()-1; i++)
        {
            if(ans[i] != nums[i+1])
                ans[i+1] = nums[i+1];
            else
            {
                ans[i] = ans[i] * 2;
                ans[i+1] = 0;
            }
        }

        // moving 0's to the end
        int left = 0, right = 0;
        while(right < ans.size())
        {
            if(ans[right] != 0)
            {
                swap(ans[left], ans[right]);
                left++;
            }

            right++;
        } 

        return ans;   
    }
};
