// Link: https://leetcode.com/problems/partitioning-into-minimum-number-of-deci-binary-numbers/description/

class Solution {
public:
    int minPartitions(string n) 
    {
        int result = 0;

        for(char c : n)
            result = max(c - 48, result);  
        
        return result;
    }
};

/*
8 2 7 3 4
-----------
1 1 1 1 1
1 1 1 1 1
1 0 1 1 1
1 0 1 0 1
1 0 1 0 0
1 0 1 0 0
1 0 1 0 0
1 0 0 0 0

easy apprach
find the number of 1s is needed to construct a digit. The highest digit has the highest number of 1s. That is the answer.
*/
