// Link: https://leetcode.com/problems/range-sum-of-sorted-subarray-sums/description/

#define pr pair<int, int>

class Solution {
public:
    int rangeSum(vector<int>& nums, int n, int left, int right) 
    {
        priority_queue<pr, vector<pr>, greater<pr>> min_pq;
        
        for (int i = 0; i < n; i++) 
            min_pq.push( {nums[i], i} );

        int result = 0, M = 1e9+7;

        for (int counter  = 1; counter  <= right; counter ++) 
        {
            auto p = min_pq.top();
            min_pq.pop();

            if (counter >= left) 
                result = (result + p.first) % M; // resu;t += p.first

            if (p.second < n-1) 
            {
                p.second++;
                p.first += nums[p.second];
                min_pq.push(p);
            }
        }
        return result;
    }
};

