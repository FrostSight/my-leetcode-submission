// Link: https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-tree/description/

class Solution {
public:
    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) 
    {
        if(root == NULL) // recursion terminating condition
            return NULL;

        if(root == p || root == q) // base condition and main root
            return root;

        TreeNode* child1 = lowestCommonAncestor(root->left, p, q);  // if right then left no problem
        TreeNode* child2 = lowestCommonAncestor(root->right, p, q);

        if(child1 != NULL && child2 != NULL) // inner root found
            return root;
        
        if(child1 == NULL)  // child2 is the parent of child1 and child2
            return child2;
        
        return child1; // child1 is the parent of child1 and child2

    }
};
