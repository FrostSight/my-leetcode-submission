// Link: https://leetcode.com/problems/special-array-ii/description/

#define ps_nums prefixSum_nums

class Solution {
public:
    vector<bool> isArraySpecial(vector<int>& nums, vector<vector<int>>& queries) 
    {
        vector<int> ps_nums(nums.size(), 0);

        for(int i = 1; i < nums.size(); i++)
        {
            if(nums[i-1] % 2 == nums[i] % 2) // same parity found so cumulatively increasing 1
                ps_nums[i] = ps_nums[i-1] + 1;
            else
                ps_nums[i] = ps_nums[i-1];
        }

        vector<bool> result(queries.size(), false);;
        int i = 0;
        for(vector<int>& query : queries) 
        {
            int start = query[0];
            int end = query[1];

            if(ps_nums[end] - ps_nums[start] == 0) // formula
                result[i] = true;
            
            i++;
        }

        return result;
    }
};
