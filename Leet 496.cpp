// Link: https://leetcode.com/problems/next-greater-element-i/description/

// approach 1a: brute force
class Solution {
public:
    vector<int> nextGreaterElement(vector<int>& nums1, vector<int>& nums2) 
    {
        vector<int> temp(nums2.size(), -1);
        vector<int> ans;

        for(int i = 0; i < nums2.size()-1; i++)
        {
            for(int j = i+1; j < nums2.size(); j++)
            {
                if(nums2[j] > nums2[i])
                {
                    temp[i] = nums2[j];
                    break;
                }
            }
        }  

        for(int i = 0; i < nums1.size(); i++)
        {
            int j = nums1[i];
            size_t index;
            auto itr  = find(nums2.begin(), nums2.end(), j);
            if(itr != nums2.end())
            {
                index = distance(nums2.begin(), itr);
            }

            int idx = (int)index;
            ans.push_back(temp[idx]);
        }

        return ans;
    }
};

// approach 1b:
class Solution {
public:
    vector<int> nextGreaterElement(vector<int>& nums1, vector<int>& nums2) 
    {
        vector<int> temp(nums2.size(), -1);
        vector<int> ans;

        for(int i = 0; i < nums2.size()-1; i++)
        {
            for(int j = i+1; j < nums2.size(); j++)
            {
                if(nums2[j] > nums2[i])
                {
                    temp[i] = nums2[j];
                    break;
                }
            }
        }  

        for(int i = 0; i < nums1.size(); i++)
        {
            int j = nums1[i];
            int index;
            auto itr  = find(nums2.begin(), nums2.end(), j);
            if(itr != nums2.end())
                index = distance(nums2.begin(), itr);

            ans.push_back(temp[index]);
        }

        return ans;
    }
};

// approach 2 
class Solution {
public:
    vector<int> nextGreaterElement(vector<int>& nums1, vector<int>& nums2) 
    {
        vector<int> result(nums1.size(), -1);
        stack<int> stk;
        unordered_map<int, int> ump;    

        for(int i = 0; i < nums2.size(); i++)
        {
            while(!stk.empty() && nums2[i] > stk.top())
            {
                ump[stk.top()] = nums2[i];
                stk.pop();
            }
            stk.push(nums2[i]);
        }

        for(int i = 0; i < nums1.size(); i++)
        {
            if(ump.count(nums1[i]) > 0)
                result[i] = ump[nums1[i]];
        }

        return result;
    }
};
