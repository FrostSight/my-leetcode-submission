// Link: https://leetcode.com/problems/linked-list-in-binary-tree/description/

class Solution {
public:

    bool check(ListNode* head, TreeNode* root)
    {
        if(head == NULL)    return true;  // terminating case and linked full found

        if(root == NULL)    return false;  // // terminating case and full tree visited but linked list not found

        if(head->val != root->val)  // check root
            return false;
        
        return check(head->next, root->left) || check(head->next, root->right);  // check left then right
    }

    bool isSubPath(ListNode* head, TreeNode* root) 
    {
        if(root == NULL)    return false;

        return check(head, root) || isSubPath(head, root->left) || isSubPath(head, root->right); // either found on root || left side || right side
    }
};
