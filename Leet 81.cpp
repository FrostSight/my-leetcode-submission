// Link: https://leetcode.com/problems/search-in-rotated-sorted-array-ii/description/

class Solution {
public:
    bool search(vector<int>& nums, int target) 
    {
        int low = 0, high = nums.size() - 1, mid;

        while(low <= high)
        {
            mid = low + (high - low) / 2;

            if(nums[mid] == target)
                return true;

            if(nums[low] == nums[mid] && nums[mid] == nums[high])
            {
                low++;
                high--;
                continue;
            }

            if(nums[low] <= nums[mid])
            {
                if(nums[low] <= target && target <= nums[mid])
                    high = mid - 1;
                else
                    low = mid + 1;
            }
            else
            {
                if(nums[mid] <= target && target <= nums[high])
                    low = mid + 1;
                else
                    high = mid - 1;
            }
        }
        return false;
    }
};



/*
approach: 2 could not pass all test case

int findMin(vector <int> &nums, int low, int high)
{
    int mid;

    while(low <= high)
    {
        if(low < high && nums[low] == nums[low + 1])
            low++;
        if(low < high && nums[high - 1] == nums[high])
            high--;
        
        mid = low + (high - low) / 2;

        if( mid > 0 && nums[mid - 1] > nums [mid])
            return nums[mid];
        else if (nums[mid] > nums[high])
            low = mid + 1;
        else
            high = mid -1;
    }
    return nums[low];
}

int searching(vector<int>& nums, int low, int high, int target)
{
    int mid; 

    while(low <= high)
    {
        mid = low + (high - low ) / 2;
        
        if(nums[mid] == target)
            return mid;
        else if (nums[mid] > target)
            high = mid - 1;
        else
            low = mid + 1;
    }
    return -1;
}

class Solution {
public:
    bool search(vector<int>& nums, int target) 
    {
        int low = 0, high = nums.size() -1, min_element, index;

        min_element = findMin(nums, low, high);

        if(min_element == target)
            return true;

        index = searching(nums, 0, min_element - 1, target);
        if(index != -1)
            return true;
        
        index = searching(nums, min_element , nums.size() - 1, target);
        if(index != -1)
            return true;
        
        return false;
    }
};

class Solution {
public:
    bool search(vector<int>& nums, int target) 
    {
        int low = 0, high = nums.size() -1, min_element;

        min_element = findMin(nums, low, high);

        int index;

        index = searching(nums, 0, min_element - 1, target);
        if(index != -1)
            return true;
        
        index = searching(nums, min_element , nums.size() - 1, target);
        if(index != -1)
            return true;
        
        return false;
    }
};
*/
