// Link: https://leetcode.com/problems/minimum-difference-between-largest-and-smallest-value-in-three-moves/description/

class Solution {
public:
    int minDifference(vector<int>& nums) 
    {
        if(nums.size() <= 3)    return 0; // corner case

        int minVal = INT_MAX, n = nums.size();

        sort(nums.begin(), nums.end());

        for(int i = 1; i <= 4; i++)
        {
            minVal = min(minVal, abs(nums[n - i] - nums[4 - i]));
        }

        return minVal;
    }
};


/*
Max move is 3
Means the combination is 
3 _ 0
2 _ 1
1 _ 2
0 _ 3

Only moving maximum and minimum elements can determine result thus sort is necessary

*/
