// Link: https://leetcode.com/problems/replace-words/description/

class Solution {
public:

    string findRoot(string& word, unordered_set<string>& st)
    {
        int i = 0;
        while(i < word.size())
        {
            string root = word.substr(0, i);
            auto it = st.find(root);
            if(it != st.end())
                return root;
            
            i++;
        }

        return word;
    }

    string replaceWords(vector<string>& dictionary, string sentence) 
    {
        unordered_set<string> st(dictionary.begin(), dictionary.end());  // st size is dictionary size along with all its elements

        stringstream ss(sentence); 
        string word, result = "";

        while(getline(ss, word, ' '))  // tokenizing to get each word separately
            result += findRoot(word, st) + " ";


        result.pop_back();   // removing the last space

        return result;
    }
};
