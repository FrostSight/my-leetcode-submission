// Link: https://leetcode.com/problems/unique-number-of-occurrences/description/

class Solution {
public:
    bool uniqueOccurrences(vector<int>& arr) 
    {
        map <int, int> counter;
        for (int i = 0; i < arr.size(); i++)
        {
            counter[arr[i]]++;
        }

        set <int> s;
        for(auto it = counter.begin(); it != counter.end(); it++)
        {
            s.insert(it->second);
        }

        if(counter.size() == s.size())
            return true;
        else
            return false;
    }
};
