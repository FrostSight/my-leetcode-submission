// Link: https://leetcode.com/problems/number-of-steps-to-reduce-a-number-in-binary-representation-to-one/description/

class Solution {
public:

    void onescomplement(string& s)
    {
        int i = s.size() - 1;
        while(i >= 0 && s[i] != '0')
        {
            s[i] = '0';
            i--;
        }

        if(i < 0)
            s.insert(0, "1");
        else
            s[i] = '1';
    }

    int numSteps(string s) 
    {
        int counter = 0;

        while(s.size() > 1)
        {
            if(s.back() == '0')
                s.pop_back();
            else
                onescomplement(s);

            counter++;
        }    

        return counter;
    }
};
