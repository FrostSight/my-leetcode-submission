// Link: https://leetcode.com/problems/take-gifts-from-the-richest-pile/description/

class Solution {
public:
    long long pickGifts(vector<int>& gifts, int k) 
    {
        priority_queue<long long> mx_pq(gifts.begin(), gifts.end());
        long long total = 0, nit_taken = 0, remain;

        for(long gift : gifts)
            total += gift;

        while(k > 0)
        {
            long long topper, returned, taken;
            topper = mx_pq.top();
            returned = topper;
            mx_pq.pop();
            taken = floor(sqrt(returned));
            nit_taken += (topper - taken);
            mx_pq.push(taken);

            k--;
        }

        remain = total - nit_taken; // this is the main theme
        return remain;
    }
};
