// Link: https://leetcode.com/problems/largest-odd-number-in-string/description/

class Solution {
public:
    string largestOddNumber(string num) 
    {
        char ch;
        int indexValue = -1;

        for (int i = num.size() - 1; i >= 0; i--)
        {
            ch = num[i];

            if(ch&1) 
            {
                indexValue = i; 
                break; 
            }
        }

        if(indexValue != -1)
            return num.substr(0, indexValue + 1);

        return "";    
    }
};
