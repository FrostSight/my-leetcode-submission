// Link: https://leetcode.com/contest/weekly-contest-384/problems/modify-the-matrix/

class Solution {
public:
    vector<vector<int>> modifiedMatrix(vector<vector<int>>& matrix) 
    {
        int m = matrix.size();
        int n = matrix[0].size();
        vector<int> maxValues(n, INT_MIN);
        
        if (m == 0) 
            return matrix;       
        
        
        for (int j = 0; j < n; ++j)
            for (int i = 0; i < m; ++i)
                maxValues[j] = max(maxValues[j], matrix[i][j]);
    
        for (int i = 0; i < m; ++i)
            for (int j = 0; j < n; ++j)
                if(matrix[i][j] == -1) 
                    matrix[i][j] = maxValues[j];
    
        return matrix;    
    }
};