// Link: https://leetcode.com/problems/fraction-addition-and-subtraction/

#define nu numerator
#define cnu current_numerator
#define de denominator
#define cde current_denominator

class Solution {
public:
    string fractionAddition(string expression) 
    {
        int nu = 0, de = 1, i = 0;  // anything/0 is undefined so initialized with 0/1
        string ans = "";

        while(i < expression.size()) 
        {
            int cnu = 0, cde = 0;
            bool isNeg = (expression[i] == '-' ? true : false);  // sign check
            
            if(expression[i] == '+' || expression[i] == '-') 
                i++; // sign check done so skipping sign 

            while(i < expression.size() && isdigit(expression[i])) // making numerator
            {
                int val = expression[i] - '0';
                cnu = (cnu * 10) + val;
                i++;
            }

            i++; 

            if(isNeg == true) // skipping "/"
                cnu *= -1;

            while(i < expression.size() && isdigit(expression[i])) // making denominator
            {
                int val = expression[i] - '0';
                cde = (cde * 10) + val;
                i++;
            }

            nu = (nu * cde) + (cnu * de); // step 3
            de = de * cde;
        }

        int GCD = abs(__gcd(nu, de)); // step 4 and abs to remove negativity due to sign

        nu /= GCD; 
        de /= GCD; 

        ans = to_string(nu) + "/" + to_string(de); // step 5
        
        return ans; 
    }
};

/*
main steps:
1. check sign
2. convert string to integet
3. perform calculation
4. find GCD (as final result should be an irreducible fraction)
5. convert string to integer
*/