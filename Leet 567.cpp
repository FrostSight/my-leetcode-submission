// Link: https://leetcode.com/problems/permutation-in-string/description/

class Solution {
public:
    bool checkInclusion(string s1, string s2) 
    {
        vector <int> s1frequency(26, 0), s2frequency(26, 0);  
        int s1len = s1.size(), s2len = s2.size(), i = 0, k = 0;

        if(s2len < s1len)  // boundary case
            return false;

        while(i < s1len) // counting alphabet frequency based on s1len as we need to look for s1 permutation
        {
            s1frequency[s1[i] - 'a']++;
            s2frequency[s2[i] - 'a']++;

            i++;
        }
        i--;

        while(i < s2len)  // sliding window running to s2 frequency
        {
            if(s1frequency == s2frequency)
                return true;
            
            i++;
            if(i < s2len)  // if frequency does not match
            {
                s2frequency[s2[i] - 'a']++;  // incrementing the next value
                s2frequency[s2[k] - 'a']--;  // decrementing former value
                k++; // decrementing pointer updated
            }
        }
        return false;    
    }
};
