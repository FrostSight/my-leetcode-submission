// Link: https://leetcode.com/problems/minimum-operations-to-exceed-threshold-value-ii/description/

using lli = long long int;

class Solution {
public:
    int minOperations(vector<int>& nums, int k) 
    {
        priority_queue<lli, vector<lli>, greater<lli>> minpq;  // min-heap so smaller values will be at top
        lli ele1, ele2, newVal; 
        int counter = 0;
        
        for(int& n : nums)
            minpq.push(n);
        
        while(minpq.size() >= 2 && minpq.top() < k)  // size at least 2 from question
        {
            ele1 = minpq.top();
            minpq.pop();
            
            ele2 = minpq.top();
            minpq.pop();
            
            newVal = (2 * min(ele1, ele2)) + max(ele1, ele2);
            minpq.push(newVal);
            
            counter++;
        }
        return counter;
    }
};