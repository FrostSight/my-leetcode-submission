// Link: https://leetcode.com/problems/count-total-number-of-colored-cells/description/

class Solution {
public:
    long long coloredCells(int n) 
    {
        long long ans = 0;

        for(int i = 1; i < n; i++)
        {
            ans += 4 * i;
        }

        return ans+1;
    }
};

/*
Simulation
n = 1 => 1
n = 2 => 5 => 1 + 4
n = 3 => 13 => 1 + 4 + 8
n = 4 => 25 => 1 + 4 + 8 + 12
n = 5 => 41 => 1 + 4 + 8 + 12 + 16
n = 6 => 61 => 1 + 4 + 8 + 12 + 16 + 20
*/