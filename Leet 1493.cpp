// Link: https://leetcode.com/problems/longest-subarray-of-1s-after-deleting-one-element/description/

class Solution {
public:
    int longestSubarray(vector<int>& nums) 
    {
        int i = 0, j = 0, zeroCounter = 0, longestWindow = -1;

        while(j < nums.size())
        {
            if(nums[j] == 0)
                zeroCounter++;
            
            while(zeroCounter > 1)  // not zeroCounter >= 1
            {
                if(nums[i] == 0)
                    zeroCounter--;

                i++;
            }

            longestWindow = max(longestWindow, j-i);  // not (j + 1 -i)

            j++;
        }    

        return longestWindow;
    }
};

/*
Approach
Maintain a sliding window where there is at most one zero on it.
*/