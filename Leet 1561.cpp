// Link: https://leetcode.com/problems/maximum-number-of-coins-you-can-get/description/

class Solution {
public:
    int maxCoins(vector<int>& piles) 
    {
        int sum = 0, i = 0, k = piles.size() - 2; // k is at 2nd max

        sort(piles.begin(), piles.end());

        while(i < k)  // if bob crosses me
        {
            sum += piles[k];
            i++;
            k -= 2; // decrementing by 2
        }   

        return sum;
    }
};

/*
after sorting 
1 2 3 4 5 6 7 8 9
B B B M A M A M A  m => me
pairs(1,8,9), (2,6,7), (3,4,5)
i need to calculate 2nd max element of pairs. Alice is not needed and bob is for tracking
*/