// Link: https://leetcode.com/problems/power-of-three/description/


// approach 1: brute force 
class Solution {
public:
    bool isPowerOfThree(int n) 
    {
        int flag = 0; 

        // corner case
        if(n <= 0)
            return false;
            
        // corner case
        if(n == 1)
            return true;
        
        while(n % 3 == 0)
        {
            n = n / 3;
            
            if(n == 1)
            {
                flag = 1;
                break;
            }
        }

        if (flag != 0)
            return true;
        else
            return false;
    }
};


// approach 2: recursive approach 
class Solution {
public:
    bool solve(int n)
    {
        if(n <= 0)  return false;

        while(n % 3 == 0)
        {
            n /= 3;
            solve(n);
        }

        return n == 1 ? true : false;
    }

    bool isPowerOfThree(int n) 
    {
        return solve(n);    
    }
};
