// Link: https://leetcode.com/problems/report-spam-message/description/

class Solution {
public:
    bool reportSpam(vector<string>& message, vector<string>& bannedWords) 
    {
        unordered_set<string> ust(bannedWords.begin(), bannedWords.end());
        int counter = 0;

        for(string word : message)
        {
            if(ust.find(word) != ust.end())
                counter++;
        }

        return counter >= 2 ? true : false;
    }
};
