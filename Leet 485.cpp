// Link: https://leetcode.com/problems/max-consecutive-ones/description/

// a more efficient approach in done on python and java

class Solution {
public:
    int findMaxConsecutiveOnes(vector<int>& nums) 
    {
        int i, counter = 0, max_counter = 0; 

        for(int i = 0; i < nums.size(); i++)
        {
            if(nums[i] == 1)
                counter++;
            else if(nums[i] == 0)
            {
                max_counter = max(max_counter, counter);
                counter = 0;
            }
        } 

        max_counter = max(max_counter, counter);

        return max_counter; 
    }
};

