// Link: https://leetcode.com/problems/length-of-longest-subarray-with-at-most-k-frequency/description/

class Solution {
public:
    int maxSubarrayLength(vector<int>& nums, int k) 
    {
        unordered_map<int, int> umap;
        int i = 0, j = 0, len, maxLen = -1;

        while(j < nums.size())
        {
            umap[nums[j]]++;

            while(umap[nums[j]] > k) // better to use while(i < j && umap[nums[j]] > k) cause i may surpass j and it is not umap[nums[i]]
            {
                umap[nums[i]]--;  // if anytime frequency gets greater than k, it shrinks
                i++;
            }

            len = j+1 -i;
            maxLen = max(len, maxLen);
            j++;
        }

        return maxLen;

    }
};
