// Link: https://leetcode.com/problems/reverse-string/description/

// approach 1:
class Solution {
public:
    void reverseString(vector<char>& s) 
    {
        stack<char> stk;
        int i = 0;

        for(char& ch : s)
            stk.push(ch);

        while(!stk.empty())    
        {            
            s[i] = stk.top();
            i++;
            stk.pop();
        }
    }
};

// approach 2:
class Solution {
public:
    void reverseString(vector<char>& s) 
    {
        int start, end;
        start = 0;
        end = s.size() - 1;

        while(start <=  end)
        {
            swap(s[start], s[end]);
            start++;
            end--;
        }
    }
};
