// Link: https://leetcode.com/problems/find-the-highest-altitude/description/

class Solution {
public:
    int largestAltitude(vector<int>& gain) 
    {
        int alt = 0, max_alt = 0;

        for(int& g : gain)
        {
            alt += g;
            max_alt = max(max_alt, alt);
        }

        return max_alt;
    }
};

/*
Failed appaorch
#define ps_gain prefixSum_gain

class Solution {
public:
    int largestAltitude(vector<int>& gain) 
    {
        vector<int> ps_gain(gain.size()+1, 0);

        ps_gain[0] = 0;
        for(int i = 1; i < gain.size(); i++)
        {
            ps_gain[i] = ps_gain[i-1] + gain[i-1];
        }

        auto ans = *max_element(ps_gain.begin(), ps_gain.end());

        return ans;
    }
};
*/