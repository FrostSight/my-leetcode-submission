// Link: https://leetcode.com/problems/minimum-operations-to-make-array-equal/description/

class Solution {
public:
    int minOperations(int n) 
    {
        int firstElement = 1, midElement, counter = 0;  // first is always 1
        int lastElement = (2 * n - 1) + 1;  // as n is the input size of array the last element is n-1

        midElement =  firstElement + (lastElement - firstElement) / 2;  // determine the middle element

        for(int i = 0; i < n / 2; i++) // summation on left and subtraction on right of the same value. So half loop.
            counter = counter + midElement - ((2 * i) + 1);

        return counter;
    }
};


/*
1 3 5
1 3 5 7 9 11 here middle is (5+7)/2
distance from middle to the first and the last element is equal. Like 5-3 = 3-1. As middle element is the one that need to be subtracted from right side of elements and added to the left side of elements to make equality. 
The challange is to figure out the middle element and subtract the left side values and count operations.
*/
