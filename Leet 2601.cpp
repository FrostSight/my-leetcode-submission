// Link: https://leetcode.com/problems/prime-subtraction-operation/

class Solution {
public:
    bool primeNums[1000];

    void seive()
    {
        fill(primeNums, primeNums+1000, true);

        primeNums[0] = false, primeNums[1] = false;
        for(int i = 2; i*i < 1000; i++)
        {
            if(primeNums[i] == true)
            {
                for(int j = i*i; j < 1000; j += i)
                {
                    primeNums[j] = false;
                }
            }
        }
    }

    bool primeSubOperation(vector<int>& nums) 
    {
        seive();

        for(int i = nums.size()-2; i >= 0; i--)
        {
            if(nums[i] < nums[i+1])
                continue;
            
            for(int p = 2; p < nums[i]; p++)
            {
                if(!primeNums[p])
                    continue;
                
                if(nums[i] - p < nums[i+1])
                {
                    nums[i] -= p;
                    break;
                }
            }

            if(nums[i] >= nums[i+1])
                return false;
        }

        return true;
    }
};
