// Link: https://leetcode.com/problems/task-scheduler/description/

class Solution {
public:
    int leastInterval(vector<char>& tasks, int n) 
    {
        vector<int> umap(26, 0);
        priority_queue<int> pq;
        int freq, counter = 0;

        for(char& ch : tasks)
            umap[ch - 'A']++; 

        for(int i = 0; i < umap.size(); i++)
        {
            if(umap[i] != 0)
                pq.push(umap[i]);
        }  

        while(!pq.empty())
        {
            vector<int> temp;

            for(int i = 1; i <= n+1; i++)
            {
                if(!pq.empty())
                {
                    freq = pq.top();
                    pq.pop();
                    freq--;
                    temp.push_back(freq);
                }
            }

            for(int& ele : temp)
            {
                if(ele > 0)
                    pq.push(ele);
            }

            if(pq.empty())
                counter += temp.size();
            else
                counter += n+1;
        }

        return counter;
    }
};