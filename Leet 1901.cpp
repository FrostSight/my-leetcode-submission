// Link: https://leetcode.com/problems/find-a-peak-element-ii/description/

class Solution {
public:

    int findMax(vector<vector<int>>& mat, int mid, int n)
    {
        int value = -1, index;
        for(int i = 0; i < n; i++)
        {
            if(mat[i][mid] > value)
            {
                value = mat[i][mid];
                index = i;
            }
        }
        return index;
    }

    vector<int> findPeakGrid(vector<vector<int>>& mat) 
    {
        int n = mat.size(), m = mat[0].size(), low, high, mid, ans, maxValue, left_side, right_side;

        low = 0;
        high = m - 1;  

        while(low <= high)
        {
            mid = low + (high - low) / 2;

            maxValue = findMax(mat, mid, n);

            if(mid-1 >= 0)
                left_side = mat[maxValue][mid-1];
            else
                left_side = -1;
            
            if(mid+1 < m)
                right_side = mat[maxValue][mid+1];
            else
                right_side = -1;
            

            if(mat[maxValue][mid] > left_side && mat[maxValue][mid] > right_side)
                return {maxValue, mid};
            else if(mat[maxValue][mid] < left_side)
                high = mid - 1;
            else
                low = mid + 1;
        }
        return {-1, -1};  
    }
};

