// Link: https://leetcode.com/problems/remove-all-adjacent-duplicates-in-string/description/

class Solution {
public:
    string removeDuplicates(string s) 
    {
        stack<char> st;
        string result = "";
    
        for (char ch : s) 
        {
            if (!st.empty() && st.top() == ch) 
                st.pop(); 
            else 
                st.push(ch);
        }

        while (!st.empty()) 
        {
            result = st.top() + result; 
            st.pop();
        }

        return result;    
    }
};
