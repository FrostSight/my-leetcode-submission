// Link: https://leetcode.com/problems/most-beautiful-item-for-each-query/description/

class Solution {
public:

    int searching(vector<vector<int>>& items, int queryPrice)
    {
        int left = 0, right = items.size()-1, mid, maxBeauty = 0;

        while(left <= right)
        {
            mid = left + (right - left) / 2;

            if(items[mid][0] <= queryPrice)
            {
                maxBeauty = max(maxBeauty, items[mid][1]);
                left = mid + 1;
            }
            else              
                right = mid - 1;
        }

        return maxBeauty;
    }

    vector<int> maximumBeauty(vector<vector<int>>& items, vector<int>& queries) 
    {
        sort(items.begin(), items.end());    

        int maxVal = 0;

        for(auto& v : items) // explanation below
        {
          if(maxVal < v[1])
            maxVal = v[1];
          else
            v[1] = maxVal;
        }

        for(int i = 0; i < queries.size(); i++)
        {
            queries[i] = searching(items, queries[i]);
        }

        return queries;
    }
};

/*
let items = [[1,2],[2,4],[3,2],[3,5],[5,6]]
now items = [[1,2],[2,4],[3,4],[3,5],[5,6]]
*/