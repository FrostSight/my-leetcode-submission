// Link: https://leetcode.com/problems/remove-all-occurrences-of-a-substring/description/

// approach 1
class Solution {
public:
    string removeOccurrences(string s, string part) 
    {
        while(s.size() != 0 && s.find(part) < s.size()) // if string size is not 0 and part in present in the string 
        {
            s.erase(s.find(part), part.size());  // find() returns the first index so erase will run from first to the length of part else string size will not decrease
        }
        return s;    
    }
};


// approach 2
class Solution {
public:
    bool check(stack<char> stk, string& part, int n) // stk is "pass by value" 
    {
        for(int i = n-1; i >= 0; i--)
        {
            if(stk.top() != part[i])
                return false;    
                
            stk.pop();
        }

        return true;
    }

    string removeOccurrences(string s, string part) 
    {
        stack<char> stk;
        int n = part.size();

        for(char ch : s)
        {
            stk.push(ch);
            
            if(stk.size() >= n && check(stk, part, n) == true)
            {
                int k = n;
                while(k > 0)
                {
                    stk.pop();
                    k--;
                }
            }
        }

        string result = "";
        while(!stk.empty())
        {
            result = stk.top() + result;
            stk.pop(); 
        }

        return result;
    }
};
