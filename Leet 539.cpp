// Link: https://leetcode.com/problems/minimum-time-difference/description/

class Solution {
public:
    int findMinDifference(vector<string>& timePoints) 
    {
        vector<int> minutes;
        int result = INT_MAX;

        for(string& time : timePoints)  // step 1
        {
            string hourTime = time.substr(0, 3);
            string minuteTime = time.substr(3, 2);

            int hour_int = stoi(hourTime);
            int minute_int = stoi(minuteTime);

            int in_minute = hour_int * 60 + minute_int;
            minutes.push_back(in_minute);
        }    

        sort(minutes.begin(), minutes.end()); // step 2

        for(int i = 1; i < minutes.size(); i++) // step 3
        {
            int adjacent_time_diff = minutes[i] - minutes[i-1];
            result = min(result, adjacent_time_diff);
        }

        int time_wrap = (24 * 60) - minutes[minutes.size()-1] + minutes[0]; // step 4
        
        result = min(result, time_wrap);

        return result;
    }
};


/*
Step - 1: Convert all timepoints to minutes
Step - 2: Sort
Step - 3: Find the difference between adjacent minutes
Step - 4: As the timepoints are cyclic, difference between the first and last minutes must be checked.
*/