// Link : https://leetcode.com/problems/find-first-and-last-position-of-element-in-sorted-array/description/

int leftmost(vector<int>& nums, int target)
{
    int mid, leftFound = -1, low = 0, high = nums.size() - 1;

    while(low <= high)
    {
        mid = low + (high - low) / 2;

        if(nums[mid] == target)
        {
            leftFound = mid;
            high = mid - 1;
           
        }
        else if(nums[mid] > target)
            high = mid - 1;
        else 
            low = mid + 1;
    }
     return leftFound;
}

int rightmost(vector<int>& nums, int target)
{
    int mid, rightFound = -1, low = 0, high = nums.size() - 1;

    while(low <= high)
    {
        mid = low + (high - low) / 2;

        if(nums[mid] == target)
        {
            rightFound = mid;
            low = mid + 1;
           
        }
        else if(nums[mid] > target)
            high = mid - 1;
        else 
            low = mid + 1;
    }
     return rightFound;
}

class Solution {
public:
    vector<int> searchRange(vector<int>& nums, int target) 
    {
        int left = leftmost(nums, target);
        int right = rightmost(nums, target);

        return {left, right};
    }
};
