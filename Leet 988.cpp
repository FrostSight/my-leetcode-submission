// Link: https://leetcode.com/problems/smallest-string-starting-from-leaf/description/

class Solution {
public:
    string result = "";

    void getString(TreeNode* root, string current)
    {
        if(root == NULL)
            return ;
        
        current = char(root->val + 'a') + current;

        if(root->left == NULL && root->right == NULL)
        {
            if(result == "" || result > current)
                result = current;
            
            return ;
        }

        getString(root->left, current);
        getString(root->right, current);
    }

    string smallestFromLeaf(TreeNode* root) 
    {
        getString(root, "");
        return result;    
    }
};