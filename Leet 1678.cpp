// Link: https://leetcode.com/problems/goal-parser-interpretation/description/

class Solution {
public:
    string interpret(string command) 
    {
        for(int i = 0; i < command.size() - 1; i++)
        {
            if(command[i] == 'G')
                continue;
            else if(command[i] == '(' && command[i+1] == ')')
            {
                command[i] = 'o';
                command[i+1] = '*';
            }
            else if(command[i] == '(' && command[i+1] == 'a')
            {
                command[i] = 'a';
                command[i+1] = 'l';
                command[i+2] = '*';
                command[i+3] = '*';
            }
        }

        command.erase(remove(command.begin(), command.end(), '*'), command.end());

        return command;    
    }
};
