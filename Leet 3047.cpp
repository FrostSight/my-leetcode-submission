// Link: https://leetcode.com/problems/find-the-largest-area-of-square-inside-two-rectangles/description/

#define ll long long
#define blx bottom_left_x
#define bly bottom_left_y
#define trx top_right_x
#define try_ top_right_y
#define sq_side square_side

class Solution {
public:
    long long largestSquareArea(vector<vector<int>>& bottomLeft, vector<vector<int>>& topRight) 
    {
        int n = bottomLeft.size();
        ll maxArea = 0;
        vector<vector<int>> rectangles(n,vector<int>(4));

        for(int i=0;i<n;i++)
        {
            rectangles[i][0] = bottomLeft[i][0];
            rectangles[i][1] = bottomLeft[i][1];
            rectangles[i][2] = topRight[i][0];
            rectangles[i][3] = topRight[i][1];
        }

        sort(rectangles.begin(),rectangles.end());
        
        
        int xf1, xf2, yf1 ,yf2;   
        int xs1, xs2, ys1, ys2;   
        int blx, bly, trx, try_;
        ll sq_side;

        for(int i = 0; i < n-1; i++)
        {
            for(int j = i+1; j < n; j++)
            {
                xf1 = rectangles[i][0];
                xf2 = rectangles[i][2];
                yf1 = rectangles[i][1];
                yf2 = rectangles[i][3];
                
                xs1 = rectangles[j][0];
                xs2 = rectangles[j][2];
                ys1 = rectangles[j][1];
                ys2 = rectangles[j][3];
                
                
                if(ys1 >= yf1 && ys1 <= yf2)  // case 1
                {  
                    if(xs1 >= xf1 && xs1 <= xf2)
                    {
                        blx = xs1;
                        bly = ys1;
                        trx = min(xs2, xf2);
                        try_ = min(yf2, ys2);
                        sq_side = min(trx - blx, try_ - bly);
                        maxArea = max(maxArea, sq_side*sq_side);
                    }
                } 
                else if(ys2 >= yf1 && ys2 <= yf2) // case 2
                {  
                    if(xs1 >= xf1 && xs1 <= xf2)
                    {
                        blx = xs1;
                        bly = yf1;
                        trx = min(xf2, xs2);
                        try_ = ys2;
                        sq_side = min(trx - blx, try_ - bly);
                        maxArea = max(maxArea, sq_side*sq_side);
                    }
                } 
                else if(xs1 >= xf1 && xs1 <= xf2) // case 3
                { 
                    if(ys1 < yf1 && ys2 > yf2)
                    {
                        blx = xs1;
                        bly = yf1;
                        trx = min(xs2, xf2);
                        try_ = yf2;
                        sq_side = min(trx - blx, try_ -bly);
                        maxArea = max(maxArea, sq_side*sq_side);
                    }
                }
            }
        }
        return maxArea;
    }
};
