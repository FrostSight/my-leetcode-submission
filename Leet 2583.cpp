// Link: https://leetcode.com/problems/kth-largest-sum-in-a-binary-tree/description/

#define ll long long

class Solution {
public:
    long kthLargestLevelSum(TreeNode* root, int k) 
    {
        if (!root) return -1;

        priority_queue<ll, vector<ll>, greater<ll>> min_qu; 
        queue<TreeNode*> qu;
        
        qu.push(root);
        
        while (!qu.empty()) 
        {
            int n = qu.size();
            ll levelSum = 0;
            while (n > 0) 
            {
                TreeNode* currentNode = qu.front();
                qu.pop();
                levelSum += currentNode->val;
                
                if (currentNode->left != NULL) 
                    qu.push(currentNode->left);
                
                if (currentNode->right != NULL) 
                    qu.push(currentNode->right);
                
                n--;
            }

            min_qu.push(levelSum);

            if (min_qu.size() > k) 
                min_qu.pop();
            }

        return min_qu.size() < k ? -1 : min_qu.top();
    }
};

