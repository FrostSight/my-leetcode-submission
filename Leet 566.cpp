// Link: https://leetcode.com/problems/reshape-the-matrix/

class Solution {
public:
    vector<vector<int>> matrixReshape(vector<vector<int>>& mat, int r, int c) 
    {
        vector<vector<int>> result(r, vector<int>(c));
        int curentRow = 0, curentCol = 0;

        if((mat.size() * mat[0].size()) != r*c) return mat; // corner case

        for(int i = 0; i < mat.size(); i++)
        {
            for(int j = 0; j < mat[0].size(); j++)
            {
                result[curentRow][curentCol] = mat[i][j];
                curentCol++;

                if(curentCol == c) // current column filled
                {
                    curentCol = 0; // new row created and current column has been reset
                    curentRow++;
                }

            }
        }

        return result;
    }
};
