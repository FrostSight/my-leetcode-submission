// Link: https://leetcode.com/problems/product-of-array-except-self/description/

class Solution {
public:
    vector<int> productExceptSelf(vector<int>& nums) 
    {
        int prod = 1, zeroCounter;
        
        zeroCounter = count(begin(nums), end(nums), 0);
        
        if(zeroCounter > 1) 
            return vector<int>(nums.size());               
        
        for(auto c : nums) 
            if(c != 0) 
                prod *= c;                                          
        
        for(int& n : nums)
        {
            if (zeroCounter != 0) 
            {
                if (n != 0) 
                    n = 0;
                else 
                    n = prod;
            }
            else 
                n = prod / n;
        }                                        
        return nums;
    }
};