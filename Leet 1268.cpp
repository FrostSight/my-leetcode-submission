// Link: https://leetcode.com/problems/search-suggestions-system/

class Solution {
public:
    vector<vector<string>> suggestedProducts(vector<string>& products, string searchWord) 
    {
        vector<vector<string>> result(searchWord.size());
        sort(products.begin(), products.end());

        int start = 0, end = products.size() - 1;

        for (int i = 0; i < searchWord.size(); i++)
        {
            char ch = searchWord[i];
            
            while (start <= end && (products[start].size() <= i || products[start][i] != ch))
                start++;
            
            while (start <= end && (products[end].size() <= i || products[end][i] != ch))
                end--;

            int remain = end - start + 1, j = 0;
            while (j < min(3, remain))
            {
                result[i].push_back(products[start + j]);
                j++;
            }
        }

        return result;    
    }
};

