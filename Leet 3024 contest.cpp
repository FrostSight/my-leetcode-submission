// Link: https://leetcode.com/contest/biweekly-contest-123/problems/type-of-triangle-ii/

class Solution {
public:
    string triangleType(vector<int>& nums) 
    {
        set<int> st;
        string s;

        if (nums[0] + nums[1] > nums[2] && nums[0] + nums[2] > nums[1] && nums[1] + nums[2] > nums[0])
        {
            for (int n : nums)
                st.insert(n);

            if (st.size() == 1)
                s = "equilateral";
            else if (st.size() == 2)
                s = "isosceles";
            else if (st.size() == 3)
                s = "scalene";
        }
        else
            s = "none";    
        
        return s;
    }
};