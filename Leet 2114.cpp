// Link: https://leetcode.com/problems/maximum-number-of-words-found-in-sentences/

class Solution {
public:
    int mostWordsFound(vector<string>& sentences) 
    {
        int spaceCounter = 0;
        vector<int> spaces;

        for(auto& sen : sentences)
        {
            for(auto& ch : sen)
            {
                if(ch == ' ')
                    spaceCounter++;
            }
            spaces.push_back(spaceCounter);
            spaceCounter = 0;
        }   

        auto word = *max_element(spaces.begin(), spaces.end());

        return word+1; // total spaces are one less than total words

    }
};