// Link: https://leetcode.com/problems/132-pattern/description/

// approach 1: brute force
class Solution {
public:
    bool find132pattern(vector<int>& nums) 
    {
        if(nums.size() < 3)
            return false;

        for(int i = 0; i < nums.size()-2; i++)
        {
            for(int j = i+1; j < nums.size()-1; j++)
            {
                for(int k = j+1; k < nums.size(); k++)
                {
                    if(nums[i] < nums[k] && nums[k] < nums[j])
                        return true;
                }
            }
        }

        return false;    
    }
};

// approach 2: using stack
class Solution {
public:
    bool find132pattern(vector<int>& nums) 
    {
        if(nums.size() < 3)
            return false;

        stack<int> stk;
        int num_k = INT_MIN; 

        for(int i = nums.size()-1; i >= 0; i--)
        {
            if(nums[i] < num_k)
                return true;

            while(!stk.empty() && nums[i] > stk.top())
            {
                num_k = stk.top();
                stk.pop();
            }

            stk.push(nums[i]);
        }   

        return false;
    }
};