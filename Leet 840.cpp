// Link: https://leetcode.com/problems/magic-squares-in-grid/description/

class Solution {
public:

    bool isMagicGrid(vector<vector<int>>& grid, int r, int c) 
    {
        unordered_set<int> ust;

        for(int i = 0; i < 3; i++) 
        {
            for(int j = 0; j < 3; j++) 
            {
                int current_num = grid[r+i][c+j];
                if(current_num < 1 || current_num > 9 || ust.count(current_num) == 1) 
                    return false;
                else 
                    ust.insert(current_num);
            }
        }

        int SUM = grid[r][c] + grid[r][c+1] + grid[r][c+2]; // any row column sum
        // Row and column
        for(int i = 0; i < 3; i++) 
        {
            if(grid[r+i][c] + grid[r+i][c+1] + grid[r+i][c+2] != SUM) 
                return false;

            if(grid[r][c+i] + grid[r+1][c+i] + grid[r+2][c+i] != SUM) 
                return false;
        }

        //diaornal and anti-diagonal
        if(grid[r][c] + grid[r+1][c+1] + grid[r+2][c+2] != SUM) 
            return false;

        if(grid[r][c+2] + grid[r+1][c+1] + grid[r+2][c] != SUM) 
            return false;
        

        return true;
    }

    int numMagicSquaresInside(vector<vector<int>>& grid) 
    {
        int rows = grid.size(), cols = grid[0].size(), counter = 0;

        for(int r = 0; r <= rows-3; r++) 
        {
            for(int c = 0; c <= cols-3; c++) 
            {
                if(isMagicGrid(grid, r, c))
                    counter++;
            }
        }

        return counter;
    }
};
