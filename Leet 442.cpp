// Link: https://leetcode.com/problems/find-all-duplicates-in-an-array/description/

class Solution {
public:
    vector<int> findDuplicates(vector<int>& nums) 
    {
        map <int, int> counter;
        for (int i = 0; i < nums.size(); i++)
        {
            counter[nums[i]]++;
        }

        vector <int> keys;
        for (auto it = counter.begin(); it != counter.end(); it++)
        {
            if(it->second == 2)
                keys.push_back(it->first);
        }

        return keys;
    }
};
