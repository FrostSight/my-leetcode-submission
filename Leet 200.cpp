// Link: https://leetcode.com/problems/number-of-islands/

#define visited '$'

class Solution {
public:

    void dfs(vector<vector<char>>& grid, int i, int j, int row, int col) 
    {
        if(i < 0 || i >= row || j < 0 || j >=col || grid[i][j] != '1')
            return;
        
        grid[i][j] = visited;

        dfs(grid, i + 1, j, row, col);
        dfs(grid, i - 1, j, row, col);
        dfs(grid, i, j + 1, row, col);
        dfs(grid, i, j - 1, row, col);

    }

    int numIslands(vector<vector<char>>& grid) 
    {   
        int row = grid.size(), col = grid[0].size(), counter = 0;

        for(int i = 0; i < row; i++) 
        {
            for(int j = 0; j < col; j++) 
            {
                if(grid[i][j] == '1') 
                {
                    counter++;
                    dfs(grid, i, j, row, col);                    
                }
            }
        }
        
        return counter;
    }
};
