// Link: https://leetcode.com/problems/reverse-substrings-between-each-pair-of-parentheses/

class Solution {
public:
    string reverseParentheses(string s) 
    {
        stack<int> bracketIdx;
        vector<int> door(s.size(), 0);
        string ans;
        int flag = 1;

        for(int i = 0; i < s.size(); i++)
        {
            if(s[i] == '(')
                bracketIdx.push(i);
            else if(s[i] == ')')
            {
                int j = bracketIdx.top();
                bracketIdx.pop();
                door[i] = j;
                door[j] = i;
            }
        }

        for(int i = 0; i < s.size(); i += flag)
        {
            if(s[i] == '(' || s[i] == ')')
            {
                i = door[i];
                flag = -flag;
            }
            else
                ans.push_back(s[i]);
        }

        return ans;
    }
};
