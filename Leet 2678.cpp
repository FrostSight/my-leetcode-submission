// Link: https://leetcode.com/problems/number-of-senior-citizens/description/

class Solution {
public:
    int countSeniors(vector<string>& details) 
    {
        int counter = 0;

        for(string person : details)
        {
            string temp = "";
            temp += person[11];
            temp += person[12];

            int age = stoi(temp);
            if (age > 60)
                counter++;
        }

        return counter;
    }
};
