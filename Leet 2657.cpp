// Link: https://leetcode.com/problems/find-the-prefix-common-array-of-two-arrays/description/

class Solution {
public:
    vector<int> findThePrefixCommonArray(vector<int>& A, vector<int>& B) 
    {
        vector<int> result;
        unordered_map<int, int> seen;    
        int counter = 0;

        for(int i = 0; i < A.size(); i++)
        {
            seen[A[i]] += 1;
            seen[B[i]] += 1;

            if(A[i] == B[i])
                counter++;
            else
            {
                if(seen[A[i]] == 2)
                    counter++;
                
                if(seen[B[i]] == 2)
                    counter++;
            }
            
            result.push_back(counter);
        }

        return result;
    }
};

/*
Explanation:
In question, it is said indirectly each array contains all distinct elements.
So 2 arrays contain each element exact twice. And that is we want
*/