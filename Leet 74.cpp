// Link: https://leetcode.com/problems/search-a-2d-matrix/description/

bool searching(vector <vector <int>>& matrix, int m, int n, int target)
{
        int low, high, mid;
        low = 0;
        high = m * n - 1;  // suppose the 2D matrix has been flatten to 1D

        while(low <= high)
        {
            mid = low + (high - low) / 2;

            if(matrix[mid / n][mid % n] == target) // [mid divide by column size = row] [mid modulus by column size = column] [2D matrix mapping]
                return true;
            else if (matrix[mid / n][mid % n] > target)
                high = mid - 1;
            else   // (matrix[mid / n][mid % n] < target)
                low = mid + 1;
        }
        return false;
}

class Solution {
public:
    bool searchMatrix(vector<vector<int>>& matrix, int target) 
    {
        int m, n;

        m = matrix.size();      // row size
        n = matrix[0].size();  // column size

        return searching(matrix, m, n, target);
    }
};
