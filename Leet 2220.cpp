// Link: https://leetcode.com/problems/minimum-bit-flips-to-convert-number/

class Solution {
public:
    int minBitFlips(int start, int goal) 
    {
        int xor_value = start ^ goal;
        int counter = 0;

        while(xor_value != 0) 
        {
            counter += (xor_value & 1); // we nned find 1, so xor with 1 gives 0 and increase the counter
            xor_value = (xor_value >> 1);  // as we can't iterate over binary digit, we do right shift
        }
        return counter;
    }
};

/*
If we perform xor operation, we get a value where the number of 1 is the desired result 
*/