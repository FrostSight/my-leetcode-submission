// Link: https://leetcode.com/problems/single-element-in-a-sorted-array/description/

class Solution {
public:
    int singleNonDuplicate(vector<int>& nums) 
    {
        int low = 0, high = nums.size() - 1, mid;

        // corner case
        if(high == 0)
            return nums[0];
        else if (nums[0] != nums[1])
            return nums[0];
        else if (nums[high] != nums[high - 1])
            return nums[high];

        while(low <= high)
        {
            mid = low + (high - low) / 2;

            if(nums[mid - 1] != nums[mid] && nums[mid] != nums[mid + 1]) // if mid is already unique
                return nums[mid];
            else if((mid % 2 == 0 && nums[mid] == nums[mid + 1]) || (mid % 2 !=0 && nums[mid - 1] == nums[mid])) // (mid is even and pair is odd-even) || (mid is odd and pair even-odd)
                low = mid + 1; // right shifting
            else 
                high = mid - 1;
        }
        return -1;    
    }
};

