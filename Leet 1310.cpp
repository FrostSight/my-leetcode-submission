// Link: https://leetcode.com/problems/xor-queries-of-a-subarray/description/

#define ps_xor prefixSum_xor

class Solution {
public:
    vector<int> xorQueries(vector<int>& arr, vector<vector<int>>& queries) 
    {
        vector<int> ps_xor(arr.size(), 0), result;
        
        ps_xor[0] = arr[0];
        for(int i = 1; i < arr.size(); i++)  // making prefix xor
            ps_xor[i] = ps_xor[i-1] ^ arr[i];
        
        for(vector<int>& query : queries)
        {
            int left = query[0], right = query[1];
            // below is basically ps_xor[right] ^ ps_xor[left-1] but this might cause an out of and in that case xor with 0
            int xor_val = ps_xor[right] ^ (left != 0 ? ps_xor[left-1] : 0);
            result.push_back(xor_val);
        }

        return result;
    }
};
