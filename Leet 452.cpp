#define current_Starting_Point csp
#define current_Ending_Point cep
#define previous_Starting_Point psp
#define previous_Ending_Point pep
#define points current

class Solution {
public:
    int findMinArrowShots(vector<vector<int>>& points) 
    {
        int current_Starting_Point, current_Ending_Point, previous_Starting_Point, previous_Ending_Point, counter = 1;

        sort(current.begin(), current.end());

        vector<int> previous = current[0];

        for(int i = 1; i < current.size(); i++)  // for [1, 6], [2, 8]
        {
            csp = current[i][0];  // 2
            cep = current[i][1];  // 8

            psp = previous[0]; // 1
            pep = previous[1]; // 6

            if(csp > pep)  // if not opverlaped
            {
                counter++;  // arrow incremented
                previous = current[i];  // previous is updated (current element becomes previous and compared to next element which becomes current)
            }
            else  // if overlaped
            {
                previous[0] = max(csp, psp);
                previous[1] = min(cep, pep);
            }
        }

        return counter;
    }
};

/*
the idea is basically about finding the overlaped area. If no overleap is found arrow increments else the overlapping aera becomes shorter.


Explanation     0  1    0  1    0  1     0   1
after sorting [[1, 6], [2, 8], [7, 12], [10, 16]]
				i[0]   i[1]    i[2]     i[3]


suppose [1, 6], [2, 8] are overlapping so the operlapping area is [2, 6]. To overlape with the next ballon, 2 points of next ballon need to inside [2, 6] inclusively else the next ballon won't be overlapping 
*/
