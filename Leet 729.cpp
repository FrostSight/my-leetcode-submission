// Link: https://leetcode.com/problems/my-calendar-i/description/

class MyCalendar {
public:
    vector<pair<int, int>> calendar;
    
    MyCalendar() 
    {}
    
    bool book(int start, int end) 
    {
        for(int i = 0; i < calendar.size(); i++) 
        {
            pair<int, int> current = calendar[i];
            if(!(end <= current.first || start >= current.second))
                return false;
        }
        
        calendar.push_back({start, end});
        
        return true;
    }
};
