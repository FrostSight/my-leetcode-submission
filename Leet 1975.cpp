// Link: https://leetcode.com/problems/maximum-matrix-sum/description/

class Solution {
public:
    long long maxMatrixSum(vector<vector<int>>& matrix) 
    {
        long long sums = 0, negativeCounter = 0, ans;
        int minValue = INT_MAX;

        for(int i = 0; i < matrix.size(); i++)
        {
            for(int j = 0; j < matrix.size(); j++)
            {
                sums += abs(matrix[i][j]);
                negativeCounter += (matrix[i][j] >= 0 ? 0 :1);
                minValue = min(minValue, abs(matrix[i][j])); // finding the minimum value in the matrix
            }
        }    

        // simple 2 case to notice
        if(negativeCounter % 2 == 0) // if even number of negative values are there
            return sums;
        else
            ans = sums - (2*minValue); // if odd numbers of negative values are there --> why 2 times see below
        
        return ans;
    }
};

/*
let matrix = 
-2 5
3 9

all cell absloute sums is = 2 + 5 + 3 + 9 = 19
all cell actual sums is = -2 + 5 + 3 + 9 = 15
19 - 15 the difference is 2 times of 2.
*/