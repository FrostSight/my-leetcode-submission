// Link: https://leetcode.com/problems/first-bad-version/description/

// The API isBadVersion is defined for you.
// bool isBadVersion(int version);

class Solution {
public:
    int firstBadVersion(int n) 
    {
        int low = 1, high = n, mid, ans;

        while(low <= high)
        {
            mid = low + (high - low) / 2;

            if(isBadVersion(mid))
            {
                ans = mid;
                high = mid - 1;
            }
            else
                low = mid + 1;
        }    
        return ans;
    }
};

/*
1 2 3 4 5 6 7 8
suppose 
bad = 4
mid  = 7
mid is definatly bad but its a possible ans, may we can find more on left
*/