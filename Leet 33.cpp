// Link: https://leetcode.com/problems/search-in-rotated-sorted-array/description/

int searching(vector<int>& nums, int low, int high, int target)
{
    int mid; 

    while(low <= high)
    {
        mid = low + (high - low ) / 2;
        
        if(nums[mid] == target)
            return mid;
        else if (nums[mid] > target)
            high = mid - 1;
        else
            low = mid + 1;
    }
    return -1;
}


class Solution {
public:
    int search(vector<int>& nums, int target) 
    {
        auto min_value = min_element(nums.begin(), nums.end());
        auto min_index = distance(nums.begin(), min_value);
        int index;

        index = searching(nums, 0, min_index - 1, target);
        if(index != -1)
            return index;
        
        index = searching(nums, min_index , nums.size() - 1, target);
        if(index != -1)
            return index;

        return -1;
    }
};
