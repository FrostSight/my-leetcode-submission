// Link: https://leetcode.com/problems/convert-1d-array-into-2d-array/description/

// approach 1 :
// first created 2D array (of 0s) then fill up the values by iteration on given 1D array
class Solution {
public:
    vector<vector<int>> construct2DArray(vector<int>& original, int m, int n) 
    {
        vector<vector<int>> result (m, vector<int> (n));

        if(original.size() != m*n)  return {};  // corner case

        for(int i = 0; i < original.size(); i++)  // iterating over the given 1D array
        {
            int row = i / n; // getting row
            int col = i % n; // getting column
            result[row][col] = original[i];
        }

        return result;
    }
};

/*
MUST REMEMBER
When we convert 1D array to 2D array, to figure location or coordinate of ith index of 1D array in 2D array is
i / total number of column = row position 
i % total number of column = column position 
*/


// approach 2: 
// first created 2D array (of 0s) then fill up the values by iteration on the created 2D array (simulation)
class Solution {
public:
    vector<vector<int>> construct2DArray(vector<int>& original, int m, int n) 
    {
        vector<vector<int>> result(m, vector<int>(n));
        int l = original.size();

        if(original.size() != m*n)  return {};

        int idx = 0;

        for (int i = 0; i < m; i++) 
        {
            for (int j = 0; j < n; j++) 
            {
                result[i][j] = original[idx];
                idx++;
            }
        }

        return result;
    }
};

