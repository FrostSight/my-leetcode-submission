class Solution {
public:

    int getLength(ListNode* head)
    {
        int counter = 1;
        while(head->next != NULL)
        {
            head = head->next;
            counter++;
        }
        return counter;
    }

    ListNode* swapNodes(ListNode* head, int k) 
    {
        int linkListLen = getLength(head); 
        int reverse_k = linkListLen-k+1;   
        ListNode* start = head;
        ListNode* end = head;

        for(int i = 1; i < k; i++) // its length not index so 1 and i am stopping 1 node head cause ->next autometically gets me the next node
            start = start->next;
        
        for(int i = 1; i < reverse_k; i++)
            end = end->next;
        
        swap(start->val, end->val);

        return head;
    }
};