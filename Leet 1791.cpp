// Link: https://leetcode.com/problems/find-center-of-star-graph/description/

class Solution {
public:
    int findCenter(vector<vector<int>>& edges) 
    {
        unordered_map<int, int> ump;
        set<int> st;
        int center;

        for(auto& edge : edges)
        {
            for(int& e : edge)
            {
                ump[e]++;
                st.insert(e);
            }
        }  

        int desired = st.size() - 1;

        for(auto it = ump.begin(); it != ump.end(); it++)
        {
            if(it->second == desired)
            {
                center = it->first;
                break;
            }
        }

        return center;
    }
};

