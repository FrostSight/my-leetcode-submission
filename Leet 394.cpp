// Link: https://leetcode.com/problems/decode-string/description/

class Solution {
public:
    string decodeString(string s) 
    {
        stack<char> stk;
        int counter;

        for(char& ch : s)
        {
            if(ch != ']')
                stk.push(ch);
            else
            {
                string temp1 = "";
                while(stk.top() != '[')
                {
                    char x = stk.top();
                    stk.pop();
                    temp1 = x + temp1;
                }

                stk.pop();  // removing '['

                string k = "";
                while(!stk.empty() && isdigit(stk.top()))
                {
                    char x = stk.top();
                    stk.pop();
                    k = x + k;
                }

                counter = stoi(k);
                string temp2 = "";
                for(int j = 0; j < counter; j++)
                    temp2 += temp1;
                
                for(char& c : temp2)
                    stk.push(c);
            }
        }    

        string ans = "";
        while(!stk.empty())
        {
            char x = stk.top();
            stk.pop();
            ans = x + ans;
        }

        return ans;
    }
};