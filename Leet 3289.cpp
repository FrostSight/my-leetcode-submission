// Link: https://leetcode.com/problems/the-two-sneaky-numbers-of-digitville/description/

class Solution {
public:
    vector<int> getSneakyNumbers(vector<int>& nums) 
    {
        vector<int> ans;    
        unordered_map<int, int> ump;

        for(int i : nums)
        {
            ump[i]++;
        }

        for(auto it = ump.begin(); it != ump.end(); it++)
        {
            if(it->second == 2)
                ans.push_back(it->first);
        }

        return ans;
    }
};

