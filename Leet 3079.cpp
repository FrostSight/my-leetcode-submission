class Solution {
public:
  int sumOfEncryptedInt(vector<int>& nums) 
  {
    int sum = 0, rem;
    
    for(int& n: nums)
    {
        int count = 0, maxDigit = -1, newNum = 0;
        
        while(n != 0)
        {
            count++;
            rem = n % 10;
            maxDigit = max(maxDigit, rem);
            n = n / 10;
        }

        while(count != 0)  // making new number consisting of maxDigit
        {
            newNum = newNum * 10 + maxDigit;
            count--;
        }

        sum += newNum;
    }
    return sum;
  }
};
