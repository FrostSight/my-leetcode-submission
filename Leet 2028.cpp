// Link: https://leetcode.com/problems/find-missing-observations/description/

class Solution {
public:
    vector<int> missingRolls(vector<int>& rolls, int mean, int n) 
    {
        int m = rolls.size(), sum, total_sum, missing_sum;
        vector<int> result;

        sum = accumulate(rolls.begin(), rolls.end(), 0);
        total_sum = mean * (m+n);
        missing_sum = total_sum - sum;

        if( (missing_sum > 6*n) || (missing_sum < n) )  // validation
            return result;
        
        while(n > 0)
        {
            int dice = min(6, missing_sum + 1 - n);  // math equation
            result.push_back(dice);
            missing_sum -= dice;
            n -= 1;
        }

        return result;
    }
};
