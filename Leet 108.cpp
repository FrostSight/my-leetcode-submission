// Link: https://leetcode.com/problems/convert-sorted-array-to-binary-search-tree/

class Solution {
public:

    TreeNode* solve(vector<int>& nums, int start, int end)
    {
        if(start > end)    return NULL;  // base condition

        int mid = start + (end - start) / 2;
        TreeNode* root = new TreeNode(nums[mid]); // mid becomes the root
        root->left = solve(nums, start, mid-1);  // recursively calling for the left sub part of nums
        root->right = solve(nums, mid+1, end);  // recursively calling for the right sub part of nums

        return root;
    }

    TreeNode* sortedArrayToBST(vector<int>& nums) 
    {
        TreeNode* ans = solve(nums, 0, nums.size()-1);

        return ans;
    }
};

