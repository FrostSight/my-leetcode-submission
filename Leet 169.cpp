// Link: https://leetcode.com/problems/majority-element/description/

class Solution {
public:
    int majorityElement(vector<int>& nums) 
    {
        unordered_map<int, int> umap;
        int val = 0, a;

        for(int n : nums)
            umap[n]++;

        for(auto it = umap.begin(); it != umap.end(); it++)
        {
            if(val < it->second)
            {
                val = it->second;
                a = it->first;
            }
        }
        return a;
    }
};