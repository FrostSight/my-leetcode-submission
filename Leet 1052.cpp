// Link: https://leetcode.com/problems/grumpy-bookstore-owner/description/

class Solution {
public:
    int maxSatisfied(vector<int>& customers, vector<int>& grumpy, int minutes) 
    {
        int total_satisfied = 0, max_unsatisfied = 0, current_unsatisfied = 0;

        for (int i = 0; i < minutes; i++) 
        {
            if (grumpy[i] == 1)
                current_unsatisfied += customers[i];
            else
                total_satisfied += customers[i];
        }

        max_unsatisfied = current_unsatisfied;

        int i = 0, j = minutes;
        while (j < customers.size()) 
        {
            if (grumpy[j] == 1)
                current_unsatisfied += customers[j];
            
            if (grumpy[i] == 1)
                current_unsatisfied -= customers[i];
            
            max_unsatisfied = max(max_unsatisfied, current_unsatisfied);
            
            if (grumpy[j] == 0)
                total_satisfied += customers[j];
            
            i++;
            j++;
        }

        return total_satisfied + max_unsatisfied;
    }
};

/*
not entirely greedy
greedily unsatisfied only
*/