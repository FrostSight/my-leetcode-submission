// Link: https://leetcode.com/problems/maximum-average-subarray-i/description/

class Solution {
public:
    double findMaxAverage(vector<int>& nums, int k) 
    {
        int i = 0, j = 0;
        double sum = 0, avg = 0, max_avg = INT_MIN;  // max_avg = -1 caused me wasted

        while(j < nums.size())
        {
            sum += nums[j];

            while(j - i + 1 > k) // shrink
            {
                sum -= nums[i];
                i++;
            }

            if (j + 1 - i == k) // optimization: no need to find average on each iteration just find find average when length is equal to k
            {
                avg = sum / k;
                max_avg = max(max_avg, avg);
            }

            j++;
        } 

        return max_avg;
    }
};
