// Link: https://leetcode.com/problems/maximum-distance-in-arrays/

class Solution {
public:
    int maxDistance(vector<vector<int>>& arrays) 
    {
        int previous_min = arrays[0].front(), previous_max = arrays[0].back(), ans = -1;

        for(int i = 1; i < arrays.size(); i++)
        {
            int current_min = arrays[i].front();
            int current_max = arrays[i].back();

            ans = max( {ans, abs(current_min - previous_max), abs(current_max - previous_min)} );  // as i am comparing 3 elements, so putting them in a vector

            previous_min = min(previous_min, current_min);
            previous_max = max(previous_max, current_max);
        }

        return ans;
    }
};
