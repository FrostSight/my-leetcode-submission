# Link: https://leetcode.com/problems/final-value-of-variable-after-performing-operations/description/

class Solution:
    def finalValueAfterOperations(self, operations: List[str]) -> int:
        umap = {}
        sum = 0
        
        for op in operations:
            if op == "++X" or op == "X++":
                umap["++X"] = umap.get("++X", 0) + 1 #  the get() method is used to check if the key exists in the dictionary. If the key does not exist, the method returns a default value of 0. This ensures that the key exists in the dictionary before trying to access it.
            elif op == "--X" or op == "X--":
                umap["--X"] = umap.get("--X", 0) - 1
        
        for key in umap:
            sum = sum + umap[key]  # [] is used to access the value of a key 
        
        return sum
        
