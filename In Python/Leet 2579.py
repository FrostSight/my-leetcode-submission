# Link: https://leetcode.com/problems/count-total-number-of-colored-cells/description/

class Solution:
    def coloredCells(self, n: int) -> int:
        ans = 1

        for i in range(1, n):
            ans += 4 * i
        
        return ans
