# Link: https://leetcode.com/problems/palindrome-partitioning/description/

class Solution:
    def isPalindrome(self, s: str, start: int, end: int) -> bool:
        while(start <= end):
            if s[start] != s[end]:
                return False
            
            start += 1
            end -= 1
        
        return True

    def partition(self, s: str) -> List[List[str]]:
        def solve(s: str, idx: int, current: List[int], result: List[List[int]]):
            if idx == len(s):
                result.append(current[:])
                return

            for i in range(idx, len(s)):
                if self.isPalindrome(s, idx, i) == True:
                    current.append(s[idx : i+1])
                    solve(s, i+1, current, result)
                    current.pop()

        current, result = [], []

        solve(s, 0, current, result)

        return result
        
