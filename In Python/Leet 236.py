# Link: https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-tree/description/

class Solution:
    def lowestCommonAncestor(self, root: 'TreeNode', p: 'TreeNode', q: 'TreeNode') -> 'TreeNode':
        if root is None:
            return None
        
        if root == p or root == q:
            return root
        
        child1 = self.lowestCommonAncestor(root.left, p, q)
        child2 = self.lowestCommonAncestor(root.right, p, q)

        if child1 != None and child2 != None:
            return root
        
        if child1 is None:
            return child2
        
        return child1

