# Link: https://leetcode.com/problems/non-decreasing-subsequences/description/

class Solution:
    def findSubsequences(self, nums: List[int]) -> List[List[int]]:
        current, result = [], []

        def solve(nums: List[int], idx: int, current: List[int], result: List[List[int]]):
            if len(current) >= 2:
                result.append(current[:])
            
            ust = set()

            for i in range(idx, len(nums)):
                if (not current or nums[i] >= current[-1]) and nums[i] not in ust:
                    current.append(nums[i])
                    solve(nums, i+1, current, result)
                    current.pop()
                    ust.add(nums[i])

        solve(nums, 0, current, result)

        return result
