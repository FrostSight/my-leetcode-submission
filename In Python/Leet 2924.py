# Link: https://leetcode.com/problems/find-champion-ii/description/

class Solution:
    def findChampion(self, n: int, edges: List[List[int]]) -> int:
        indegree = [0] * n
        champ = -1
        counter = 0

        for edge in edges:
            u, v = edge[0], edge[1]
            indegree[v] += 1
        
        for i in range(0, len(indegree)):
            if indegree[i] == 0:
                champ = i
                counter += 1
            
            if counter > 1:
                return -1
                break
        
        return champ

