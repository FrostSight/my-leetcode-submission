# Link: https://leetcode.com/problems/water-bottles/description/

class Solution:
    def numWaterBottles(self, numBottles: int, numExchange: int) -> int:
        consumed, emptyBottle = numBottles, numBottles

        while emptyBottle >= numExchange:
            newBottle = emptyBottle // numExchange
            remain = emptyBottle % numExchange
            consumed += newBottle
            emptyBottle = remain + newBottle
        
        return consumed
        
        
