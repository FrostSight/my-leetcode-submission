# Link: https://leetcode.com/problems/maximum-sum-of-distinct-subarrays-with-length-k/description/

class Solution:
    def maximumSubarraySum(self, nums: List[int], k: int) -> int:
        i, j, sums, maxSum = 0, 0, 0, 0
        ust = set()

        while j < len(nums):
            while nums[j] in ust:
                ust.discard(nums[i])
                sums -= nums[i]
                i += 1

            ust.add(nums[j])
            sums += nums[j] 

            while j-i+1 == k:
                maxSum = max(maxSum, sums)
                ust.discard(nums[i])
                sums -= nums[i]
                i += 1

            j += 1
        
        return maxSum
        
