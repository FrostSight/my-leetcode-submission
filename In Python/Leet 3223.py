# Link: https://leetcode.com/problems/minimum-length-of-string-after-operations/description/

class Solution:
    def minimumLength(self, s: str) -> int:
        freq = [0] * 26

        for ch in s:
            x = ord(ch) - ord("a")
            freq[x] += 1

            if freq[x] == 3:
                freq[x] = 1
        
        counter = sum(freq)

        return counter
