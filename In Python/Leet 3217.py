# Link: https://leetcode.com/problems/delete-nodes-from-linked-list-present-in-array/description/

class Solution:
    def modifiedList(self, nums: List[int], head: Optional[ListNode]) -> Optional[ListNode]:
        ust = set(nums)

        while head.val in ust:
            t = head
            head = head.next
            del t

        current = head

        while current.next is not None:
            if current.next.val in ust:
                t = current.next
                current.next = current.next.next
                del t
            else:
                current = current.next

        return head
