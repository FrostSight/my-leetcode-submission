# Link: https://leetcode.com/problems/adding-spaces-to-a-string/description/

class Solution:
    def addSpaces(self, s: str, spaces: List[int]) -> str:
        ans = ""
        ust = set()

        for space in spaces:
            ust.add(space)
        
        for i in range(0, len(s)):
            if i in ust:
                ans += ' '
            
            ans += s[i]
        
        return ans

