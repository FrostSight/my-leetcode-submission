# Link: https://leetcode.com/problems/merge-strings-alternately/

class Solution:
    def mergeAlternately(self, word1: str, word2: str) -> str:
        i, j = 0, len(word1)
        word4 = ""

        word3 = word1 + word2

        for i in range(0, len(word1)):
            word4 += word1[i]
            if j < len(word3):
                word4 += word3[j]
                j += 1
        
        while j < len(word3):
            word4 += word3[j]
            j += 1
        
        return word4
