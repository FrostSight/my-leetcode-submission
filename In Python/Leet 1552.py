# Link: https://leetcode.com/problems/magnetic-force-between-two-balls/description/

class Solution:

    def force(self, position: List[int], mid: int, m: int):
        current_ball = position[0]
        ball_placed = 1

        for i in range(1, len(position)):
            if abs(current_ball - position[i]) >= mid:
                ball_placed += 1

                if ball_placed == m:
                    return True
                
                current_ball = position[i]
        
        return False

    def maxDistance(self, position: List[int], m: int) -> int:
        position.sort()

        start, end = 1, position[-1]

        while(start <= end):
            mid = start + (end - start) // 2
            if self.force(position, mid, m) is True:
                ans = mid
                start = mid + 1
            else:
                end = mid - 1
        
        return ans
        

