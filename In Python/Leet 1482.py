# Link: https://leetcode.com/problems/minimum-number-of-days-to-make-m-bouquets/description/

class Solution:
    def isPossible(self, bloomDay, mid, m, k):
        counter, bouquet_counter = 0, 0

        for day in bloomDay:
            if mid >= day:
                counter += 1
                if counter == k:
                    bouquet_counter += 1
                    counter = 0
            else:
                counter = 0

        return True if bouquet_counter >= m else False

    def minDays(self, bloomDay, m, k):
        if len(bloomDay) < m * k:   return -1

        start, end = min(bloomDay), max(bloomDay)

        while start <= end:
            mid = start + (end - start) // 2

            if self.isPossible(bloomDay, mid, m, k):
                ans = mid
                end = mid - 1
            else:
                start = mid + 1

        return ans

