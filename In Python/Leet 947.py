# Link: https://leetcode.com/problems/most-stones-removed-with-same-row-or-column/

class Solution:
    def solve(self, visited, stones, index):
        visited[index] = True

        for i in range(0, len(stones)):
            if visited[i] == False and (stones[i][0] == stones[index][0] or stones[i][1] == stones[index][1]):
                self.solve(visited, stones, i)

    def removeStones(self, stones):
        visited = [False] * len(stones)
        numberOfStones, numberOfGroups = len(stones), 0

        for i in range(0, len(stones)):
            if visited[i] == True:
                continue

            self.solve(visited, stones, i)
            numberOfGroups += 1

        removedStones = numberOfStones - numberOfGroups
        return removedStones

