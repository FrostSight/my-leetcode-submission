# Link: https://leetcode.com/problems/longest-subarray-of-1s-after-deleting-one-element/description/

class Solution:
    def longestSubarray(self, nums: List[int]) -> int:
        i, j, zeroCounter, longestWindow = 0, 0, 0, 0

        while j < len(nums):
            if nums[j] == 0:
                zeroCounter += 1
            
            while zeroCounter > 1:
                if nums[i] == 0:
                    zeroCounter -= 1 
                
                i += 1

            longestWindow = max(longestWindow, j-i)

            j += 1
        
        return longestWindow
        
