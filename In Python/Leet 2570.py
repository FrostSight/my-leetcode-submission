# Link: https://leetcode.com/problems/merge-two-2d-arrays-by-summing-values/description/

class Solution:
    def mergeArrays(self, nums1: List[List[int]], nums2: List[List[int]]) -> List[List[int]]:
        result = []
        mp = defaultdict(int)

        for i in range(0, len(nums1)):
            ids = nums1[i][0]
            vals = nums1[i][1]

            mp[ids] += vals
        
        for i in range(0, len(nums2)):
            ids = nums2[i][0]
            vals = nums2[i][1]

            mp[ids] += vals
        
        for ids, vals in sorted(mp.items()): # Unlike c++ dictionary need to be sorted manually
            result.append([ids, vals])
        
        return result
