# Link: https://leetcode.com/problems/longest-palindrome/description/

class Solution:
    def longestPalindrome(self, s: str) -> int:
        if len(s) == 1: return 1

        leng = 0
        st = set()

        for i in range(0, len(s)):
            if s[i] not in st:
                st.add(s[i])
            else:
                leng += 2
                st.remove(s[i])
        
        if len(st) != 0:
            leng += 1
        
        return leng
       
