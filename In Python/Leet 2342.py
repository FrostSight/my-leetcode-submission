# Link: https://leetcode.com/problems/max-sum-of-a-pair-with-equal-sum-of-digits/description/

class Solution:
    def getDigitSum(self, n : int) -> int:
        sums = 0
        while n != 0:
            sums += n % 10
            n //= 10
        
        return sums

    def maximumSum(self, nums: List[int]) -> int:
        ump = defaultdict(int)
        result, temp = -1, -1

        for n in nums:
            digitSum = self.getDigitSum(n)

            if digitSum in ump:
                temp = ump[digitSum] + n

                if  ump[digitSum] < n:
                    ump[digitSum] = n
                
                result = max(result, temp)
            else:
                ump[digitSum] = n
        
        return result
        
