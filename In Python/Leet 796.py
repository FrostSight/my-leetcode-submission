# Link: https://leetcode.com/problems/rotate-string/

# way 1:
class Solution:
    def rotateString(self, s: str, goal: str) -> bool:

        if len(s) != len(goal):
            return False
        
        s = s + s

        if goal in s:
            return True
        
        return False
        

# way 2:
class Solution:
    def rotateString(self, s: str, goal: str) -> bool:

        if len(s) != len(goal):
            return False
        
        s = s + s

        if s.find(goal) != -1:  # if found then the index of first character of substring will be returned not -1 
            return True
        
        return False
        