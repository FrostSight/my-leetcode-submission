# Link: https://leetcode.com/problems/minimum-amount-of-time-to-collect-garbage/description/

class Solution:
    def garbageCollection(self, garbage: List[str], travel: List[int]) -> int:
        m_index = 0 
        p_index = 0 
        g_index = 0 
        picking_time = 0

        travel_sum = [0] * len(travel)
        travel_sum[0] = travel[0]
        for i in range(1, len(travel)):
            travel_sum[i] = travel_sum[i-1] + travel[i]

        for i in range(len(garbage)):

            temp = garbage[i]
            for ch in temp:
                if ch == 'M':
                    m_index = i
                if ch == 'G':
                    g_index = i
                if ch == 'P':
                    p_index = i

            picking_time = picking_time + len(temp)

        if m_index >= 1:
            picking_time = picking_time + travel_sum[m_index-1]
        if g_index >= 1:
            picking_time = picking_time + travel_sum[g_index-1]
        if p_index >= 1:
            picking_time = picking_time + travel_sum[p_index-1]

        return picking_time
        
