# Link: https://leetcode.com/problems/reshape-the-matrix/

class Solution:
    def matrixReshape(self, mat: List[List[int]], r: int, c: int) -> List[List[int]]:
        result = [[0] * c for _ in range(r)]
        currentRow, curentCol = 0, 0

        if len(mat)*len(mat[0]) != (r*c):
            return mat

        for i in range(0, len(mat)):
            for j in range(0, len(mat[0])):
                result[currentRow][curentCol] = mat[i][j]
                curentCol += 1

                if curentCol == c:
                    curentCol = 0
                    currentRow += 1
        
        return result

