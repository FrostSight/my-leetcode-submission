# Link: https://leetcode.com/problems/maximum-points-you-can-obtain-from-cards/description/

class Solution:
    def maxScore(self, cardPoints: List[int], k: int) -> int:
        i, j, currentSum, mxSum = 0, 0, 0, 0
        m = len(cardPoints) - k

        totalSum = sum(cardPoints)

        if m == 0:  return totalSum

        while j < len(cardPoints):
            currentSum += cardPoints[j]

            if j+1-i >= m:
                mxSum = max(mxSum, totalSum - currentSum)
                currentSum -= cardPoints[i]
                i += 1

            j += 1
        
        return mxSum

