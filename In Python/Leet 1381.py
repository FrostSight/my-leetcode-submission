# Link: https://leetcode.com/problems/design-a-stack-with-increment-operation/description/

class CustomStack:

    def __init__(self, maxSize: int):
        self.stk = []
        self.top_idx = -1
        self.n = maxSize

    def push(self, x: int) -> None:
        if len(self.stk) < self.n:
            self.stk.append(x)
            self.top_idx += 1
        

    def pop(self) -> int:
        if len(self.stk) == 0:
            return -1
        
        top_val = self.stk[-1]
        self.stk.pop()
        self.top_idx -= 1
        return top_val
        

    def increment(self, k: int, val: int) -> None:
        x = min(k, len(self.stk))
        for i in range(0, x):
            self.stk[i] += val
        
