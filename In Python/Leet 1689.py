# Link: https://leetcode.com/problems/partitioning-into-minimum-number-of-deci-binary-numbers/description/

class Solution:
    def minPartitions(self, n: str) -> int:
        result = 0

        for c in n:
            result = max(result, ord(c) - 48)
    
        return result
        
