# Link: https://leetcode.com/problems/string-matching-in-an-array/description/

class Solution:
    def stringMatching(self, words: List[str]) -> List[str]:
        ans = []

        for i in range(0, len(words)):
            for j in range(0, len(words)):
                if i == j:
                    continue
                
                if words[i] in words[j]:
                    ans.append(words[i])
                    break
        
        return ans
        
