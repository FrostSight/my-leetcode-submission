# Link: https://leetcode.com/problems/permutations/

class Solution:
    def permute(self, nums: List[int]) -> List[List[int]]:
        result, temp = [], []
        seen = set()

        def solve(nums: List[int], temp: List[int], seen: set, result: List[List[int]]):
            if len(temp) == len(nums):
                result.append(temp[:])
                return
            
            for i in range(0, len(nums)):
                if nums[i] not in seen:
                    seen.add(nums[i])
                    temp.append(nums[i])
                    solve(nums, temp, seen, result)
                    seen.remove(nums[i])
                    temp.pop()
        
        solve(nums, temp, seen, result)

        return result
