# Link: https://leetcode.com/problems/convert-1d-array-into-2d-array/description/

class Solution:
    def construct2DArray(self, original: List[int], m: int, n: int) -> List[List[int]]:
        result = [[0] * n for _ in range(m)] 

        if len(original) != m * n:
            return []

        for i in range(len(original)):
            row = i // n
            col = i % n
            result[row][col] = original[i]

        return result

