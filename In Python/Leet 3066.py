# Link: https://leetcode.com/problems/minimum-operations-to-exceed-threshold-value-ii/description/

class Solution:
    def minOperations(self, nums: List[int], k: int) -> int:
        minpq = []
        counter = 0

        for n in nums:
            heapq.heappush(minpq, n)
        
        while len(minpq) >= 2 and minpq[0] < k:
            ele1 = heapq.heappop(minpq)
            ele2 = heapq.heappop(minpq)

            newVal = 2 * min(ele1, ele2) + max(ele1, ele2)
            heapq.heappush(minpq, newVal)

            counter += 1
        
        return counter
