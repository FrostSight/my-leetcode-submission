# Link: https://leetcode.com/problems/minimum-operations-to-make-array-equal/description/

class Solution:
    def minOperations(self, n: int) -> int:
        firstElement = 1
        lastElement = (2 * n - 1) + 1
        midElement = firstElement + (lastElement - firstElement) // 2
        counter = 0
        
        for i in range(n // 2):
            counter = counter + (midElement - ((2 * i) + 1))
        
        return counter
        
