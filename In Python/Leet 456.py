# Link: https://leetcode.com/problems/132-pattern/description/

# approach 1: brute force
class Solution:
    def find132pattern(self, nums: List[int]) -> bool:

        if(len(nums) < 3):
            return False
        
        for i in range(len(nums) - 2):
            for j in range(i+1, len(nums) - 1):
                for k in range(j+1, len(nums)):
                    if nums[i] < nums[k] and nums[k] < nums[j]:
                        return True

        return False
        