# https://leetcode.com/problems/check-if-array-is-sorted-and-rotated/description/

class Solution:
    def check(self, nums: List[int]) -> bool:
        n = len(nums)
        counter = 0
        
        if nums[n-1] > nums[0] or nums[n-1] == nums[0]:
            counter += 1
        
        for i in range(1, n):
            if nums[i-1] > nums[i]:
                counter += 1
        
        if counter == 1:
            return True
        elif counter == 2 and nums[n-1] == nums[0]:
            return True
        
        return False
        
