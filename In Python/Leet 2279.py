# Link: https://leetcode.com/problems/maximum-bags-with-full-capacity-of-rocks/description/

class Solution:
    def maximumBags(self, capacity: List[int], rocks: List[int], additionalRocks: int) -> int:

        remaining = [0] * len(capacity)
        bag_counter = 0

        for i in range(len(capacity)):
            remaining[i] = capacity[i] - rocks[i]
        
        remaining.sort()

        for i in range(len(capacity)):
            if remaining[i] == 0 or additionalRocks >= remaining[i]:
                bag_counter += 1
                additionalRocks = additionalRocks - remaining[i]
            
        return bag_counter
        