# Link: https://leetcode.com/problems/sort-the-jumbled-numbers/description/

class Solution:

    def getMappedValue(self, mapping: List[int], n: int) -> int:
        val = 0
        placeValue = 1

        if n < 10:  return mapping[n]

        while n != 0:
            lastDigit = n % 10
            mappedValue = mapping[lastDigit]
            val += placeValue * mappedValue
            n = n // 10
            placeValue = placeValue * 10
        
        return val

    def sortJumbled(self, mapping: List[int], nums: List[int]) -> List[int]:
        result = []
        valuePair = []

        for i in range(0, len(nums)):
            mappedValue = self.getMappedValue(mapping, nums[i])
            valuePair.append( (mappedValue, i) )
        
        valuePair.sort()

        for pr in valuePair:
            originalIndex = pr[1]
            result.append(nums[originalIndex])
        
        return result
        