# Link: https://leetcode.com/problems/validate-stack-sequences/description/

class Solution:
    def validateStackSequences(self, pushed: List[int], popped: List[int]) -> bool:

        stk = []
        i, j = 0, 0

        while i < len(pushed) and j < len(popped):
            stk.append(pushed[i])

            while stk and popped[j] == stk[-1]:
                stk.pop()
                j += 1

            i += 1
        
        return True if not stk else False
        
        