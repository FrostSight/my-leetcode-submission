# Link: https://leetcode.com/problems/goal-parser-interpretation/description/

class Solution:
    def interpret(self, command: str) -> str:
        new_command = ""  # string is immutable

        for i in range(len(command)):
            if command[i] == 'G':
                new_command = new_command + 'G'
            elif command[i] == '(' and command[i+1] == ')':
                new_command = new_command + "o*"
            elif command[i] == '(' and command[i+1] == 'a':
                new_command = new_command + "al**"

        new_command =  new_command.replace('*', '')
        
        return new_command  
