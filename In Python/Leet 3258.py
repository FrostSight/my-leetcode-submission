# Link: https://leetcode.com/problems/count-substrings-that-satisfy-k-constraint-i/description/

class Solution:
    def countKConstraintSubstrings(self, s: str, k: int) -> int:
        counter1, counter0, i, j, ans = 0, 0, 0, 0, 0

        while j < len(s):
            if s[j] == '0':
                counter0 += 1
            else:
                counter1 += 1
            
            while counter0 > k and counter1 > k:
                if s[i] == '0':
                    counter0 -= 1
                else:
                    counter1 -= 1
                
                i += 1

            ans += j+1 -i
            j += 1
        
        return ans
