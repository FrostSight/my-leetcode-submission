# Link: https://leetcode.com/problems/average-waiting-time/description/

class Solution:
    def averageWaitingTime(self, customers: List[List[int]]) -> float:
        totalWait, currentTime = 0, 0
        
        for v in customers:
            arrivalTime = v[0]
            cookTime = v[1]

            if currentTime <= arrivalTime:
                currentTime = arrivalTime
            
            waitTime = currentTime + cookTime - arrivalTime
            totalWait += waitTime
            currentTime += cookTime
        
        avg = totalWait / len(customers)
        return avg

