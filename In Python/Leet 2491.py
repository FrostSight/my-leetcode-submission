# Link: https://leetcode.com/problems/divide-players-into-teams-of-equal-skill/description/

class Solution:
    def dividePlayers(self, skill: List[int]) -> int:
        skill.sort()

        i, j, result = 0, len(skill)-1, 0
        initial_sum = skill[i] + skill[j]

        while(i < j):
            currentSum = skill[i] + skill[j]

            if currentSum != initial_sum:
                return -1
            
            result += skill[i] * skill[j]

            i += 1
            j -= 1
        
        return result

