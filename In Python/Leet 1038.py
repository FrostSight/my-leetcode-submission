# Link: https://leetcode.com/problems/binary-search-tree-to-greater-sum-tree/description/

class Solution:

    def solve(self, root: TreeNode, sum: int):
        if root is None:    return

        self.solve(root.right, sum)
        sum[0] += root.val
        root.val = sum[0]
        self.solve(root.left, sum)

    def bstToGst(self, root: TreeNode) -> TreeNode:
        sum = [0]

        self.solve(root, sum)

        return root
        
