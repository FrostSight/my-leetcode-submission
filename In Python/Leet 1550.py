# Link: https://leetcode.com/problems/three-consecutive-odds/description/

# a less better approach is done in cpp
class Solution:
    def threeConsecutiveOdds(self, arr: List[int]) -> bool:
        counter = 0
        
        for i in arr:
            if i % 2 != 0:
                counter += 1
                if counter == 3:
                    return True
            else:
                counter = 0
        
        return False

