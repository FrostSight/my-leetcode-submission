# Link: https://leetcode.com/problems/time-needed-to-buy-tickets/description/

class Solution:
    def timeRequiredToBuy(self, tickets: List[int], k: int) -> int:

        qu = deque()
        time = 0

        for i in range(len(tickets)):
            qu.append(i)
        
        while True:
            if tickets[k] == 0:
                break

            current = qu.popleft()  # poping the top of queue
            tickets[current] -= 1
            time += 1
            if tickets[current] != 0:
                qu.append(current)  # pushing at back of queue
        
        return time
        
