# Link: https://leetcode.com/problems/binary-tree-preorder-traversal/description/

class Solution:

    def solve(self, root: Optional[TreeNode], ans: List[int]):
        if root is None:    return

        ans.append(root.val)

        self.solve(root.left, ans)
        self.solve(root.right, ans)

    def preorderTraversal(self, root: Optional[TreeNode]) -> List[int]:
        ans = []

        self.solve(root, ans)

        return ans
