# Link: https://leetcode.com/problems/number-of-senior-citizens/description/

class Solution:
    def countSeniors(self, details: List[str]) -> int:
        counter = 0
        
        for person in details:
            temp = person[11:13]
            age = int(temp)
            if age > 60:
                counter += 1
        
        return counter
        
