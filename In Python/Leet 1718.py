# Link: https://leetcode.com/problems/construct-the-lexicographically-largest-valid-sequence/description/

class Solution:
    def constructDistancedSequence(self, n: int) -> List[int]:
        result = [-1] * (2*n-1)
        seen = [False] * (n+1)

        def solve(idx: int, n: int, seen: List[bool], result: List[int]) -> bool:
            if idx == len(result):  return True

            if result[idx] != -1:
                return solve(idx+1, n, seen, result)
            
            for num in range(n, 0, -1):
                if seen[num] == True:
                    continue
                else:
                    seen[num] = True
                    result[idx] = num

                    if num == 1:
                        if solve(idx+1, n, seen, result) == True:
                            return True
                    else:
                        j = num + idx
                        if j < len(result) and result[j] == -1:
                            result[j] = num
                            
                            if solve(idx+1, n, seen, result) == True:
                                return True
                        
                            result[j] = -1
                
                seen[num] = False
                result[idx] = -1
            
            return False


        solve(0, n, seen, result)

        return result
