# Link: https://leetcode.com/problems/merge-in-between-linked-lists/description/

class Solution:
    def mergeInBetween(self, list1: ListNode, a: int, b: int, list2: ListNode) -> ListNode:
        
        start = None
        end = list1

        for i in range(b + 1):
            if i == a-1:
                start = end
            end = end.next

        start.next = list2
        l2 = list2

        while l2.next != None:
            l2 = l2.next

        l2.next = end

        return list1
        