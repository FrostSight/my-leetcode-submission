# Link: https://leetcode.com/problems/maximum-score-from-removing-substrings/description/

class Solution:

    def removeSubstring(self, s: str, match: str) -> str:
        stk = []

        for ch in s:
            if len(stk) > 0 and stk[-1] == match[0] and ch == match[1]:
                stk.pop()
            else:
                stk.append(ch)
        
        temp = ""
        while(len(stk) > 0):
            temp += stk[-1]
            stk.pop()
        
        temp = temp[::-1]
        
        return temp

    def maximumGain(self, s: str, x: int, y: int) -> int:
        first_remove = "ab" if x > y else "ba"
        second_remove = "ab" if x < y else "ba"

        score = 0

        temp_s1 = self.removeSubstring(s, first_remove)
        removed_pairs = (len(s) - len(temp_s1)) // 2
        score += removed_pairs * max(x, y)

        temp_s2 = self.removeSubstring(temp_s1, second_remove)
        removed_pairs = (len(temp_s1) - len(temp_s2)) // 2
        score += removed_pairs * min(x, y)

        return score

