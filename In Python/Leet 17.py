# Link: https://leetcode.com/problems/letter-combinations-of-a-phone-number/description/

class Solution:
    def letterCombinations(self, digits: str) -> List[str]:
        if len(digits) == 0:    return []

        result = [] 
        temp = ""

        ump = {
            "2" : "abc",
            "3" : "def",
            "4" : "ghi",
            "5" : "jkl",
            "6" : "mno",
            "7" : "pqrs",
            "8" : "tuv",
            "9" : "wxyz",
        }

        def solve(idx: int, digits: str, ump: dict, temp: str, result: List[str]):
            if idx == len(digits):
                result.append(temp[:])
                return
            
            ch = digits[idx]
            s = ump[ch]
            for i in range(0, len(s)):
                temp += s[i]
                solve(idx+1, digits, ump, temp, result)
                temp = temp[:-1]

        solve(0, digits, ump, temp, result)

        return result
        
