# Link: https://leetcode.com/problems/maximum-average-subarray-i/description/

class Solution:
    def findMaxAverage(self, nums: List[int], k: int) -> float:
        i, j, summation, avg = 0, 0, 0, 0
        max_avg = float("-inf")

        while j < len(nums):
            summation += nums[j]

            while j + 1 - i > k:
                summation -= nums[i]

                i += 1

            if j + 1 - i == k:
                avg = summation / k
                max_avg = max(avg, max_avg)

            j += 1
        
        return max_avg
        