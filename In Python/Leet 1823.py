# Link: https://leetcode.com/problems/find-the-winner-of-the-circular-game/description/

class Solution:
    def findTheWinner(self, n: int, k: int) -> int:
        qu = deque(range(1, n+1))

        while len(qu) != 1:
            temp = k-1
            while temp != 0:
                x = qu.popleft()
                qu.append(x)
                temp -= 1
            
            qu.popleft()
        
        x = qu[0]
        return x
        

