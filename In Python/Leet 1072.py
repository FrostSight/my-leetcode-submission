# Link: https://leetcode.com/problems/flip-columns-for-maximum-number-of-equal-rows/description/

class Solution:
    def maxEqualRowsAfterFlips(self, matrix: List[List[int]]) -> int:
        maxRows = 0

        for currentRow in matrix:
            invertRow = [-1] * len(currentRow)

            for col in range(0, len(currentRow)):
                invertRow[col] = 1 if currentRow[col] == 0 else 0
            
            counter = 0

            for row in matrix:
                if row == currentRow or row == invertRow:
                    counter += 1
            
            maxRows = max(maxRows, counter)
        
        return maxRows


