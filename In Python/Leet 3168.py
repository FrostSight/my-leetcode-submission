# Link: https://leetcode.com/problems/minimum-number-of-chairs-in-a-waiting-room/description/

class Solution:
    def minimumChairs(self, s: str) -> int:
        psChair = [0] * len(s)
        psChair[0] = 1

        for i in range(1, len(s)):
            if s[i] == 'E':
                psChair[i] = psChair[i-1] + 1
            else:
                psChair[i] = psChair[i-1] - 1
        
        ans = max(psChair)
        return ans
                

