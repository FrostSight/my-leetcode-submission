# Link: https://leetcode.com/problems/minimum-cost-to-make-array-equal/

def findCost(nums, cost, x):
    sum = 0
    for i in range(len(nums)):
        sum = sum + abs(x - nums[i]) * cost[i]
    return sum

class Solution:
    def minCost(self, nums: List[int], cost: List[int]) -> int:
        low = min(nums)
        high = max(nums)

        while low <= high:
            mid = low + (high - low) // 2

            cost1 = findCost(nums, cost, mid)
            cost2 = findCost(nums, cost, mid+1)
            minimumCost = min(cost1, cost2)
            
            if cost2 > cost1:
                high = mid - 1
            else:
                low = mid + 1
        
        return minimumCost
