# Link: https://leetcode.com/problems/largest-local-values-in-a-matrix/

class Solution:
    def findMax(self, grid: List[List[int]], i: int, j: int) -> int:
        value = 0

        for x in range(i, i+3):
            for y in range(j, j+3):
                value = max(grid[x][y], value)

        return value

    def largestLocal(self, grid: List[List[int]]) -> List[List[int]]:
        
        n = len(grid)
        ans = [[0] * (n-2) for _ in range(n-2)]

        for i in range(len(ans)):
            for j in range(len(ans[i])):
                ans[i][j] = self.findMax(grid, i, j)

        return ans
