# Link: https://leetcode.com/problems/minimized-maximum-of-products-distributed-to-any-store/

class Solution:

    def isPossible(self, quantities: List[int], mid: int, n: int):
        store = 0
        for i in range(0, len(quantities)):
            store += (quantities[i] + mid - 1) // mid
        
        return True if store <= n else False

    def minimizedMaximum(self, n: int, quantities: List[int]) -> int:
        start = 1
        end = max(quantities)

        while start <= end:
            mid = start + (end - start) // 2

            if self.isPossible(quantities, mid, n) is True:
                ans = mid
                end = mid - 1
            else:
                start = mid + 1
        
        return ans
       
