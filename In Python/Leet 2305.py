# Link: https://leetcode.com/problems/fair-distribution-of-cookies/description/

### Python code gives TLE 

# approach 1 : class based
class Solution:
    def __init__(self):
        self.result = float('inf')

    def solve(self, cookies: List[int], children: List[int], idx: int):
        if idx >= len(cookies):
            unfair = max(children)
            self.result = min(self.result, unfair)
            
            return

        for i in range(0, len(children)):
            children[i] += cookies[idx]
            self.solve(cookies, children, idx+1)
            children[i] -= cookies[idx]

    def distributeCookies(self, cookies: List[int], k: int) -> int:
        children = [0] * k
        self.solve(cookies, children, 0)
        
        return self.result

# approach 2: non class based
class Solution:
    def distributeCookies(self, cookies: List[int], k: int) -> int:
        result = pow(2, 31) - 1

        def solve(cookies: List[int], children: List[int], idx: int):
            nonlocal result
            
            if idx >= len(cookies):
                unfair = max(children)
                result = min(result, unfair)
                
                return
            
            for i in range(0, len(children)):
                children[i] += cookies[idx]
                solve(cookies, children, idx+1)
                children[i] -= cookies[idx]

        children = [0] * k
        solve(cookies, children, 0)

        return result

 