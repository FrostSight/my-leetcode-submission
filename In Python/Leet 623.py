# Link: https://leetcode.com/problems/add-one-row-to-tree/description/

# approach 1: using DFS
class Solution:

    def makeNewTree(self, root: Optional[TreeNode], val: int, depth: int, current: int) -> Optional[TreeNode]:
        if not root:
            return None
        
        if current == depth-1:
            oldLeft = root.left
            oldRight = root.right

            newLeft = TreeNode(val)
            newRight = TreeNode(val)

            root.left = newLeft
            root.right = newRight
            root.left.left = oldLeft
            root.right.right = oldRight
        
        root.left = self.makeNewTree(root.left, val, depth, current+1)
        root.right = self.makeNewTree(root.right, val, depth, current+1)

        return root

    def addOneRow(self, root: Optional[TreeNode], val: int, depth: int) -> Optional[TreeNode]:    
        if depth == 1:
            newRoot = TreeNode(val)
            newRoot.left = root

            return newRoot
        
        newTree = self.makeNewTree(root, val, depth, 1)

        return newTree