# Link: https://leetcode.com/problems/max-consecutive-ones/description/

class Solution:
    def findMaxConsecutiveOnes(self, nums: List[int]) -> int:
        i, counter, max_counter = 0, 0, 0

        while i < len(nums):
            if nums[i] == 1:
                counter += 1
                
                if counter > max_counter:
                    max_counter = counter
            else:
                counter = 0
            
            i += 1
        
        return max_counter
        
