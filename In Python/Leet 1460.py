# Link: https://leetcode.com/problems/make-two-arrays-equal-by-reversing-subarrays/description/

class Solution:
    def canBeEqual(self, target: List[int], arr: List[int]) -> bool:
        freq = Counter(target)

        for a in arr:
            freq[a] -= 1
            if freq[a] == 0:
                freq.pop(a)
        
        return True if not bool(freq) else False
        
    