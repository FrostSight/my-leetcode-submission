# Link: https://leetcode.com/problems/combination-sum/description/

class Solution:
    def solve(self, index: int, candidates: List[int], current: int, target: int, result: List[List[int]]):
        if target == 0:
            result.append(current[:])
            return

        if index >= len(candidates) or target < 0:
            return

        current.append(candidates[index])
        self.solve(index, candidates, current, target - candidates[index], result)
        current.pop()
        self.solve(index + 1, candidates, current, target, result)
        
    def combinationSum(self, candidates: List[int], target: int) -> List[List[int]]:
        result = []
        current = []

        self.solve(0, candidates, current, target, result)

        return result
