# LInk: https://leetcode.com/problems/count-the-number-of-consistent-strings/description/

class Solution:
    def countConsistentStrings(self, allowed: str, words: List[str]) -> int:
        allowset = set(allowed)
        counter = 0

        for word in words:
            status = True
            for i in range(0, len(word)):
                if word[i] not in allowset:
                    status = False
                    break
            if status: 
                counter += 1
        
        return counter
