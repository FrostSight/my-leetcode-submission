# Link: https://leetcode.com/problems/number-of-ways-to-split-array/description/

class Solution:
    def waysToSplitArray(self, nums: List[int]) -> int:
        splitCounter = 0
        ps_nums = [0] * len(nums)

        ps_nums[0] = nums[0]
        for i in range(1, len(ps_nums)):
            ps_nums[i] = ps_nums[i - 1] + nums[i]
        
        total = ps_nums[-1]

        for i in range(0, len(ps_nums)-1):
            leftSum = ps_nums[i]
            rightSum = total - leftSum
            if leftSum >= rightSum:
                splitCounter += 1
        
        return splitCounter

