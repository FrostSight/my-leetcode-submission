# Link: https://leetcode.com/problems/make-string-a-subsequence-using-cyclic-increments/description/

class Solution:
    def canMakeSubsequence(self, str1: str, str2: str) -> bool:
        i, j = 0, 0

        while i < len(str1) and j < len(str2):
            if str1[i] == str2[j]:
                j += 1
            elif str1[i] != str2[j] and (ord(str1[i]) + 1 == ord(str2[j]) ):
                j += 1
            elif str1[i] != str2[j] and (ord(str1[i]) - 25 == ord(str2[j]) ):
                j += 1
            
            i += 1
        
        return True if j == len(str2) else False
        
