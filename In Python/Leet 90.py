# Link: https://leetcode.com/problems/subsets-ii/description/

class Solution:
    def subsetsWithDup(self, nums: List[int]) -> List[List[int]]:
        ans, temp = [], []

        nums.sort()

        def solve(idx: int, nums: List[int], temp: List[int], ans: List[List[int]]):
            if idx == len(nums):
                ans.append(temp[:])
                return
            
            temp.append(nums[idx])
            solve(idx + 1, nums, temp, ans)
            temp.pop()
            while idx < len(nums)-1 and nums[idx] == nums[idx + 1]:
                idx += 1
            solve(idx + 1, nums, temp, ans)
        

        solve(0, nums, temp, ans)

        return ans

