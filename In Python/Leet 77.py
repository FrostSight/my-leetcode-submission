# Link: https://leetcode.com/problems/combinations/description/

class Solution:
    def combine(self, n: int, k: int) -> List[List[int]]:
        result, temp = [], []

        def solve(start: int, k: int, n: int, temp: List[int]):
            if k == 0:
                result.append(temp[:])
                return
            
            if start > n:  # another compulsory base condition as start will be greater eventually
                return

            temp.append(start)
            solve(start+1, k-1, n, temp)
            temp.pop()
            solve(start+1, k, n, temp)


        solve(1, k, n, temp)

        return result
        
