# Link: https://leetcode.com/problems/count-sub-islands/description/

class Solution:

    def solve(self, grid1: List[List[int]], grid2: List[List[int]], i: int, j: int) -> bool:
        if i < 0 or i >= len(grid1) or j < 0 or j >= len(grid1[0]):
            return True
        
        if grid2[i][j] != 1:
            return True
        
        grid2[i][j] = -1

        found = True if grid1[i][j] == 1 else False

        found = found & self.solve(grid1, grid2, i+1, j)
        found = found & self.solve(grid1, grid2, i-1, j)
        found = found & self.solve(grid1, grid2, i, j+1)
        found = found & self.solve(grid1, grid2, i, j-1)

        return found

    def countSubIslands(self, grid1: List[List[int]], grid2: List[List[int]]) -> int:
        subIsland = 0

        for i in range(0, len(grid2)):
            for j in range(0, len(grid2[0])):
                if grid2[i][j] == 1 and self.solve(grid1, grid2, i, j):
                    subIsland += 1
        
        return subIsland
        
