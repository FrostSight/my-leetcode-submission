# Link: https://leetcode.com/problems/maximum-height-of-a-triangle/description/

class Solution:

    def getHeight(self, ball1: int, ball2: int) -> int:
        height, row, row_length = 0, 1, 1

        while ball1 > 0 or ball2 > 0:
            if row == 1:
                if ball1 >= row_length:
                    ball1 -= row_length
                else:
                    break
            else:
                if ball2 >= row_length:
                    ball2 -= row_length
                else:
                    break
            
            height += 1
            row_length += 1
            row = row ^ 1
        
        return height

    def maxHeightOfTriangle(self, red: int, blue: int) -> int:
        h1 = self.getHeight(red, blue)
        h2 = self.getHeight(blue, red)

        mxHeight = max(h1, h2)

        return mxHeight
        