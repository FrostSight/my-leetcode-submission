# Link: https://leetcode.com/problems/most-beautiful-item-for-each-query/description/

class Solution:

    def searching(self, items: List[List[int]], queryPrice: int) -> int:
        start, end, maxBeauty = 0, len(items) - 1, 0

        while start <= end:
            mid = start + (end - start) // 2

            if items[mid][0] <= queryPrice:
                maxBeauty = max(maxBeauty, items[mid][1])
                start = mid + 1
            else:
                end = mid - 1
        
        return maxBeauty

    def maximumBeauty(self, items: List[List[int]], queries: List[int]) -> List[int]:
        items.sort()

        maxVal = 0

        for v in items:
            if maxVal < v[1]:
                maxVal = v[1]
            else:
                v[1] = maxVal

        for i in range(0, len(queries)):
            queries[i] = self.searching(items, queries[i])
        
        return queries

