# Link: https://leetcode.com/problems/maximum-score-after-splitting-a-string/

class Solution:
    def maxScore(self, s: str) -> int:
        n = len(s)
        ones = 0
        zeros = 0
        score = float('-inf')

        for i in range(0, n-1):
            if s[i] == '1':
                ones+=1
            else:
                zeros+=1
            
            score = max(score, zeros - ones)
        
        if s[n-1] == '1':
            ones+=1
        
        return score + ones
        
