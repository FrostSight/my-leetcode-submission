# Link: https://leetcode.com/problems/prime-subtraction-operation/

class Solution:
    def __init__(self):
        self.primeNums = [True] * 1000

    def seive(self):
        self.primeNums[0], self.primeNums[1] = False, False
        for i in range(2, int(len(self.primeNums) ** 0.5) + 1):
            if self.primeNums[i]:
                for j in range(i * i, len(self.primeNums), i):
                    self.primeNums[j] = False

    def primeSubOperation(self, nums: List[int]) -> bool:
        self.seive()

        for i in range(len(nums) - 2, -1, -1):
            if nums[i] < nums[i + 1]:
                continue
            
            for p in range(2, nums[i]):
                if not self.primeNums[p]:
                    continue
                
                if nums[i] - p < nums[i + 1]:
                    nums[i] -= p
                    break

            if nums[i] >= nums[i + 1]:
                return False
        
        return True

