# Link: https://leetcode.com/problems/check-if-n-and-its-double-exist/description/

class Solution:
    def checkIfExist(self, arr: List[int]) -> bool:
        ump = {}

        for i in range(0, len(arr)):
            ump[arr[i]] = i
        
        for j in range(0, len(arr)):
            desired = arr[j] * 2

            if desired in ump and ump[desired] != j:
                return True
        
        return False
