# Link: https://leetcode.com/problems/string-compression/description/

class Solution:
    def compress(self, chars: List[str]) -> int:
        
        i, j, ans = 0, 0, 0

        while i < len(chars):
            while j < len(chars) and chars[i] == chars[j]:
                j += 1
            
            chars[ans] = chars[i]
            ans += 1

            counter = j - i
            if counter > 1:
                converted_counter = str(counter)
                for ch in converted_counter:
                    chars[ans] = ch
                    ans += 1
            
            i = j
        
        return ans

        