# Link: https://leetcode.com/problems/boats-to-save-people/description/

class Solution:
    def numRescueBoats(self, people: List[int], limit: int) -> int:

        boat_counter = 0
        i = 0

        people.sort()

        j = len(people) - 1

        if people[j] > limit:
            return boat_counter
        
        while i <= j:
            if people[i] + people[j] <= limit:
                boat_counter += 1
                i += 1
                j -= 1
            else:
                boat_counter += 1
                j -= 1
        
        return boat_counter
        
