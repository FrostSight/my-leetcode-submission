# Link: https://leetcode.com/problems/largest-positive-integer-that-exists-with-its-negative/description/

class Solution:
    def findMaxK(self, nums: List[int]) -> int:

        if len(nums) == 0:
            return -1

        nums.sort()

        i, j = 0, len(nums)-1
        while i < j:
            sum = nums[i] + nums[j]
            if sum < 0:
                i += 1
            elif sum > 0:
                j -= 1
            else:
                return nums[j]
        
        return -1
        
        