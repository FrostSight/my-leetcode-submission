# Link: https://leetcode.com/problems/maximum-ascending-subarray-sum/description/

class Solution:
    def maxAscendingSum(self, nums: List[int]) -> int:
        ans = nums[0]
        i = 0        
        while i in range(len(nums)-1):
            sums = nums[i]
            j = i+1
            while j < len(nums) and nums[j] > nums[j-1]:
                sums += nums[j]
                j += 1
            
            i = j
            ans = max(ans, sums)
        
        return ans
        
