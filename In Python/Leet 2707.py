# Link: https://leetcode.com/problems/extra-characters-in-a-string/description/

class Solution:
    def __init__(self):
        self.t = [-1] * 51
    
    def solve(self, idx: int, s: str, ust: set, n: int) -> int:
        if idx >= n:    return 0

        if self.t[idx] != -1:
            return self.t[idx]
        
        counter = float('inf')
        counter = 1 + self.solve(idx + 1, s, ust, n)

        for j in range(idx, n):
            current = s[idx:j + 1]
            if current in ust:
                counter = min(counter, self.solve(j + 1, s, ust, n))

        self.t[idx] = counter
        return counter

    def minExtraChar(self, s: str, dictionary: List[str]) -> int:
        self.t = [-1] * 51
        ust = set(dictionary)
        
        result = self.solve(0, s, ust, len(s))

        return result

