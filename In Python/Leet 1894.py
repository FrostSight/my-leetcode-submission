# Link: https://leetcode.com/problems/find-the-student-that-will-replace-the-chalk/

class Solution:
    def chalkReplacer(self, chalk: List[int], k: int) -> int:
        ans = 0
        chalk_sum = sum(chalk)
        chalk_remain = k % chalk_sum

        for i in range(0, len(chalk)):
            if chalk_remain < chalk[i]:
                ans = i
                break

            chalk_remain -= chalk[i]

        return ans
