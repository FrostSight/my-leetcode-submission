# Link: https://leetcode.com/problems/get-equal-substrings-within-budget/description/

class Solution:
    def equalSubstring(self, s: str, t: str, maxCost: int) -> int:

        currentCost, mxLen, i, j = 0, 0, 0, 0

        while j < len(s):
            currentCost += abs(ord(s[j]) - ord(t[j]))  # ord for getting ascii values
            
            while currentCost > maxCost:
                currentCost -= abs(ord(s[i]) - ord(t[i]))
                i += 1
            
            mxLen = max(mxLen, j+1 -i)

            j += 1
        
        return mxLen
        