# Link: https://leetcode.com/problems/replace-words/description/

class Solution:

    def findRoot(self, word: str, st: set) -> str:
        i = 0
        while i < len(word):
            root = word[0 : i]
            if root in st:
                return root

            i += 1
        
        return word

    def replaceWords(self, dictionary: List[str], sentence: str) -> str:
        st = set(dictionary)
        word = sentence.split()
        result = [self.findRoot(w, st) for w in word]
        result =  " ".join(result)

        return result

