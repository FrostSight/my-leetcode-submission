// Link: https://leetcode.com/problems/remove-colored-pieces-if-both-neighbors-are-the-same-color/description/

class Solution:
    def winnerOfGame(self, colors: str) -> bool:

        counterA = 0
        counterB = 0

        for i in range(1, len(colors)-1):
            if colors[i] == 'A' and colors[i-1] == 'A' and colors[i+1] == 'A':
                counterA += 1
            elif colors[i] == 'B' and colors[i-1] == 'B' and colors[i+1] == 'B':
                counterB += 1
        
        if counterA > counterB:
            return True
        else:
            return False