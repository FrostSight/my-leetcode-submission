# Link: https://leetcode.com/problems/n-ary-tree-postorder-traversal/description/

class Solution:

    def solve(self, root: 'Node', ans: List[int]):
        if root is None:    return ans

        for i in range(0, len(root.children)):
            self.solve(root.children[i], ans)
        
        ans.append(root.val)

    def postorder(self, root: 'Node') -> List[int]:
        ans = []
        self.solve(root, ans)
        return ans

