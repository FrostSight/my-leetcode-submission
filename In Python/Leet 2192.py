# Link: https://leetcode.com/problems/all-ancestors-of-a-node-in-a-directed-acyclic-graph/description/

class Solution:

    def traverse(self, ancestor: int, currentNode: int, graph: List[List[int]], result: List[List[int]]):
        for successor in graph[currentNode]:
            if not result[successor] or result[successor][-1] != ancestor:
                result[successor].append(ancestor)
                self.traverse(ancestor, successor, graph, result)

    def getAncestors(self, n: int, edges: List[List[int]]) -> List[List[int]]:
        graph = [[] for i in range(n)]
        result = [[] for i in range(n)]

        for u, v in edges:
            graph[u].append(v)
        
        for u in range(n):
            ancestor = u
            self.traverse(ancestor, u, graph, result)
        
        return result


        