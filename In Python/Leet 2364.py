# Link: https://leetcode.com/problems/count-number-of-bad-pairs/description/

class Solution:
    def countBadPairs(self, nums: List[int]) -> int:
        diffs = [-1] * len(nums)
        badPair = 0

        for i in range(0, len(nums)):
            diffs[i] = nums[i] - i

        ump = defaultdict(int)
        ump[nums[0]] = 1
        for j in range(1, len(diffs)):
            totalPair = j
            goodPair = ump[diffs[j]]
            badPair += totalPair - goodPair
            ump[diffs[j]] += 1

        return badPair
