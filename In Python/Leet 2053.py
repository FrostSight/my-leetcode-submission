# Link: https://leetcode.com/problems/kth-distinct-string-in-an-array/description/

class Solution:
    def kthDistinct(self, arr: List[str], k: int) -> str:
        freq = Counter(arr)
        ans = ""

        for s in arr:
            if freq[s] > 1:
                continue
            
            k -= 1
            if k == 0:
                ans = s
                break
        
        return ans
        
        