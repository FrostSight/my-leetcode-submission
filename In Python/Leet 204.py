# Link: https://leetcode.com/problems/count-primes/description/

class Solution:
    def countPrimes(self, n: int) -> int:
        if n <= 2:
            return 0

        isPrime = [True] * n
        isPrime[0] = isPrime[1] =False

        for i in range(2, int(n**0.5) + 1):
            if isPrime[i] == True:
                for j in range(i*i, n, i):
                    isPrime[j] = False

        primrCount = 0
        for i in range(2, n):
            if isPrime[i] == True:
                primrCount += 1

        return primrCount
        
