# Link: https://leetcode.com/problems/bag-of-tokens/description/

class Solution:
    def bagOfTokensScore(self, tokens: List[int], power: int) -> int:

        score = 0 
        max_score = 0 
        i = 0 
        j = len(tokens) - 1

        tokens.sort()

        while i <= j:
            if power >= tokens[i]:
                power = power - tokens[i]
                i += 1
                score += 1

                max_score = max(score, max_score)

            elif score >= 1 and i != j:
                power = power + tokens[j]
                j -= 1
                score -= 1

            else:
                break
        
        return max_score