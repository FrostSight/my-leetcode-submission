# Link: https://leetcode.com/problems/word-search/

class Solution:
    def __init__(self):
        self.row, self.col = 0, 0
        self.directions = [[1, 0], [-1, 0], [0, 1], [0, -1]]
    
    def find(self, board: List[List[str]], i: int, j: int, word: str, idx: int) -> bool:
        if idx == len(word):    return True

        if i < 0 or i >= self.row or j < 0 or j >= self.col or board[i][j] != word[idx] or board[i][j] == "$":  return False

        temp = board[i][j]
        board[i][j] = "$"
        for direction in self.directions:
            new_i = i + direction[0]
            new_j = j + direction[1]

            found = self.find(board, new_i, new_j, word, idx+1)
            if found == True:
                return True
                
        board[i][j] = temp
        
        return False

    def exist(self, board: List[List[str]], word: str) -> bool:
        self.row = len(board)
        self.col = len(board[0])

        for i in range(self.row):
            for j in range(self.col):
                if board[i][j] == word[0]:
                    if self.find(board, i, j, word, 0):
                        return True
        
        return False

