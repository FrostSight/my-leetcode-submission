# Link: https://leetcode.com/problems/row-with-maximum-ones/description/

class Solution:
    def oneCounter(self, v: [List]) -> int:
        low, high = 0, len(v) - 1
        ans = -1

        while low <= high:
            mid = low + (high - low) // 2

            if v[mid] == 1:
                ans = mid
                high = mid - 1
            else:
                low = mid + 1

        numberOfOnes = len(v) - ans if ans != -1 else 0
        
        return numberOfOnes

    def rowAndMaximumOnes(self, mat: List[List[int]]) -> List[int]:
        index, countMaxOnes = 0, 0

        for row in range(0, len(mat)):
            mat[row].sort()

            if mat[row][-1] == 1:
                countOnes = self.oneCounter(mat[row])
            else:
                continue

            if countOnes > countMaxOnes:
                countMaxOnes = countOnes
                index = row

        return [index, countMaxOnes]

