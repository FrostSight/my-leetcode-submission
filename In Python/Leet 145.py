# Link: https://leetcode.com/problems/binary-tree-postorder-traversal/description/

class Solution:

    def solve(self, root: Optional[TreeNode], ans: List[int]):
        if root is None:    return

        self.solve(root.left, ans)
        self.solve(root.right, ans)

        ans.append(root.val)

    def postorderTraversal(self, root: Optional[TreeNode]) -> List[int]:
        ans = []

        self.solve(root, ans)

        return ans
        
