# Link: https://leetcode.com/problems/minimum-add-to-make-parentheses-valid/description/

class Solution:
    def minAddToMakeValid(self, s: str) -> int:
        opening, closing = 0, 0

        for ch in s:
            if ch == "(":
                opening += 1
            elif ch == ")":
                if opening > 0:
                    opening -= 1
                else:
                    closing += 1
        
        ans = opening + closing
        return ans
        
