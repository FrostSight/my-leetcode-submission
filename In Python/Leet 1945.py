# Link: https://leetcode.com/problems/sum-of-digits-of-string-after-convert/description/

class Solution:
    def getLucky(self, s: str, k: int) -> int:
        num = ""

        for ch in s:
            alpha_int = ord(ch) - ord("a") + 1
            num = num + str(alpha_int)
        
        while(k > 0):
            total = sum(int(ch) for ch in num)
            num = str(total)
            k -= 1
        
        ans = int(num)
        return ans

