# Link: https://leetcode.com/problems/uncommon-words-from-two-sentences/description/

class Solution:
    def uncommonFromSentences(self, s1: str, s2: str) -> List[str]:
        result = []

        ump = defaultdict(int)

        for word in s1.split():
            ump[word] += 1
        
        for word in s2.split():
            ump[word] += 1
        
        for word, freq in ump.items():
            if freq == 1:
                result.append(word)
        
        return result
        
