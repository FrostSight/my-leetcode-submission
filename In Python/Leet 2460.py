# Link: https://leetcode.com/problems/apply-operations-to-an-array/submissions/

class Solution:
    def applyOperations(self, nums: List[int]) -> List[int]:
        ans = [0] * len(nums)
        ans[0] = nums[0]

        # performing operations
        for i in range(0, len(nums)-1):
            if ans[i] != nums[i+1]:
                ans[i+1] = nums[i+1]
            else:
                ans[i] = ans[i] * 2
                ans[i+1] = 0
        
        # moving 0s
        left = 0
        for right in range(0, len(ans)):
            if ans[right] != 0:
                ans[left], ans[right] = ans[right], ans[left]
                left += 1
        
        return ans
    
