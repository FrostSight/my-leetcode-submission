# Link: https://leetcode.com/problems/maximum-matrix-sum/description/

class Solution:
    def maxMatrixSum(self, matrix: List[List[int]]) -> int:
        sums, negativeCounter = 0, 0
        minValue = pow(2, 31) - 1

        for i in range(len(matrix)):
            for j in range(len(matrix[i])):
                sums += abs(matrix[i][j])
                if matrix[i][j] < 0:
                    negativeCounter += 1
                minValue = min(minValue, abs(matrix[i][j]))
        
        if negativeCounter % 2 == 0:
            return sums
        else:
            ans = sums - (2 * minValue)
        
        return ans
