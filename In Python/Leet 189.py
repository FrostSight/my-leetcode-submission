# Link: https://leetcode.com/problems/rotate-array/description/

class Solution:
    def rotate(self, nums: List[int], k: int) -> None:
        n = len(nums)
        temp = [0] * n  # "temp" list of n size with all 0s

        for i in range(n):
            temp[(i+k) % n] = nums[i]

        nums[:] = temp # copy elements of temp list to nums list
        
