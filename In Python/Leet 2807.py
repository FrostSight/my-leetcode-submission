# Link: https://leetcode.com/problems/insert-greatest-common-divisors-in-linked-list/description/

class Solution:
    def insertGreatestCommonDivisors(self, head: Optional[ListNode]) -> Optional[ListNode]:
        currentNode = head
        nextNode = currentNode.next

        while currentNode is not None and nextNode != None:
            gcdNode = ListNode(gcd(currentNode.val, nextNode.val))

            gcdNode.next = nextNode
            currentNode.next = gcdNode

            currentNode = nextNode
            nextNode = currentNode.next
        
        return head
        
