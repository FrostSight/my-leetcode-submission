# Link: https://leetcode.com/problems/longest-strictly-increasing-or-strictly-decreasing-subarray/description/

class Solution:
    def longestMonotonicSubarray(self, nums: List[int]) -> int:
        result = 1

        for i in range(0, len(nums)-1):
            increasing = 1
            j = i+1
            while j in range(len(nums)) and nums[j] > nums[j-1]:
                increasing += 1
                j += 1
            
            decreasing = 1
            j = i+1
            while j in range(len(nums)) and nums[j] < nums[j-1]:
                decreasing += 1
                j += 1
            
            result = max(result, increasing, decreasing)
        
        return result
