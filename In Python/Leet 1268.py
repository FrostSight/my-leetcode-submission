# Link: https://leetcode.com/problems/search-suggestions-system/

class Solution:
    def suggestedProducts(self, products: List[str], searchWord: str) -> List[List[str]]:

        result = [[] for _ in range(len(searchWord))]

        products.sort()

        start, end = 0, len(products) - 1

        for i in range(0, len(searchWord)):
            ch = searchWord[i]

            while start <= end and (len(products[start]) <= i or products[start][i] != ch):
                start += 1

            while start <= end and (len(products[end]) <= i or products[end][i] != ch):
                end -= 1

            remain = end - start + 1
            j = 0
            while j < min(3, remain):
                result[i].append(products[start + j])
                j += 1

        return result

