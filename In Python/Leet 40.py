# Link: https://leetcode.com/problems/combination-sum-ii/description/

class Solution:

    def solve(self, idx: int, candidates: List[int], target: int, temp_result: int, result: List[List[int]]):
        if target < 0:
            return
        
        if target == 0:
            result.append(temp_result[:])
            return
        
        for i in range(idx, len(candidates)):
            if idx < i and candidates[i-1] == candidates[i]:
                continue
            
            temp_result.append(candidates[i])
            self.solve(i+1, candidates, target - candidates[i], temp_result, result)
            temp_result.pop()

    def combinationSum2(self, candidates: List[int], target: int) -> List[List[int]]:
        result, temp_result = [], []

        candidates.sort()

        self.solve(0, candidates, target, temp_result, result)

        return result
        
