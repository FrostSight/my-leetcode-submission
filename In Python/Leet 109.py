# Link: https://leetcode.com/problems/convert-sorted-list-to-binary-search-tree/

class Solution:
    def sortedListToBST(self, head: Optional[ListNode]) -> Optional[TreeNode]:

        if not head:
            return None
        if not head.next:
            return TreeNode(head.val)
        
        slow = head
        fast = head
        slow_previous = None

        while fast != None and fast.next != None:
            slow_previous = slow
            slow = slow.next
            fast = fast.next.next
        
        slow_previous.next = None
        root = TreeNode(slow.val)
        root.left = self.sortedListToBST(head)
        root.right = self.sortedListToBST(slow.next)
    
        return root
