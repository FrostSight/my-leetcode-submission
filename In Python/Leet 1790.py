# Link: https://leetcode.com/problems/check-if-one-string-swap-can-make-strings-equal/description/

class Solution:
    def areAlmostEqual(self, s1: str, s2: str) -> bool:
        if s1 == s2:
            return True
        
        diffs, firstIdx, secondIdx = 0, 0, 0

        for i in range(0, len(s1)):
            if s1[i] != s2[i]:
                diffs += 1
            
                if diffs > 2:
                    return False
                elif diffs == 1:
                    firstIdx = i
                else:
                    secondIdx = i
        
        return True if s1[firstIdx] == s2[secondIdx] and s1[secondIdx] == s2[firstIdx] else False
    