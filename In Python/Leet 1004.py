# Link: https://leetcode.com/problems/max-consecutive-ones-iii/description/

class Solution:
    def longestOnes(self, nums: List[int], k: int) -> int:
        i, j, zero_counter, max_length = 0, 0, 0, 0

        while j < len(nums):
            if nums[j] == 0:
                zero_counter += 1

            while zero_counter > k:
                if nums[i] == 0:
                    zero_counter -= 1
                i += 1
            
            max_length = max(max_length, j+1-i)

            j += 1
        
        return max_length

