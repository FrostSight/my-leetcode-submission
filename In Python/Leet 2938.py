# Link: https://leetcode.com/problems/separate-black-and-white-balls/

class Solution:
    def minimumSteps(self, s: str) -> int:
        counter, total_swap, i = 0, 0, 0

        while i < len(s):
            if s[i] == "1":
                counter += 1
            elif s[i] == "0":
                total_swap += counter

            i += 1
        
        return total_swap
        
