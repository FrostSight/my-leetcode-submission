# Link: https://leetcode.com/problems/check-if-digits-are-equal-in-string-after-operations-i/

class Solution:
    def hasSameDigits(self, s: str) -> bool:
        while len(s) > 2:
            new_s = ""

            for i in range(0, len(s)-1):
                digit1 = int(s[i])
                digit2 = int(s[i + 1])
                outcome = (digit1 + digit2) % 10

                new_s += str(outcome)
            
            s = new_s
        
        return True if s[0] == s[1] else False
