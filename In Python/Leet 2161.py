# Link: https://leetcode.com/problems/partition-array-according-to-given-pivot/description/

class Solution:
    def pivotArray(self, nums: List[int], pivot: int) -> List[int]:
        result = [0] * len(nums)
        smaller, larger, equals = 0, 0, 0

        for n in nums:
            if n < pivot:
                smaller += 1
            elif n == pivot:
                equals += 1
            else:
                larger += 1
        
        i = 0
        j = smaller
        k = smaller + equals

        for n in nums:
            if n < pivot:
                result[i] = n
                i += 1
            elif n == pivot:
                result[j] = n
                j += 1
            else:
                result[k] = n
                k += 1
        
        return result
    
