# Link: https://leetcode.com/problems/isomorphic-strings/

class Solution:
    def isIsomorphic(self, s: str, t: str) -> bool:
        
        ump1, ump2 = {}, {}

        for i in range(len(s)):
            cS = s[i]
            cT = t[i]

            if cS in ump1 and ump1[cS] != cT or cT in ump2 and ump2[cT] != cS:
                return False
            
            ump1[cS] = cT
            ump2[cT] = cS
        
        return True
