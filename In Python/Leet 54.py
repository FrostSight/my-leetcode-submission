# Link: https://leetcode.com/problems/spiral-matrix/description/

class Solution:
    def spiralOrder(self, matrix: List[List[int]]) -> List[int]:
        top, left = 0, 0
        down, right = len(matrix) - 1, len(matrix[0]) - 1

        step = 1
        result = []

        while top <= down and left <= right:
            if step == 1:
                for col in range(left, right + 1):
                    result.append(matrix[top][col])
                
                top += 1

            elif step == 2:
                for row in range(top, down + 1):
                    result.append(matrix[row][right])
                
                right -= 1

            elif step == 3:
                for col in range(right, left - 1, -1):
                    result.append(matrix[down][col])
                
                down -= 1

            elif step == 4:
                for row in range(down, top - 1, -1):
                    result.append(matrix[row][left])
                
                left += 1

            step += 1
            if step == 5:
                step = 1

        return result

