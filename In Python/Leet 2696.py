# Link: https://leetcode.com/problems/minimum-string-length-after-removing-substrings/description/

class Solution:
    def minLength(self, s: str) -> int:
        stk = []

        for ch in s:
            if len(stk) != 0 and ch == 'B' and stk[-1] == 'A':
                stk.pop()
            elif len(stk) != 0 and ch == 'D' and stk[-1] == 'C':
                stk.pop()
            else:
                stk.append(ch)
        
        ans = len(stk)

        return ans
        