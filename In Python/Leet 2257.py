# Link: https://leetcode.com/problems/count-unguarded-cells-in-the-grid/description/

class Solution:
    def solve(self, grid: List[List[int]], i: int, j: int, di: int, dj: int):
        m, n = len(grid), len(grid[0])

        i += di
        j += dj

        if i < 0 or i >= m or j < 0 or j >= n or grid[i][j] == 'G' or grid[i][j] == 'W':
            return

        if grid[i][j] == 0:
            grid[i][j] = 1

        self.solve(grid, i, j, di, dj)

    def countUnguarded(self, m: int, n: int, guards: List[List[int]], walls: List[List[int]]) -> int:
        grid = [[0] * n for _ in range(m)]
        counter = 0

        for guard in guards:
            i = guard[0]
            j = guard[1]
            grid[i][j] = 'G'

        for wall in walls:
            i = wall[0]
            j = wall[1]
            grid[i][j] = 'W'

        for i in range(0, m):
            for j in range(0, n):
                if grid[i][j] == 'G':
                    self.solve(grid, i, j, 1, 0)
                    self.solve(grid, i, j, -1, 0)
                    self.solve(grid, i, j, 0, 1)
                    self.solve(grid, i, j, 0, -1)

        for i in range(0, m):
            for j in range(0, n):
                if grid[i][j] == 0:
                    counter += 1

        return counter
