// Link: https://leetcode.com/problems/single-number/description/

class Solution {
public:
    int singleNumber(vector<int>& nums) 
    {
        int a;
        map <int, int> counter;
        for(int i = 0; i < nums.size(); i++)
            counter[nums[i]]++;

        for(auto it = counter.begin(); it != counter.end(); it++)
        {
            if(it->second == 1)
            {
                a = it->first;
            }
        } 
        return a;
    }
};


// updated

class Solution {
public:
    int singleNumber(vector<int>& nums) 
    {
        int ans = 0;
        for(int i = 0; i < nums.size(); i++)
        {
            ans = ans ^ nums[i];
        }
        return ans;
    }
};
