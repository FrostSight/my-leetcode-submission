// Link: https://leetcode.com/problems/group-anagrams/description/

class Solution {
public:
    vector<vector<string>> groupAnagrams(vector<string>& strs) 
    {
        unordered_map <string, vector <string> > umap;
        string temp;

        for(int i = 0; i < strs.size(); i++)
        {
            temp = strs[i];
            sort(strs[i].begin(), strs[i].end());  // sorted value is the key
            umap[strs[i]].push_back(temp);         // pushing the elements whose sorted value is same else new key is assigned 
        }

        vector < vector < string > > ana;

        for(auto it = umap.begin(); it != umap.end(); it++)
            ana.push_back(it->second);              // pushing the values of map to a vector for output.
        
        return ana;
    }
};

/*
key     |   value
------------------------------
aet     |   eat, tea, ate
ant     |   tan, nat
abt     |   bat
*/
