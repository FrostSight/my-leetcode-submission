// Link: https://leetcode.com/problems/max-consecutive-ones-iii/description/

class Solution {
public:
    int longestOnes(vector<int>& nums, int k) 
    {
        int i = 0, j = 0, zero_counter = 0, max_length = 0;

        while(j < nums.size())
        {
            if(nums[j] == 0)
                zero_counter++;

                while(zero_counter > k) // shrink
                {
                    if(nums[i] == 0)
                        zero_counter--;
                    
                    i++;
                }

            max_length = max(max_length, j+1-i);

            j++;
        }

        return max_length;
    }
};

