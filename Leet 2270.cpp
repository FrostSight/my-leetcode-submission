// Link: https://leetcode.com/problems/number-of-ways-to-split-array/description/

#define ll long long

class Solution {
public:
    int waysToSplitArray(vector<int>& nums) 
    {
        int splitCounter = 0;
        vector<ll> ps_nums(nums.size(), 0);

        ps_nums [0] = nums[0];
        for(int i = 1; i < ps_nums.size(); i++)
            ps_nums[i] = ps_nums[i-1] + nums[i];
        
        ll total = ps_nums[ps_nums.size() - 1];

        for(int i = 0; i < ps_nums.size()-1 ; i++) // ignore that "i+1 element". Poorly written question
        {
            ll leftSum = ps_nums[i];
            ll rightSum = total - leftSum;
            if(leftSum >= rightSum)
                splitCounter++;
        }

        return splitCounter;
    }
};

