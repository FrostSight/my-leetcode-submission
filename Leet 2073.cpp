// Link: https://leetcode.com/problems/time-needed-to-buy-tickets/description/

class Solution {
public:
    int timeRequiredToBuy(vector<int>& tickets, int k) 
    {
        queue<int> qu;
        int time = 0;

        for(int i = 0; i < tickets.size(); i++)  // pushing index
            qu.push(i);
        
        while(1)
        {
        	if(tickets[k] == 0)  // tracking terminating condition each time
                break;
        
            int current = qu.front();
            qu.pop();
            tickets[current]--;  // decrementing the of value of current index by 1
            time++;  // decrement occurs time taken 1 second
            if(tickets[current] != 0) // value is not 0 means it can decremented again so pushed back to the last of queue
                qu.push(current);
        }

        return time;
    }
};
