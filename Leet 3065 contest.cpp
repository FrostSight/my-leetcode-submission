// Link: https://leetcode.com/contest/biweekly-contest-125/problems/minimum-operations-to-exceed-threshold-value-i/

// Link: https://leetcode.com/problems/minimum-operations-to-exceed-threshold-value-i/description/

class Solution {
public:
    int minOperations(vector<int>& nums, int k) 
    {
        int counter = 0;
        
        for(int n : nums)
        {
            if(n < k)
                counter++;
        }
        
        return counter;
    }
};