// Link: https://leetcode.com/problems/minimum-remove-to-make-valid-parentheses/description/

class Solution {
public:
    string minRemoveToMakeValid(string s) 
    {
        stack<int> stk;
        unordered_set<int> ust;
        string ans = "";

        for(int i = 0; i < s.size(); i++)  // covers case 1
        {
            if(s[i] == '(')
                stk.push(i);
            else if(s[i] == ')')
            {
                if(!stk.empty())
                    stk.pop();
                else
                    ust.insert(i);
            }
        }    

        while(!stk.empty())  // covers case 2
        {
            int x = stk.top();
            stk.pop();
            ust.insert(x);
        }

        for(int i = 0; i < s.size(); i++)
        {
            auto it = ust.find(i);
            if(it == ust.end())
                ans += s[i];
        }

        return ans;
    }
};

/*
Explanation
Case 1: Got ) but stack is empty
as "lee(t(c)o)de)", "a(b(c)d))"
Case 2: String iteration is done but there might be one or multiple ( remaning
as "(((", a(b(c(d)(e)
*/