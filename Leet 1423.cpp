// Link: https://leetcode.com/problems/maximum-points-you-can-obtain-from-cards/description/

class Solution {
public:
    int maxScore(vector<int>& cardPoints, int k) 
    {
        int i = 0, j = 0, m = cardPoints.size() - k, currentSum = 0, mx_sum = -1;

        int sum = accumulate(cardPoints.begin(), cardPoints.end(), 0);
        
        if(m == 0) return sum;

        while(j < cardPoints.size())
        {
            currentSum += cardPoints[j];
            
            if(j+1-i >= m)
            {
                mx_sum = max(mx_sum, sum - currentSum);
                currentSum -= cardPoints[i];
                i++;
            }

            j++;
        }    

        return mx_sum;
    }
};
