// Link: https://leetcode.com/problems/container-with-most-water/description/

class Solution {
public:
    int maxArea(vector<int>& height) 
    {
        int area, maxArea = 0, left = 0, right = height.size()-1;

        while(left < right)
        {
            area = min(height[left], height[right]) * (right - left);
            maxArea = max(maxArea, area);
            
            height[left] < height[right] ? left++ : right--;
        }

        return maxArea;
    }
};