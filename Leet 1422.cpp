// Link: https://leetcode.com/problems/maximum-score-after-splitting-a-string/


/*
approach 1 pass
score = zeros(left) + ones()right
ones(total) = ones(left) + ones(right)
=> ones(right) = ones(total) - ones(left)
so scoer becomes zeros(left) + ones(total) - ones(right) => score = {zeros(left) - ones(right)} + ones(total)
only need to maximize {inner} as ones(total) is constant
*/
class Solution {
public:
    int maxScore(string s) 
    {
        int n = s.size(), ones = 0, zeros = 0, score = INT_MIN;

        for(int i = 0; i <= n-2; i++)
        {
            if(s[i] == '1')
                ones++;
            else
                zeros++;
            
            score = max(score, zeros - ones);
        }

        if(s[n-1] == '1') // loop ran till n-2 so the rightmost element might be 1
            ones++;

        return score + ones;    
    }
};
