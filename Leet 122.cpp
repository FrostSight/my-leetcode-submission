// Link: https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/description/

class Solution {
public:
    int maxProfit(vector<int>& prices) 
    {
        int sum = 0, profit;

        for(int i = 1; i < prices.size(); i++)
        {
            if(prices[i] > prices[i - 1])
                sum = sum + (prices[i] - prices[i - 1]);
        }

        profit = sum;
        return profit;
    }
};
