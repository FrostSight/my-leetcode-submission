// Link: https://leetcode.com/problems/combination-sum-ii/description/

class Solution {
public:

    void solve(int idx, vector<int>& candidates, int target, vector<int>& temp_result, vector<vector<int>>& result)
    {
        if(target < 0)  return;  // improvement. As target is getting subtracted, anytime gets less 0, it returns

        if(target == 0)
        {
            result.push_back(temp_result);
            return;
        }

        for(int i = idx; i < candidates.size(); i++)
        {
            if(idx < i && candidates[i-1] == candidates[i]) // i is the next element of idx and the value of i == idx
                continue;
            
            temp_result.push_back(candidates[i]); //picking
            solve(i+1, candidates, target - candidates[i], temp_result, result);
            temp_result.pop_back(); // not picking
        }
    }

    vector<vector<int>> combinationSum2(vector<int>& candidates, int target) 
    {
        vector<vector<int>> result;
        vector<int> temp_result;

        sort(candidates.begin(), candidates.end());  // to avoid duplicacy see below

        solve(0, candidates, target, temp_result, result);

        return result;
    }
};

/*
Explanation:
To avoid duplicacy
let candidates = {1, 6, 1}, target = 7
so (1, 6) and (6, 1){it is also a (1, 6)} are duplicate
after sorting  candidates = {1, 1, 6} and by skipping duplicate 1, avoding of duplicacy is hendled
*/