// Link: https://leetcode.com/problems/unique-paths-iii/description/

class Solution {
public:
    int ans = 0, row, col, emptyCell;
    vector<vector<int>> directions = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};

    void findPath(vector<vector<int>>& grid, int i, int j, int cell_counter)
    {
        if(i < 0 || i >= row || j < 0 || j >= col || grid[i][j] == -1)  return; // base case

        if(grid[i][j] == 2)
        {
            if(emptyCell == cell_counter) // reached target visiting all empty cells
                ans++;
            
            return;
        }

        grid[i][j] = -1;
        for(auto direction : directions)
        {
            int new_i = i + direction[0];
            int new_j = j + direction[1];

            findPath(grid, new_i, new_j, cell_counter+1);
        }

        grid[i][j] = 1;
    }

    int uniquePathsIII(vector<vector<int>>& grid) 
    {
        int cell_counter = 0, start_x, start_y;
        emptyCell = 0, row =  grid.size(), col = grid[0].size();

        for(int i = 0; i < row; i++)
        {
            for(int j = 0; j < col; j++)
            {
                if(grid[i][j] == 0)
                    emptyCell++;
                
                if(grid[i][j] == 1)
                {
                    start_x = i;
                    start_y = j;
                }
            }
        }  

        emptyCell += 1; // cosidering starting cell 

        findPath(grid, start_x, start_y, cell_counter);

        return ans;
    }
};

