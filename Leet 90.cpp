// Link: https://leetcode.com/problems/subsets-ii/description/

class Solution {
public:
    void solve(int idx, vector<int>& nums, vector<int>& temp, vector<vector<int>>& ans)
    {
        if(idx == nums.size())
        {
            ans.push_back(temp);
            return;
        }

        temp.push_back(nums[idx]);
        solve(idx + 1, nums, temp, ans);
        temp.pop_back();
        while(idx < nums.size()-1 && nums[idx] == nums[idx+1]) // skipping same digits
            idx++;
        solve(idx + 1, nums, temp, ans);
    }

    vector<vector<int>> subsetsWithDup(vector<int>& nums) 
    {
        vector<vector<int>> ans;
        vector<int> temp;

        sort(nums.begin(), nums.end());
        solve(0, nums, temp, ans);

        return ans;    
    }
};
