// Link: https://leetcode.com/problems/kids-with-the-greatest-number-of-candies/description/

class Solution {
public:
    vector<bool> kidsWithCandies(vector<int>& candies, int extraCandies) 
    {
        vector<bool> ans;
        
        int maxCandy = *max_element(candies.begin(), candies.end());

        for(int i = 0; i < candies.size(); i++)
        {
            if(candies[i] + extraCandies < maxCandy)
                ans.push_back(false);
            else
                ans.push_back(true);
        }

        return ans;
    }
};
        