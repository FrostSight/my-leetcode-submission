// Link: https://leetcode.com/problems/daily-temperatures/

// approach 1: brute force (got TLE)
#define t temperatures

class Solution {
public:
    vector<int> dailyTemperatures(vector<int>& temperatures) 
    {
        vector<int> ans(t.size());
        
        for(int i = 0; i < t.size()-1; i++)
        {
            for(int j = i+1; j < t.size(); j++)
            {                
                if(t[j] > t[i])
                {
                    ans[i] = j-i;
                    break;
                }
                else
                    continue;
            }         
        }

        return ans;    
    }
};


// approach 2
#define t temperatures

class Solution {
public:
    vector<int> dailyTemperatures(vector<int>& temperatures) 
    {
        stack<int> stk;
        vector<int> ans(t.size());

        for(int i = t.size()-1; i >= 0; i--)
        {
            while(!stk.empty() && t[i] >= t[stk.top()]) // working with index
                stk.pop();
            
            if(stk.empty())
                ans[i] = 0;
            else
                ans[i] = stk.top() - i;
        
            stk.push(i);
        }

        return ans;    
    }
};
