// Link: https://leetcode.com/problems/jewels-and-stones/description/

class Solution {
public:
    int numJewelsInStones(string jewels, string stones) 
    {
        unordered_map <char, int> umap;
        int sum = 0;

        for(char ch : stones)
            umap[ch]++;

        for(char ch : jewels)
        {
            for (auto it = umap.begin(); it != umap.end(); it++)
            {
                if(ch == it->first)
                    sum = sum + it->second;
            }
        }
        return sum;
    }
};
