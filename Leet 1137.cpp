// Link: https://leetcode.com/problems/n-th-tribonacci-number/

class Solution {
public:

    int memo[39];
    int finding(int n)
    {
        // base conditions
        if(n == 0)
            return 0;
        if(n == 1 || n == 2)
            return 1;
        
        if(memo[n] != -1)
            return memo[n];
        
        int a = finding(n-1);
        int b = finding(n-2);
        int c = finding(n-3);

        return memo[n] = a+b+c;
    }

    int tribonacci(int n) 
    {
        memset(memo, -1, sizeof(memo));
        int sum = finding(n);

        return sum;    
    }
};
