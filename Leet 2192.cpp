// Link: https://leetcode.com/problems/all-ancestors-of-a-node-in-a-directed-acyclic-graph/description/

// approach 1
class Solution {
public:

    void traverse(int ancestor, int currentNode, vector<vector<int>>& graph, vector<vector<int>>& result)
    {
        for(int& successor : graph[currentNode])
        {
            if(result[successor].empty() || result[successor].back() != ancestor)  //to avoid duplicate entry
            {
                result[successor].push_back(ancestor);
                traverse(ancestor, successor, graph, result);
            }
        }
    }

    vector<vector<int>> getAncestors(int n, vector<vector<int>>& edges) 
    {
        vector<vector<int>> graph(n), result(n);  // size is must

        for(auto& edge : edges) // making the graph/adjacent matrix
        {
            int u = edge[0];
            int v = edge[1];

            graph[u].push_back(v);
        }

        for(int u = 0; u < n; u++)
        {
            int ancestor = u;
            traverse(ancestor, u, graph, result);  // DFS traverse
        }

        return result;
    }
};


/*
after making adjacent matrix
u   v
_   _

0 | 3,4
1 | 4
2 | 4,7
3 | 5,6,7
4 | 6
5 | 
6 | 
7 | 
*/