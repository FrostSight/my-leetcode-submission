// Link: https://leetcode.com/problems/find-peak-element/description/

class Solution {
public:
    int findPeakElement(vector<int>& nums) 
    {
        int low = 0, high = nums.size() - 1, mid;

        //corner case
        if(nums.size() == 1)
            return low;

        while(low <= high)
        {
            mid = low + (high - low) / 2;

             if((mid == 0 || nums[mid] > nums[mid - 1]) && (mid == nums.size() - 1 || nums[mid] > nums[mid + 1]))
                return mid;
            else if(nums[mid] < nums[mid + 1])
                low = mid + 1;
            else // (nums[mid] > nums[mid + 1])
                high = mid - 1;
        }
        return high;    
    }
};


/*
test cases:
1. [1,2,3,1] 
2.[1,2,1,3,5,6,4] 
3.[1,2,3,4,5,6]
4. [1,2,1,2,1,2]
5. [1,2,1,2,1]
6. [3,4,5,6,7,8,9] 
7. [8,9,5,6,7,8,1,3]
8. [9,4,5,6,7,8,1,3]
9. [3,2,1]
10. [1,3,2,1]
11. [2,1]
*/
