// Link: https://leetcode.com/problems/merge-two-2d-arrays-by-summing-values/description/

class Solution {
public:
    vector<vector<int>> mergeArrays(vector<vector<int>>& nums1, vector<vector<int>>& nums2) 
    {
        map<int, int> mp;
        vector<vector<int>> result;

        for(int i = 0; i < nums1.size(); i++)
        {
            int id = nums1[i][0];
            int val = nums1[i][01];

            mp[id] += val;  // if the same id is found then its corresponding value will be incremented
        }    
        
        for(int i = 0; i < nums2.size(); i++)
        {
            int id = nums2[i][0];
            int val = nums2[i][01];

            mp[id] += val; // if the same id is found then its corresponding value will be incremented
        }    

        for(auto it = mp.begin(); it != mp.end(); it++)
        {
            int id = it->first;
            int val = it->second;

            result.push_back({id, val});
        }

        return result;
    }
};
