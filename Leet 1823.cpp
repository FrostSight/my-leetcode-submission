// Link: https://leetcode.com/problems/find-the-winner-of-the-circular-game/description/

class Solution {
public:
    int findTheWinner(int n, int k) 
    {
        queue<int> qu;

        for(int i = 1; i <= n; i++)
            qu.push(i);   

        while(qu.size() != 1)
        {
            int temp = k-1;
            
            while(temp != 0)
            {
                int x = qu.front();
                qu.pop();
                qu.push(x);
                temp--;
            }

            qu.pop();
        } 

        int x = qu.front();
        return x;
    }
};

